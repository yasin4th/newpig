$(function() {
	showProductsOfCart();
	deleteProductFromCart();
	updateCartProducts();
})

function showProductsOfCart() {
	var req = {}
    req.action = 'showProductsOfCart';

	$.ajax({
		type : 'post',
		url  : EndPoint,
		data : JSON.stringify(req),
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1) {
			fillProductDetail(res.products);

	 	}
	 	else {
			$('#cart-products-table tbody').html('');
			$('.btn-checkout').hide();
			html = '<tr><td colspan="6" class="text-center" style="border-top: 1px solid #F7F7F0;">Your cart is empty...</td></tr>';
			$('table#cart-products-table tfoot').html(html);
			//toastr.warning(res.message);
	 	}
	 	
	});
}


function fillProductDetail(products) {
	html = '';
	
	// console.log(products);
        $.each(products, function(i, product) {
			html += '<tr data-id="'+product.id+'">';
			html += '<td class="cart_product"><a><img class="img-responsive" src="'+product.image+'" alt="'+product.name+'" title="'+product.name+'"></a></td>';
            html += '<td class="cart_description"> <h4><a>'+product.name+'</a></h4> <p>'+product.part_name+'</p> </td>'
			html += '<td class="cart_quantity"><div class="cart_quantity_button"><input class="cart_quantity_input product-quantity" type="number" name="quantity" value="'+product.quantity+'" autocomplete="off" size="2"></div></td>';
			html += '<td class="cart_delete" class="cart_delete"> <a class="delete-product cart_quantity_delete btn btn-primary" title="Remove product from cart"><i class="fa fa-times"></i></a></td>';
	        html += '</tr>';
	        
		});

	$('#cart-products-table tbody').html(html);
	deleteProductFromCart();

}




function deleteProductFromCart() {
	$(".delete-product").off('click');
	$(".delete-product").on('click', function(e){
		e.preventDefault();
		var req = {};
		req.action = 'deleteProductFromCart';
		req.id = $(this).parents('tr:eq(0)').data('id');

        $.ajax({
			type :	'POST',
			url  :	EndPoint,
			data :	JSON.stringify(req),
		}).done(function(res){
			res = $.parseJSON(res);
			if(res.status == 1) {
				//showProductsOfCart();
				setTimeout(function() {document.location = "cart";}, 100);
			}
		});
	});
}

function updateCartProducts() {
	$('#update-cart-products').off('click');
	$('#update-cart-products').on('click', function(e) {
		e.preventDefault();
		if ($('#cart-products-table tbody tr').length > 0) {
			var products = [];
			$.each($('#cart-products-table tbody tr'), function(i, tr) {
				var p = {};
				p.product_id = $(tr).data('id');
				p.quantity = $(tr).find('.product-quantity').val();
				products.push(p);
			});

			var req = {};
			req.action = "updateCartProducts";
			req.cart = products;
			$.ajax({
			type :	'POST',
			url  :	EndPoint,
			data :	JSON.stringify(req),
			}).done(function(res){
				res = $.parseJSON(res);
				if(res.status == 1) {
					showProductsOfCart();
					toastr.success(res.message);
				}
				else {
					toastr.error(res.message);
				}
			});
		};
	});
}

