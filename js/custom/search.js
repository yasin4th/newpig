var search='';
/*function searchEvent () {
	$('#search-form').off('submit');
	$('#search-form').on('submit', function(e) {
		e.preventDefault();
		search = $("#search-text").val();
		fetchResult();
	});


	// body...
}*/
$(function() {
	var i = window.location.pathname.indexOf('/search/');
 	search = window.location.pathname.slice(i+8);

 	var find = '%20';
	var re = new RegExp(find, 'g');
	search = search.replace(re, ' ');
 	search = search.replace("%20", " ");
	
	fetchResult();
	fetchAllArticle();
	//searchEvent();
});

function fetchResult() {
	var req = {};
	req.action = 'fetchSearchProducts';
	req.search = search;

	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		// console.log(res);
		res = $.parseJSON(res);
		if(res.status == 1) {
			fillResult(res.products);
			// toastr.success(res.message);
		}
		else {
			toastr.error(res.message);
		}
	});		
} 
fillResult = function(products) {
	var count = 0;

	html = '';
	$.each(products, function(i, product) {

		if(product.subcatalias != null){
			pos = product.subcatalias.indexOf('_');
			subcat = product.subcatalias.slice(0,pos);

			html += '<div class="row search-item">';
			html += '<div class="col-md-3"><a href="'+subcat+'/'+product.alias+'" class="text-center">';
			html += '<img src="'+product.image+'" class="img-responsive" alt="'+product.name+'" title="'+product.name+'" />';
			html += '</a></div><div class="col-md-9">';
			html += '<h4>' + product.name + '</h4>';
			html += '<p>'+product.part_name+'</p>';
			html += '<p>'+product.detailed_description.substr(0,300)+'</p>';
			html += '<a href="'+subcat+'/'+product.alias+'" class="pull-right">View Details</a>';
			html += '</div></div>';
			count++;
		}
	});
	$('div.search-result').html(html);
	html='';
	
	if(count != 1) {
		html += '<span class="counts-number">'+count+'</span> results for <span class="search-title">"'+search.replace(/\+/g, " ")+'"</span>';
	} else {
		html += '<span class="counts-number">'+count+'</span> result for <span class="search-title">"'+search.replace(/\+/g, " ")+'"</span>';
		
	}
		$('h4.search-reasult-counts').html(html);


}

function fetchAllArticle() {
	var req = {};
	req.action = 'fetchArticles';
	req.active = '1';

	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		//console.log(res);
		res = $.parseJSON(res);
		if(res.status == 1) {
			fillArticle(res.article);
			// toastr.success(res.message);
		}
		else {
			toastr.error(res.message);
		}
	});		
}

fillArticle = function(articles) {
	html = '';
	
	var count = 1;
	$.each(articles, function(i, article) {		
		if(count<=8){
			html += '<p><i class="fa fa-certificate"></i><a href="blog/'+article.alias+'"> '+article.name+'</a></p>'
			count++;
		}
		
	});
	html += '<p><a href="blog" class="pull-right">View all solution</a></p>';

	//console.log(html);
	$('div.problem').append(html);
}