//var class_id;

$(function() {
 	//var i = window.location.pathname.indexOf('_n');
 	//class_id = window.location.pathname.slice(i+2);
	//fetchClassDetails();
	//fetchAllProducts();
    setEvents();
})

fetchAllProducts = function() {
	var req = {};
	req.action = 'fetchAllProducts';
	req.class_id = class_id;
	req.active = 1;

	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		// console.log(res);
		res = $.parseJSON(res);
		if(res.status == 1) {
			fillProducts(res.products);
			// toastr.success(res.message);
		}
		else {
			toastr.error(res.message);
		}
	});		
}


fillProducts = function(products) {
	html = '';
	$('span#countProducts').html(products.length + " items found");

	$.each(products, function(i, product) {
		html += '<div class="col-sm-4"> <div class="product-image-wrapper"> <div class="single-products"> <div class="productinfo text-center">';
		var i = product.calias.indexOf('_');	
		calias = product.calias.slice(0,i);
		html += '<a href="'+calias+'/'+product.alias+'">';
		html += '<div class="img-block">';
			if(product.relimage != ''){
				html += '<img src="'+product.image+'" alt="'+product.name+'" title="'+product.name+'" onmouseover="this.src=\''+product.relimage+'\';" onmouseout="this.src=\''+product.image+'\';" />';
			}else{
				html += '<img src="'+product.image+'" alt="'+product.name+'" title="'+product.name+'" />';
			}
			html += '</div>';
		//html += '<img src="'+product.image+'" alt="'+product.name+'" title="'+product.name+'" />';
		if(product.name.length > 50){
			html += '<p>'+product.name.substr(0,50)+'... ('+product.part_name+')</p>';
		}else{	
			html += '<p>'+product.name+' ('+product.part_name+')</p>';
		}			
		//html += '<a href="product/'+product.alias+'" class="btn btn-primary add-to-cart"><i class="fa fa-eye"></i>View </a></a>';			
		html += '</a>';

		html += '<div class="points-div">';
		html += "<ul class='points'>";
		if(product.point1.trim() != '')
		{
			html += "<li>"+product.point1+"</li>";
		}
		if(product.point2.trim() != '')
		{
			html += "<li>"+product.point2+"</li>";
		}
		if(product.point3.trim() != '')
		{
			html += "<li>"+product.point3+"</li>";
		}			
		html += "</ul>";
		html += '<a class="btn btn-primary btn-block cart" data-id="'+product.id+'"><i class="fa fa-shopping-cart"></i> Add to Quote</a>';
		html += '</div>';

		html += '</div></div></div></div>';
		$('h5.title').html('<span class="white-bg"> Products </span>');
	});
	$('#classwise-products').html(html);
	setEvents();
}

fetchClassDetails = function() {
	var req = {};         
	req.action = 'getClassDetails';
	req.class_id = class_id;

	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		// console.log(res);
		res = $.parseJSON(res);
		if(res.status == 1) {
			var i = res.class.salias.indexOf('_');	
			subcat = res.class.salias.slice(0,i);
			$('h2#title').html('<span class="white-bg">'+res.class.name+'</span>');
			var i = res.class.calias.indexOf('_');	
			cat = res.class.calias.slice(0,i);
			breadcrumb = '<a href="products/'+res.class.calias+'">'+res.class.cname+'</a> > <a href="'+cat+'/'+res.class.salias+'">'+res.class.scname+'</a> > <a href="'+subcat+'/'+res.class.alias+'">'+res.class.name+'</a>';			
			$('p#breadcrumb').html(breadcrumb);			
		}
		else {
			toastr.error(res.message);
		}
	});
}
setEvents = function()
{
	$(".cart").off('click');
	$(".cart").on('click',function(e) {
		e.preventDefault();
		
		var req = {}
		req.product_id = $(this).attr('data-id');
		req.quantity = 1;
        req.action = 'addProductTocart';

		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res){
			res = $.parseJSON(res);
			if(res.status == 1) {
				toastr.success(res.message);
				setTimeout(function() {window.location="./cart"}, 800);
			}
			else {
				toastr.error(res.message);
			}
		});				
	});
}

