/// useless file

$(function() {

 	//product_id = getUrlParameter1();
 	//product_id = getUrlParameter('alias');
 	//alert(product_id);
 	//var i = window.location.pathname.indexOf('/product/');
 	//alias = window.location.pathname.slice(i+9);
	//fetchAllProducts();
	eventAddToCart();
})

// fetchAllProducts = function() {
// 	var req = {};
// 	req.action = 'getProductDetails';
// 	req.alias = alias;

// 	$.ajax({
// 		'type'	:	'post',
// 		'url'	:	EndPoint,
// 		'data'	:	JSON.stringify(req)
// 	}).done(function(res) {
// 		res = $.parseJSON(res);
// 		//console.log(res);
// 		if(res.status == 1) {
// 			fillProducts(res.product);
// 			// toastr.success(res.message);
// 		}
// 		else {
// 			toastr.error(res.message);
// 		}
// 	});
// }


// fillProducts = function(product) {
// 	product_id = product.id;
// 	$('.product-name').text(product.name);
// 	$('.product-name').attr('src', product.image);
// 	$('#media p').html(product.media_title);
// 	$('#video-preview').attr('src', 'https://youtube.com/embed/'+product.media_url);
// 	$('.small-description').html(product.small_description);
// 	$('#description').html('<div class="col-sm-12 padd-LR25">'+product.detailed_description+'</div>');
// 	$('meta[name=description]').attr('content',product.meta_description);
// 	$('meta[name=keywords]').attr('content',product.meta_keywords);
// 	$('title').html(product.meta_title);
// 	//$('img.product-name').attr('src',);
// 	var json = $.parseJSON(product.specs);
// 	var html = "<tbody>";
// 	var keys = $.map(json, function(value, spec) {
// 		  //alert( spec + " "+value);
// 		  html += '<tr><td>'+spec+':</td><td>'+value+'</td></tr>';
// 	});
// 	html += '</tbody>';
// 	$('table#specs').html(html);
// 	if(product.relimg != null){
// 		html = '<img class="img-responsive" src="'+product.relimg+'" alt="" />';
// 		$('.related-img').html(html);
// 	}else{
// 		$('.related-img').remove();
// 	}
// }
	
function eventAddToCart() {
	$("#add-to-cart").off('click');
	$("#add-to-cart").on('click',function(e) {
		e.preventDefault();
		if ($("#product-quantity").val() != 0) {
			var req = {}
			req.product_id = product_id;
			req.quantity = $("#product-quantity").val();
	        req.action = 'addProductTocart';

			$.ajax({
				'type'	:	'post',
				'url'	:	EndPoint,
				'data'	:	JSON.stringify(req)
			}).done(function(res){
				res = $.parseJSON(res);
				if(res.status == 1) {
					toastr.success(res.message);
					setTimeout(function() {window.location="cart/"}, 800);
				}
				else {
					toastr.error(res.message);
				}
			});
		}else {
			toastr.info("Product quantity can't be zero.");
		};
	});
}