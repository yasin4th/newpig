var  c=false;
$(function() {
	// var i = window.location.pathname.indexOf('_');
 // 	var subcat = window.location.pathname.slice(i+2);
 	//alert(alias+"#");
	// getCategoryName(subcat);
	// fetchAllClass(subcat);
	// fetchAllProducts(subcat);
	setEvents();
})


function fetchAllClass(subcat) {
	var req = {};
	req.action = 'fetchAllClass';
	req.category_id = subcat;

	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		// console.log(res);
		res = $.parseJSON(res);
		if(res.status == 1) {
			fillClasses(res.classes);
			getCategoryName(subcat);
			// toastr.success(res.message);
		}
		else {
			toastr.error(res.message);
		}
	});		
}


fillClasses = function(classes) {
	if(classes.length > 0){
		html = '<div class="row">';
		//$('span#countClasses').html(classes.length + " items found");
		$.each(classes, function(i, cls) {
			var i = cls.subcatalias.indexOf('_');
			subcat = cls.subcatalias.slice(0,i);
			html += '<div class="col-sm-4"> <div class="product-image-wrapper"> <div class="single-products"> <div class="productinfo text-center">';
	        html += '<a href="'+subcat+'/'+cls.alias+'">';
	        html += '<div class="img-block"><img src="'+cls.image+'" alt="'+cls.name+'" title="'+cls.name+'" /></div>';	

	        if(cls.name.length > 50){
				html += '<p>'+cls.name.substr(0,50)+'...</p>';
			}else{	
				html += '<p>'+cls.name+'</p>';
			}		
					
			//html += '<a href="class/'+cls.alias+'" class="btn btn-primary add-to-cart"><i class="fa fa-eye"></i>View </a></a>';			
			html += '</a></div> </div> </div></div>';

			c=true;
		});
		html += "</div>";
		$('#divClass h5.title').html('<span class="white-bg"> Product Categories </span>');
		$('#classes').html(html);
	}else{
		$('#divClass').remove();
	}

}


fetchAllProducts = function(subcat) {
	var req = {};
	req.action = 'fetchAllProducts';
	req.category_id = subcat;
	req.active = '1';
	req.class_id = '0';

	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		//console.log(res);
		res = $.parseJSON(res);
		if(res.status == 1) {
			fillProducts(res.products);
			getCategoryName(subcat);
		}
		else {
			toastr.error(res.message);
		}
	});		
}
getCategoryName = function(parId){
	$('#uniCount').html('');
	var req = {};
	req.action = 'getCategoryDetails';
	req.category_id = parId;
	
	$.ajax({
		'type'	:	'post',
		'url'	:	'vendor/requests.php',
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		
		if(res.status == 1) {
			//console.log(res.category.name);			
			$('h2#title').html('<span class="white-bg">'+res.category.name+'</span>');
			var i = res.category.calias.indexOf('_');	
			cat = res.category.calias.slice(0,i);
			
			breadcrumb = '<a href="products/'+res.category.calias+'">'+res.category.cname+'</a> > <a href="'+cat+'/'+res.category.alias+'">'+res.category.name+'</a>';			
			$('p#breadcrumb').html(breadcrumb);	
			if(c==false){
				$('#uniCount').html('0 items found.');
			}		
		}
	});	
}

fillProducts = function(products) {
	if(products.length > 0){
		$('span#countProducts').html(products.length + " items found");
		html = '<div class="row">';
		
		$.each(products, function(i, product) {
			html += '<div class="col-sm-4"> <div class="product-image-wrapper"> <div class="single-products"> <div class="productinfo text-center">';


			pos = product.subcatalias.indexOf('_');
			subcat = product.subcatalias.slice(0,pos);
			html += '<a href="'+subcat+'/'+product.alias+'">';
			html += '<div class="img-block">';
			if(product.relimage != ''){
				html += '<img src="'+product.image+'" alt="'+product.name+'" title="'+product.name+'" onmouseover="this.src=\''+product.relimage+'\';" onmouseout="this.src=\''+product.image+'\';" />';
			}else{
				html += '<img src="'+product.image+'" alt="'+product.name+'" title="'+product.name+'" />';
			}
			html += '</div>';

			if(product.name.length > 50)
			{
				html += '<p>'+product.name.substr(0,50)+'... ('+product.part_name+')</p>';			
			}
			else
			{	
				html += '<p>'+product.name+' ('+product.part_name+')</p>';
				
			}			
			//html += '<a href="product/'+product.alias+'" class="btn btn-primary add-to-cart"><i class="fa fa-eye"></i>View </a></a>';			
			html += '</a>';

			html += '<div class="points-div">';
			html += "<ul class='points'>";
			if(product.point1.trim() != '')
			{
				html += "<li>"+product.point1+"</li>";
			}
			if(product.point2.trim() != '')
			{
				html += "<li>"+product.point2+"</li>";
			}
			if(product.point3.trim() != '')
			{
				html += "<li>"+product.point3+"</li>";
			}			
			html += "</ul>";
			html += '<a class="btn btn-primary btn-block cart" data-id="'+product.id+'"><i class="fa fa-shopping-cart"></i> Add to Quote</a>';
			html += '</div>';

			html += '</div></div></div></div>';
			c=true;
		});
		html += '</div>';
		$('#divProducts h5.title').html('<span class="white-bg"> Products </span>');
		$('#products').html(html);
	}
	else{
		$('#divProducts').remove();
	}
	setEvents();
}

setEvents = function()
{
	$(".cart").off('click');
	$(".cart").on('click',function(e) {
		e.preventDefault();
		
		var req = {}
		req.product_id = $(this).attr('data-id');
		req.quantity = 1;
        req.action = 'addProductTocart';

		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res){
			res = $.parseJSON(res);
			if(res.status == 1) {
				toastr.success(res.message);
				setTimeout(function() {window.location="/cart"}, 800);
			}
			else {
				toastr.error(res.message);
			}
		});				
	});
}
