$(function(){fetchCategories();fetchAllArticle();fetchAllProducts();});fetchCategories=function(){var req={};req.action='fetchAllCategories';$.ajax({'type':'post','url':'vendor/requests.php','data':JSON.stringify(req)}).done(function(res){res=$.parseJSON(res);if(res.status==1){//createProduct(res.categories);}
else{toastr.error(res.message);}});}
createProduct=function(categories){html='';$.each(categories,function(i,category){if(category.parent_id==0&&category.status==1){html+='<div class="col-sm-3">';html+='<a href="products/'+category.alias+'">';html+='<div class="product-image-wrapper"><div class="single-products"><div class="productinfo text-center">';html+='<img src="'+category.image+'" alt="'+category.name+'" title="Click to see details" />';html+='<p>'+category.name+'</p>';html+='</div></div></div></a></div>';};});html+='<div class="col-sm-3">';html+='<a href="best_sellers">';html+='<div class="product-image-wrapper"><div class="single-products"><div class="productinfo text-center">';html+='<img src="images/categories/Best Sellers_103.png" alt="Best Sellers" title="Click to see details" />';html+='<p>Best Sellers</p>';html+='</div></div></div></a></div>';$('div.features_items').append(html);}
function fetchAllArticle(){var req={};req.action='fetchArticles';req.active='1';$.ajax({'type':'post','url':EndPoint,'data':JSON.stringify(req)}).done(function(res){res=$.parseJSON(res);if(res.status==1){fillArticle(res.article);}
else{toastr.error(res.message);}});}
fillArticle=function(articles){html='';var count=1;$.each(articles,function(i,article){if(count<=3){html+='<p><i class="fa fa-certificate"></i><a href="blog/'+article.alias+'"> '+article.name+'</a></p>'
count++;}});html+='<p><a href="blog" class="pull-right">View all solution</a></p>';$('div.problem').append(html);}
fetchAllProducts=function(){var req={};req.action='fetchAllProducts';req.featured='1';req.active=1;$.ajax({'type':'post','url':EndPoint,'data':JSON.stringify(req)}).done(function(res){res=$.parseJSON(res);if(res.status==1){fillFeaturedProducts(res.products);}
else{toastr.error(res.message);}});}
fillFeaturedProducts=function(products){var count=1,flag=true;html='';$.each(products,function(i,product){if(count==1&&flag){html+='<div class="item active">';}
if(count==1&&!flag){html+='<div class="item">';}
if(product.subcatalias!=null){pos=product.subcatalias.indexOf('_');subcat=product.subcatalias.slice(0,pos);flag=false;html+='<div class="col-sm-4">';html+='<div class="product-image-wrapper">';html+='<a href="'+subcat+'/'+product.alias+'">';html+='<div class="single-products">';html+='<div class="productinfo text-center">';html+='<img src="'+product.image+'" alt="'+product.name+'" />';html+='<p>'+product.name+' ('+product.part_name+')</p>';html+='</div>';html+='</div>';html+='</a>';html+='<div class="points-div">';html+="<ul class='points'>";if(product.point1.trim()!='')
{html+="<li>"+product.point1+"</li>";}
if(product.point2.trim()!='')
{html+="<li>"+product.point2+"</li>";}
if(product.point3.trim()!='')
{html+="<li>"+product.point3+"</li>";}
html+="</ul>";html+='<a class="btn btn-primary btn-block cart" data-id="'+product.id+'"><i class="fa fa-shopping-cart"></i> Add to Quote</a>';html+='</div>';html+='</div>';html+='</div>';}
if(count==3)
{html+='</div>';count=0;}
count++;})
$('.featured-product').html(html);setEvents();}
setEvents=function()
{$(".cart").off('click');$(".cart").on('click',function(e){e.preventDefault();var req={};req.product_id=$(this).attr('data-id');req.quantity=1;req.action='addProductTocart';$.ajax({'type':'post','url':EndPoint,'data':JSON.stringify(req)}).done(function(res){res=$.parseJSON(res);if(res.status==1){toastr.success(res.message);setTimeout(function(){window.location="cart"},800);}
else{toastr.error(res.message);}});});}