$(function() {
	contact();
})


function contact() {
	$('#main-contact-form').off('submit');
	$('#main-contact-form').on('submit', function(e) {
		e.preventDefault();

		var req = {}
		req.action = 'sendContactDetails';
		req.name = $('#name').val();
		
		req.email = $('#email').val();
		req.mobile_number = $('#mobile-number').val();
		req.subject = $('#subject').val();
		req.message = $('#message').val();		

		
		$.ajax({
			'type'	:	'post',
			'url'	:	'vendor/requests.php',
			'data'	:	JSON.stringify(req),
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1) {
				toastr.success(res.message);
				//setTimeout(function() {window.location="index/"}, 1000);
				
			}
			else {
				toastr.error(res.message);
			}
		});	

	})

}
