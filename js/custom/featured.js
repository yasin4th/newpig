$(function() {

	
	fetchAllProducts();
});


fetchAllProducts = function() {

	var req = {};
	req.action = 'fetchAllProducts';
	req.featured = '1';
	req.active = 1;

	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		// console.log(res);
		res = $.parseJSON(res);
		if(res.status == 1) {
			
			//console.log(res);
			fillFeaturedProducts(res.products);
			// toastr.success(res.message);
		}
		else {
			toastr.error(res.message);
		}
	});
}
fillFeaturedProducts = function(products) {
	if(products.length > 0){
		$('span#countProducts').html(products.length + " items found");
		html = '<div class="row">';
		
		$.each(products, function(i, product) {
			html += '<div class="col-sm-4"> <div class="product-image-wrapper"> <div class="single-products"> <div class="productinfo text-center">';


			pos = product.subcatalias.indexOf('_');
			subcat = product.subcatalias.slice(0,pos);
			html += '<a href="'+subcat+'/'+product.alias+'">';
			html += '<div class="img-block">';
			if(product.relimage != '')
			{
				html += '<img src="'+product.image+'" alt="'+product.name+'" title="'+product.name+'" onmouseover="this.src=\''+product.relimage+'\';" onmouseout="this.src=\''+product.image+'\';" />';
			}
			else
			{
				html += '<img src="'+product.image+'" alt="'+product.name+'" title="'+product.name+'" />';
			}
			html += '</div>';

			if(product.name.length > 50)
			{
				html += '<p>'+product.name.substr(0,50)+'... ('+product.part_name+')</p>';			
			}
			else
			{	
				html += '<p>'+product.name+' ('+product.part_name+')</p>';
				
			}			
			//html += '<a href="product/'+product.alias+'" class="btn btn-primary add-to-cart"><i class="fa fa-eye"></i>View </a></a>';			
			html += '</a>';

			html += '<div class="points-div">';
			html += "<ul class='points'>";
			if(product.point1.trim() != '')
			{
				html += "<li>"+product.point1+"</li>";
			}
			if(product.point2.trim() != '')
			{
				html += "<li>"+product.point2+"</li>";
			}
			if(product.point3.trim() != '')
			{
				html += "<li>"+product.point3+"</li>";
			}			
			html += "</ul>";
			html += '<a class="btn btn-primary btn-block cart" data-id="'+product.id+'"><i class="fa fa-shopping-cart"></i> Add to Quote</a>';
			html += '</div>';

			html += '</div></div></div></div>';
			c = true;
		});
		html += '</div>';
		$('#countProducts').html(products.length+ ' products found.');
		//$('#divProducts h5.title').html('<span class="white-bg"> Products </span>');
		$('#products').append(html);
		setEvents();
	}
	else{
		$('#countProducts').html('0 product found.');
		$('#products').remove();
	}
}	

setEvents = function()
{
	$(".cart").off('click');
	$(".cart").on('click',function(e) {
		e.preventDefault();
		
		var req = {}
		req.product_id = $(this).attr('data-id');
		req.quantity = 1;
        req.action = 'addProductTocart';

		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res){
			res = $.parseJSON(res);
			if(res.status == 1) {
				toastr.success(res.message);
				setTimeout(function() {window.location="cart"}, 800);
			}
			else {
				toastr.error(res.message);
			}
		});				
	});
}