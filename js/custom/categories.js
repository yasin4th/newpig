
$(function() {
 	
	showAllCategories();

});


showAllCategories = function() {
	var req = {};
	req.action = 'fetchAllCategories';

	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		// console.log(res);
		res = $.parseJSON(res);
		if(res.status == 1) {
			fillAllCategories(res.categories);
			// toastr.success(res.message);
		}
		else {
			//window.location="404";
			toastr.error(res.message);

		}
	});		
}

fillAllCategories = function(categories) {
	html = '';
	if(categories.length==0){
		html += '<span> 0 result found.</span>';
	}
	$.each(categories, function(i, category) {
		if (category.parent_id == 0) {
			html += '<div class="col-sm-4"> <div class="product-image-wrapper"> <div class="single-products"> <div class="productinfo text-center">';
			html += '<a href="products/'+category.alias+'">';
			html += '<img src="'+category.image+'" alt="'+category.name+'" title="'+category.name+'" />';			
			html += '<p>'+category.name+'</p>';			
			//html += '<a href="category/'+category.alias+'" class="btn btn-primary add-to-cart"><i class="fa fa-eye"></i>View </a></a>';			
			html += '</a></div> </div> </div></div>';
		};
	});
	$('#all-categories').append(html);
}
