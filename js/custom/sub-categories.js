
$(function() {
 // 	var i = window.location.pathname.indexOf('_');
 // 	cat = window.location.pathname.slice(i+2);
	// calias = window.location.pathname.indexOf('/products/');
	// calias = window.location.pathname.slice(calias+10);
	fetchSubCategories();
	getCategoryName();
	//getCategoryName(category_id);	
	
});


fetchSubCategories = function() {
	var req = {};
	req.action = 'fetchAllCategories';
	req.parent_id = cat;
	
	$.ajax({
		'type'	:	'post',
		'url'	:	'vendor/requests.php',
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		//console.log(res);
		if(res.status == 1) {
			fillSubCategories(res.categories);
			// toastr.success(res.message);
		}
		else {
			toastr.error(res.message);
		}
	});		
}
fillSubCategories = function(sub_categories) {
	html = '';
	//alert("#"+sub_categories.length+"$");
	
	id="";
	
	$.each(sub_categories, function(i, sub_category) {
		
		var i = sub_category.calias.indexOf('_');	
		cat = sub_category.calias.slice(0,i);
			//console.log(c);
		html += '<div class="col-sm-4"><div class="product-image-wrapper"><div class="single-products"><div class="productinfo text-center">';
		html += '<a href="'+cat+'/'+sub_category.alias+'">';
		html += '<div class="img-block"><img src="'+sub_category.image+'" alt="'+sub_category.name+'" title="'+sub_category.name+'" /></div>';
		if(sub_category.name.length > 50){
			html += '<p>'+sub_category.name.substr(0,50)+'...</p>';			
		}else{	
			html += '<p>'+sub_category.name+'</p>';
		}

		//html += '<a href="class&product/'+sub_category.alias+'" class="btn btn-primary add-to-cart"><i class="fa fa-eye"></i>View </a></a>';			
		html += '</a></div> </div> </div></div>';
		id = sub_category.parent_id;
	});
	if(sub_categories.length == 0){		
		$('div#categories').remove();
	}else{
		
	}
	$('#sub-categories').html(html);
	// getCategoryName();
	$('span#count').html(sub_categories.length + " items found");
	
}

getCategoryName = function(){
	var req = {};
	req.action = 'getCategoryDetails';
	req.category_id = cat;
	
	$.ajax({
		'type'	:	'post',
		'url'	:	'vendor/requests.php',
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		//console.log(res);
		if(res.status == 1) {
			//console.log(res.category.name);			
			$('h2.title').html('<span class="white-bg">'+res.category.name+'</span>');
			breadcrumb = '<a href="products/'+res.category.alias+'">'+res.category.name+'</a> >';			
			$('p#breadcrumb').html(breadcrumb);	
			$('h5.title').html("Product Categories");			
		}
	});
}
