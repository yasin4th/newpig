$(function() {
	checkout();
    fetchProductsOfCart();
	$('#select-country-id').off('change');
	$("#select-country-id").on('change',function (e) {
		if($(this).val()=="Australia"){
			html='<select id="select-state-id" class="form-control">';
			html += '<option value="0">Select A Region</option>';
			html += '<option value="Australian Captial Territory">Australian Captial Territory</option>';
			html += '<option value="Queensland">Queensland</option>';
			html += '<option value="New South Wales">New South Wales</option>';
			html += '<option value="Northern Territory">Northern Territory</option>';
			html += '<option value="South Australia">South Australia</option>';
			html += '<option value="Tasmania">Tasmania</option>';
			html += '<option value="Victoria">Victoria</option>';
			html += '<option value="Western Australia">Western Australia</option>';
    		html += '</select>';
			$('#select-state-id').remove();
			$("#div-state").append(html);			
		}else{
			$('#select-state-id').remove();
			$("#div-state").append("<input type='text' id='select-state-id' class='form-control' />");
		}
	})
})

var product_details = '';

function fetchProductsOfCart() {
	var req = {}
    req.action = 'showProductsOfCart';

	$.ajax({
		type : 'post',
		url  : EndPoint,
		data : JSON.stringify(req),
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1) {
			appendProductDetails(res.products);
	 	}	 	
	});
}
function appendProductDetails(products){
  
  	$.each(products, function(i, product) {
       product_details += product.quantity + ' ' + product.part_name + ' ' + product.name + ' \n';	        
	});
}


function checkout() {
	$('#main-contact-form').off('submit');
	$('#main-contact-form').on('submit', function(e) {
		e.preventDefault();
		if(validate()) {
			var req = {}
			req.action = 'saveCheckoutDetail';
			req.first_name = $('#first-name').val();
			req.last_name = $('#last-name').val();
			req.email = $('#email').val();
			req.phone_number = $('#phone-number').val();
			req.company_name = $('#company-name').val();
			req.country = $('#select-country-id').val();
			req.address = $('#address').val();
			req.city_id = $('#city-locality').val();
			req.state_id = $('#select-state-id').val();
			req.postal_zip_code = $('#postal-zip-code').val();	
			req.comments = $('#comments').val();	

			
			$.ajax({
				'type'	:	'post',
				'url'	:	'vendor/requests.php',
				'data'	:	JSON.stringify(req),
			}).done(function(res) {
				res = $.parseJSON(res);
				if(res.status == 1) {
					toastr.success("Your order successfully placed.");
                    $('#comments').val($('#comments').val()+'\n'+product_details);
					setTimeout(function() {$('#main-contact-form')[0].submit()}, 2000);
					
				}
				else {
					toastr.error(res.message);
				}
			});	
		}
	})
}



validate = function() {

	state = $('#select-state-id').val();
	if(state=='0'){
		toastr.error("Please Enter State/Region.");
		return false;
	}
	country = $('#select-country-id').val();
	if(country=='0'){
		toastr.error("Please Select Country.");
		return false;
	}
	
	return true;
}
