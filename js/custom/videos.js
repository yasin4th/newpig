//var article_id=0;
$(function() {
 	
	fetchAllVideos();
	
})


function fetchAllVideos() {
	var req = {};
	req.action = 'fetchVideos';
	req.active = '1';
	//console.log(req.action);
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		 // console.log(res);
		if(res.status == 1) {
			fillAllVideos(res.video);
			
		}
		else {
			toastr.error(res.message);
		}
	});		
}


fillAllVideos = function(videos) {
	html = '';
	modal = '';
	if(videos.length==0){
		html += '<span> 0 result found.</span>';
	}
	//var article_id=0;
	//article_id = getUrlParameter('id');
	
	//alert('undefines');
	$.each(videos, function(i, video) {		
		
		html += '<div class="col-sm-12 mrg-tb20">';
		html += '<div class="col-sm-5">';
		html += '<a href="" data-toggle="modal" data-target="#' + video.url + '"><img src="http://img.youtube.com/vi/'+video.url+'/0.jpg" style="width:300px;height:180px;" alt="" /></a>';
		html += '</div>';
		html += '<div class="col-sm-7">';
		html += '<a href="" data-toggle="modal" data-target="#'+ video.url + '"><h5>' + video.name + '</h5></a>';
		//html += '<p>Video Description of Video : '+video.name+''+video.url+'</p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#' + video.name + '">View Video</button>';
		html += '<p>';
		html +=  video.description;
		html += '</p>'
		html += '<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#'+video.url+'">View Video</button>';
		html +=  '</div></div>';		

		modal += '<div class="modal fade" id="'+video.url+'" role="dialog"><div class="modal-dialog modal-md"><div class="modal-content">' ;
		modal += '<div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title">'+video.name+'</h4></div>';
		modal += '<div class="modal-body"><iframe width="560" height="315" src="https://youtube.com/embed/'+video.url+'" frameborder="0" allowfullscreen></iframe></div>'
		modal += '<div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div>';
		modal += '</div></div></div>'
	});

	$('div.row.video').html(html);
	$('body').append(modal);
}