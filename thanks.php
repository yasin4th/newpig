<!DOCTYPE html>
<html lang="en">
<head>
    <?php include('html/head-tag.php'); ?>
</head>
<!--/head-->

<body>
	<!--header-->
	<?php include('html/header.php'); ?>
	<!--/header-->
	
	<section class="mrg-top30">
		<div class="container">
			<div class="row"> 
				<div class="col-sm-3">
					<?php include('html/side-bar.php'); ?>
				</div>

	    		<div class="col-sm-9 padding-right mrg-bot30">
	    			<div class="row">
	    				<div class="col-md-12">
		    				Thanks for submitting your quote request. Our customer service team will get in touch shortly.<br>
		    				<a href="/">Go Back to Home Page</a>
	    			    </div>
    			    </div>
	    		</div>   			
	    	</div> 
		</div>
	</section>
	
	<!--Footer-->
	<?php include('html/footer.php'); ?>
	<!--/Footer-->

	<!--/js-files-->
	<?php include('html/js-files.php'); ?>
	
	<!--/js-files-->

</body>
</html>