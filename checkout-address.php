<!DOCTYPE html>
<html lang="en">
<head>
	<title>iQSafety</title>
    <?php include('html/head-tag.php'); ?>
</head>
<!--/head-->

<body>
	<!--header-->
	<?php include('html/header.php'); ?>
	<!--/header-->
	
	<section class="mrg-top30">
		<div class="container">
			<div class="row"> 
				<div class="col-sm-3">
					<?php include('html/side-bar.php'); ?>
				</div>

	    		<div class="col-sm-9 padding-right mrg-bot30">
	    			<div class="row">
	    				<div class="col-md-12">
		    				<div class="contact-form">
			    				<div class="status alert alert-success" style="display: none"></div>
						    	<form id="main-contact-form" class="contact-form row" name="contact-form" method="post" action="http://analytics.clickdimensions.com/forms/h/abbJQXtGikKyogDWSlnMg">
						            <div class="col-md-12">
    									<h2 class="main-title"><span class="white-bg">Your Details</span></h2>
	    								<p>Please enter the following details</p>
    								<div>
						            <!-- <div class="form-group col-md-6">
						            	<label class="form-label">Email Address <span class="required">*</span></label>
						                <input type="email" name="email" class="form-control" required="required" placeholder="Email">
						            </div>
						            <div class="col-md-12">
	    								<h2>Address Details</h2>
	    								<p>Please enter your address details</p>
    								</div> -->
						            <div class="form-group col-md-6">
						            	<label class="form-label">First Name <span class="required">*</span></label>
					                	<input id="first-name" name="first-name" type="text" class="form-control" required="required" placeholder="First Name">
						            </div>
						            <div class="form-group col-md-6">
						            	<label class="form-label">Last Name <span class="required">*</span></label>
					                	<input id="last-name" name="last-name" type="text" class="form-control" required="required" placeholder="Last Name">
						            </div>
						            <div class="form-group col-md-6">
						            	<label class="form-label">Email Address <span class="required">*</span></label>
						                <input id="email" name="email" type="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" class="form-control" required="required" placeholder="Email">
						            </div>
						            <div class="form-group col-md-6">
						            	<label class="form-label">Phone Number <span class="required">*</span></label>
						                <input id="phone-number"   name="phone-number" type="text" class="form-control" required="required" placeholder="Phone Number">
						            </div>
						            <div class="form-group col-md-6">
						            	<label class="form-label">Company Name <span class="required">*</span></label>
					                	<input id="company-name" name="company-name" type="text" class="form-control" required="required" placeholder="Company Name">
						            </div>
						            <div class="form-group col-md-6">
						            	<label class="form-label">Country <span class="required">*</span></label>
					                	<select id="select-country-id" name="select-country-id" class="form-control">
					                		<!-- <option value="0">Australia</option>
											<option value="1">Indonesia</option>
											<option value="3">New Zealand</option>
											<option value="4">Other</option>
											<option value="5">Papua New Guinea</option>
											<option value="6">Singapore</option>
											<option value="7">United Kingdom</option>
											<option value="8">United States</option> -->
											<option value="Australia">Australia</option>
											<option value="Indonesia">Indonesia</option>
											<option value="New Zealand">New Zealand</option>
											<option value="Papua New Guinea">Papua New Guinea</option>
											<option value="Singapore">Singapore</option>
											<option value="United Kingdom">United Kingdom</option>
											<option value="United States">United States</option>
											<option value="Other">Other</option>

					                	</select>
						            </div>
						            <div class="form-group col-md-12">
						            	<label class="form-label">Address <span class="required">*</span></label>
					                	<textarea id="address" name="address" required="required" class="form-control" rows="2" placeholder="Address"></textarea>
						            </div>
						            <div class="form-group col-md-6">
						            	<label class="form-label">City/Locality <span class="required">*</span></label>
					                	<input id="city-locality" name="city-locality" type="text" class="form-control" required="required" placeholder="City/Locality">
						            </div>
						            <div class="form-group col-md-6" id="div-state">
						            	<label class="form-label">State/Region</label>
					                	<select id="select-state-id" name="select-state-id" class="form-control">
					                		<!-- <option value="0">Select A Region</option>
											<option value="1">Australian Captial Territory</option>
											<option value="3">New South Wales</option>
											<option value="4">Northern Territory</option>
											<option value="5">Queensland</option>
											<option value="6">South Australia</option>
											<option value="7">Tasmania</option>
											<option value="8">Victoria</option>
											<option value="9">Western Australia</option> -->

											<option value="0">Select A Region</option>
											<option value="917190000">Australian Captial Territory</option>
											<option value="917190003">Queensland</option>
											<option value="917190001">New South Wales</option>
											<option value="917190002">Northern Territory</option>
											<option value="917190004">South Australia</option>
											<option value="917190005">Tasmania</option>
											<option value="917190006">Victoria</option>
											<option value="917190007">Western Australia</option>
                                            <option value="917190008">Other</option>
					                	</select>
						            </div>
						            <div class="form-group col-md-6">
						            	<label class="form-label">Postal/Zip Code <span class="required">*</span></label>
					                	<input id="postal-zip-code" name="postal-zip-code" type="text" class="form-control" required="required" placeholder="Postal/Zip Code">
						            </div>
						            <div class="form-group col-md-6">
						            	<label class="form-label">Comments</label>
						            	<textarea name="comments" id="comments" class="form-control"></textarea>
					                	
						            </div>
						            <div class="form-group col-md-12">
						            	<a href="cart" class="btn btn-primary">Back</a>						            	
						            	<button type="submit" class="btn btn-primary pull-right">Place Quote</button>
						            </div>
						        </form>
			    			</div>
	    			    </div>
    			    </div>
	    		</div>   			
	    	</div> 
		</div>
	</section>
	
	<!--Footer-->
	<?php include('html/footer.php'); ?>
	<!--/Footer-->

	<!--/js-files-->
	<?php include('html/js-files.php'); ?>
	<script type="text/javascript" src="js/custom/checkout-address.js"></script>
	<!--/js-files-->

</body>
</html>