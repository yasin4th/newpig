<!DOCTYPE html>
<html lang="en">
<head>
	<title>	Contact Us for all your Safety Products & Spill Clean-up Needs </title>
	<meta name="description" content="Contact Us for all your Safety Products & Spill Clean-up Needs and choose from a rane of Spill Kits, Spill containment, Spill control & clean-up Products." />

    <?php include('html/head-tag.php'); ?>
</head>
<!--/head-->

<body>
	<!--header-->
	<?php include('html/header.php'); ?>
	<!--/header-->
	
	<section class="mrg-top30">
		<div class="container">
			<div class="row">  	
	    		<div class="col-sm-3">
					<?php include('html/about-side-bar.php'); ?>
	    		</div>
	    		<div class="col-sm-9">
	    			<div class="contact-form">
	    				<h2 class="main-title"><span class="white-bg">Get In Touch</span></h2>
	    				<script type="text/javascript">var loc = "https://analytics.clickdimensions.com/matthewscomau-aegcz/pages/";</script>
						<script type="text/javascript" src="https://az124611.vo.msecnd.net/web/v10/CDWidget.js"></script>
						<div pageID="1nujz7f5eesa1wamkyxjoa"></div>
	    			</div>
	    		</div>
	    		<!-- <div class="col-sm-4">
	    			<div class="contact-info">
	    				<h2 class="title text-center"><span class="white-bg">Contact Info</span></h2>
	    				<address>
	    					<p>Newpig</p>
							<p>35 Laser Drive • Rowville, VIC 3178</p>
							<p>Mobile: +2346 17 38 93</p>
							<p>Fax: 1-714-252-0026</p>
							<p>Email: info@e-shopper.com</p>
	    				</address>
	    				<div class="social-networks">
	    					<h2 class="title text-center"><span class="white-bg">Social Networking</span></h2>
							<ul>
								<li>
									<a href="#"><i class="fa fa-facebook"></i></a>
								</li>
								<li>
									<a href="#"><i class="fa fa-twitter"></i></a>
								</li>
								<li>
									<a href="#"><i class="fa fa-google-plus"></i></a>
								</li>
								<li>
									<a href="#"><i class="fa fa-youtube"></i></a>
								</li>
							</ul>
	    				</div>
	    			</div>
    			</div>  -->   			
	    	</div> 
		</div>
	</section>
	
	<!--Footer-->
	<?php include('html/footer.php'); ?>
	<!--/Footer-->

	<!--/js-files-->
	<?php include('html/js-files.php'); ?>
	<script type="text/javascript"> 
	   	$(function () { 
	      	$('.about-side-bar h4 a').removeClass('active'); 
	      	$('.about-side-bar h4 a#contact-us').addClass('active'); 
	   	}); 
	</script>
	<script type="text/javascript" src="js/custom/contact-us.js"></script>
	<!--/js-files-->

</body>
</html>