<!DOCTYPE html>
<html lang="en">
<head>
	<title>iQSafety - Matthews Safety Products Office Location in Australia</title>
    <meta name="description" content="iQSafety previously known as Matthews Safety Products has offices at various location across Australia. We have our office at these locations: Victoria - Head Office, Western Australia, South Australia, Queensland & New South Wales." />
    <?php include('html/head-tag.php'); ?>
</head>
<!--/head-->

<body>
	<!--header-->
	<?php include('html/header.php'); ?>
	<!--/header-->
	
	<section class="mrg-top30">
		<div class="container">
			<div class="row"> 
				<div class="col-sm-3">
					<?php include('html/about-side-bar.php'); ?>
				</div>

	    		<div class="col-sm-9 padding-right mrg-bot30">
    				<h2 class="main-title"><span class="white-bg">Contact Us</span></h2>
	    			<div class="row">
	    				<div class="col-sm-12">
		    				<div class="about">
				    			<p>Matthews is a truly Australasian organisation, with sales and service capabilities nationwide. To find the office location 
				    				and contacts that can help you, please click on your state below. You can always call 1300 CODING for sales or service 
				    				and we will direct your call to the right person.</p>
			    				<div class="row">
			    					<div class"col-sm-12">
					    				<h4>Victoria: Head Office</h4>
					    				<p>35 Laser Drive, Rowville, Victoria, 3178.<br>
											Freecall 1800 HOT HOG   Fax 03 9763 2020<br>
											<a href="mailto:hothog@newpig.com.au">Email: hothog@newpig.com.au</a></p>
										<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d25184.33051717247!2d145.244811!3d-37.906097!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6ad63dc1b1c3c665%3A0xe642f820b6c850ea!2s35+Laser+Dr%2C+Rowville+VIC+3178%2C+Australia!5e0!3m2!1sen!2sus!4v1448284243314" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
			    					</div>
			    				</div>
			    				<div class="row padd-top20">
			    					<div class"col-sm-12">
					    				<h4>Western Australia</h4>
					    				<p>48 June Road, Safety Bay, WA, 6169<br>
											Key Contact for WA: Randall Collins<br>
											Freecall 1800 HOT HOG <br>
											<a href="mailto:hothog@newpig.com.au">Email: hothog@newpig.com.au</a></p>
										<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d26978.795675399335!2d115.724008!3d-32.302464!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2a32833d6399c72f%3A0x894f59262e8c1fe3!2s48+June+Rd%2C+Safety+Bay+WA+6169%2C+Australia!5e0!3m2!1sen!2sus!4v1448284627383" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
			    					</div>
			    				</div>
			    				<div class="row padd-top20">
			    					<div class"col-sm-12">
					    				<h4>South Australia</h4>
					    				<p>Unit 2, 53 Galway Avenue, Marleston, SA, 5033<br>
											Ph: +61 8 8351 6799 Fax: +61 8 8351 6499<br>
											Key Contact for SA: Paul Molloy<br>
											Freecall 1800 HOT HOG <br>
											<a href="mailto:hothog@newpig.com.au">Email: hothog@newpig.com.au</a></p>
										<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d26162.62651699207!2d138.561078!3d-34.948379!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6b9158d9a70170af%3A0xf931d75523e2fa1c!2sMatthews+Intelligent+Identification!5e0!3m2!1sen!2sus!4v1448284888455" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
			    					</div>
			    				</div>
			    				<div class="row padd-top20">
			    					<div class"col-sm-12">
					    				<h4>Queensland</h4>
					    				<p>Unit 6, 56 Lavarack Ave, Eagle Farm, QLD, 4009<br>
											Ph: +61 7 3268 7155  Fax: +61 7 3268 2041<br>
											Key Contact for QLD: David Alexander<br>
											Freecall 1800 HOT HOG <br>
											<a href="mailto:hothog@newpig.com.au">Email: hothog@newpig.com.au</a></p>
										<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d28329.624148322906!2d153.094475!3d-27.431783!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6b9158d9a70170af%3A0x6b6d963d54c1c5cb!2s56+Lavarack+Ave%2C+Eagle+Farm+QLD+4009%2C+Australia!5e0!3m2!1sen!2sus!4v1448285060939" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
			    					</div>
			    				</div>
			    				<div class="row padd-top20">
			    					<div class"col-sm-12">
					    				<h4>New South Wales</h4>
					    				<p>Unit 3, 7 Millennium Court, Silverwater, NSW, 2128<br>
											Ph: +61 2 9352 1100 Fax: +61 2 9748 0399<br>
											Key Contact for NSW: Warren Hutt<br>
											Freecall 1800 HOT HOG <br>
											<a href="mailto:hothog@newpig.com.au">Email: hothog@newpig.com.au</a></p>
										<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d26513.287813298295!2d151.041725!3d-33.833959!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6b12a35983e87b6d%3A0x75e3ae809d7299c1!2s3%2F7+Millennium+Ct%2C+Silverwater+NSW+2128%2C+Australia!5e0!3m2!1sen!2sus!4v1448285214418" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
			    					</div>
			    				</div>

		    			    </div>
	    			    </div>
    			    </div>
	    		</div>   			
	    	</div> 
		</div>
	</section>
	
	<!--Footer-->
	<?php include('html/footer.php'); ?>
	<!--/Footer-->

	<!--/js-files-->
	<?php include('html/js-files.php'); ?>
	<script type="text/javascript"> 
	   	$(function () { 
	      	$('.about-side-bar h4 a').removeClass('active'); 
	      	$('.about-side-bar h4 a#office-locations').addClass('active'); 
	   	}); 
	</script>
	<!--/js-files-->

</body>
</html>