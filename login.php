<!DOCTYPE html>
<html lang="en">
<head>
    <?php include('html/head-tag.php'); ?>
</head>
<!--/head-->

<body>
	<!--header-->
	<?php include('html/header.php'); ?>
	<!--/header-->
	
	<section class="mrg-top30">
		<div class="container">
			<div class="row">  	
				<div class="col-sm-4"></div>
	    		<div class="col-sm-4">
	    			<div class="contact-form">
	    				<h2 class="title text-center"><span class="white-bg">Login with your own account</span></h2>
	    				<div class="status alert alert-success" style="display: none"></div>
				    	<form id="main-contact-form" class="contact-form row" name="contact-form" method="post">
				            <div class="form-group col-md-12">
				            	<label class="form-label">User Name</label>
				                <input type="text" name="User-name" class="form-control" required="required" placeholder="User Name">
				            </div>  
				            <div class="form-group col-md-12"> 
				            	<label class="form-label">Password</label>
				                <input type="password" name="password" class="form-control" required="required" placeholder="Password">
				            </div>                    
				            <div class="form-group col-md-12">
				                <input type="submit" name="submit" class="btn btn-primary pull-right" value="login">
				            </div>
				        </form>
	    			</div>
	    		</div>
	    		<div class="col-sm-4"></div>
	    	</div> 
		</div>
	</section>
	
	<!--Footer-->
	<?php include('html/footer.php'); ?>
	<!--/Footer-->

	<!--/js-files-->
	<?php include('html/js-files.php'); ?>
	<!--/js-files-->

</body>
</html>