<?php
    if(isset($_GET['id'])) {
        if(!empty($_GET['id'])) {

        }
    } else {

    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>	NewPig Blog - Read and know more about Spill Clean-up & Safety Products. </title>
	<meta name="description" content="NewPig Blog - Read and know more about Spill Clean-up & Safety Products." />
    <?php include('html/head-tag.php'); ?>

</head>
<!--/head-->

<body>
    
    <?php
       
        $articles = getData(array('action' => 'fetchArticles', 'active' => '1'));
        
    ?>
	<!--header-->
	<?php include('html/header.php'); ?>
	<!--/header-->

	<section class="mrg-top30">
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<?php include('html/side-bar.php'); ?>
				</div>
				
				<div class="col-sm-9 padding-right mrg-bot30">
                    <?php
                        $str = '<h2 class="main-title"><span class="white-bg">Blog</span></h2>';
                        if(count($articles['article']) == 0){
                            $str .= '<span> 0 result found.</span>';                            
                        } else {

                            foreach ($articles['article'] as $article) {

                                if(isset($_GET['id']))
                                {
                                    if($_GET['id'] === $article['id']) {

                                        $str .= "<div class='col-sm-12'>";
                                        $str .= "<div class='blog-question-div'>";
                                        $str .= "<a href='blog/{$article['alias']}'><h4> {$article['name']} </h4></a>";
                                        $str .= "<div><p> {$article['description']} </p></div>";
                                        $str .= "<a href='blog' type='button' class='btn btn-primary'>Back to Blog</a>";
                                        $str .= "</div>";
                                        $str .= "</div>";
                                    }
                                } else {                                    
                                    $str .= "<div class='col-sm-12'>";
                                    $str .= "<div class='blog-question-div'>";
                                    $str .= "<a href='blog/{$article['alias']}'><h4> {$article['name']} </h4></a>";
                                    $str .= "<div><p>". substr($article['description'], 0, 300) . "</p></div>";
                                    $str .= "<a href='blog/{$article['alias']}' type='button' class='btn btn-primary'>View Blog</a>";
                                    $str .= "</div>";
                                    $str .= "</div>";                                    
                                }                                
                            }
                        }
                        echo $str;

                    ?>
					<!-- <h2 class="main-title"><span class="white-bg">Blog</span></h2> -->
					<div class="row">
						
						<!--<div class="col-sm-12">
							<div class="blog-question-div">
								<a href="#"><h4>Spills: Should you absorb them, contain them…or both?</h4></a>
								<p>When it comes to spill response, absorbents may not be the only things that you need in your arsenal. </p>
								<a type="button" class="btn btn-primary">View Article</a>
							</div>
						</div>-->
					</div>
				</div>
			</div>
		</div>
	</section>
		
	<!--Footer-->
	<?php include('html/footer.php'); ?>
	<!--/Footer-->

	<!--/js-files-->
	<?php include('html/js-files.php'); ?>
	<!-- <script type="text/javascript" src="js/custom/blog.js"></script> -->
	<!--/js-files-->
<?php	
	
	// if(isset($_GET['id'])){
	// 	if(!empty($_GET['id'])){
			
	// 		$id = $_GET['id'];
	// 		echo '<script type="text/javascript">';
	// 		echo 'var article_id;';
	// 		echo 'article_id = "'.$id.'"';
	// 		echo '</script>';
	// 	}else{
	// 		echo '<script type="text/javascript">';
	// 		echo 'var article_id="";';
	// 		echo '</script>';
	// 	}
	// }else{
	// 		echo '<script type="text/javascript">';
	// 		echo 'var article_id="";';
	// 		echo '</script>';
	// 	}
?>
</body>
</html>