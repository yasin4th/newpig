<?php
	$meta_title='';
	$meta_keywords='';
	$meta_description='';

	if(isset($_GET['id'])){
		if(!empty($_GET['id'])){
			$request =new stdClass();
			$request->action = 'getCategoryDetails';

			// echo '<pre>';
			//  print_r($_SERVER);
			// echo '</pre>';
			$id=$_GET['id'];
			$request->category_id = $_GET['id'];
				
			require('vendor/requests1.php');

			$res = Api::getCategoryDetails($request);
			//print_r($res);
			//$array = json_decode($res->product[0], true );
			$json = json_encode($res);
			$array = json_decode($json,true);

			// if($array['status'] != 1){
			// 	header('Location: ../404.php');
			// }
			$meta_title=$array['category']['meta_title'];
			$meta_keywords=$array['category']['meta_keywords'];
			$meta_description=$array['category']['meta_description'];
			$details=$array['category']['details'];
            $breadcrumb = "<a href='products/{$array['category']['alias']}'>{$array['category']['name']}</a> >";   
            $title="<span class=\"white-bg\">{$array['category']['name']}</span>";

		}else{
			header('Location: ../404.php');
		}

	}else{
		header('Location: ../404.php');
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="description" content="<?=$meta_description; ?>">
	<meta name="keywords" content="<?=$meta_keywords; ?>">
	<title><?=$meta_title;?></title>
    <?php include('html/head-tag.php'); ?>
</head><!--/head-->

<body>
	<!--header-->

	<?php include('html/header.php'); ?>
	<!--/header-->
	
	<section class="mrg-top30">
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<?php include('html/side-bar.php'); ?>
				</div>
				
				<div class="col-sm-9 padding-right">
					<div class="features_items"><!--features_items-->
						
						<h1 class="title"><?=$title;?></h1>
						<p id="breadcrumb"><?=$breadcrumb;?></p>
						<span id="count">
                            <?php 
                                $counter = 0;
                                foreach ($cats['categories'] as $cat) {
                                    if($cat['parent_id'] == $id) {
                                        $counter++;
                                    }
                                }
                                echo "{$counter} items found";
                            ?>

                        </span>
                        <?php if($counter > 0) { ?>
						<div id="categories">
							<h5 class="title"></h5>

							<div id="sub-categories">
								<?php 
                                    foreach ($cats['categories'] as $cat) {
                                        if($cat['parent_id'] == $id)
                                        {
                                            $catalias = preg_replace('/_[a-z]\d+/', '', $cat['calias']);

                                            echo "<div class=\"col-sm-4\"><div class=\"product-image-wrapper\"><div class=\"single-products\"><div class=\"productinfo text-center\"> <a href=\"{$catalias}/{$cat['alias']}\"><div class=\"img-block\"><img src=\"{$cat['image']}\" alt=\"{$cat['name']}\" title=\"{$cat['name']}\"></div><p>";
                                            if(strlen($cat['name']) > 50) { echo substr($cat['name'], 0, 50)."..."; }
                                            else { echo "{$cat['name']}"; }
                                            echo "</p></a></div> </div> </div></div>";                                   
                                        }
                                    }
                                ?>								
							</div>
													
						</div>
                        <?php } ?>
						<div class="row">
							<div class="col-sm-12 col-md-12">									
								<?php if(trim($details) != '') { ?>
								<div class="details">
									<?php echo $details;?>
								</div>
								<?php } ?>			
							</div>
						</div>
					</div><!--features_items-->
				</div>
			</div>
		</div>
	</section>
	
	<!--Footer-->
	<?php include('html/footer.php'); ?>
	<!--/Footer-->

	<!--/js-files-->
	<?php include('html/js-files.php'); ?>
	<!-- <script type="text/javascript" src="js/custom/sub-categories.js"></script> -->
	<!--/js-files-->
	<script type="text/javascript">
		var cat;
		var cat = '<?=$id;?>';
	</script>

</body>
</html>