<div class="left-sidebar about-side-bar">
	<div class="panel-group category-products" id="accordian"><!--category-productsr-->
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title"><a href="about-us" id="about-company">About Company <span class="badge pull-right"><i class="fa fa-angle-right"></i></span></a></h4>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title"><a href="why-new-pig" id="why-new-pig">Why New Pig? <span class="badge pull-right"><i class="fa fa-angle-right"></i></span></a></h4>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title"><a href="faqs" id="faqs">FAQ's <span class="badge pull-right"><i class="fa fa-angle-right"></i></span></a></h4>
			</div>
		</div>
		<!-- <div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title"><a href="catalogue-request" id="catalogue-request">Catalogue Request <span class="badge pull-right"><i class="fa fa-angle-right"></i></span></a></h4>
			</div>
		</div> -->
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title"><a href="contact-us" id="contact-us">Contact Us <span class="badge pull-right"><i class="fa fa-angle-right"></i></span></a></h4>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title"><a href="office-locations" id="office-locations">Office Locations <span class="badge pull-right"><i class="fa fa-angle-right"></i></span></a></h4>
			</div>
		</div>
	</div>
</div>
