    <script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.scrollUp.min.js"></script>
	<script src="js/price-range.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
    <script src="admin/js/toastr.js"></script>

    <script src="js/common.js"></script>

	<script>
	$(document).ready(function(){
	  	$('.show-nav').on('click', function(e) {
			// e.preventDefault();
			if($(window).width() < 767) {
				$('#'+$(this).data("toggle")).toggle();
			}
	  	});

	});
	</script>
	
	<script type="text/javascript">
	    $(function() {
	    	// fetchAllCategories();
	    });
		fetchAllCategories = function() {
			var req = {};
			req.action = 'fetchAllCategories';
			$.ajax({
				'type'	:	'post',
				'url'	:	'vendor/requests.php',
				'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				// console.log(res);
				if(res.status == 1) {
					// createNavigation(res.categories);
					// createSideBar(res.categories);
					// toastr.success(res.message);
				}
				else {
					toastr.error(res.message);
				}
			});		
		}

		createNavigation = function(categories) {
			html = '';
			$.each(categories, function(i, category) {
				if (category.parent_id == 0 && category.status == 1) {
					html += '<li>';
			        html += '<a href="products/'+category.alias+'">'+category.name+'</a>';
					html += '<ul class="sub-menu-cat">'
					$.each(categories, function(i, sub_category) {
						if (sub_category.parent_id == category.id && sub_category.status == 1) {

							var i = sub_category.calias.indexOf('_');	
							cat = sub_category.calias.slice(0,i);
							
							html += '<li class="dropdown">';
					        html += '<a href="'+cat+'/'+sub_category.alias+'">'+sub_category.name+'</a>';
							html += '</li>';
						}
					});

					html += '</ul>';
					html += '</li>';
				};
			});
			html += '<li><a href="best_sellers">Best Sellers</a></li>';
			$('#sub-menu-product').html(html);
		}
		createSideBar = function(categories) {
			html = '';
			var count=1;
			$.each(categories, function(i, category) {
				if (category.parent_id == 0 && category.status == 1) {
					html += '<div class="panel panel-default"><div class="panel-heading"><h4 class="panel-title">';
			        html += '<a data-toggle="collapse" data-parent="#accordian" href="#count'+count+'">';
			        html += '<span class="badge pull-right"><i class="fa fa-plus"></i></span>';
			        html += category.name;
			        html += '</a>';
			        html += '</h4></div>';

			        //html += '<a href="sub-categories.php?category_id='+category.id+'">'+category.name+'</a>';
					//html += '<ul class="sub-menu-cat">'
					html += '<div id="count'+count+'" class="panel-collapse collapse"><div class="panel-body"><ul>';
					$.each(categories, function(i, sub_category) {
						if (sub_category.parent_id == category.id && sub_category.status == 1) {

							var i = sub_category.calias.indexOf('_');	
							cat = sub_category.calias.slice(0,i);
							
							html += '<li><a href="'+cat+'/'+sub_category.alias+'""><i class="fa fa-angle-right"></i> '+sub_category.name+' </a></li>';
					        html += '</li>';				
						}
					});
					html += '</ul></div></div></div>';
					count++;
					
				};
			});
			
			// html +='<div class="panel panel-default"><div class="panel-heading">';
			// html += '<h4 class="panel-title"><a href="shop1.php">Specials</a></h4></div></div>';
			html += '<div class="panel panel-default"><div class="panel-heading">';
			html += '<h4 class="panel-title"><a href="best_sellers">Best Sellers</a></h4>';
			html += '</div></div>';

			$('div[side=cat]').append(html);
		}
    </script>
    <script type="text/javascript"> 
       var cdJsHost = (("https:" == document.location.protocol) ? "https://" : "http://"); 
       document.write(unescape("%3Cscript src='" + cdJsHost + "analytics.clickdimensions.com/ts.js' type='text/javascript'%3E%3C/script%3E")); 
     </script> 

     <script type="text/javascript"> 
       var cdAnalytics = new clickdimensions.Analytics('analytics.clickdimensions.com'); 
       cdAnalytics.setAccountKey('aegCZu9UUoEyDysJlieW7C'); 
       cdAnalytics.setDomain('newpig.com.au'); 
       cdAnalytics.setScore(typeof(cdScore) == "undefined" ? 0 : (cdScore == 0 ? null : cdScore)); 
       cdAnalytics.trackPage(); 
     </script> 		 
