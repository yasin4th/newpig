<footer id="footer">
	<div class="footer-widget">
		<div class="container">
			<div class="row">
				<!-- <div class="col-sm-2">
					<div class="companyinfo">
						<h2><span>New</span>Pig</h2>
						<p>Spill response products designed for liquid management, industrial safety and plant maintenance from New Pig Australia.</p>
					</div>
				</div> -->
				<div class="col-sm-5">
					<div class="single-widget">
						<h2>About IQSafety</h2>

						<p>iQSafety (previously known as Matthews Safety Products), a division of Matthews Australasia, is the authorised Australian 
						distributor of New Pig Safety Products, offering a huge variety of spill absorbent and spill containment products as well as 
						safe storage solutions.</p>
						<!-- <div class="row">
							
						</div> -->
						<!-- <ul class="nav nav-pills nav-stacked">
							<li><a href="#"><i class="fa fa-angle-right"></i> Absorbents</a></li>
							<li><a href="#"><i class="fa fa-angle-right"></i> Spill Kits</a></li>
							<li><a href="#"><i class="fa fa-angle-right"></i> Spill Control</a></li>
							<li><a href="#"><i class="fa fa-angle-right"></i> Spill Containment</a></li>
						</ul> -->
					</div>
				</div>
				<!-- <div class="col-sm-3">
					<div class="single-widget">
						<h2>&nbsp;</h2>
						<ul class="nav nav-pills nav-stacked">
							<li><a href="#"><i class="fa fa-angle-right"></i> Storage & Materials Handling</a></li>
							<li><a href="#"><i class="fa fa-angle-right"></i> Patch, Repair & Maintenance</a></li>
							<li><a href="#"><i class="fa fa-angle-right"></i> Specials</a></li>
							<li><a href="#"><i class="fa fa-angle-right"></i> Best Sellers</a></li>
						</ul>
					</div>
				</div> -->
				<div class="col-sm-2">
					<div class="single-widget">
						<h2>Quick Links</h2>
						<ul class="nav nav-pills nav-stacked">
							<li><a href=""><i class="fa fa-angle-right"></i> Home</a></li>
							<li><a href="about-us"><i class="fa fa-angle-right"></i> About</a></li>
							<li><a href="why-new-pig"><i class="fa fa-angle-right"></i> Why New Pig?</a></li>
							<li><a href="faqs"><i class="fa fa-angle-right"></i> FAQ's</a></li>
						</ul>
					</div>
				</div>
				<div class="col-sm-2">
					<div class="single-widget">
						<h2>&nbsp;</h2>
						<ul class="nav nav-pills nav-stacked">
							<li><a href="office-locations"><i class="fa fa-angle-right"></i> Office Locations</a></li>
							<li><a href="contact-us"><i class="fa fa-angle-right"></i> Contact Us</a></li>
							<li><a href="terms-and-conditions"><i class="fa fa-angle-right"></i> Terms & Conditions</a></li>
							<li><a href="privacy-policy"><i class="fa fa-angle-right"></i> Privacy Policy</a></li>
						</ul>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="address">
						<h2>Address</h2>
						<img src="images/home/map.png" alt="" />
						<p>35 Laser Drive • Rowville, VIC 3178<br>
							1-800-HOT-HOG® (468-464).<br>
							E-mail: <a href="mailto:hothog@newpig.com.au">hothog@newpig.com.au</a></p>
					</div>
				</div>
				
			</div>
		</div>
	</div>
	
	<div class="footer-bottom">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
				<p class="pull-left">© iQSafety 2016. All Rights Reserved</p>
				</div>
				<div class="col-md-4">
				<p class="text-center">We Accept: <span><img src="images/home/iq-master-card-icon.png"></span><span><img src="images/home/iq-visa-footer-icon.png"></span></p>	
				</div>
				<div class="col-md-4">
				<p class="pull-right">PIG<sup>®</sup> is a registered trademark of New Pig.</p>
				</div>
			</div>
		</div>
	</div>
	<div id="loader" style="width: 100%; height: 100%; position: fixed; top: 0px; left: 0px; z-index: 10000; display: none; background: rgba(0, 0, 0, 0.309804);">
	<img src="images/ajax-loader.gif" style="position: fixed;
	top: 50%;
	left: 50%;">
</div>

</footer>