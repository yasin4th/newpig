<?php
    $cats = getData(array('action' => 'fetchAllCategories'));
    //dd($cats);
?>

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-5KN6GT"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5KN6GT');</script> 
<!-- End Google Tag Manager -->
<div class="view-catalogue">
		<a href="https://www.newpig.com.au/eBook/index.html" target="_blank"><i class="fa fa-book"></i> View Catalogue Online</a>
	</div>
	<header id="header">
		<div class="header_top"><!--header_top-->
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="contactinfo">
							<ul class="nav nav-pills">
								<!-- <li class="number"><a href="#"><i class="fa fa-phone"></i> 1800 HOT HOG (468 464) &nbsp;&nbsp;&nbsp;<span>Mon-Fri 8am - 5pm EST</span></a></li> -->
								<li><a href="mailto:hothog@newpig.com.au"><i class="fa fa-envelope"></i> hothog@newpig.com.au</a></li>
								<!-- <li><a href="https://www.newpig.com.au/ebook/index.html" target="_blank"><i class="fa fa-book"></i> View Catalogue Online</a></li> -->
							</ul>
						</div>
					</div>
					<div class="col-sm-4">
						<span class="same-day-shipping-place-order"><i class="fa fa-truck"></i> Same day shipping. Place order before 1pm EST</span>
					</div>
					<div class="col-sm-4">
						<div class="contactinfo pull-right">
							<ul class="nav nav-pills">
								<!-- <li><a href="#"><i class="fa fa-sign-in"></i> Login</a></li>
								<li><a href="#"><i class="fa fa-user"></i> Register</a></li> -->
								<li><a href="cart" id="cartDetails"><i class="fa fa-shopping-cart"></i> View Cart ( <?php @session_start(); if (isset($_SESSION['products'])) {
									echo count($_SESSION['products']);
								} else {
									echo 0;
								} ?> )</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header_top-->
		
		
		<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row">
					<div class="col-sm-3">
						<div class="logo pull-left">
							<a href=""><img src="images/home/iq_logo.png" alt="Newpig" title="Newpig" /></a>
						</div>
					</div>
					<div class="col-sm-9">
						<!-- <div class="shop-menu">
							<div class="row border0 padd0">
								<div class="col-sm-12">
									<ul class="nav navbar-nav pull-right">
										<li><a href="login.php"><i class="fa fa-sign-in"></i> Login</a></li>
										<li><a href="registration.php"><i class="fa fa-user"></i> Register</a></li> -->
										<!-- <li><a href="#"><i class="fa fa-pencil-square-o"></i> Request Pigalog</a></li> -->
									<!-- </ul>
								</div>
							</div>
						</div> -->
						<div class="drop-nemu">
							<div class="row border0">
								<div class="col-sm-8">
									<!-- <div class="search_box">
										<input type="text" placeholder="Search for products, categories & solutions"/>
									</div> -->
									<form id="search-form">
									<div class="form-group">
										<div class="input-group stylish-input-group">
					                        <input type="text" class="form-control" id="search-text" placeholder="Search for products, categories & solutions" >
					                        <span class="input-group-addon btn-success">
					                           <button type="submit">
					                              <span class="fa fa-search"></span>
					                           </button>  
					                        </span>
					                    </div>
				                    </div>
				                </form>
								</div>
								<div class="col-sm-4">
									<h4 class="hot-hog-num"><i class="fa fa-phone"></i> 1800 HOT HOG (468 464)</h4>
									<h4 class="hot-hog-num1"><i class="fa fa-phone"></i> 1800 468 464</h4>
									<a href="mailto:hothog@newpig.com.au" class="secondary-email"><i class="fa fa-envelope"></i> hothog@newpig.com.au</a>
									<p class="hot-hog-time">Mon-Fri 8am - 5pm EST</p>
									<!--<div id="bellmeDiv"><a href="https://bellmedia.com/b">Click to call</a> loading ...</div><script type="text/javascript">var bellMeId = "beb794228784742171e95b59e0e2e68e",bmaId="";document.write(unescape('%3Cscript src="https://wpc.1BE2.edgecastcdn.net/801BE2/bme/b/js/bellme.js" type="text/javascript"%3E%3C/script%3E'));</script>-->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--/header-middle-->
		<div class="main-menu">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<div class="mainmenu pull-left">
							<ul class="nav navbar-nav collapse navbar-collapse">
								<li><a href=""> Home</a></li>
								<li class="dropdown"><a data-toggle="sub-menu-about" class="show-nav">About Us<i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu" id="sub-menu-about">
                                        <li><a href="about-us"> About Company</a></li>
                                        <li><a href="why-new-pig"> Why New Pig?</a></li>
										<li><a href="faqs"> FAQ's</a></li> 										
                                    </ul>
                                </li> 
                                <li class="dropdown"><a href="products" data-toggle="sub-menu-product" class="show-nav">Products<i class="fa fa-angle-down pull-right"></i></a>
                                    <ul role="menu" class="sub-menu" id="sub-menu-product">
                                        <?=createNavigation($cats['categories']);?>
                                        <!-- <li class="dropdown"><a href="shop.php" data-toggle="sub-menu-absorbents" class="show-nav sub-menu-cat-atag">Absorbents</a>
                                        	<ul role="menu" class="sub-menu-cat" id="sub-menu-absorbents">
                                        		
                                        	</ul>
                                        </li>
										<li><a href="shop.php" data-toggle="sub-menu-spill-kits" class="show-nav sub-menu-cat-atag">Spill Kits</a>
											<ul role="menu" class="sub-menu-cat" id="sub-menu-spill-kits">
                                        		
										</li> 
										<li><a href="shop.php" data-toggle="sub-menu-spill-control" class="show-nav sub-menu-cat-atag">Spill Control</a>
											<ul role="menu" class="sub-menu-cat" id="sub-menu-spill-control">
                                        		
                                        	</ul>
										</li> 
										<li><a href="shop.php" data-toggle="sub-menu-spill-containment" class="show-nav sub-menu-cat-atag">Spill Containment</a>
											<ul role="menu" class="sub-menu-cat" id="sub-menu-spill-containment">
                                        		
                                        	</ul>
										</li> 
										<li><a href="shop.php" data-toggle="sub-menu-storage-materials" class="show-nav sub-menu-cat-atag">Storage & Materials Handling</a>
											<ul role="menu" class="sub-menu-cat" id="sub-menu-storage-materials">
                                        		
                                        	</ul>
										</li> 
										<li><a href="shop.php" data-toggle="sub-menu-patch-repair" class="show-nav sub-menu-cat-atag">Patch, Repair & Maintenance</a>
											<ul role="menu" class="sub-menu-cat" id="sub-menu-patch-repair">
                                        		
                                        	</ul>
										</li> 
										<li><a href="shop.php">Specials</a></li> 
										<li><a href="shop.php">Best Sellers</a></li>  -->
                                    </ul>
                                </li>
								<!-- <li class="dropdown"><a data-toggle="sub-menu-resource" class="show-nav">Resource Library<i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu" id="sub-menu-resource">
                                        <li><a href="blog.php">Find the solution to your Problem!</a></li>
										<li><a href="videos.php">Videos</a></li>
                                    </ul>
                                </li> --> 
                                <li><a href="blog"> Blog</a></li>
								<li><a href="videos"> Videos</a></li>
								<li><a href="free-pigalog"> Request Pigalog</a></li>
                              	<li><a href="services"> Spill Kit Maintenance</a></li>
								<li><a href="contact-us"> Contact Us</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>	
	</header>