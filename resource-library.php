<!DOCTYPE html>
<html lang="en">
<head>
    <?php include('html/head-tag.php'); ?>
</head>
<!--/head-->

<body>
	<!--header-->
	<?php include('html/header.php'); ?>
	<!--/header-->
	
	<section class="mrg-top30">
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<?php include('html/side-bar.php'); ?>
				</div>
				
				<div class="col-sm-9 padding-right mrg-bot30">
					<h2>Resources</h2>
					<p>Welcome to our resource library. We strive to continuously update this section to include resources that you will find interesting and useful.</p>
					<div class="col-sm-6">
						<a href="find-the-solution-to-your-problem.php" class="resource-library">
							<img class="img-responsive" src="images/iq-blog.png" />
							<h4>Please read our blog articles.</h4>
						</a>
					</div>
					<div class="col-sm-6">
						<a href="videos.php" class="resource-library">
							<img class="img-responsive" src="images/iq-video.png" />
							<h4>Please view our videos.</h4>
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<!--Footer-->
	<?php include('html/footer.php'); ?>
	<!--/Footer-->

	<!--/js-files-->
	<?php include('html/js-files.php'); ?>
	<!--/js-files-->

</body>
</html>