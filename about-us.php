<!DOCTYPE html>
<html lang="en">
<head>
	<title>	iQSafety - New Pig spill absorbents & containment solutions </title>

	<meta name="description" content="iQSafety is a division of Matthews Australasia. iQSafety has a range of New Pig spill absorbent & containment solutions that helps to clean any spill. New Pig's range of mats, booms, socks and pillows are available loose or in the form of spill kits." />
    <?php include('html/head-tag.php'); ?>
</head>
<!--/head-->

<body>
	<!--header-->
	<?php include('html/header.php'); ?>
	<!--/header-->
	
	<section class="mrg-top30">
		<div class="container">
			<div class="row"> 
				<div class="col-sm-3">
					<?php //include('html/side-bar.php'); ?>
					<?php include('html/about-side-bar.php'); ?>
				</div>

	    		<div class="col-sm-9 padding-right mrg-bot30">
    					<h2 class="main-title"><span class="white-bg">About Us</span></h2>
	    			<div class="row">
	    				<div class="col-md-12">
		    				<div class="about">
				    			<p>iQSafety (previously known as Matthews Safety Products), a division of Matthews Australasia, is the authorised Australian
				    			 distributor of New Pig Safety Products, offering a huge variety of spill absorbent and spill containment products as well 
				    			 as safe storage solutions.</p>
				    			<p>iQSafety's range of New Pig spill absorbent and containment solutions can help you prepare for any spill. New Pig's range
				    			 of mats, booms, socks and pillows are available loose or in the form of spill kits.</p>
				    			<p>Matthews also carry a range of Justrite Safety Cabinets that meet all Australian standards. Available in almost any shape,
				    			 size and type, JUSTRITE Dangerous Goods and Safety Storage Cabinets provide a safe, convenient and secure storage option for
				    			  your dangerous goods. </p>
				    			  <p>iQSafety can help you address a range of safety issues you might have in your workplace.</p>
				    			  <h4>Our Product Range</h4>
				    			  <p>Our unique and best in class product range includes:</p>
				    			  <ul>
				    			  	<li>Absorbents</li>
				    			  	<li>Spill Kits</li>
				    			  	<li>Spill Control Solutions</li>
				    			  	<li>Spill Containment Solutions</li>
				    			  	<li>Storage and Material Handling Solutions</li>
				    			  	<li>Patch, Repair and Maintenance Solutions</li>
				    			  </ul>
		    			    </div>
	    			    </div>
    			    </div>
	    		</div>   			
	    	</div> 
		</div>
	</section>
	
	<!--Footer-->
	<?php include('html/footer.php'); ?>
	<!--/Footer-->

	<!--/js-files-->
	<?php include('html/js-files.php'); ?>
	<script type="text/javascript"> 
	   	$(function () { 
	      	$('.about-side-bar h4 a').removeClass('active'); 
	      	$('.about-side-bar h4 a#about-company').addClass('active'); 
	   	}); 
	</script> 
	<!--/js-files-->

</body>
</html>