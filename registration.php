<!DOCTYPE html>
<html lang="en">
<head>
    <?php include('html/head-tag.php'); ?>
</head>
<!--/head-->

<body>
	<!--header-->
	<?php include('html/header.php'); ?>
	<!--/header-->
	
	<section class="mrg-top30">
		<div class="container">
			<div class="row">  	
				<div class="col-sm-12">
					<h2>Registration</h2>
				</div>
	    		<div class="col-sm-8">
	    			<div class="contact-form">
			    		<form id="main-contact-form" class="contact-form row" name="contact-form" method="post">
		    				<h2 class="title text-center mrg-top30"><span class="white-bg">Personal details</span></h2>
		    				<div class="status alert alert-success" style="display: none"></div>
			    			<div class="row">
					            <div class="form-group col-md-6">
					            	<label class="form-label">First Name <span class="required">*</span></label>
			                		<input type="text" name="first-name" class="form-control" required="required" placeholder="First Name">
					            </div>
					            <div class="form-group col-md-6">
					            	<label class="form-label">Last Name <span class="required">*</span></label>
					                <input type="text" name="last-name" class="form-control" required="required" placeholder="Last Name">
					            </div>
					            <div class="form-group col-md-6">
					            	<label class="form-label">Email Address <span class="required">*</span></label>
					                <input type="email" name="email" class="form-control" required="required" placeholder="Email Address">
					            </div>
					            <div class="form-group col-md-6">
					            	<label class="form-label">Phone Number <span class="required">*</span></label>
					                <input type="text" name="contact-number" class="form-control" required="required" placeholder="Phone Number">
					            </div>
				            </div>
				            <h2 class="title text-center mrg-top30"><span class="white-bg">Shipping Address</span></h2>
				            <div class="row">
					            <div class="form-group col-md-6">
					            	<label class="form-label">Address Line 1</label>
					                <input type="text" name="address-line-1" class="form-control" required="required" placeholder="Address Line 1">
					            </div>
					            <div class="form-group col-md-6">
					            	<label class="form-label">Address Line 2</label>
					                <input type="text" name="address-line-2" class="form-control" required="required" placeholder="Address Line 2">
					            </div>
					            <div class="form-group col-md-6">
					            	<label class="form-label">City</label>
					                <input type="text" name="city" class="form-control" required="required" placeholder="City">
					            </div>
					            <div class="form-group col-md-6">
					            	<label class="form-label">State</label>
					                <input type="text" name="state" class="form-control" required="required" placeholder="State">
					            </div>
					            <div class="form-group col-md-6">
					            	<label class="form-label">Zip Code</label>
					                <input type="text" name="zip-code" class="form-control" required="required" placeholder="Zip Code">
					            </div>
					            <div class="form-group col-md-6">
					            	<label class="form-label">Country</label>
					                <input type="text" name="country" class="form-control" required="required" placeholder="Country">
					            </div>
				            </div>
				            <div class="form-group col-md-12">
				                <input type="submit" name="submit" class="btn btn-primary pull-right" value="login">
				            </div>
			        	</form>
		    		</div>
    			</div>
	    		<div class="col-sm-4"></div>
	    	</div> 
		</div>
	</section>
	
	<!--Footer-->
	<?php include('html/footer.php'); ?>
	<!--/Footer-->

	<!--/js-files-->
	<?php include('html/js-files.php'); ?>
	<!--/js-files-->

</body>
</html>