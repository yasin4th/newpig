<!DOCTYPE html>
<html lang="en">
<head>
	<title>	NewPig Videos - Pig Spill Clean-up & Safety Products Videos </title>
 	<meta name="description" content="NewPig Videos - Pig Spill Clean-up & Safety Products Videos" />
    <?php include('html/head-tag.php'); ?>
</head>
<!--/head-->

<body>
	<!--header-->
	<?php include('html/header.php'); ?>
	<!--/header-->
    
    <?php $videos = getData(array('action' => 'fetchVideos', 'active' => '1')); ?>
	
	<section class="mrg-top30">
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<?php include('html/side-bar.php'); ?>
				</div>
				
				<div class="col-sm-9 padding-right mrg-bot30">
					<h2 class="main-title"><span class="white-bg">Videos</span></h2>
					<div class="row video">
						<?php
                            $str = $modal = '';
                            if(count($videos['video']) == 0) {
                                $str .= '<span> 0 result found.</span>';
                            }
                            else
                            {
                                foreach ($videos['video'] as $video) {
                                    $str .= '<div class="col-sm-12 mrg-tb20">';
                                    $str .= '<div class="col-sm-5">';
                                    $str .= '<a href="" data-toggle="modal" data-target="#' . $video['url'] . '"><img src="http://img.youtube.com/vi/' . $video['url'].'/0.jpg" style="width:300px;height:180px;" alt="" /></a>';
                                    $str .= '</div>';
                                    $str .= '<div class="col-sm-7">';
                                    $str .= '<a href="" data-toggle="modal" data-target="#'. $video['url'] . '"><h5>' . $video['name'] . '</h5></a>';
                                    //$str .= '<p>Video Description of Video : '+video.name+''+video.url+'</p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#' + video.name + '">View Video</button>';
                                    $str .= '<p>';
                                    $str .=  $video['description'];
                                    $str .= '</p>';
                                    $str .= '<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#'.$video['url'].'">View Video</button>';
                                    $str .= '</div></div>';        

                                    $modal .= '<div class="modal fade" id="'. $video['url'] .'" role="dialog"><div class="modal-dialog modal-md"><div class="modal-content">' ;
                                    $modal .= '<div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title">'. $video['name'] .'</h4></div>';
                                    $modal .= '<div class="modal-body"><iframe width="560" height="315" src="https://youtube.com/embed/'. $video['url'] .'" frameborder="0" allowfullscreen></iframe></div>';
                                    $modal .= '<div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div>';
                                    $modal .= '</div></div></div>';
                                }
                            }
                            echo $str;
                        ?>
					</div>
				</div>
			</div>
		</div>
	</section>

    <?php 
        if($modal) { echo $modal; }
    ?>
	
	<!--Footer-->
	<?php include('html/footer.php'); ?>
	<!--/Footer-->

	<!--/js-files-->
	<?php include('html/js-files.php'); ?>
	<!-- <script type="text/javascript" src="js/custom/videos.js"></script> -->
	<!--/js-files-->

</body>
</html>