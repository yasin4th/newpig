<!DOCTYPE html>
<html lang="en">
<head>
	<title>	Privacy Policy - iQSafety - Spill Clean-up Products & Safety Solution </title>
	<meta name="description" content="Privacy Policy - iQSafety - Spill Clean-up Products & Safety Solution" />

    <?php include('html/head-tag.php'); ?>
</head>
<!--/head-->

<body>
	<!--header-->
	<?php include('html/header.php'); ?>
	<!--/header-->
	
	<section class="mrg-top30">
		<div class="container">
			<div class="row"> 
				<div class="col-sm-3">
					<?php include('html/side-bar.php'); ?>
				</div>

	    		<div class="col-sm-9 padding-right mrg-bot30">
	    			<h2>Privacy Policy</h2>
	    			<div class="row">
	    				<div class="col-md-12">
		    				<div class="about">
		    					<h4>About this privacy policy</h4>
				    			<p>This document sets out the policy of Matthews Australasia Pty Ltd (ACN 079 545 145) (referred to as “we”, “us” or “our”) 
				    			relating to the privacy of your personal information (Privacy Policy).<br>Matthews Australasia Pty Ltd (ACN 079 545 145) and its related entities (including, but not limited to, iQ Vision Pty Ltd 
				    			and iQ Safety Products Pty Ltd) are committed to protecting the privacy of the personal information it collects and receives.  
				    			We are bound by the Australian Privacy Principles (APP) contained in the Privacy Act 1988 (Cth).<br> This Privacy Policy applies 
				    			to all your dealings with us whether at one of our division offices, through our call centres, via our websites or with one 
				    			of our representatives. This Privacy Policy seeks to explain:</p>
				    			<ul>
				    			  	<li>the kinds of personal information that we collect and hold; </li>
				    			  	<li>how we collect and hold your personal information; </li>
				    			  	<li>why we collect, hold, use and disclose your personal information; </li>
				    			  	<li>how you may access and seek the correction of your personal information as held by us; </li>
				    			  	<li>how you may complain about a breach of the APPs or a registered APP code (if any) that we are 
				    			  	bound by, and how we will deal with such a complaint; and</li>
				    			</ul>
				    			<p>if we are likely to disclose your personal information to overseas recipients and the countries in which such recipients 
				    			are likely to be located (if it is practicable to specify those countries in this policy).<br>You accept this Privacy Policy and expressly consent to our collection, use and disclosure of your personal information 
				    			as described in this Privacy Policy by using our websites (www.matthews.com.au; www.iqvision.com.au; www.newpig.com.au ), 
				    			by purchasing our products and services, by liaising with us. </p>
				    			<h4>Your Personal Information </h4>
				    			<p>We only collect personal information (being information that identifies or could reasonably identify an individual) 
				    			where we consider it to be reasonably necessary for one or more of our business functions or activities, and will 
				    			do so in accordance with the APPs, including: </p>
				    			<ul>
				    			  	<li>when you participate in a promotion, competition, promotional activity, survey, market research, subscribe to 
				    			  	our mailing list or interact or follow our social media pages;</li>
				    			  	<li>via our stores, head office or trade shows; </li>
				    			  	<li>when you place an order with us;</li>
				    			  	<li>when applying for a position with us; and/or</li>
				    			  	<li>as you navigate through our website.</li>
				    			</ul>
				    			<p>The kinds of personal information we may collect from you will depend on what type of interaction you have with us but may include:</p>
				    			<ul>
				    			  	<li>your name, date of birth, age and occupation; </li>
				    			  	<li>your postal address, email address, telephone number, fax number and your IP address;</li>
				    			  	<li>previous order information; and</li>
				    			  	<li>credit card and other banking details.</li>
				    			</ul>
				    			<h4>Your Sensitive Information</h4>
				    			<p>Sensitive information is information about you that reveals your racial or ethnic origin, political opinions, religious or 
				    			philosophical beliefs or affiliations, membership of a professional or trade association, membership of a trade union, details 
				    			of health, disability, sexual orientation or criminal record.<br> It is our policy to only collect your sensitive information where 
				    			it is reasonably necessary for our functions or activities and either you have consented or we are required or authorised under 
				    			law to do so.</p>
				    			<h4>How we collect your personal information</h4>
				    			<p>We will solicit your personal information by lawful and fair means directly from you unless it is unreasonable or impracticable 
				    			to do so.<br> We will collect directly personal information: </p>
				    			<ul>
				    				<li>by email;</li>
				    				<li>over the telephone;</li>
				    				<li>by written correspondence (such as letters, faxes);</li>
				    				<li>on hard copy forms (including registration forms, competition entry forms and surveys);</li>
				    				<li>in person (at job interviews, exams or seminars); or</li>
				    				<li>through our website (online purchases, web form submission or participate in a live chat);</li>
				    				<li>electronic systems such as applications; and</li>
				    				<li>through our security surveillance cameras.</li>
				    			</ul>
				    			<p>We may collect your personal information from third parties including: </p>
				    			<ul>
				    				<li>your legal representatives;</li>
				    				<li>direct marketing database providers;</li>
				    				<li>the Australian Tax Office (ATO);</li>
				    				<li>the Australian Securities and Investment Commission (ASIC); and</li>
				    				<li>public sources (phone directories, membership lists, professional and trade associations, ASIC, bankruptcy or court registry searches).</li>
				    			</ul>
				    			<h4>Unsolicited personal information</h4>
				    			<p>Unsolicited personal information is personal information we receive that we have taken no active steps to collect (such as a 
				    			job application sent to us by an individual on their own initiative, rather than in response to an advertisement). <br>We may keep records of unsolicited personal information if the information is reasonably necessary for one or more of our 
				    			functions or activities. If not, it is our policy to destroy the unsolicited information or ensure that the information is 
				    			de-identified, provided it is lawful and reasonable to do so.</p>
				    			<h4>Using your personal information</h4>
				    			<p>We collect your personal information so that we can use it for our functions and activities and provide products and services 
				    			to you, which include, among other things:</p>
				    			<ul>
				    				<li>processing orders you place with us; </li>
				    				<li>providing you with email or SMS confirmation of your orders, providing you with our products and processing refunds where applicable;</li>
				    				<li>administering and responding to your enquiry or feedback about our products;</li>
				    				<li>conducting, and allowing you to participate in, a promotion, competition, promotional activity, survey or market research;</li>
				    				<li>promoting and marketing current and future products and services to you, informing you of upcoming events and special promotions and offers 
				    				and analysing our products and services so as to improve and develop new products and services;</li>
				    				<li>administering and conducting traineeships;</li>
				    				<li>facilitating our internal business operations, including fulfilment of any legal and regulatory requirements;</li>
				    				<li>improving the operation or navigation of our websites;</li>
				    				<li>to provide the product or service you have requested or to answer your inquiry; </li>
				    				<li>manage and run our website;  </li>
				    				<li>delivering or enhancing our products and services;</li>
				    				<li>maintain contact with you about our products and services; and</li>
				    				<li>any other purpose directly related to our work and for which you have provided consent (where it is reasonably required by law).</li>
				    			</ul>
				    			<p>Our website requires subscriptions or registrations to use certain services, functions or content. You will know what information 
				    			is being collected via these processes when you complete the relevant forms and provide the required details prior to submitting 
				    			the application. We will collect data relating to any transactions you carry out via our website and the fulfilment of your order. 
				    			We may also use your data to monitor for any unauthorised use of our website, content or subscriptions to our services.</p>
				    			<h4>Purpose of collection</h4>
				    			<p>If we collect personal information for a purpose (the primary purpose), we will not use or disclose the information for 
				    			any other purpose (the secondary purpose) unless:</p>
				    			<ul>
				    				<li>you would have consented to the use or disclosure of the your personal information; or</li>
				    				<li>in relation to the use or disclosure of your personal information:</li>
				    				<li>you would reasonably expect us to use or disclose your information for the secondary purpose and the secondary purpose is directly 
				    				related to the primary purpose (sensitive information) or related to the primary purpose (not sensitive information); 
					    				<ul>
					    					<li>use or disclosure is required or authorised under Australian law or a court/tribunal; </li>
					    					<li>a permitted situation exists in relation to our use or disclosure of the information; </li>
					    					<li>a permitted health situation exists in relation to use or disclosure of the information; or</li>
					    					<li>we reasonably believes that the use or disclosure of the information is reasonably necessary for one or more enforcement related 
					    					activities conducted by, or on behalf of, an enforcement body.</li>
					    				</ul>
				    				</li>
				    			</ul>
				    			<p>If we use or disclose your personal information because we reasonably believe that the use or disclosure of your information 
				    			is reasonably necessary for one or more enforcement related activities conducted by, or on behalf of, an enforcement body, 
				    			we will make a written note of the use or disclosure. </p>
				    			<h4>Do you have to provide personal information?</h4>
				    			<p>You can refuse to provide personal information. However a refusal may mean that the service you requested is not provided or 
				    			membership will be refused or forfeited. </p>
				    			<h4>Sharing your personal information</h4>
				    			<p>We may use and disclose personal information for related purposes to third parties. Types of organisations to whom we may 
				    			disclose your personal information includes: </p>
				    			<ul>
				    				<li>government bodies such as ATO, ASIC, Australian Prudential Regulatory Authority and the police or courts (as required by law);</li>
				    				<li>professional or government organisations; and</li>
				    				<li>our contracted service providers including: information technology service providers; marketing and communications agencies; 
				    				mailing houses, freight and courier services; printers & distributors of marketing material; and external advisers (recruiters, 
				    				auditors & lawyers).</li>
				    			</ul>
				    			<p>We do not provide, rent, sell or exchange your personal information to third parties without your prior approval.</p>
				    			<h4>Government Identifiers</h4>
				    			<p>We do not adopt, use or disclose government related identifiers (such as a Medicare number or driver's licence number) as 
				    			our own identifier for you unless it’s required/authorised by law or court/tribunal order, it’s reasonably necessary for 
				    			us to verify your identity, it’s reasonably necessary to fulfil our obligations or we reasonably believes it is reasonably 
				    			necessary for one or more enforcement related activities.</p>
				    			<h4>Cookies </h4>
				    			<p>To improve our services, we sometimes collect de-identified information from web users. Although the information collected does 
				    			not identify an individual, it does provide us with useful statistics that permits us to analyse and improve its web services.<br>When you visit our website, a record of your visit is logged and the following data is supplied by your browser:</p>
				    			<ul>
				    				<li>your IP address and/or domain name;</li>
				    				<li>your operating system (type of browser & platform);</li>
				    				<li>the date, time and length of your visit; and </li>
				    				<li>the resources you accessed or downloaded. </li>
				    			</ul>
				    			<p>We use this information to customize information for website visitors and to collect aggregated data for the purposes of 
				    			analysis, quality control, administering and improving the website. It is not used for any other purpose.</p>
				    			<p>Aggregated data may be shared with third parties. You can stop your browser from accepting new cookies or disable 
				    			cookies altogether by changing your browser preferences.</p>
				    			<p>Cookies might also be used for Google conversion tracking and Google Analytics. When Google uses such cookies, Google 
				    			uses a variety of techniques to protect a user’s privacy. You can read more here. You can opt out of Google's use of cookies 
				    			by visiting the Google Advertising opt-out page.<br>We use Google AdWords Remarketing features in order to better serve our advertising to you on other sites. This uses the 
				    			Teracent cookie. You may opt out of the Teracent cookie by visiting the Network advertising opt-out page.</p>
				    			<h4>Links to other websites</h4>
				    			<p>Links to third party websites that we do not operate or control are provided for your convenience. We are not responsible 
				    			for the privacy or security practices of websites that are not covered by this Privacy Policy. Third party websites should 
				    			have their own privacy and security policies which we encourage you to read before supplying any personal information to them.</p>
				    			<h4>Direct Marketing </h4>
				    			<p>If we hold your personal information, we may use or disclose that personal information (other than sensitive information) 
				    			for direct marketing if: </p>
				    			<ul>
				    				<li>we collected the information from you; and </li>
				    				<li>you would reasonably expect us to use or disclose the information for that purpose; and </li>
				    				<li>we provided you with a simple way to opt out of receiving direct marketing from us; and </li>
				    				<li>you have not made such an opt out request to us.</li>
				    			</ul>
				    			<p>We may also use or disclose your personal information (other than sensitive information) for direct marketing if:</p>
				    			<ul>
				    				<li>we collected the information from you and you would not reasonably expect us to use or disclose the information 
				    				for that purpose or someone other than you; and </li>
				    				<li>either: 
				    					<ul>
				    						<li>you have consented to the use or disclosure of the information for that purpose; or</li>
				    						<li>it is impracticable to obtain that consent; and </li>
				    					</ul>
				    				</li>
				    				<li>we provided you with a simple way to opt out of receiving direct marketing from us; and </li>
				    				<li>in each direct marketing communication with you: 
				    					<ul>
				    						<li>we include a prominent statement that you can request to opt out; or</li>
				    						<li>we otherwise draw your attention to the fact that you can request to opt out; and </li>
				    						<li>you have not made such a request to us.</li>
				    					</ul>
				    				</li>
				    			</ul>
				    			<p>We can use or disclose your sensitive information for the purpose of direct marketing if you have consented to the use or disclosure of that information for direct marketing.<br>
				    			We may also use or disclose your personal information for direct marketing if:</p>
				    			<ul>
				    				<li>we are a contracted service provider for a Commonwealth contract; and</li>
				    				<li>we collected your information in order to meet an obligation under that contract; and</li>
				    				<li>the use or disclosure is necessary to meet (directly or indirectly) such an obligation.</li>
				    			</ul>
				    			<p>If we have collected the personal information that we used to send you direct marketing material from a third party, you can 
				    			ask us to notify you of its source of information.  It is our policy is to do so unless it is unreasonable or impracticable.</p>
				    			<h4>How to opt out of direct marketing</h4>
				    			<p>If we use or disclose your personal information for the purpose of direct marketing, you may request not to receive direct marketing communications from us.<br>
								If we use or disclose your personal information for the purpose of facilitating direct marketing by other organisations, you may request that we do not use or disclose your information for this purpose.<br>
								We will give effect to your request not to receive direct marketing from us or an entity facilitated by us free of charge within a reasonable time after the request is made.</p>
								<h4>Collection Notices</h4>
								<p>At or before the time of collection of personal information from you, or as soon as practicable afterwards, we will take reasonable steps to notify you or to otherwise ensure that you are aware of: </p>
								<ul>
									<li>our contact details; </li>
									<li>the fact that we collect, or have collected, your personal information and the circumstances of that collection (if collected from someone other than you); </li>
									<li>if the collection is required or authorised by law or a court/tribunal order; </li>
									<li>why the personal information was collected by us; </li>
									<li>the consequences to you if we do not collect all or some of your personal information; </li>
									<li>any other entity, body or person, or the types of any other entities, bodies or persons, to which we usually disclose personal information; </li>
									<li>if we are likely to disclose your personal information to overseas recipients and if so, the countries in which such recipients are likely to be located (if it is practicable to specify those countries or to otherwise make the individual aware of them); and</li>
									<li>this privacy policy and that it explains how:
										<ul>
											<li>you may access personal information held by  us and seek the correction of the information; </li>
											<li>you may complain about a breach of the APP, or a registered APP code (if any) that we are bound by and how we will deal with a complaint.</li>
										</ul>
									</li>
								</ul>
								<p>We will generally include these matters in a collection notice. For example, where personal information is collected on a 
								paper or website form, we will include a collection notice, or a clear link to it, on the form. <br>
								Collection notices may provide more specific information than this Privacy Policy in relation to particular collections of 
								personal information. The terms of this Privacy Policy are subject to any specific provisions contained in collection notices 
								and in terms and conditions of particular offers, products and services. We encourage you to read those provisions carefully.</p>	
								<ul>
									<li>secure password protected databases for storage;</li>
									<li>confidentiality requirements of staff; </li>
									<li>security for access to our systems including firewalls; </li>
									<li>servers kept at a secure location with limited access;</li>
									<li>document storage security requirements; </li>
									<li>access controls for our building;</li>
									<li>limited the provision of personal information to third parties and subject to guarantees about use; and</li>
									<li>our staff are trained to deal with the information. </li>
								</ul>
								<p>We cannot guarantee that personal information will be protected against unauthorized access or misuse and we do not accept any liability for the improper actions of unauthorized third parties. <br>
								We will retain your personal information for as long as necessary to fulfil our obligations to you, to protect our legal interests, to comply with an Australian law or as otherwise stated to you when we collected your personal information.<br>
								Once we are no longer required to retain your personal information, we will take reasonable steps to destroy your personal information or to ensure that your personal information is de-identified.</p>
								<h4>Disclosing your personal information overseas</h4>
								<p>We may disclose your personal information to other third party service providers operating outside Australia who work with us or one of our suppliers, agents, or partners.  We may also store your personal information on servers based overseas or in the “cloud” or other types of networked or electronic storage. <br>
								Before disclosing your personal information to an overseas third party, we will take reasonable steps to ensure that the overseas recipient:</p>
								<ul>
									<li>does not breach the APPs in relation to your personal information; or</li>
									<li>the recipient is subject to a law, or binding scheme, that has the effect of protecting your personal information in a way that is substantially similar to the way in which the APPs protect the information.</li>
								</ul>
								<p>Personal information may be disclosed by us to our related entities and third parties in jurisdictions including: New Zealand, Sweden, Germany, United Kingdom, Singapore and the United States of America.  The European Commission has recognized these countries as providing adequate protection of personal information.<br>
								To make it easy for you to deal with us and provide you with a more personal and consistent experience, we may exchange and combine personal information with our related entities for the purposes described in this policy.  <br>
								If your personal information is collected using a document that references this policy, you are taken to consent to the disclosure, transfer, storing or processing of your personal information outside of Australia.  </p>
								<p>You acknowledge that by providing such consent:</p>
								<ul>
									<li>we will not be required to take steps as are  reasonable in the circumstances to ensure that such third parties comply with the APPs;</li>
									<li>if the overseas recipient breaches the APPs, we won’t be liable under the Act and you won’t be able to seek redress under Act;</li>
									<li>the overseas recipient may not be subject to any privacy law or principles similar to the APPs;</li>
									<li>you may be unable to seek redress overseas; and</li>
									<li>the overseas recipient is subject to a foreign law that could compel the disclosure of personal information to a third party, such as an overseas authority.</li>
								</ul>
								<p>If you withdraw consent, we will not rely on this consent when dealing with your information going forward.</p>
								<h4>Anonymity </h4>
								<p>You have the option of not identifying yourself or using a pseudonym when dealing with us provided that is it lawful and practicable.<br>
								We will try to accommodate a request for anonymity if possible.  However, your right to anonymity does not apply in relation to a matter if:</p>
								<ul>
									<li>we are required or authorised by or by law or a court/tribunal order to deal with individuals who have identified themselves; or</li>
									<li>it is impracticable for us to deal with individuals who have not identified themselves.</li>
								</ul>
								<p>It is our policy to enable you to access our website and make general phone queries without having to identify yourself and to enable you to respond to our surveys anonymously.<br>
								In some cases, if you don't provide us with your personal information when requested, we may not be able to respond to your request or provide you with products or services. </p>
								<h4>Accessing to your personal information </h4>
								<p>You have the right, upon request, to access any of our records containing your personal information. <br>
								To request access to your personal information please contact our privacy officer. We will respond to your request to access your personal information within a reasonable period of time.<br>
								On the basis that it is reasonable and practicable to do so, we will give you access to the information requested.<br>
								If we refuse your request to access personal information, we will provide a written notice setting out the reasons for the refusal & how you can complain about the refusal.<br>
								Reasons for a refusal may include if it:</p>
								<ul>
									<li>poses a serious threat to the life, health or safety; </li>
									<li>would have an unreasonable impact on other’s privacy; </li>
									<li>is frivolous or vexatious or unlawful; </li>
									<li>is required or authorised by law or court/tribunal order; </li>
									<li>would reveal our intentions, prejudice any negotiations or relate to commercially sensitive decisions or legal proceedings; or</li>
									<li>would be likely to prejudice one or more enforcement related activities.</li>
								</ul>
								<p>When you make a request to access personal information, we will require you to provide some form of identification (such as a driver's licence or passport) so we can verify that you are the person to whom the information relates. </p>
								<h4>Help us keep your personal information accurate</h4>
								<p>We will take reasonable steps to ensure our records of personal information are accurate, up to date and complete.<br>
								The accuracy of information depends largely on the information you provide.  If you don’t give us all the personal information we may require, or the personal information provided is inaccurate or incomplete, then the products, services and information we provide may be affected. <br>
								If you think there is something wrong with the information we hold about you please contact our privacy officer to let us know and keep us up-to-date with any changes, and we will try to correct your personal information.  <br>
								We will respond to your request to correct your personal information free of charge and in a reasonable period of time.  If we refuse your request, we will provide you with a written notice setting out the reasons for the refusal and mechanisms available to complain about the refusal.<br>
								If we refuse to correct your personal information, you may request that we associate your information with a statement that the information is inaccurate, out of date, incomplete, irrelevant or misleading.  We will take reasonable steps to make the statement visible to users of your personal information.<br>
								If we do correct your personal information and we has previously disclosed your personal information to a third party, upon your request, we will notify that third party of the correction unless it is impracticable or unlawful to do so.</p>
								<h4>Resolving your privacy issues</h4>
								<p>If you have any issues you wish to discuss with us or if you’re concerned about how we have collected or managed your personal information please contact the privacy officer.<br>
								For information about privacy or if your concerns are not resolved to your satisfaction, you may contact the Office of the Australian Information Commissioner at www.oaic.gov.au and on 1300 363 992.</p>
								<h4>Changes to this privacy policy</h4>
								<p>We may, from time to time, review and update this privacy policy to take account of new laws and changes to our operations.  Changes to this Privacy Policy will not affect our use of previously provided information.</p>
								<h4>How to access this privacy policy</h4>
								<p>A copy of this Privacy Policy is available on our website. A printed copy of the privacy policy can be obtained free of charge by contacting our Privacy Officer.</p>
								<h4>Our privacy officer</h4>
								<p>Our privacy officer can be contacted at:<br>
								Email: dcprpich@matthews.com.au<br>
								Telephone: (03) 9763-0533<br>
								Fax: (03) 9763-2020<br>
								Post: Private Bag 1, Rowville VIC 3178.</p>
		    			    </div>
	    			    </div>
    			    </div>
	    		</div>   			
	    	</div> 
		</div>
	</section>
	
	<!--Footer-->
	<?php include('html/footer.php'); ?>
	<!--/Footer-->

	<!--/js-files-->
	<?php include('html/js-files.php'); ?>
	<!--/js-files-->

</body>
</html>