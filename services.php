<!DOCTYPE html>
<html lang="en">
<head>
	<title>	iQSafety - New Pig spill absorbents & containment solutions </title>

	<meta name="description" content="iQSafety is a division of Matthews Australasia. iQSafety has a range of New Pig spill absorbent & containment solutions that helps to clean any spill. New Pig's range of mats, booms, socks and pillows are available loose or in the form of spill kits." />
    <?php include('html/head-tag.php'); ?>
</head>
<!--/head-->

<body>
	<!--header-->
	<?php include('html/header.php'); ?>
	<!--/header-->
	
	<section class="mrg-top30">
		<div class="container">
			<div class="row"> 
				<div class="col-sm-3">
					<?php //include('html/side-bar.php'); ?>
					<?php include('html/about-side-bar.php'); ?>
				</div>

	    		<div class="col-sm-9 padding-right mrg-bot30">
    					<h2 class="main-title"><span class="white-bg">Spill Kit Maintenance Program</span></h2>
	    			<div class="row">
	    				<div class="col-md-12">
		    				<div class="about">
				    			<p>Often some spill kit materials like absorbent mats are used out of a spill kit for small clean ups and not replaced. The Spill kit maintenance 
                                  service program ensures your spill kits are always topped up and ready to be deployed when a spill occurs.</p>
             
				    			  <h4>What are the benefits of the spill kit maintenance program?</h4>
				    			  <ul>
				    			  	<li>You are fulfilling your environmental duty of care</li>
				    			  	<li>Kits are re-stocked on the spot</li>
				    			  	<li>You pay no freight on re-stock items</li>
				    			  	<li>Spill Kits are maintained at the highest level</li>
				    			  	<li>Written report becomes part of your EMS and OH&amp;S compliance system</li>
				    			  </ul>
                              
                              	  <h4>Service Program includes:</h4>
				    			  <ul>
				    			  	<li>Programmed spill kit inspection on site</li>
				    			  	<li>On the spot re stock (you pay for the components only)</li>
				    			  	<li>You receive a written inspection report on your kits and the work carried out</li>
				    			  	<li>Regular cleaning of spill kits</li>
				    			  </ul>
                              
                              	<p>
                                To find out more about this service, the cost and to enrol, please fill in the form below and our customer service team will be in touch shortly.  
                              	</p>
                              
                              	<br>
                              	<script type="text/javascript">var loc = "https://analytics.clickdimensions.com/matthewscomau-aegcz/pages/";</script>
								<script type="text/javascript" src="https://az124611.vo.msecnd.net/web/v10/CDWidget.js"></script>
								<div pageID="zvevqzs6eeebdqamkyxjoa"></div>
		    			    </div>
	    			    </div>
    			    </div>
	    		</div>   			
	    	</div> 
		</div>
	</section>
	
	<!--Footer-->
	<?php include('html/footer.php'); ?>
	<!--/Footer-->

	<!--/js-files-->
	<?php include('html/js-files.php'); ?>
	<script type="text/javascript"> 
	   	$(function () { 
	      	$('.about-side-bar h4 a').removeClass('active'); 
	      	$('.about-side-bar h4 a#about-company').addClass('active'); 
	   	}); 
	</script> 
	<!--/js-files-->

</body>
</html>