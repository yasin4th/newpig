<!DOCTYPE html>
<html lang="en">
<head>
    <?php include('html/head-tag.php'); ?>
</head>
<!--/head-->

<body>
	<!--header-->
	<?php include('html/header.php'); ?>
	<!--/header-->
	
	<section class="mrg-top30">
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<?php include('html/side-bar.php'); ?>
				</div>
				
				<div class="col-sm-9 padding-right mrg-bot30">
					<h2 class="main-title"><span class="white-bg">3 Questions to Ask When Selecting a Spill Kit</span></h2>
					<div class="row">
						<div class="col-sm-12">
							<div class="blog-question-div-question">
								<p>Being ready and able for quick spill response is critical in keeping your employees safe, protecting the environment,
								 preventing downtime, and complying with spill control regulations.<br> With all the different spill kit options out there,
								  the choices can be overwhelming.  Spill kit contents can be widely different. There are many different types, sizes
								   and container options.  However, asking a few basic questions can lead you to the correct spill kit choice.
								    Here are three questions to ask yourself before you begin to choose a spill response kit:</p>
								<h4>1.  What type of liquid has the potential to spill at your facility?</h4>
								<p>Often these liquids can be characterized as non-corrosive or corrosive.  The lists below show common fluids and how they are categorized.</p>
								<p>Non-Corrosive examples: Motor oils, solvents, water, coolants, gasoline</p>
								<p>Corrosive examples: Sodium hydroxide, sulfuric acid, hydrochloric acids</p>
								<p>Once you know the type of liquid, you can determine the type of spill kit you need.</p>
								<p>New Pig Spill Kits are broken into three categories: universal, oil and chemical spill kits. 
								 Universal and Oil Spill Kits are used for non-corrosive liquid spills.  Universal Kits are used for 
								 land-based spills, while Oil Kits can be used for land or water-born spills to only absorb oil, but not water.</p>
								 <p>Chemical Spill Kits are used for responding to hazardous spills of corrosive or unknown liquids.  Certain chemical 
								 	kits are also designed to neutralize the corrosive liquid or help eliminate hazardous vapors associated with the chemical.</p>
							 	<h4>2.  What spill volume are you preparing for?</h4>
							 	<p>This volume is typically based on the largest size of container or your worst case scenario at your facility.  If the largest 
							 		container on-site is a 209 liter drum, then choose a kit that will contain and absorb at least that volume.</p>
						 		<p>New Pig offers a wide variety of kits, for small volume spills such as a few liters, to our largest ones that
						 		 respond to spills in excess of 380 liters or more.</p>
						 		<h4>3.  Where will the Spill Kit be stored or used?</h4>
						 		<p>You should take into account whether the spill clean-up kit will be used indoors or outdoors, whether or not you need to
						 		 the kit to be portable or stationary, or if the kit container needs to be used for the disposal of the spent absorbents. 
						 		  UN-rated Overpack/Salvage Drum containers are ideal for shipping waste off-site once it is cleaned up. </p>
					 		  	<p>Other kits are mobile to help facilitate quicker response, and some are packed in portable containers or bags which 
						 		  	are ideal for first responders, for use in vehicles or when a grab and go kit is needed.  Kits in high visibility 
						 		  	containers are often preferred to quickly help locate the kit when a spill occurs.</p>
					 		  	<p>New Pig provides the largest spill kit container options in the leak & spill market to help meet your needs.</p>
					 		  	<p class="mar-bot10">There are other items that are commonly used in spill response, but often not included in spill kits:</p>
					 		  	<ul>
					 		  		<li>Do I need personal protective equipment (PPE), patch and repair products, or brooms and shovels?</li>
					 		  		<li>Will I need mounting brackets or a protective cover?</li>
					 		  	</ul>
					 		  	<p><i>Answering the questions above should help you select the kit that best meets your needs.  Contact us 
					 		  		if you need assistance choosing the right spill kit for your requirements.</i></p>
				 		  		<p>A version of this post was created and originally featured on the New Pig Blog</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<!--Footer-->
	<?php include('html/footer.php'); ?>
	<!--/Footer-->

	<!--/js-files-->
	<?php include('html/js-files.php'); ?>
	<!--/js-files-->

</body>
</html>