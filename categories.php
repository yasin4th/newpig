<!DOCTYPE html>
<html lang="en">
<head>
    <title>Spill Kits, Spill containment, Spill control & clean-up Products</title>

	<meta name="description" content="Fins all types of Spill Kits, spill containment, spill control and spill clean-up solutions at iQSafety. 
Wide range of absorbents, spill kits, spill containment products such as bunding, spill containment pallets and safety cabinets for storing flammable liquids. " />
  <?php include('html/head-tag.php'); ?>
    

</head><!--/head-->

<body>
	<!--header-->
	<?php include('html/header.php'); ?>
	<!--/header-->
	
	<section class="mrg-top30">
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<?php include('html/side-bar.php'); ?>
				</div>
				
				<div id="all-categories" class="col-sm-9 padding-right">
					<div class="features_items"><!--features_items-->
						<h1 class="title"><span class="white-bg"> Categories </span></h1>
                        <?php
                            $str = '';
                            if(count($cats['categories']) == 0 ) {
                                $str .= '<span> 0 result found.</span>';
                            }

                            foreach ($cats['categories'] as $cat) {
                                if($cat['parent_id'] == 0) {
                                    $str .= '<div class="col-sm-4"> <div class="product-image-wrapper"> <div class="single-products"> <div class="productinfo text-center">';
                                    $str .= "<a href=\"products/{$cat['alias']}\">";
                                    $str .= "<img src=\"{$cat['image']}\" alt=\"{$cat['name']}\" title=\"{$cat['name']}\" />";
                                    $str .= "<p>{$cat['name']}</p>";
                                    //$str .= '<a href="$cat'[''+$cat']}'alias']}" class="btn btn-primary add-to-cart"><i class="fa fa-eye"></i>View </a></a>';
                                    $str .= '</a></div></div></div></div>';
                                }
                            }
                            echo $str;
                        ?>



						<!-- <div class="col-sm-4">
							<div class="product-image-wrapper">
								<div class="single-products">
									<div class="productinfo text-center">
										<img src="images/product/absorbent/pig-universal-absorbent-mats.jpg" alt="Absorbents" title="" />
										<p>PIG Universal Absorbent Mats</p>
										<a href="products-details.php" class="btn btn-primary add-to-cart"><i class="fa fa-eye"></i>View </a>
									</div>
								</div>
							</div>
						</div> -->
					</div><!--features_items-->
				</div>
			</div>
          	<div class="row">
				<div class="col-sm-12"><div class="details">
At iQSafety, we have a large range of products as well as years of experience to provide you with the right spill containment, spill control and spill clean-up solutions. <br>
Explore our product categories to see detailed information on our range of absorbents, spill kits, spill containment products such as bunding, spill containment pallets and safety cabinets for storing flammable liquids.
Do get in touch even if you don’t see the exact product you need for your application. iQSafety has access to the range of innovative spill management products from NewPig USA.<br>

  
If you have any questions, such as how to choose the right spill kit, check out our <a href="http://www.newpig.com.au/faqs">FAQ’s</a> and <a href="http://www.newpig.com.au/blog">blog</a>, or contact us on 1800 HOT HOG (1800 468 464).
</div>				
				</div>
			</div> 
		</div>
	</section>
	
	<!--Footer-->
	<?php include('html/footer.php'); ?>
	<!--/Footer-->

	<!--/js-files-->
	<?php include('html/js-files.php'); ?>
	<!-- <script type="text/javascript" src="js/custom/categories.js"></script> -->
	<!--/js-files-->

</body>
</html>