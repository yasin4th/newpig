<!DOCTYPE html>
<html lang="en">
<head>
<title> Spill Kits | Buy Spill Control & Containment Kits in Australia </title>
<meta name="keywords" content="spill kits, sorbents, spill containment, oil spill cleanup, spill response, absorbent mats, industrial absorbents, absorbent pads, absorbent wipers, drum funnels, pig, spill kit, absorbents, newpig, mat, sponge, spong, new pig corporation, containment, loose sorbent, absorbent socks, material handling, sorbent socks, sorbent mats, oil spills, oil absorbents, hazmat spill, clean, clean-up, putty, absorbants, absorbent booms, hazmat, hazardous materials, absorbent rags, pallets, containment pallets, sorbent pads, sorbent booms, personal safety protection, industrial sorbents, secondary spill containment, spill containment pallets, chemical storage building, safety storage cabinets, absorbent oil boom, safety gas cans, spill berms, oil rag, shop rag, shop towel, roll, acid spill, acid, clean up, personal protection, ppe gear, industrial clean-up, blood spill, sponge, oil leak, oil leak, oil leek, fuel spill, eat oil, eats oil, hydrocarbon, poylpropalyne, polypropylene, better than kitty l" />
<meta name="description" content="Buy Spill kits from the global leaders of Spill control. New Pig is home to PIG® Mat, the world's best spill kit manufacturer. Click here to request a quote" />
<?php include('html/head-tag.php'); ?>
</head>
<body>
	<!--header-->
	<?php include('html/header.php'); ?>
  	<?php
        $blog = getData(array('action' => 'fetchArticles', 'active' => 1));
        $featured = getData(array('action' => 'fetchAllProducts', 'active' => 1, 'featured' => '1'));
    ?>
	<!--/header-->
	<section id="slider" class="slider-overlay padd-top30 padd-bot20"><!--slider-->
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div id="slider-carousel" class="carousel slide" data-ride="carousel">
						<ol class="carousel-indicators">
							<li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
							<li data-target="#slider-carousel" data-slide-to="1"></li>
							<li data-target="#slider-carousel" data-slide-to="2"></li>
						</ol>
						<div class="carousel-inner">
							<div class="item active">
								<div class="col-sm-7">
									<h1><span>Spill</span> Kits</h1>
									<!-- <h2>Free E-Commerce Template</h2> -->
									<h2>Comply with Spill Plan regs, avoid fines and be ready to respond with our Spill Kits.</h2>
									<p>Comply with Spill Plan regs, avoid fines and be ready to respond with our Spill Kits.</p>
									<!-- <button type="button" class="btn btn-default get">Get it now</button> -->
								</div>
								<div class="col-sm-5">
									<img src="images/home/spill-kit-slider.png" class="girl img-responsive" alt="" />
									<!-- <img src="images/home/pricing.png"  class="pricing" alt="" /> -->
								</div>
							</div>
							<div class="item">
								<div class="col-sm-7">
									<h1><span>Patch</span>, Repair & Maintenance</h1>
									<!-- <h2>100% Responsive Design</h2> -->
									<h2>Keep your facility in good repair from floor to ceiling with top-quality solutions.</h2>
									<p>Keep your facility in good repair from floor to ceiling with top-quality solutions.</p>
									<!-- <button type="button" class="btn btn-default get">Get it now</button> -->
								</div>
								<div class="col-sm-5">
									<img src="images/home/leak-diverter-slider.png" class="girl img-responsive" alt="" />
									<!-- <img src="images/home/pricing.png"  class="pricing" alt="" /> -->
								</div>
							</div>
							<div class="item">
								<div class="col-sm-7">
									<h1><span>Storage </span>& Material Handling</h1>
									<!-- <h2>Free Ecommerce Template</h2> -->
									<h2>Exclusive solutions make storing and handling your liquids faster and easier.</h2>
									<p>Exclusive solutions make storing and handling your liquids faster and easier.</p>
									<!-- <button type="button" class="btn btn-default get">Get it now</button> -->
								</div>
								<div class="col-sm-5">
									<img src="images/home/Justrite-Flammable-Safety-Can-slider.png" class="girl img-responsive" alt="" />
									<!-- <img src="images/home/pricing.png" class="pricing" alt="" /> -->
								</div>
							</div>
						</div>
						<a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev"><i class="fa fa-angle-left"></i></a>
						<a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next"><i class="fa fa-angle-right"></i></a>
					</div>
				</div>
			</div>
		</div>
	</section><!--/slider-->
	<div class="left-sidebar-index">
		<div class="panel-group category-products" id="accordian" side="cat"><!--category-productsr-->
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title"><a href="products">All Product Categories</a></h4>

				</div>
			</div>
		</div><!--/category-productsr-->
	</div>
	<section>
		<div class="container">
			<div class="row mrg-bot30">
				<div class="col-sm-9 padding-right">
					<div class="features_items"><!--features_items-->
						<h2 class="title"><span class="white-bg">Product Categories</span></h2>
                        <?php

                            foreach ($cats['categories'] as $cat) {
                                if($cat['parent_id']==0 && $cat['status']==1)
                                {                                    
                                    echo "<div class='col-sm-3'>";
                                    echo "<a href=\"products/{$cat['alias']}\">";
                                    echo "<div class=\"product-image-wrapper\"><div class=\"single-products\"><div class=\"productinfo text-center\">";
                                    echo "<img src=\"{$cat['image']}\" alt=\"{$cat['name']}\" title=\"Click to see details\" />";
                                    echo "<p>{$cat['name']}</p>";
                                    echo "</div></div></div></a></div>";
                                }
                            }
                        ?>
                        <div class="col-sm-3"><a href="best_sellers"><div class="product-image-wrapper"><div class="single-products"><div class="productinfo text-center"><img src="images/categories/Best Sellers_103.png" alt="Best Sellers" title="Click to see details"><p>Best Sellers</p></div></div></div></a></div>
                               
					</div>
					<div class="recommended_items"><!--recommended_items-->
						<h2 class="title"><span class="white-bg">Featured Items</span></h2>
						<div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
							<div class="carousel-inner featured-product">
                                


                                <?php foreach($featured['products'] as $key=>$prod) {
                                    if($key == 0){ echo '<div class="item active">'; }else
                                    if($key%3 == 0){ echo '</div><div class="item">';}
                                ?>
                                
                                <?php echo "<div class=\"col-sm-4\">
                                                <div class=\"product-image-wrapper\">
                                                    <a href=\"products/{$prod['alias']}\"><div class=\"single-products\"><div class=\"productinfo text-center\"><img src=\"{$prod['image']}\" alt=\"PIG 4 in 1 Absorbent Mat Roll in Dispenser Box\"><p>{$prod['name']} ({$prod['part_name']})</p></div></div></a>
                                                    <div class=\"points-div\">
                                                        <ul class=\"points\">";
                                                        if(trim($prod['point1']) != ''){ echo "<li>".$prod['point1']."</li>";}
                                                        if(trim($prod['point2']) != ''){ echo "<li>".$prod['point2']."</li>";}
                                                        if(trim($prod['point3']) != ''){ echo "<li>".$prod['point3']."</li>";}
                                                        echo "</ul>
                                                        <a class=\"btn btn-primary btn-block cart\" data-id=\"{$prod['id']}\"><i class=\"fa fa-shopping-cart\"></i> Add to Quote</a>
                                                    </div>
                                                </div>
                                            </div>";
                                ?>

                                <?php }echo '</div>'; ?>


							</div><!-- .featured-product-->
							<a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
								<i class="fa fa-angle-left"></i>
							</a>
							<a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
								<i class="fa fa-angle-right"></i>
							</a>
                        </div>
					</div><!--/recommended_items-->
				</div>
				<div class="col-sm-3">
					<!-- <div class="left-sidebar">

						<!--start find solution to your problem-->
						<div class="solution-problem">
							<h2>Find solution to your problem!</h2>
							<div class="problem">
                                <?php 
                                    for ($i=0; $i < 3; $i++) { 
                                        echo "<p><i class='fa fa-certificate'></i><a href='blog/{$blog['article'][$i]['alias']}'>{$blog['article'][$i]['name']}</a></p>";
                                    }
                                ?>
                                <p><a href="blog" class="pull-right">View all solution</a></p>								
							</div>
						</div>
						<!--end find solution to your problem-->
						<!--start same day shipping-->
						<div class="same-day-shipping">
							<h2>Same Day <span>Shipping!</span></h2>
							<img src="images/home/same-day-shipping.png" class="img-responsive" alt="" />
							<p>Most in-stock orders ship the same day if ordered before 1pm EST</p>
						</div>
						<!--end same day shipping-->
									<!-- </div> -->

					<div class="partner">
						<div class="partner-img">
							<img class="img-responsive" src="images/home/iq_Matthews_Footer_Logo.png" alt="" />
						</div>
						<div class="partner-details">
							<p>Matthews Safety Products is now iQSafety, an authorised distributor of  New Pig Products</p>
						</div>
					</div>
					<div class="partner">
						<div class="partner-img">
							<img class="img-responsive" src="images/home/iq_Defence_Footer_Logo.png" alt="" />
						</div>
						<div class="partner-details">
							<p>We are a defence  recognised supplier to the Department of Defence.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
              <h1> Spill Kits </h1>
				<div class="col-sm-12"><div class="details">	Worlds best selling spill kit : At iQSafety we have a large range of high quality spill containment, spill control and spill clean-up products from New Pig USA. One of our top sellers is the universal spill kit. A spill kit has absorbent mats, socks and pillows to quickly contain spills and clean them up. Keep your spill kit in an accessible location to use it easily and do not forgot to restock the used items. iQSafety has a service to refill spill kits.<br>
Some of the other popular products are drain covers to stop spills from going into drains, spill containment decks to store liquid filled drums, universal absorbent mats to absorb most common industrial liquids and our fully compliant Justrite safety cabinets for storing flammable liquids.<br>
If you have any questions, such as how to choose the right spill kit, check out our <a href="http://www.newpig.com.au/faqs">FAQ’s</a> and <a href="http://www.newpig.com.au/blog">blog</a>, or contact us on 1800 HOT HOG (1800 468 464).
</div>
				</div>
			</div>
		</div>
	</section>
	<!--Footer-->
	<?php include('html/footer.php'); ?>
	<!--/Footer-->
	<!--/js-files-->
	<?php include('html/js-files.php'); ?>
	<script src="js/custom/index.js"></script>
	<!--/js-files-->
</body>
</html>