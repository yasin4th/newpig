<!DOCTYPE html>
<html lang="en">
<head>
	<title>	Catalogue request - iQSafety </title>
	<meta name="description" content="Catalogue request" />

    <?php include('html/head-tag.php'); ?>
</head>
<!--/head-->

<body>
	<!--header-->
	<?php include('html/header.php'); ?>
	<!--/header-->
	
	<section class="mrg-top30">
		<div class="container">
			<div class="row">  	
	    		<div class="col-sm-3">
	    			<?php include('html/about-side-bar.php'); ?>
    			</div>
	    		<div class="col-sm-8">
	    			<div class="contact-form">
    					<h2 class="main-title"><span class="white-bg">Catalogue request</span></h2>
	    				<!-- <h2 class="title text-center"><span class="white-bg">Catalogue request</span></h2> -->
	    				<div class="status alert alert-success" style="display: none"></div>
				    	<form id="main-contact-form" class="contact-form row" name="contact-form" method="post">
				            <div class="form-group col-md-6">
				            	<label class="form-label">Name <span class="required">*</span></label>
			                	<input type="text" name="name" class="form-control" required="required" placeholder="Name">
				            </div>
				            <div class="form-group col-md-6">
				            	<label class="form-label">Email <span class="required">*</span></label>
				                <input type="email" name="email" class="form-control" required="required" placeholder="Email">
				            </div>
				            <div class="form-group col-md-6">
				            	<label class="form-label">Business Phone <span class="required">*</span></label>
				                <input type="text" name="businessphone" class="form-control" required="required" placeholder="Business Phone">
				            </div>
				            <div class="form-group col-md-6">
				            	<label class="form-label">Contact Number <span class="required">*</span></label>
				                <input type="text" name="mobile-number" class="form-control" required="required" placeholder="Mobile Phone Number">
				            </div>
				            <div class="form-group col-md-6">
				            	<label class="form-label">Your Company <span class="required">*</span></label>
				                <input type="text" name="yourcompany" class="form-control" required="required" placeholder="Your Company Name">
				            </div>
				            <div class="form-group col-md-6">
				            	<label class="form-label">Job Title </label>
				                <input type="text" name="jobtitle" class="form-control" required="required" placeholder="Job Title">
				            </div>
				            <div class="form-group col-md-6">
				            	<label class="form-label">Address Line 1 </label>
				                <input type="text" name="addressline1" class="form-control" required="required" placeholder="Address Line 1">
				            </div>
				            <div class="form-group col-md-6">
				            	<label class="form-label">Address Line 2 </label>
				                <input type="text" name="addressline2" class="form-control" required="required" placeholder="Address Line 2">
				            </div>
				            <div class="form-group col-md-6">
				            	<label class="form-label">Suburb </label>
				                <input type="text" name="suburb" class="form-control" required="required" placeholder="Suburb">
				            </div>
				            <div class="form-group col-md-6">
				            	<label class="form-label">State <span class="required">*</span></label>
				                <input type="text" name="state" class="form-control" required="required" placeholder="State">
				            </div>
				            <div class="form-group col-md-6">
				            	<label class="form-label">Postcode <span class="required">*</span></label>
				                <input type="text" name="postcode" class="form-control" required="required" placeholder="Suburb">
				            </div>
				            <div class="form-group col-md-6">
				            	<label class="form-label">Country <span class="required">*</span></label>
				                <input type="text" name="country" class="form-control" required="required" placeholder="State">
				            </div>                      
				            <div class="form-group col-md-12">
				            	<a type="submit" class="btn btn-primary pull-right">Submit</a>
				            </div>
				        </form>
	    			</div>
	    		</div>   
	    		<div class="col-sm-1"></div> 			
	    	</div> 
		</div>
	</section>
	
	<!--Footer-->
	<?php include('html/footer.php'); ?>
	<!--/Footer-->

	<!--/js-files-->
	<?php include('html/js-files.php'); ?>
	<script type="text/javascript"> 
	   	$(function () { 
	      	$('.about-side-bar h4 a').removeClass('active'); 
	      	$('.about-side-bar h4 a#catalogue-request').addClass('active'); 
	   	}); 
	</script> 
	<!--/js-files-->

</body>
</html>