<?php
  @session_start();

  if(isset($_SESSION['role'])){
    $role = $_SESSION['role'];
  }else {
    header('Location: index.php');
  }
  if($role!='1'){
      header('Location: redirect.php');
  }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <?php include('html/head-tag.php'); ?>
    <style>
    ul#sortable {
      list-style-type: none;
    }
    ul#sortable > li {
      width : 500px;
      margin: 5px;
      padding : 5px 10px;
      border:1px solid #797979;
      cursor : pointer;

    }
    </style>
  </head>
  <body>

  <section id="container" class="">
      <!--header start-->
      <?php include('html/header.php'); ?>
      <!--header end-->
      <!--sidebar start-->
      <?php include('html/sidemenu.php'); ?>
      <!--sidebar end-->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
              <!-- page start-->
              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                            Manage Category Reordering
                            
                          </header>
                          <div class="panel-body">
                            <div class="row">
                              <div class="col-sm-12">
                                <ol id="sortable">
                                  
                                </ol>
                                <button id="save-order" class="btn btn-success pull-right">Save Order</button>
                              </div>
                            </div>
                            <section>
                              
                            </section>
                          </div>
                        </div>
                      </section>
                  </div>
              </div>
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->

     

      <!--footer start-->
      <?php include('html/footer.php'); ?>
      <!--footer end-->
  </section>

    
  <?php include('html/js-files.php'); ?>
  <script type="text/javascript" src="js/jquery-ui.min.js"></script>
  <script type="text/javascript" src="js/custom/manage-category-reordering.js"></script>

  </body>
</html>