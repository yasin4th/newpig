<?php
  @session_start();

?>
<header class="header white-bg">
          <div class="sidebar-toggle-box">
              <div data-original-title="Toggle Navigation" data-placement="right" class="fa fa-bars tooltips"></div>
          </div>
          <!--logo start-->
          <a href="#" class="logo"><img src="img/iq_logo.png" alt=""></a>
          <!--logo end-->
          <div class="top-nav ">
              <ul class="nav pull-right top-menu">
                  <!-- user login dropdown start-->
                  <li class="dropdown">
                      <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                          Welcome <span class="username">
                          <?php
                            if(isset($_SESSION['role']) && isset($_SESSION['name'])){
                              $role = $_SESSION['role'];
                              if($role=='1'){
                                echo 'admin';
                              }else{
                                echo $_SESSION['name'];
                              }
                            }
                            ?>
                          </span>
                          <b class="caret"></b>
                      </a>
                      <ul class="dropdown-menu extended logout">
                          <div class="log-arrow-up"></div>
                          <li><a href="logout.php"><i class="fa fa-power-off"></i> Log Out</a></li>
                      </ul>
                  </li>
                  <!-- user login dropdown end -->
              </ul>
          </div>
      </header>