<!-- js placed at the end of the document so the pages load faster -->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="js/jquery.scrollTo.min.js"></script>
    <script src="js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="js/respond.min.js" ></script>
    <script src="js/toastr.js" ></script>
    <script src="js/jcrop/js/jquery.Jcrop.min.js"></script>
    <script src="js/jcrop/js/jquery.color.js"></script>
    <script type="text/javascript" src="assets/data-tables/js/jquery.dataTables.min.js"></script>

    <!--common script for all pages-->
    <script src="js/common-scripts.js"></script>
    <script src="js/common.js"></script>

    