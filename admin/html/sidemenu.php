<?php
  @session_start();
  if(isset($_SESSION['role'])){
    $role = $_SESSION['role'];
  }else {
    header('Location: index.php');
  }

?>
<aside>
  <div id="sidebar"  class="nav-collapse ">
    <!-- sidebar menu start-->
    <ul class="sidebar-menu" id="nav-accordion">
      <!-- <li>
        <a href="welcome-page.php">
          <i class="fa fa-home"></i>
          <span>Dashboard</span>
        </a>
      </li> -->
      <?php
        if($role!='3') {
      ?>
      <li>
        <a href="manage-product.php">
          <i class="fa fa-briefcase"></i>
          <span>Manage Products </span>
        </a>
      </li>
      <?php } ?>


      <?php if($role=='1') {?>
      <li class="sub-menu">
        <a href="javascript:;">
          <i class="fa fa-cog"></i>
          <span>Master Entry</span>
        </a>
        <ul class="sub">
          <li><a  href="manage-category.php">Manage Category</a></li>
          <li><a  href="manage-category-reordering.php">Category Reordering</a></li>
          <li><a  href="manage-sub-category.php">Manage Sub-Category</a></li>
          <li><a  href="manage-class.php">Manage Class</a></li>
          <li><a  href="manage-subcat-reordering.php">SubCategory Reordering</a></li>
          <li><a  href="manage-specification.php">Manage Specification</a></li>
        </ul>
      </li>

      <li class="sub-menu">
        <a href="javascript:;">
          <i class="fa fa-book"></i>
          <span>Resource Library</span>
        </a>
        <ul class="sub">
          <li><a  href="solution-to-your-problem.php">Blog</a></li>
          <li><a  href="manage-videos.php">Videos</a></li>
        </ul>
      </li>
      <li class="sub-menu">
        <a href="manage-role.php">
          <i class="fa fa-cogs"></i>
          <span>Manage Users</span>
        </a>
        <!-- <ul class="sub">
          <li><a  href="#">Data Entry Operator</a></li>
          <li><a  href="#">Order Manager</a></li>
        </ul> -->
      </li>
      <?php } ?>

      <?php if($role!='2') {?>
      <li>
        <a href="manage-order.php">
          <i class="fa fa-shopping-cart"></i>
          <span>Manage Order </span>
        </a>
      </li>
      <?php } ?>
      <li>
        <a href="change-password.php">
          <i class="fa fa-key"></i>
          <span>Change Password </span>
        </a>
      </li>
    </ul>
    <!-- sidebar menu end-->
  </div>
</aside>
