    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keyword" content="">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>Newpig | <?php
                            if(isset($_SESSION['role']) && isset($_SESSION['name'])){
                              $role = $_SESSION['role'];
                              if($role=='1'){
                                echo 'Admin';
                              }else if($role=='2'){
                                echo 'Data Entry Operator';
                              }else if($role=='3'){
                                echo 'Order Manager';
                              }
                            }
                            ?></title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="css/table-responsive.css" rel="stylesheet" />
    <link href="css/toastr.css" rel="stylesheet" />
    <link href="js/jcrop/css/jquery.Jcrop.min.css" rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet" />
    <link href="css/custom.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="assets/data-tables/css/jquery.dataTables.css"/ media="all">
    <link rel="icon" href="../images/favicon.ico" type="image/x-icon">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->