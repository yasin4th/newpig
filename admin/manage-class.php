<?php
  @session_start();

  if(isset($_SESSION['role'])){
    $role = $_SESSION['role'];
  }else {
    header('Location: index.php');
  }
  if($role!='1'){
      header('Location: redirect.php');
  }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <?php include('html/head-tag.php'); ?>
  </head>

  <body>

  <section id="container" class="">
      <!--header start-->
      <?php include('html/header.php'); ?>
      <!--header end-->
      <!--sidebar start-->
      <?php include('html/sidemenu.php'); ?>
      <!--sidebar end-->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
              <!-- page start-->
              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              Manage Class
                              <a data-toggle="modal" href="#add-class" class="btn btn-success pull-right btn-xs"><i class="fa fa-plus"></i> Class</a>
                          </header>
                          <div class="panel-body">
                            <div class="row">
                              <form role="form" id="filter" name="filter">
                              <div class="col-sm-3">
                                <div class="form-group">
                                  <label>Category Name</label>
                                  <select class="form-control" name="catname">
                                    <option value="0"> Select Category </option>
                                    
                                  </select>
                                </div>
                              </div>
                              <div class="col-sm-3">
                                <div class="form-group">
                                  <label>Sub-Category Name</label>
                                  <select class="form-control" name="subcatname">
                                    <option value="0"> Select Sub Category </option>
                                    
                                  </select>
                                </div>
                              </div>
                              <div class="col-sm-3">
                                <div class="form-group">
                                  <label>Class Name</label>
                                  <input name="classname" type="text" class="form-control" placeholder="Class Name">
                                </div>
                              </div>
                              <div class="col-sm-3">
                                <div class="form-group">
                                  <label>Status</label>
                                  <select class="form-control" name="status">
                                    <option value="0">Select Status</option>
                                    <option value="1">Active</option>
                                    <option value="2">Not Active</option>                                    
                                  </select>
                                </div>
                              </div>
                              <div class="col-sm-12">
                                <div class="form-group pull-right">
                                  <button class="btn btn-success" type="submit"><i class="fa fa-filter"></i> Filter</button>
                                  <a class="btn btn-primary" href="">Clear</a>
                                </div>
                              </div>
                              <div class="col-sm-3">
                                    <input name="action" id="action" type="hidden" value="filterClass">
                              </div>
                              </form>
                            </div>
                              <section>
                                <div class="table-responsive">
                                  <table id="class-table" class="table table-bordered table-striped table-condensed">
                                    <thead>
                                    <tr>
                                        <th>Category Name</th>
                                        <th>Sub-Category Name</th>
                                        <th>Class Name</th>
                                        <th>Class Description</th>
                                        <th>Class Image</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <!--<tr>
                                        <td>PIG Universal Absorbent Mat Pads</td>
                                        <td class="product-img"><img src="../images/product/hero-absorbents.png" class="img-responsive" alt=""></td>
                                        <td>Active</td>
                                        <td>
                                          <a data-toggle="modal" href="#view-edit-class" class="btn btn-primary btn-xs" title="View / Edit"><i class="fa fa-pencil"></i></a>
                                          <a class="btn btn-danger btn-xs" title="Delete"><i class="fa fa-trash-o "></i></a>
                                        </td>
                                    </tr>-->
                                    </tbody>
                                </table>
                              </div>
                              <div class="row" id="error-msg"></div>
                              </section>
                          </div>
                        </div>
                      </section>
                  </div>
              </div>
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->

      <!-- Start Class Modal -->
      <div class="modal fade" id="add-class" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
        <form id="add-class-form" method="post" >
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Add class</h4>
            </div>
            <div class="modal-body">

              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                      <label>Category</label>
                      <select id="categories" name="categories" class="categories form-control">
                        <option value="0"> Select Category </option>
                      </select>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                      <label>Sub-Category</label>
                      <select id="sub-categories" name="sub-categories" class="sub-categories form-control">
                        <option value="0"> Select Sub-Category </option>
                      </select>
                  </div>
                </div>
                <div class="col-sm-6"></div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                      <label>Class Name</label>
                      <input type="text" id="class-name" name="class-name" class="form-control" required="required" placeholder="Ex. PIG Universal Absorbent Mat Pads">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                      <label>Status</label>
                      <select id="class-status" name="class-status" class="form-control">
                        <option value="0">Select Status</option>
                        <option value="1">Active</option>
                        <option value="2">Deactive</option>
                      </select>
                  </div>
                </div>
                <div class="col-sm-12">
                  <div class="form-group">
                      <label>Class Description <span class="small-text">( Text Limit : 150 Letters )</span></label>
                      <textarea id="class-description" name="class-description" class="form-control" rows="2" placeholder="Description..."></textarea>
                  </div>
                </div>
                <div class="col-sm-12">
                  <div class="form-group">
                      <label>Upload Class Image <span class="small-text">( Image Resolution : 500px X 800px )</span></label>
                    <div class="row">
                      <div class="col-md-3">
                        <div class="add-product-image">
                          <a data-toggle="modal" href="#jcrop-image-modal">Preview Image</a>
                        </div>
                      </div>
                      <div class="col-md-9">
                        <input name="class-image" id="class-image" type="file" required="required" class="btn form-control">
                        <input type="hidden" id="x" name="x" value="0">
                        <input type="hidden" id="y" name="y" value="0">
                        <input type="hidden" id="w" name="w" value="0">
                        <input type="hidden" id="h" name="h" value="0">
                        <input name="action" id="action" type="hidden" value="addClass">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <h4>Meta Data</h4>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Meta Title</label>
                      <input id="meta-title-add" name="meta-title-add" type="text" class="form-control" placeholder="Meta Title">
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Meta Keywords</label>
                      <input id="meta-keywords-add" name="meta-keywords-add" type="text" class="form-control" placeholder="Meta Keywords">
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group">
                      <label>Meta Description</label>
                      <textarea id="meta-description-add" name="meta-description-add"  class="form-control" rows="5" cols="150" placeholder="Meta Description"></textarea>
                    </div>
                  </div>
                  <div class="col-sm-12">
                      <div class="form-group">
                        <label>Class Details</label>
                        <textarea id="details-add" name="details-add"  class="form-control" rows="5" cols="150" placeholder="Class Details"></textarea>
                      </div>
                    </div>
                </div>
              </div>

            </div>
            <div class="modal-footer">
              <button class="btn btn-success" type="submit"><i class="fa fa-plus"></i> Add</button>
              <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            </div>
          </div>
        </form>
        </div>
      </div>
      <!--End Class modal -->
      <!-- Start Class Modal -->
      <div class="modal fade" id="view-edit-class-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <form id="edit-category-form" name="edit-category-form">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Edit class</h4>
              </div>
              <div class="modal-body">

                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                        <label>Category</label>
                        <select id="edit-class-category" name="edit-class-category" class="categories form-control">
                          <option value="0"> Select Category </option>
                        </select>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                        <label>Sub-Category</label>
                        <select id="edit-class-sub-category" name="edit-class-sub-category" class="sub-categories form-control" >
                          <option value="0"> Select Sub-Category </option>
                        </select>
                    </div>
                  </div>
                  <div class="col-sm-6"></div>
                </div>
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                        <label>Class Name</label>
                        <input id="edit-class-name" name="edit-class-name" type="text" class="form-control" required="required" placeholder="Ex. PIG Universal Absorbent Mat Pads">
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                        <label>Status</label>
                        <select id="edit-class-status" name="edit-class-status" class="form-control">
                          <option value="0">Select Status</option>
                          <option value="1">Active</option>
                          <option value="2">Inactive</option>
                        </select>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group">
                        <label>Class Description <span class="small-text">( Text Limit : 150 Letters )</span></label>
                        <textarea id="edit-class-description" name="edit-class-description" class="form-control" rows="2" placeholder="Description..."></textarea>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group">
                        <label>Upload Class Image <span class="small-text">( Image Resolution : 500px X 800px )</span></label>
                      <div class="row">
                        <div class="col-md-3">
                          <div class="add-product-image">
                            <!-- <img id="edit-class-preview-image" src="../images/product/hero-absorbents.png" class="img-responsive" alt=""> -->
                            <a data-toggle="modal" href="#jcrop-image-modal-edit">Preview Image</a>
                          </div>
                        </div>
                        <div class="col-md-9">
                          <input name="edit-class-image" id="edit-class-image" type="file" class="btn form-control">
                          <input type="hidden" id="x1" name="x1" value="0">
                          <input type="hidden" id="y1" name="y1" value="0">
                          <input type="hidden" id="w1" name="w1" value="0">
                          <input type="hidden" id="h1" name="h1" value="0">
                          <input name="action" id="action" type="hidden" value="updateClass">
                          <input name="edit-class-id" id="edit-class-id" type="hidden" value="0">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                          <div class="row">
                            <div class="col-sm-12">
                              <h4>Meta Data</h4>
                              <div class="col-sm-6">
                                <div class="form-group">
                                  <label>Meta Title</label>
                                  <input id="meta-title-edit" name="meta-title-edit" type="text" class="form-control" placeholder="Meta Title">
                                </div>
                              </div>
                              <div class="col-sm-6">
                                <div class="form-group">
                                  <label>Meta Keywords</label>
                                  <input id="meta-keywords-edit" name="meta-keywords-edit" type="text" class="form-control" placeholder="Meta Keywords">
                                </div>
                              </div>
                              <div class="col-sm-12">
                                <div class="form-group">
                                  <label>Meta Description</label>
                                  <textarea id="meta-description-edit" name="meta-description-edit"  class="form-control" rows="5" cols="150" placeholder="Meta Description"></textarea>
                                </div>
                              </div>
                              <div class="col-sm-12">
                                <div class="form-group">
                                  <label>Class Details</label>
                                  <textarea id="details-edit" name="details-edit"  class="form-control" rows="5" cols="150" placeholder="Class Details"></textarea>
                                </div>
                              </div>
                            </div>
                          </div>

              </div>
              <div class="modal-footer">
                <button class="btn btn-success" type="submit"><i class="fa fa-floppy-o"></i> Save</button>
                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
              </div>
            </div>
          </form>
        </div>
      </div>
      <!--End Class modal -->
      <!-- Start jcrop image Modal -->
      <div class="modal fade" id="jcrop-image-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md">
          <div class="modal-content">
            
            <div class="modal-body">
              <div style="height:400px">
                <img title="Jcrop Image Preview" src="" alt="Croping Image" id="crop-image" class="img-responsive" >
                <!-- <p class="text-center">Actual Image</p> -->
              </div>
              <!-- <div style="float: right;">
                <div id="preview-pane">
                    <div class="preview-container"> -->
                        <!-- <img src="images/na4.jpg" class="jc-image jcrop-preview" alt="Preview" id="image-preview" /> -->
                       <!-- <img title="Jcrop Image Preview" class="jc-image jcrop-preview" src="" alt="Croping Image Preview" id="crop-preview" >
                    </div>
                    <p class="text-center">Final Image - Resolution (500x500 pixels)</p>
                </div>
              </div> -->
              <div class="row">
                <div class="col-md-12">
                  <hr>
                  <a data-dismiss="modal" class="btn btn-default pull-right">Set</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--End jcrop image modal -->
       <!-- Start jcrop image edit Modal -->
      <div class="modal fade" id="jcrop-image-modal-edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md">
          <div class="modal-content">
            
            <div class="modal-body">
              <div style="height:400px">
                <img title="Jcrop Image Preview" src="" alt="Croping Image" id="crop-image-edit" class="img-responsive" >
              </div>
              <div class="row">
                <div class="col-md-12">
                  <hr>
                  <a data-dismiss="modal" class="btn btn-default pull-right">Set</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--End jcrop image modal -->

      <!--footer start-->
      <?php include('html/footer.php'); ?>
      <!--footer end-->
  </section>

    
  <?php include('html/js-files.php'); ?>
  <script type="text/javascript" src="js/ckeditor/ckeditor.js" ></script>
  <script type="text/javascript" src="js/custom/manage-class.js" ></script>

  </body>
</html>