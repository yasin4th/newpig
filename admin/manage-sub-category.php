<?php
  @session_start();

  if(isset($_SESSION['role'])){
    $role = $_SESSION['role'];
  }else {
    header('Location: index.php');
  }
  if($role!='1'){
      header('Location: redirect.php');
  }
?><!DOCTYPE html>
<html lang="en">
  <head>
    <?php include('html/head-tag.php'); ?>
  </head>

  <body>

  <section id="container" class="">
      <!--header start-->
      <?php include('html/header.php'); ?>
      <!--header end-->
      <!--sidebar start-->
      <?php include('html/sidemenu.php'); ?>
      <!--sidebar end-->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
              <!-- page start-->
              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              Manage Sub-Category
                              <a data-toggle="modal" href="#add-Sub-category" class="btn btn-success pull-right btn-xs"><i class="fa fa-plus"></i> Sub-Category</a>
                          </header>
                          <div class="panel-body">
                            <div class="row">
                              <form role="form" id="filter" name="filter">
                              <div class="col-sm-3">
                                <div class="form-group">
                                  <label>Category Name</label>
                                  <select class="form-control" name="catname" id="categories">
                                    <option value="0">Select Category</option>
                                    
                                  </select>
                                </div>
                              </div>
                              <div class="col-sm-3">
                                <div class="form-group">
                                  <label>Sub-Category Name</label>
                                  <input name="subcatname" type="text" class="form-control" placeholder="Sub-Category Name">
                                </div>
                              </div>
                              <div class="col-sm-3">
                                <div class="form-group">
                                  <label>Status</label>
                                  <select class="form-control" name="status">
                                    <option value="0">Select Status</option>
                                    <option value="1">Active</option>
                                    <option value="2">Not Active</option>                                    
                                  </select>
                                </div>
                              </div>
                              <div class="col-sm-3">
                                <button class="btn btn-success mrg-top24" type="submit"><i class="fa fa-filter"></i> Filter</button>
                                <a class="btn btn-primary mrg-top24" href="">Clear</a>
                              </div>
                              <div class="col-sm-3">
                                    <input name="action" id="action" type="hidden" value="filterSubCategory">
                              </div>
                              </form>
                            </div>
                              <section>
                                <div class="table-responsive">
                                  <table id="sub-categories-table" class="table table-bordered table-striped table-condensed">
                                    <thead>
                                    <tr>
                                        <th>Category Name</th>
                                        <th>Sub-Category Name</th>
                                        <th>Description</th>
                                        <th>Image</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <!-- <tr>
                                        <td>PIG Universal Absorbent Mats</td>
                                        <td class="product-img"><img src="../images/product/hero-absorbents.png" class="img-responsive" alt=""></td>
                                        <td>Active</td>
                                        <td>
                                          <a data-toggle="modal" href="#view-edit-Sub-category" class="btn btn-primary btn-xs" title="View / Edit"><i class="fa fa-pencil"></i></a>
                                          <a class="btn btn-danger btn-xs" title="Delete"><i class="fa fa-trash-o "></i></a>
                                        </td>
                                    </tr> -->
                                    
                                    </tbody>
                                </table>
                              </div>
                              <div class="row" id="error-msg"></div>
                              </section>
                          </div>
                        </div>
                      </section>
                  </div>
              </div>
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->

      <!-- Start Add Sub-Category Modal -->
      <div class="modal fade" id="add-Sub-category" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <form id="add-category-form" method="post">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Add Sub-Category</h4>
              </div>
              <div class="modal-body">

                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                        <label>Category</label>
                        <select id="parent-id" name="parent-id" class="parent-categories form-control">
                          <!--<option> Select Category </option>-->
                        </select>
                    </div>
                  </div>
                  <div class="col-sm-6"></div>
                </div>
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                        <label>Sub-Category Name</label>
                        <input id="category-name" name="category-name" required="required" type="text" class="form-control" placeholder="Ex. PIG Universal Absorbent Mats">
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                        <label>Status</label>
                        <select id="category-status" name="category-status" class="form-control">
                          <option value="0">Select Status</option>
                          <option value="1">Active</option>
                          <option value="2">Not Active</option>
                        </select>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group">
                        <label>Sub-Category Description <span class="small-text">( Text Limit : 150 Letters )</span></label>
                        <textarea id="category-description" name="category-description" class="form-control" rows="2" placeholder="Description..."></textarea>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group">
                        <label>Upload Sub-Category Image <span class="small-text">( Image Resolution : 500px X 800px )</span></label>
                      <div class="row">
                        <div class="col-md-3">
                          <div class="add-product-image">
                            <a data-toggle="modal" href="#jcrop-image-modal">Preview Image</a>
                          </div>
                        </div>
                        <div class="col-md-9">
                          <input name="category-image" id="category-image" required="required" type="file" class="btn form-control">
                          <input type="hidden" id="x" name="x" value="0">
                          <input type="hidden" id="y" name="y" value="0">
                          <input type="hidden" id="w" name="w" value="0">
                          <input type="hidden" id="h" name="h" value="0">
                          <input name="action" id="action" type="hidden" value="addCategory">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                          <div class="row">
                            <div class="col-sm-12">
                              <h4>Meta Data</h4>
                              <div class="col-sm-6">
                                <div class="form-group">
                                  <label>Meta Title</label>
                                  <input id="meta-title-add" name="meta-title-add" type="text" class="form-control" placeholder="Meta Title">
                                </div>
                              </div>
                              <div class="col-sm-6">
                                <div class="form-group">
                                  <label>Meta Keywords</label>
                                  <input id="meta-keywords-add" name="meta-keywords-add" type="text" class="form-control" placeholder="Meta Keywords">
                                </div>
                              </div>
                              <div class="col-sm-12">
                                <div class="form-group">
                                  <label>Meta Description</label>
                                  <textarea id="meta-description-add" name="meta-description-add"  class="form-control" rows="5" cols="150" placeholder="Meta Description"></textarea>
                                </div>
                              </div>
                              <div class="col-sm-12">
                                <div class="form-group">
                                  <label>Sub Category Details</label>
                                  <textarea id="details-add" name="details-add"  class="form-control" rows="5" cols="150" placeholder="Sub Category Details"></textarea>
                                </div>
                              </div>
                            </div>
                          </div>



              </div>
              <div class="modal-footer">
                <button class="btn btn-success" type="submit"><i class="fa fa-plus"></i> Add</button>
                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
              </div>
            </div>
          </form>
        </div>
      </div>
      <!--End Add Sub-Category modal -->
      <!-- Start View And Edit Sub-Category Modal -->
      <div class="modal fade" id="view-edit-Sub-category-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <form id="edit-category-form" method="post">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Edit Sub-Category</h4>
              </div>
              <div class="modal-body">

                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                        <label>Category</label>
                        <select id="edit-category-parent-id" name="edit-category-parent-id" class="parent-categories form-control">
                          <option> Select Category </option>
                        </select>
                    </div>
                  </div>
                  <div class="col-sm-6"></div>
                </div>
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                        <label>Sub-Category Name</label>
                        <input id="edit-category-name" name="edit-category-name" required="required" type="text" class="form-control" required="required" placeholder="Ex. PIG Universal Absorbent Mats">
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                        <label>Status</label>
                        <select id="edit-category-status" name="edit-category-status" class="form-control">
                          <option value="0">Select Status</option>
                          <option value="1">Active</option>
                          <option value="2">Not Active</option>
                        </select>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group">
                        <label>Sub-Category Description <span class="small-text">( Text Limit : 150 Letters )</span></label>
                        <textarea id="edit-category-description" name="edit-category-description" class="form-control" rows="2" placeholder="Description..."></textarea>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group">
                        <label>Upload Sub-Category Image <span class="small-text">( Image Resolution : 500px X 800px )</span></label>
                      <div class="row">
                        <div class="col-md-3">
                          <div class="add-product-image">
                            <!-- <img src="../images/product/hero-absorbents.png" class="img-responsive" id="edit-category-preview-image" alt=""> -->
                            <a data-toggle="modal" href="#jcrop-image-modal-edit">Preview Image</a>
                          </div>
                        </div>
                        <div class="col-md-9">
                          <input name="edit-category-image" id="edit-category-image" type="file" class="btn form-control">
                          <input type="hidden" id="x1" name="x1" value="0">
                          <input type="hidden" id="y1" name="y1" value="0">
                          <input type="hidden" id="w1" name="w1" value="0">
                          <input type="hidden" id="h1" name="h1" value="0">
                          <input name="action" id="action" type="hidden" value="updateCategory">
                          <input name="edit-category-id" id="edit-category-id" type="hidden" value="0">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>


                 <div class="row">
                  <div class="col-sm-12">
                    <h4>Meta Data</h4>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Meta Title</label>
                        <input id="meta-title-edit" name="meta-title-edit" type="text" class="form-control" placeholder="Meta Title">
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Meta Keywords</label>
                        <input id="meta-keywords-edit" name="meta-keywords-edit" type="text" class="form-control" placeholder="Meta Keywords">
                      </div>
                    </div>
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label>Meta Description</label>
                        <textarea id="meta-description-edit" name="meta-description-edit"  class="form-control" rows="5" cols="150" placeholder="Meta Description"></textarea>
                      </div>
                    </div>
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label>Sub Category Details</label>
                        <textarea id="details-edit" name="details-edit"  class="form-control" rows="5" cols="150" placeholder="Sub Category Details"></textarea>
                      </div>
                    </div>
                  </div>
                </div>




              </div>
              <div class="modal-footer">
                <button class="btn btn-success" type="submit"><i class="fa fa-floppy-o"></i> Save</button>
                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
              </div>
            </div>
          </form>
        </div>
      </div>
      <!--End View And Edit Sub-Category modal -->
    <!-- Start jcrop image Modal -->
      <div class="modal fade" id="jcrop-image-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md">
          <div class="modal-content">
            
            <div class="modal-body">
              <div style="height:400px">
                <img title="Jcrop Image Preview" src="" alt="Croping Image" id="crop-image" class="img-responsive" >
              </div>
              <div class="row">
                <div class="col-md-12">
                  <hr>
                  <a data-dismiss="modal" class="btn btn-default pull-right">Set</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--End jcrop image modal -->
       <!-- Start jcrop image edit Modal -->
      <div class="modal fade" id="jcrop-image-modal-edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md">
          <div class="modal-content">
            
            <div class="modal-body">
              <div style="height:400px">
                <img title="Jcrop Image Preview" src="" alt="Croping Image" id="crop-image-edit" class="img-responsive" >
              </div>
              <div class="row">
                <div class="col-md-12">
                  <hr>
                  <a data-dismiss="modal" class="btn btn-default pull-right">Set</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--End jcrop image modal -->

      <!--footer start-->
      <?php include('html/footer.php'); ?>
      <!--footer end-->
  </section>

    
  <?php include('html/js-files.php'); ?>
  <script type="text/javascript" src="js/ckeditor/ckeditor.js" ></script>
  <script type="text/javascript" src="js/custom/manage-sub-category.js"></script>

  </body>
</html>