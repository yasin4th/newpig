<?php
  @session_start();

  if(isset($_SESSION['role'])){
    $role = $_SESSION['role'];
  }else {
    header('Location: index.php');
  }
  if($role!='1'){
      header('Location: redirect.php');
  }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <?php include('html/head-tag.php'); ?>
  </head>

  <body>

  <section id="container" class="">
      <!--header start-->
      <?php include('html/header.php'); ?>
      <!--header end-->
      <!--sidebar start-->
      <?php include('html/sidemenu.php'); ?>
      <!--sidebar end-->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
              <!-- page start-->
              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              Manage Specification
                              <a data-toggle="modal" href="#add-Specification" class="btn btn-success pull-right btn-xs"><i class="fa fa-plus"></i> Specification</a>
                          </header>
                          <div class="panel-body">
                            
                              <section>
                                <div class="table-responsive">
                                  <table id="specification" class="table table-bordered table-striped table-condensed">
                                    <thead>
                                    <tr>
                                        <th>Specification Name</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <!-- <tr>
                                        <td>PIG Universal Absorbent Mat Pads</td>
                                        <td>Active</td>
                                        <td>
                                          <a class="btn btn-danger btn-xs" title="Active"><i class="fa fa-check"></i></a>
                                          <a data-toggle="modal" href="#view-edit-specification" class="btn btn-primary btn-xs" title="View / Edit"><i class="fa fa-pencil"></i></a>
                                          <a class="btn btn-danger btn-xs" title="Delete"><i class="fa fa-trash-o"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>PIG Universal Absorbent Mat Pads</td>
                                        <td>Inactive</td>
                                        <td>
                                          <a class="btn btn-danger btn-xs" title="Inactive"><i class="fa fa-times"></i></a>
                                          <a data-toggle="modal" href="#view-edit-specification" class="btn btn-primary btn-xs" title="View / Edit"><i class="fa fa-pencil"></i></a>
                                          <a class="btn btn-danger btn-xs" title="Delete"><i class="fa fa-trash-o"></i></a>
                                        </td>
                                    </tr> -->
                                    </tbody>
                                </table>
                              </div>
                              <div class="row" id="error-msg"></div>
                              </section>
                          </div>
                        </div>
                      </section>
                  </div>
              </div>
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->

      <!-- Start Class Modal -->
      <div class="modal fade" id="add-Specification" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
        <form method="post" id="add-spec-form" name="add-spec-form">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Add Specification</h4>
            </div>
            <div class="modal-body">

              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                      <label>Specification Name</label>
                      <input type="text" name="add-name" class="form-control" required="required" placeholder="Ex. PIG Universal Absorbent Mat Pads">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                      <label>Status</label>
                      <select name="add-status" id="add-status" class="form-control">
                        <option value="0">Select Status</option>
                        <option value="1">Active</option>
                        <option value="2">Inactive</option>
                      </select>
                  </div>
                </div>
              </div>
              <input name="action" type="hidden" value="addSpecification">
            </div>
            <div class="modal-footer">
              <button class="btn btn-success" type="submit"><i class="fa fa-plus"></i> Add</button>
              <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            </div>
          </div>
        </form>
        </div>
      </div>
      <!--End Class modal -->
      <!-- Start Class Modal -->
      <div class="modal fade" id="edit-spec-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <form id="edit-spec-form" name="edit-spec-form">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Edit Specification</h4>
              </div>
              <div class="modal-body">

                <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                      <label>Specification Name</label>
                      <input type="text" name="edit-name" id="edit-name" class="form-control" required="required" placeholder="Ex. PIG Universal Absorbent Mat Pads">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                      <label>Status</label>
                      <select name="edit-status" id="edit-status" class="form-control">
                        <option value="0">Select Status</option>
                        <option value="1">Active</option>
                        <option value="2">Inactive</option>
                      </select>
                  </div>
                </div>
              </div>
              <div class="col-md-9">                
                <input name="action" id="action" type="hidden" value="updateSpecification">               
                <input name="edit-spec-id" id="edit-spec-id" type="hidden" value="0">
              </div>
              </div>
              <div class="modal-footer">
                <button class="btn btn-success" type="submit"><i class="fa fa-floppy-o"></i> Save</button>
                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
              </div>
            </div>
          </form>
        </div>
      </div>
      <!--End Class modal -->
      <!--footer start-->
      <?php include('html/footer.php'); ?>
      <!--footer end-->
  </section>

    
  <?php include('html/js-files.php'); ?>
  <script type="text/javascript" src="js/custom/manage-specification.js" ></script>

  </body>
</html>