<?php
  @session_start();

  if(isset($_SESSION['role'])){
    $role = $_SESSION['role'];
  }else {
    header('Location: index.php');
  }
  if($role!='1'){
      header('Location: redirect.php');
  }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <?php include('html/head-tag.php'); ?>
  </head>

  <body>

  <section id="container" class="">
      <!--header start-->
      <?php include('html/header.php'); ?>
      <!--header end-->
      <!--sidebar start-->
      <?php include('html/sidemenu.php'); ?>
      <!--sidebar end-->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
              <!-- page start-->
              <div class="row">
                <div class="col-lg-12">
                  <section class="panel">
                    <header class="panel-heading">
                      Add Video
                      <a href="manage-videos.php" class="btn btn-success pull-right btn-xs">Back</a>
                    </header>
                    <div class="panel-body">
                      <form id="add-video-form" role="form">
                        <div class="row">
                          <div class="col-sm-3">
                            <div class="form-group">
                              <label> Category</label>
                                <select name="cat" id="categories" class="categories form-control">
                                  <option value="0"> Select Category </option>
                                </select>
                            </div>
                          </div>
                          <div class="col-sm-3">
                            <div class="form-group">
                              <label> Sub-Category</label>
                                <select name="subcat" id="sub-category" class=" form-control">
                                  <option value="0">Select Sub Category</option>
                                </select>
                            </div>
                          </div>
                          <div class="col-sm-3">
                            <div class="form-group">
                              <label>Product</label>
                              <select id="select-products" class="products form-control">
                                <option value="0"> Select Product </option>
                              </select>
                            </div>
                          </div>
                          <div class="col-sm-3">
                            <div class="form-group">
                              <label>Status</label>
                              <select id="status" class="form-control">
                                <option value="-1"> Select Status </option>
                                <option value="1"> Active </option>
                                <option value="0"> Not Active </option>
                              </select>
                            </div>
                          </div>
                          <div class="col-sm-8">
                            <div class="form-group">
                              <label> Video Title</label>
                              <input id="title" type="text" class="form-control" required="required" placeholder="Video Title">
                            </div>
                          </div>
                          <div class="col-sm-8">
                            <div class="form-group">
                              <label> Media URL</label>
                              <input id="url" type="text" class="form-control" required="required" placeholder="Media URL">
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-sm-12">
                              <div class="col-sm-12">
                                <div class="form-group">
                                  <label> Category Description <span class="small-text">( Text Limit : 150 Letters )</span></label>
                                  <textarea id="video-description" name="video-description" class="form-control" rows="2" placeholder="Description..."></textarea>
                                </div><!-- <iframe id="video-preview" width="560" height="315" src="https://www.youtube.com/embed/GlFlSlAa-j4" frameborder="0" allowfullscreen></iframe> -->
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-sm-12">
                            <div class="form-group pull-right">
                              <button type="submit" class="btn btn-success"><i class="fa fa-plus"></i> Add</button>
                              <a href="manage-videos.php" class="btn btn-default">Cancel</a>
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                  </section>
                </div>
              </div>
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->
      <!--footer start-->
      <?php include('html/footer.php'); ?>
      <!--footer end-->
  </section>

    
  <?php include('html/js-files.php'); ?>

  <script type="text/javascript" src="js/custom/add-videos/add-videos.js"></script>
  <script type="text/javascript" src="js/custom/add-videos/fetch-master-data.js"></script>


  </body>
</html>