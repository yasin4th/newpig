<?php
  @session_start();

  if(isset($_SESSION['role'])){
    $role = $_SESSION['role'];
  }else {
    header('Location: index.php');
  }
  if($role=='2'){
      header('Location: redirect.php');
  }
?><!DOCTYPE html>
<html lang="en">
  <head>
    <?php include('html/head-tag.php'); ?>
  </head>

  <body>

  <section id="container" class="">
      <!--header start-->
      <?php include('html/header.php'); ?>
      <!--header end-->
      <!--sidebar start-->
      <?php include('html/sidemenu.php'); ?>
      <!--sidebar end-->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
              <!-- page start-->
              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              Product Order Details
                              <a href="manage-order.php" class="btn btn-success pull-right btn-xs"><i class="fa fa-filter"></i> Back</a>
                          </header>
                          <div class="panel-body">
                              <section>
                                <div class="row" id="customer-details">

                                 <!--<div class="col-sm-5">
                                      <h2>Name</h2>
                                      <h4>Email@gmail.com</h4>
                                  </div> -->
                                </div>
                                <div class="row" id="products">
                                  
                                    
                                    
                                    
                                </div>                                
                              </section>
                          </div>
                        </div>
                      </section>
                  </div>
              </div>
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->
      <!--footer start-->
      <?php include('html/footer.php'); ?>
      <!--footer end-->
  </section>
      <?php include('html/js-files.php'); ?>
  <script type="text/javascript" src="js/custom/product-order-details.js"> </script>


  </body>
</html>