<?php
  @session_start();

  if(isset($_SESSION['role'])){
    $role = $_SESSION['role'];
  }else {
    header('Location: index.php');
  }
  if($role!='1'){
      header('Location: redirect.php');
  }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <?php include('html/head-tag.php'); ?>
  </head>

  <body>

  <section id="container" class="">
      <!--header start-->
      <?php include('html/header.php'); ?>
      <!--header end-->
      <!--sidebar start-->
      <?php include('html/sidemenu.php'); ?>
      <!--sidebar end-->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
              <!-- page start-->
              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              Manage Users
                              <a href="add-user.php" class="btn btn-success pull-right btn-xs"><i class="fa fa-plus"></i> User</a>
                          </header>
                          <br>
                          <div class="panel-body">
                          <!-- <div class="panel-body">
                                <form role="form" name="filter" id="filter">
                                  <div class="row">
                                    
                                    
                                    <div class="col-sm-3">
                                      <div class="form-group">
                                          <label>User Name</label>
                                          <input type="text" class="form-control" placeholder="User Name" name="name">
                                      </div>
                                    </div>
                                  
                                    <div class="col-sm-3">
                                      <div class="form-group">
                                        <label>User Role</label>
                                        <select name="class" class="form-control" id="user-role">
                                          <option value="0"> Select User Role </option>
                                          <option value="2"> Data Entry Operator </option>
                                          <option value="3"> Order Manager </option>
                                          
                                        </select>
                                      </div>
                                    </div>
                                  
                                    <div class="col-sm-7">
                                      <div class="form-group">
                                        <button class="btn btn-success" type="submit"><i class="fa fa-filter"></i> Filter</button>                                        
                                      </div>
                                    </div>
                                  </div>
                                    <div class="col-sm-3">
                                      <input name="action" id="action" type="hidden" value="filterUsers">
                                    </div>
                                </form>
                              </div> -->
                              <section>
                                <div class="table-responsive">
                                  <table id="users-table" class="table table-bordered table-striped table-condensed">
                                    <thead>
                                    <tr>
                                        <!-- <th>Sr. No</th> -->
                                        <th>User Name</th>
                                        <th>Email</th>                                    
                                        <th>Phone No</th>                                    
                                        <th>Role</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <!--<tr>
                                        <td>1</td>
                                        <td>Category 1</td>
                                        <td>Sub-Category 1</td>
                                        <td>Product Class 1</td>
                                        <td>Product 1</td>
                                        <td class="product-img"><img src="../images/product/hero-absorbents.png" class="img-responsive" alt=""></td>
                                        <td>In Stock</td>
                                        <td>
                                          <a href="edit-product.php" class="btn btn-primary btn-xs" title="View / Edit"><i class="fa fa-pencil"></i></a>
                                          <a href="product-details.php" class="btn btn-primary btn-xs" title="Product Details"><i class="fa fa-info-circle"></i></a>
                                          <a class="btn btn-success btn-xs" title="Activate"><i class="fa fa-check"></i></a>
                                          <a class="btn btn-danger btn-xs" title="Delete"><i class="fa fa-trash-o "></i></a>
                                        </td>
                                    </tr>-->
                                    </tbody>
                                </table>
                              </div>
                              <div class="row" id="error-msg"></div>
                              </section>
                          </div>
                      </section>
                  </div>
              </div>
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->
      <!--footer start-->
      <?php include('html/footer.php'); ?>
      <!--footer end-->
  </section>
    
  <?php include('html/js-files.php'); ?>
  <script type="text/javascript" src="js/custom/manage-role.js"></script>
  
  </body>
</html>