<?php
  @session_start();

  if(isset($_SESSION['role'])){
    $role = $_SESSION['role'];
  }else {
    header('Location: index.php');
  }
  if($role=='2'){
      header('Location: redirect.php');
  }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <?php include('html/head-tag.php'); ?>
  </head>

  <body>

  <section id="container" class="">
      <!--header start-->
      <?php include('html/header.php'); ?>
      <!--header end-->
      <!--sidebar start-->
      <?php include('html/sidemenu.php'); ?>
      <!--sidebar end-->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
              <!-- page start-->
              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              Manage Order
                              <!-- <a data-toggle="modal" href="#filter-order" class="btn btn-success pull-right btn-xs"><i class="fa fa-filter"></i> Back</a> -->
                          </header>
                          <div class="panel-body">
                            <div class="row">
                              <form id="filter" role="form" name="filter">
                                <div class="col-sm-3">
                                  <div class="form-group">
                                    <label>Customer Name</label>
                                    <input name="name" type="text" class="form-control" placeholder="Customer Name">
                                  </div>
                                </div>
                                <!-- <div class="col-sm-3">
                                  <div class="form-group">
                                    <label>Order Date</label> -->
                                    <!-- <input type="text" class="form-control" placeholder="Order Date"> -->

                                      <!-- <div data-date-viewmode="years" data-date-format="dd-mm-yyyy" data-date="12-02-2012"  class="input-append date dpYears">
                                          <input type="text" readonly="" value="12-02-2012" size="16" class="form-control">
                                              <span class="input-group-btn add-on">
                                                <button class="btn btn-danger" type="button"><i class="fa fa-calendar"></i></button>
                                              </span>
                                      </div>
                                  </div>
                                </div> -->
                                <div class="col-sm-3">
                                  <div class="form-group">
                                    <label>Status</label>
                                    <select class="form-control" name="status">
                                      <option value="0">Select Status</option>
                                      <option value="1">Open</option>
                                      <option value="2">Close</option>
                                      <option value="3">Pending</option>
                                      <option value="4">In-Progress</option>
                                    </select>
                                  </div>                                
                                </div>
                                <div class="col-sm-4">
                                  <div class="form-group">
                                    <label>Order Date</label>
                                    <!-- <input type="text" class="form-control" placeholder="Order Date"> -->

                                      <div class="input-group input-large" data-date="13/07/2013" data-date-format="dd/mm/yyyy">
                                        <input type="text" class="form-control dpd1" name="from">
                                        <span class="input-group-addon">To</span>
                                        <input type="text" class="form-control dpd2" name="to"> 
                                      </div>
                                  </div>
                                </div>
                                <div>
                                      <input name="action" id="action" type="hidden" value="filterOrder">
                                </div>
                                <div class="col-sm-2">
                                  <button class="btn btn-success mrg-top24" type="submit"><i class="fa fa-filter"></i> Filter</button>
                                  <a class="btn btn-primary mrg-top24 " href="">Clear</a>
                                </div>
                              </form>
                            </div>
                              <section>
                                <div class="table-responsive">
                                  <table id="orders-table" class="table table-bordered table-striped table-condensed">
                                    <thead>
                                    <tr>
                                        <th>Customer Name</th>
                                        <th>Email Id</th>
                                        <th>Phone No.</th>
                                        <th>Order Date</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                              </div>
                              <div class="row" id="error-msg"></div>
                              </section>
                          </div>
                        </div>
                      </section>
                  </div>
              </div>
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->

      <!-- Start Filter Order Modal -->
      <!-- <div class="modal fade" id="filter-order" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Filter Order</h4>
            </div>
            <div class="modal-body">

              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label>Customer Name</label>
                    <input type="text" class="form-control" placeholder="Customer Name">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label>Order Date</label>
                    <input type="text" class="form-control" placeholder="Order Date">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label>Status</label>
                    <select class="form-control">
                      <option value="0">Select Status</option>
                      <option value="1">Open</option>
                      <option value="2">Close</option>
                      <option value="3">Pending</option>
                      <option value="4">In-Progress</option>
                    </select>
                  </div>
                </div>
                <div class="col-sm-6">
                  <button class="btn btn-success mrg-top24 pull-right" type="button"><i class="fa fa-filter"></i> Filter</button>
                </div>
              </div>

            </div>
            <div class="modal-footer">
              <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            </div>
          </div>
        </div>
      </div> -->
      <!--End Filter Order modal -->
      <!-- Start Change Status Modal -->
      <div class="modal fade" id="change-status" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Change Status</h4>
            </div>
            <div class="modal-body">

              <div class="row">
                <div class="col-sm-6"></div>
              </div>
              <div class="row">
                <div class="col-sm-2"></div>
                <div class="col-sm-6">
                  <div class="form-group">
                      <label>Status</label>
                      <select class="form-control" id="status">
                        <option value="0">Select Status</option>
                        <option value="1">Open</option>
                        <option value="2">Close</option>
                        <option value="3">Pending</option>
                        <option value="4">In-Progress</option>
                      </select>
                  </div>
                </div>
                <div class="col-sm-2">
                  <button id="change" class="btn btn-success mrg-top24" type="button">
                  <i class="fa fa-floppy-o"></i> Save</button>
                </div>
                <div class="col-sm-2"></div>
              </div>

            </div>
            <div class="modal-footer">
              <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            </div>
          </div>
        </div>
      </div>
      <!--End Change Status modal -->
      <!--footer start-->
      <?php include('html/footer.php'); ?>
      <!--footer end-->
  </section>

    
  <?php include('html/js-files.php'); ?>
  <script type="text/javascript"src="js/custom/manage-order.js"></script>
  <script type="text/javascript" src="assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
  <!--<script src="js/advanced-form-components.js"></script>-->

  </body>
</html>