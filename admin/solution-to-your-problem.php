<?php
  @session_start();

  if(isset($_SESSION['role'])){
    $role = $_SESSION['role'];
  }else {
    header('Location: index.php');
  }
  if($role!='1'){
      header('Location: redirect.php');
  }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <?php include('html/head-tag.php'); ?>
  </head>

  <body>

  <section id="container" class="">
      <!--header start-->
      <?php include('html/header.php'); ?>
      <!--header end-->
      <!--sidebar start-->
      <?php include('html/sidemenu.php'); ?>
      <!--sidebar end-->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
              <!-- page start-->
              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              Blog
                              <a href="add-article.php" class="btn btn-success pull-right btn-xs"><i class="fa fa-plus"></i> Add Article</a>
                          </header>
                          <div class="panel-body">
                            <form role="form" id="filter" name="filter">
                              <div class="row">
                                <div class="col-sm-3">
                                  <div class="form-group">
                                      <label>Article Category</label>
                                      <select class="form-control" name="cat" id="categories">
                                        <option value="0"> Select Category </option>
                                        
                                      </select>
                                  </div>
                                </div>
                                <div class="col-sm-3">
                                  <div class="form-group">
                                      <label>Article Sub-Category</label>
                                      <select class="form-control" name="subcat" id="sub-category">
                                        <option value="0"> Select Sub Category</option>
                                        
                                      </select>
                                  </div>
                                </div>
                                <div class="col-sm-3">
                                  <div class="form-group">
                                      <label>Article Status</label>
                                      <select class="form-control" name="status">
                                        <option value="-1">Select Status</option>
                                        <option value="1">Active</option>
                                        <option value="0">Not Active</option>
                                      </select>
                                  </div>
                                </div>
                                <div class="col-sm-3">
                                  <div class="form-group">
                                      <label>Article Title</label>
                                      <input type="text" name="name" class="form-control" placeholder="Article Title">
                                  </div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-sm-12">
                                  <div class="form-group pull-right">
                                    <button class="btn btn-success" type="submit"><i class="fa fa-filter"></i> Filter</button>
                                    <a class="btn btn-primary" href="">Clear</a>
                                    <!-- <a class="btn btn-default">Cancel</a> -->
                                  </div>
                                </div>
                              </div>
                              <div class="col-sm-3">
                                    <input name="action" id="action" type="hidden" value="filterArticle">
                              </div>
                            </form>
                              <section>
                                <div class="table-responsive">
                                  <table id="articles-table" class="table table-bordered table-striped table-condensed">
                                    <thead>
                                    <tr>
                                        <th>Article Category</th>
                                        <th>Article Sub-Category</th>
                                        <th>Article Title</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                      
                                    <!-- <tr>
                                        <td>Spill Kit</td>
                                        <td>PIG Chemical Neutralizing Spill Kits</td>
                                        <td>3 Questions to Ask When Selecting a Spill Kit</td>
                                        <td>
                                          <a href="view-edit-article.php" class="btn btn-primary btn-xs" title="View / Edit"><i class="fa fa-pencil"></i></a>
                                          <a class="btn btn-success btn-xs" title="Active"><i class="fa fa-check"></i></a>
                                          <a class="btn btn-danger btn-xs" title="Delete"><i class="fa fa-trash-o "></i></a>
                                        </td>
                                    </tr> -->
                                    
                                    </tbody>
                                </table>
                              </div>
                              <div class="row" id="error-msg"></div>
                              </section>
                          </div>
                        </div>
                      </section>
                  </div>
              </div>
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->

      <!--footer start-->
      <?php include('html/footer.php'); ?>
      <!--footer end-->
  </section>

    
  <?php include('html/js-files.php'); ?>

  <script type="text/javascript" src="js/custom/solution-to-your-problem.js"></script>

  </body>
</html>