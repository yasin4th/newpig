<?php
   @session_start();
    if(isset($_SESSION['role'])){
      header('Location: redirect.php');
    }
   
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keyword" content="">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>Newpig | Admin</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet" />
    <link href="css/custom.css" rel="stylesheet" />
    <link rel="icon" href="../images/favicon.ico" type="image/x-icon">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>

  <body class="login-body">

    <div class="container">
      <div class="login-logo">
        <a href="#"><img src="../images/home/iq_logo.png" alt="" title="Newpig"></a>
      </div>
      <form id="admin-login-form" class="form-signin" method="post">
        <h2 class="form-signin-heading">Welcome Admin</h2>
        <div class="login-wrap">
          <div class="form-group has-error">
              <center><span id="show-error" class="show-message"></span></center>
          </div>
          <div class="form-group">
            <input id="e-mail" type="text" class="form-control" placeholder="email@demo.com" autofocus>
          </div>
          <div class="form-group">
            <input id="password" type="password" class="form-control" placeholder="Password">
          </div>
            <label class="checkbox">
                <input type="checkbox" value="remember-me"> Remember me
                <span class="pull-right">
                    <a data-toggle="modal" href="#forgot-password-modal"> Forgot Password?</a>
                </span>
            </label>
            <button type="submit" class="btn btn-lg btn-login btn-block">Sign in</button>
        </div>
      </form>



          <!-- Modal -->
          <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="forgot-password-modal" class="modal fade">
              <div class="modal-dialog">
                <form id="forgot-password-form" method="post">
                  <div class="modal-content">
                      <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                          <h4 class="modal-title">Forgot Password ?</h4>
                      </div>
                      <div class="modal-body">
                          <p>Enter your e-mail address below to reset your password.</p>
                          <input type="text" id="fg-email" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix">

                      </div>
                      <div class="modal-footer">
                          <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                          <button type="submit" class="btn btn-success" type="button">Submit</button>
                      </div>
                  </div>
                </form>  
              </div>
          </div>
          <!-- modal -->


    </div>



    <!-- js placed at the end of the document so the pages load faster -->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/common.js"></script>
    <script src="js/custom/index.js"></script>


  </body>
</html>
