<?php
  @session_start();

  if(isset($_SESSION['role'])){
    $role = $_SESSION['role'];
  }else {
    header('Location: index.php');
  }
  if($role=='3'){
      header('Location: redirect.php');
  }
  //print_r($_SESSION);
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <?php include('html/head-tag.php'); ?>
  </head>

  <body>

  <section id="container" class="">
      <!--header start-->
      <?php include('html/header.php'); ?>
      <!--header end-->
      <!--sidebar start-->
      <?php include('html/sidemenu.php'); ?>
      <!--sidebar end-->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
              <!-- page start-->
              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              Product Details<a href="manage-product.php" class="btn btn-success pull-right btn-xs">Back</a>
                          </header>
                          <div class="panel-body">
                              <section>
                                <div class="row">
                                  <div class="col-sm-5">
                                    <div class="product-details-img">
                                      <img class="product-image" src="" alt="Absorbents" title="" />
                                    </div>
                                    
                                    <div class="row mrg-top10">
                                      <div class="col-sm-12">
                                        <div class="add-related-img">
                                          <div class="related-img">
                                            <!-- <img src="../images/product/hero-absorbents.png" class="img-responsive" alt=""> -->
                                          </div>
                                          <!-- <div class="related-img">
                                            <img src="../images/product/hero-absorbents.png" class="img-responsive" alt="">
                                          </div>
                                          <div class="related-img">
                                            <img src="../images/product/hero-absorbents.png" class="img-responsive" alt="">
                                          </div> -->
                                        </div>
                                      </div>
                                    </div>

                                  </div>
                                  <div class="col-sm-7">
                                    <div class="product-details">
                                      <h2 class="product-name"></h2>
                                      <p class="product-part-name"></p>
                                      <p class="small-description"></p>
                                      <p>
                                        <ul class="points">
                                            
                                        </ul>
                                      </p>
                                      <span>
                                        <span class="mrp"></span>
                                      </span>
                                      <div class="product-details-options">
                                        <p><b>Quantity:</b> <span class="quantity"></span></p>
                                        <p><b>Availability:</b> <span class="stock"></span></p>
                                        <p><b>Net Weight:</b> <span class="weight"></span></p>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="row mrg-top30">
                                  <div class="col-sm-12">
                                    <!--tab nav start-->
                                    <section class="panel">
                                        <header class="panel-heading tab-bg-dark-navy-blue ">
                                            <ul class="nav nav-tabs">
                                                <li class="active">
                                                    <a data-toggle="tab" href="#description">Description</a>
                                                </li>
                                                <li class="">
                                                    <a data-toggle="tab" href="#specifications">Specifications</a>
                                                </li>
                                                <li class="video">
                                                    <a data-toggle="tab" href="#media">Media</a>
                                                </li>
                                            </ul>
                                        </header>
                                        <div class="panel-body">
                                            <div class="tab-content">
                                                <div id="description" class="description tab-pane active text-justify">
                                                  
                                                </div>
                                                <div id="specifications" class="tab-pane">
                                                  <table class="table table-bordered" id="specs">
                                                    <tbody>
                                                    
                                                    </tbody>
                                                  </table>
                                                </div>
                                                <div id="media" class="tab-pane">
                                                  <p id="media-title"></p>
                                                  <div class="vedio">
                                                    <iframe id="video-preview" class="video-iframe" width="560" height="315" src="" frameborder="0" allowfullscreen=""></iframe>
                                                  </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                    <!--tab nav start-->
                                  </div>
                                </div>



                          <header class="panel-heading">
                              Related Products
                              <a data-toggle="modal" href="#related-product-modal" class="btn btn-success pull-right btn-xs"><i class="fa fa-plus"></i> Add More</a>
                          </header>
                          <div class="panel-body" id="related-product">
                            <!-- <form role="form" name="filter" id="filter">
                                <div class="row">
                                  <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Product Category</label>
                                        <select class="form-control" id="categories" name="cat">
                                          <option value="0"> Select Product Category </option>
                                          
                                        </select>
                                    </div>
                                  </div>
                                  <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Product Sub-Category</label>
                                        <select name="subcat" id="sub-category" class="form-control">
                                          <option value="0"> Select Product Sub-Category </option>
                                          
                                        </select>
                                    </div>
                                  </div>
                                  <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Product Class</label>
                                        <select name="class" class="form-control" id="product-class">
                                          <option value="0"> Select Product Class </option>
                                          
                                        </select>
                                    </div>
                                  </div>
                                  <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Search</label>
                                        <input type="text" class="form-control" placeholder="Product Name, Supplier Part No" name="name">
                                    </div>
                                  </div>
                                  <div class="col-sm-3">
                                    <div class="form-group ">
                                      <button class="btn btn-success mrg-top24" type="submit"><i class="fa fa-filter"></i> Filter</button>
                                      <a class="btn btn-success" id="filter">Filter</a>
                                      <a class="btn btn-default">Cancel</a>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-sm-3">
                                  <input name="action" id="action" type="hidden" value="filterProduct">
                                </div>
                            </form> -->
                              
                            </div>
                                
                            </section>
                          </div>
                        </div>
                      </section>
                  </div>
              </div>
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->

      <div class="modal fade" id="related-product-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <form id="filter" role="form" method="post">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Filter Products</h4>
              </div>
              <div class="modal-body">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                        <label> Category</label>
                        <select class="form-control" id="categories" name="cat">
                          <option value="0"> Select Category </option>
                          
                        </select>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                        <label> Sub-Category</label>
                        <select name="subcat" id="sub-category" class="form-control">
                          <option value="0"> Select Sub-Category </option>
                          
                        </select>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                        <label> Class</label>
                        <select name="class" class="form-control" id="product-class">
                          <option value="0"> Select Class </option>
                          
                        </select>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                        <label>Search</label>
                        <input type="text" class="form-control" placeholder="Product Name, Supplier Part No" name="name">
                    </div>
                  </div>
                  <div class="col-sm-1">
                    <div class="form-group">
                        <button class="btn btn-success" type="submit"><i class="fa fa-filter"></i></button>                        
                    </div>
                  </div>
                  
                </div>
                <div class="col-sm-3">
                  <input name="action" id="action" type="hidden" value="filterProduct">
                </div>



                <section>
                  <table id="products-table" class="table table-bordered table-striped table-condensed">
                    <thead>
                    <tr>
                        <!-- <th>Sr. No</th> -->
                        <th><input type="checkbox" id="checkall"></th>
                        <th>Product Category</th>
                        <th>Product Sub-Category</th>
                        <th>Product Class</th>
                        <th>Product Name</th>
                        <th>Supplier Part No.</th>
                        <th>Image</th>
                        

                        
                    </tr>
                    </thead>
                    <tbody>
                    
                    </tbody>
                </table>
                <div class="row" id="error-msg"></div>
                </section>



              </div>
              <div class="modal-footer">
                <a class="btn btn-success" id="save-related"><i class="fa fa-floppy-o"></i> Save</a>
                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
              </div>
            </div>
          </form>
        </div>
      </div>

      <!-- end modal related product -->
      <!--footer start-->
      <?php include('html/footer.php'); ?>
      <!--footer end-->
  </section>

    
  <?php include('html/js-files.php'); ?>
  <script type="text/javascript" src="js/custom/view-product.js"></script>
  </body>
</html>