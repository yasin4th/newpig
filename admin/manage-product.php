<?php
  @session_start();

  if(isset($_SESSION['role'])){
    $role = $_SESSION['role'];
  }else {
    header('Location: index.php');
  }
  if($role=='3'){
      header('Location: redirect.php');
  }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <?php include('html/head-tag.php'); ?>
  </head>

  <body>

  <section id="container" class="">
      <!--header start-->
      <?php include('html/header.php'); ?>
      <!--header end-->
      <!--sidebar start-->
      <?php include('html/sidemenu.php'); ?>
      <!--sidebar end-->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
              <!-- page start-->
              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              Manage Products
                              <a href="add-product.php" class="btn btn-success pull-right btn-xs"><i class="fa fa-plus"></i> Product</a>
                          </header>
                          <div class="panel-body">
                            <form role="form" name="filter" id="filter">
                              <div class="row">
                                <div class="col-sm-3">
                                  <div class="form-group">
                                      <label>Product Category</label>
                                      <select class="form-control" id="categories" name="cat">
                                        <option value="0"> Select Product Category </option>
                                        
                                      </select>
                                  </div>
                                </div>
                                <div class="col-sm-3">
                                  <div class="form-group">
                                      <label>Product Sub-Category</label>
                                      <select name="subcat" id="sub-category" class="form-control">
                                        <option value="0"> Select Product Sub-Category </option>
                                        
                                      </select>
                                  </div>
                                </div>
                                <div class="col-sm-3">
                                  <div class="form-group">
                                      <label>Product Class</label>
                                      <select name="class" class="form-control" id="product-class">
                                        <option value="0"> Select Product Class </option>
                                        
                                      </select>
                                  </div>
                                </div>
                                <div class="col-sm-3">
                                  <div class="form-group mrg-top30">
                                      <input type="checkbox" id="featured" name="featured" value="1">                                
                                      Featured Products
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                  <div class="form-group">
                                      <label>Search</label>
                                      <input type="text" class="form-control" placeholder="Product Name, Supplier Part No" name="name">
                                  </div>
                                </div>
                                <div class="col-sm-3">
                                  <div class="form-group ">
                                    <button class="btn btn-success mrg-top24" type="submit"><i class="fa fa-filter"></i> Filter</button>
                                    <a class="btn btn-primary mrg-top24" href="">Clear</a>
                                    <!-- <a class="btn btn-success" id="filter">Filter</a> -->
                                    <!-- <a class="btn btn-default">Cancel</a> -->
                                  </div>
                                </div>
                              </div>
                              <div class="col-sm-3">
                                <input name="action" id="action" type="hidden" value="filterProduct">
                              </div>
                            </form>
                            <section>
                              <div class="table-responsive">
                                <table id="products-table" class="table table-bordered table-striped table-condensed">
                                  <thead>
                                  <tr>
                                      <!-- <th>Sr. No</th> -->
                                      <th>Product Category</th>
                                      <th>Product Sub-Category</th>
                                      <th>Product Class</th>
                                      <th>Product Name</th>
                                      <th>Supplier Part No.</th>
                                      <th class="no-sort">Image</th>
                                      <th class="no-sort">Availability</th>
                                      <th class="no-sort" style="width:90px;">Actions</th>
                                  </tr>
                                  </thead>
                                  <tbody>
                                  <!--<tr>
                                      <td>1</td>
                                      <td>Category 1</td>
                                      <td>Sub-Category 1</td>
                                      <td>Product Class 1</td>
                                      <td>Product 1</td>
                                      <td class="product-img"><img src="../images/product/hero-absorbents.png" class="img-responsive" alt=""></td>
                                      <td>In Stock</td>
                                      <td>
                                        <a href="edit-product.php" class="btn btn-primary btn-xs" title="View / Edit"><i class="fa fa-pencil"></i></a>
                                        <a href="product-details.php" class="btn btn-primary btn-xs" title="Product Details"><i class="fa fa-info-circle"></i></a>
                                        <a class="btn btn-success btn-xs" title="Activate"><i class="fa fa-check"></i></a>
                                        <a class="btn btn-danger btn-xs" title="Delete"><i class="fa fa-trash-o "></i></a>
                                      </td>
                                  </tr>-->
                                  </tbody>
                              </table>
                            </div>
                            <div class="row" id="error-msg"></div>
                            </section>
                          </div>
                        </div>
                      </section>
                  </div>
              </div>
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->
      <!--footer start-->
      <?php include('html/footer.php'); ?>
      <!--footer end-->
  </section>
    
  <?php include('html/js-files.php'); ?>
  <script type="text/javascript" src="js/custom/manage-product/manage-product.js"></script>
  <script type="text/javascript" src="js/custom/manage-product/fetch-master-data.js"></script>
  </body>
</html>