<!DOCTYPE html>
<html lang="en">
  <head>
    <?php include('html/head-tag.php'); ?>
  </head>

  <body>

  <section id="container" class="">
      <!--header start-->
      <?php include('html/header.php'); ?>
      <!--header end-->
      <!--sidebar start-->
      <?php include('html/sidemenu.php'); ?>
      <!--sidebar end-->
      <!--main content start-->
      <section id="main-content">
        <section class="wrapper site-min-height">
          <!-- page start-->
          <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6 mrg-top30">
              <section class="panel">
                <header class="panel-heading">
                    Change Password
                </header>
                <div class="panel-body">
                  <form id="change-password-form" role="form" method="post">
                    <div class="row">
                      <div class="col-sm-12 text-center">
                        <span id="message" class="show-message"> </span>
                      </div>
                      <div class="col-sm-12">
                        <div class="form-group">
                            <label>Current Password</label>
                            <input id="old-pass" type="password" class="form-control" placeholder="Old Password">
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="form-group">
                            <label>New Password</label>
                            <input id="new-pass" type="password" class="form-control" placeholder="New Password">
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="form-group">
                            <label>Confirm Password</label>
                            <input id="confirm-pass" type="password" class="form-control" placeholder="Confirm Password">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-group pull-right">
                          <button type="submit" class="btn btn-success">Save</button>
                          <a href="redirect.php" class="btn btn-default">Back</a>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </section>
            </div>
            <div class="col-lg-3"></div>
          </div>
          <!-- page end-->
        </section>
      </section>
      <!--main content end-->
      <!--footer start-->
      <?php include('html/footer.php'); ?>
      <!--footer end-->
  </section>

    
  <?php include('html/js-files.php'); ?>

  <script type="text/javascript" src="js/custom/change-password.js"></script>


  </body>
</html>
