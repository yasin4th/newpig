<?php
  @session_start();

  if(isset($_SESSION['role'])){
    $role = $_SESSION['role'];
  }else {
    header('Location: index.php');
  }
  if($role!='1'){
      header('Location: redirect.php');
  }
?><!DOCTYPE html>
<html lang="en">
  <head>
    <?php include('html/head-tag.php'); ?>
  </head>

  <body>

  <section id="container" class="">
      <!--header start-->
      <?php include('html/header.php'); ?>
      <!--header end-->
      <!--sidebar start-->
      <?php include('html/sidemenu.php'); ?>
      <!--sidebar end-->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
              <!-- page start-->
              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              Videos
                              <a href="add-videos.php" class="btn btn-success pull-right btn-xs"><i class="fa fa-plus"></i> Add Video</a>
                          </header>
                          <div class="panel-body">
                            <form role="form" id="filter" name="filter">
                              <div class="row">
                                <div class="col-sm-3">
                                  <div class="form-group">
                                      <label>Category</label>
                                      <select class="form-control" id="categories" name="cat">
                                        <option value="0"> Select Category </option>
                                        
                                      </select>
                                  </div>
                                </div>
                                <div class="col-sm-3">
                                  <div class="form-group">
                                      <label>Sub-Category</label>
                                      <select class="form-control" id="sub-category"name="subcat">
                                        <option value="0"> Select Sub Category </option>
                                        
                                      </select>
                                  </div>
                                </div>
                                <div class="col-sm-3">
                                  <div class="form-group">
                                      <label>Video Status</label>
                                      <select class="form-control" name="status">
                                        <option value="-1"> Select Status </option>
                                        <option value="1"> Active</option>
                                        <option value="0"> Not Active</option>
                                      </select>
                                  </div>
                                </div>
                                <div class="col-sm-3">
                                  <div class="form-group">
                                      <label>Video Title</label>
                                      <input name="name" type="text" class="form-control" placeholder="Video Title">
                                  </div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-sm-12">
                                  <div class="form-group pull-right">
                                    <button class="btn btn-success mrg-top24" type="submit"><i class="fa fa-filter"></i> Filter</button>
                                    <a class="btn btn-primary mrg-top24" href="">Clear</a>                                    
                                    <input name="action" id="action" type="hidden" value="filterVideos">                              
                                  </div>
                                </div>
                              </div>
                              <div class="col-sm-3">
                                    <input name="action" id="action" type="hidden" value="filterVideos">
                              </div>

                            </form>
                              <section>
                                <div class="table-responsive">
                                  <table id="videos-table" class="table table-bordered table-striped table-condensed">
                                    <thead>
                                    <tr>
                                        <th>Video Category</th>
                                        <th>Video Sub-Category</th>
                                        <th>Video Title</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <!-- <tr>
                                        <td>Spill Kit</td>
                                        <td>PIG Chemical Neutralizing Spill Kits</td>
                                        <td>Grippy Adhesive Back Floor Mat I iQSafety</td>
                                        <td>
                                          <a href="view-edit-videos.php" class="btn btn-primary btn-xs" title="View / Edit"><i class="fa fa-pencil"></i></a>
                                          <a class="btn btn-success btn-xs" title="Active"><i class="fa fa-check"></i></a>
                                          <a class="btn btn-danger btn-xs" title="Delete"><i class="fa fa-trash-o "></i></a>
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td>Spill Kit</td>
                                        <td>PIG Chemical Neutralizing Spill Kits</td>
                                        <td>Grippy Adhesive Back Floor Mat I iQSafety</td>
                                        <td>
                                          <a href="view-edit-videos.php" class="btn btn-primary btn-xs" title="View / Edit"><i class="fa fa-pencil"></i></a>
                                          <a class="btn btn-success btn-xs" title="Active"><i class="fa fa-check"></i></a>
                                          <a class="btn btn-danger btn-xs" title="Delete"><i class="fa fa-trash-o "></i></a>
                                        </td>
                                    </tr> -->
                                    </tbody>
                                </table>
                              </div>
                              <div class="row" id="error-msg"></div>
                              </section>
                          </div>
                        </div>
                      </section>
                  </div>
              </div>
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->

      <!--footer start-->
      <?php include('html/footer.php'); ?>
      <!--footer end-->
  </section>

    
  <?php include('html/js-files.php'); ?>
  <script type="text/javascript" src="js/custom/manage-videos.js"></script>


  </body>
</html>