<?php
  @session_start();

  if(isset($_SESSION['role'])){
    $role = $_SESSION['role'];
  }else {
    header('Location: index.php');
  }
  if($role!='1'){
      header('Location: redirect.php');
  }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <?php include('html/head-tag.php'); ?>
  </head>

  <body>

  <section id="container" class="">
      <!--header start-->
      <?php include('html/header.php'); ?>
      <!--header end-->
      <!--sidebar start-->
      <?php include('html/sidemenu.php'); ?>
      <!--sidebar end-->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
              <!-- page start-->
              <div class="row">
                <div class="col-lg-12">
                  <form id="add-user-form" role="form">
                    <section class="panel">
                      <header class="panel-heading">
                        Add User
                        <a href="manage-role.php" class="btn btn-success pull-right btn-xs">Back</a>
                      </header>
                      <div class="panel-body">
                          <div class="row">
                            <div class="col-sm-3">
                              <div class="form-group">
                                <label>First Name</label>
                                <input id="first-name" name="first-name" type="text" class="form-control" placeholder="First Name">
                              </div>
                            </div>
                            <div class="col-sm-3">
                              <div class="form-group">
                                <label>Last Name</label>
                                <input id="last-name" name="last-name" type="text" class="form-control" placeholder="Last Name">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-sm-3">
                              <div class="form-group">
                                <label>Email</label>
                                <input id="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" name="email" type="text" class="form-control" placeholder="Email Name">
                              </div>
                            </div>
                            <div class="col-sm-3">
                              <div class="form-group">
                                <label>Phone No.</label>
                                <input id="phone" name="phone" type="text" class="form-control" placeholder="Phone No">
                              </div>
                            </div>
                            
                          </div>
                          <div class="row">
                            <div class="col-sm-3">
                              <div class="form-group">
                                <label>Select Role</label>
                                <select name="role" id="role" class="role form-control">
                                  <option value="2"> Data Entry Operator </option>
                                  <option value="3"> Order Manager </option>
                                </select>
                              </div>
                            </div>
                          </div>
                          
                          
                          <div class="row">
                            <div class="col-sm-12">
                              <div class="form-group">
                                <button type="submit" class="btn btn-success"><i class="fa fa-plus"></i> Add</button>
                                <a class="btn btn-default">Cancel</a>
                              </div>
                            </div>
                          </div>
                      </div>
                    </section>
                  </form>
                </div>
              </div>

              
          </section>
      </section>
      <!--main content end-->
      <!--footer start-->
      <?php include('html/footer.php'); ?>
      <!--footer end-->
  </section>

    
  <?php include('html/js-files.php'); ?>
  
  <script type="text/javascript" src="js/custom/add-user.js" ></script>


  </body>
</html>