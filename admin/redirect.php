<?php
	 @session_start();
	  if(isset($_SESSION['role'])){
	    $role = $_SESSION['role'];
	  }else {
	    header('Location: index.php');
	  }

	  if($role=='3'){
	  	header('Location: manage-order.php');
	  }else if($role=='1' || $role=='2'){
	  	header('Location: manage-product.php');	  	
	  }else{
	  	header('Location: logout.php');
	  }
?>