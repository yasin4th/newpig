/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
    // Define changes to default configuration here. For example:
    // config.language = 'fr';
    // config.uiColor = '#AADC6E';
    config.skin = 'kama';
    config.extraPlugins = "mathjax","menubutton","menu","htmlbuttons";

   config.htmlbuttons =  [{
        name:'button1',
        icon:'icon-stempel-gruen.png',
        html:'<?php echo $stempel; ?>',
        title:'Your name & time stamp'
    }]; 

    config.toolbar = 'MyFullToolBar'
    config.toolbar_MyFullToolBar = [{
            name: "document",
            groups: ["document"],
            items: ["NewPage"]
        }, {
            name: "clipboard",
            groups: ["clipboard", "undo"],
            items: ["Cut", "Copy", "Paste", "PasteText", "PasteFromWord", "-", "Undo", "Redo"]
        }, {
            name: "editing",
            groups: ["find", "selection"],
            items: ["Find", "Replace", "-", "SelectAll",]
        }, "/", {
            name: "basicstyles",
            groups: ["basicstyles"],
            items: ["Bold", "Italic", "Underline", "Strike", "Subscript", "Superscript"]
        }, {
            name: "paragraph",
            groups: ["list", "align"],
            items: ["NumberedList", "BulletedList", "-", "JustifyLeft", "JustifyCenter", "JustifyRight", "JustifyBlock"]
        }, {
            name: "links",
            items: ["Link", "Unlink"]
        }, {
            name: "colors", 
            items : ['TextColor','BGColor'] }
        , {
            name: "insert",
            items: ["Image", "SpecialChar", "Mathjax","Fibdropdown", "Table"]
        },
        { name: 'forms', items : ['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField'] },
        {
            name: "button1",
            items: ["htmlbuttons"]
        }];
         // config.filebrowserImageUploadUrl = 'filemanager/connectors/asp/upload.asp?Type=Image';
    



};