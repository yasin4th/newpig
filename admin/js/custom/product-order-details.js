var order_id;

$(function() {
 	order_id = getUrlParameter('order_id');
	fetchAllProducts();
})

fetchAllProducts = function() {
	var req = {};
	req.action = 'getProductsByOrderId';
	req.order_id = order_id;

	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		//console.log(res);
		if(res.status == 1) {
			fillProducts(res.products);
			fillDetails(res.customer);
			// toastr.success(res.message);
		}
		else {
			toastr.error(res.message);
		}
	});		
}

fillProducts = function(products) {
	html = '';
	$.each(products, function(i, product) {
		stock = (product.stock == 1)? 'In-Stock':'Out Of Stock';

		html += '<div class="col-md-12">';
		html += ' <div class="col-sm-5">';
		html += ' <div class="product-details-img"> <img src="../'+product.image+'" alt="'+product.name+'" title="'+product.name+'" /> </div>';
		html += ' <div class="row mrg-top10"> </div> </div> <div class="col-sm-7">';
		html += ' <div class="product-details">';
		//html += ' <h2>'+product.name+'</h2> <p>'+product.part_name+'</p> '+product.small_description+' <span> <span>US $ '+product.mrp+'</span> </span> <div class="product-details-options"> <p><b>Quantity:</b>'+product.quantity+'</p> <p><b>Availability:</b> '+stock+'</p> <p><b>Net Amount:</b> '+product.quantity*product.mrp+'</p>';
		html += ' <h2>'+product.name+'</h2> <p>'+product.part_name+'</p> '+product.small_description+' <span>  </span> <div class="product-details-options"> <p><b>Quantity:</b>'+product.quantity+'</p>';
		html +=' </div> </div> </div> </div>'
	
 	});
   $('#products').html(html);

  

}

fillDetails = function(customer) {
	html = '<div class="col-sm-12">';
	html += '<p><span class="glyphicon glyphicon-user"></span> '+customer.first_name+' '+customer.last_name+' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ';
	html += '<span class="glyphicon glyphicon-earphone"></span> '+customer.phone_number+' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span class="glyphicon glyphicon-envelope" ></span> '+customer.email+' <hr></p>';
	html += '</div>';
	$('#customer-details').html(html);
}