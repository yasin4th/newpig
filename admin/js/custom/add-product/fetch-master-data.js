var Categories;
fetchAllCategories = function() {
	var req = {};
	req.action = 'fetchAllCategories';

	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		// console.log(res);
		res = $.parseJSON(res);
		if(res.status == 1) {
			Categories = res.categories;
			fillCategories(res.categories);
			// toastr.success(res.message);
		}
		else {
			toastr.error(res.message);
		}
	});		
}


fillCategories = function(categories) {
	html = '<option value="0"> Select Category </option>';
	$.each(categories, function(i, category) {
		if (category.parent_id == 0) {
			html += '<option value="'+category.id+'"> '+category.name+' </option>';
		};
	});
	$('.categories').html(html);
}


setEventLoadSubCategories = function() {
	$('.categories').off('change');
	$('.categories').on('change', function(e) {
		e.preventDefault();
		var cat = {};
		cat.id = $(this).attr('id');
		cat.sub_cat = $('#sub-category');
		fillSubCategories(cat);		
	});
}

fillSubCategories = function(cat) {
	html = '<option value="0"> Select Sub-Categories </option>';
	$.each(Categories, function(i, category) {
		if (category.parent_id == $('#'+cat.id).val() && $('#'+cat.id).val()!='0') {
			html += '<option value="'+category.id+'">'+category.name+'</option>';
		};
	});
	cat.sub_cat.html(html);
}

setEventLoadClasses = function() {
	$('#sub-category').off('change');
	$('#sub-category').on('change', function(e) {
		e.preventDefault();
		var cat = {};
		cat.id = $(this).attr('id');
		cat.classes = $('#product-class');
		fetchAllClass(cat);		
	});
}

function fetchAllClass(cat) {
	var req = {};
	req.action = 'fetchAllClass';
	req.category_id = $('#'+cat.id).val();

	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		// console.log(res);
		res = $.parseJSON(res);
		if(res.status == 1) {
			res.target = cat.classes;
			fillClasses(res);
			// toastr.success(res.message);
		}
		else {
			toastr.error(res.message);
		}
	});		
}

fillClasses = function(data) {
	html = '<option value="0"> Select Class </option>';
	$.each(data.classes, function(i, cls) {
		html += '<option value="'+cls.id+'">'+cls.name+'</option>';
	});
	$(data.target).html(html);
}
