
$(function() {
	fetchAllCategories();
	Product.fetchAllSpecification();
	setEventLoadSubCategories();
	
	Product.addProduct();
	setEventLoadClasses();

	CKEDITOR.config.customConfig = 'basic-toolbar-config.js';
	CKEDITOR.disableAutoInline = true;
	CKEDITOR.replace('small-description', { height: 100 });
	CKEDITOR.replace('detailed-description', { height: 100 });

	// imagePreview($('#product-image'), $('#product-image-preview'));
	setJcrop($('#product-image'), $('#crop-image'), updateCords, 1.5,1); // add category jcrop
	//specs
	bindEvent();
});

var specCount = 0;
var specs = [0];

var Product = {};

Product.addProduct = function() {
	$('#add-product-form').off('submit');
	$('#add-product-form').on('submit', function(e) {
		e.preventDefault();
		CKEDITOR.instances['small-description'].updateElement();
		CKEDITOR.instances['detailed-description'].updateElement();
		
		if (Product.validateAddProduct()) {
			
			var specStr = getSpecString();
			$('textarea[name=specs]').text(specStr);
			
			var req = {};
			req.action = 'addProduct';
			req.specs = specStr;
			$.ajax({
				'type'	:	'post',
				'url'	:	FileEndPoint,
				// 'data'	:	JSON.stringify(req)
				data: new FormData($('#add-product-form').get(0)),
		    	processData: false,
				contentType: false

			}).done(function(res) {
				
				res = $.parseJSON(res);
				//console.log(res);
				if(res.status == 1) {
					// Product.fetchAllCategories();
					toastr.success(res.message);
					//$('#add-class').modal('hide');
					setTimeout(function() {document.location = "manage-product.php";}, 1000);
					
				}
				else {
					toastr.error(res.message);
					setTimeout(function() {document.location = "manage-product.php";}, 1000);
				}
			});
		};
	});
}

Product.validateAddProduct = function() {

	cat = $('#categories').val();
	if(cat=='0'){
		toastr.error("Please Select Category.");
		return false;
	}
	subcat = $('#sub-category').val();
	if(subcat=='0'){
		toastr.error("Please Select Sub-Category.");
		return false;
	}
	name = $('#product-name').val();
	if(name==''){
		toastr.error("Please Enter Name.");
		return false;
	}
	var sd = CKEDITOR.instances['small-description'].getData().replace(/<[^>]*>/gi, '').length;	
	if(!sd){
		toastr.error("Please Enter Small Description.");
		return false;
	}
	img = $('#product-image').val();
	if(img==''){
		toastr.error("Please Select Product Image.");
		return false;
	}
	for(i=0;i<specs.length;i++){
		if($('#spec'+specs[i]).val()==0){
			toastr.error("Please select specification.");
			return false;
		}
	}
	for(i=0;i<specs.length;i++){
		count=0;
		for(j=0;j<specs.length;j++){

			a=$('#spec'+specs[i]).val();
			b=$('#spec'+specs[j]).val();
			if(a==b){
				count++;
			}
		}
		if(count>1){
			toastr.error("Duplicate specification.");
			return false;
		}

	}
	return true;
}

bindEvent = function() {

	addSpec();	
	subSpec();
	if(specs.length > 1){
		$('.btn-add-specifications:last').show();
		$('.btn-add-specifications:not(:last)').hide();
		$('.btn-minus-specifications').show();

	}else if(specs.length == 1 ){

		$('.btn-add-specifications').show();
		$('.btn-minus-specifications').hide();
	}
	
}

addSpec = function() {
	
	$('.btn-add-specifications').off('click');
	$('.btn-add-specifications').on('click',function(e) {

		e.preventDefault();
		specCount++;
		html = "";
		html += '<div class="form-group specifications"><div class="row"><div class="col-md-5">';
		html += '<Select type="text" id="spec'+specCount+'" class="Specification form-group form-control">'+SpecificationsHtml+'</select>';
		html += '</div><div class="col-md-6">';
		html += '<textarea name="value'+specCount+'" class="form-control" ></textarea>';
		//html += '<input type="text" name="value'+specCount+'" class="form-control">';
		html += '</div><div class="col-md-1">';
		html += '<a class="btn-minus-specifications" val="'+specCount+'"><i class="fa fa-trash-o fa-2x"></i></a>';
		html += ' <a class="btn-add-specifications pull-right" val="'+specCount+'"><i class="fa fa-plus-circle fa-2x"></i></a>';
		html += '</div></div></div>';
		
		$(this).parents('div.specifications-border').append(html);
		specs.push(specCount);
		bindEvent();
		//console.log(specs);		
	});	
	
}

subSpec = function() {
	
	if(specs.length > 1){
		$('.btn-minus-specifications').off('click');
		$('.btn-minus-specifications').on('click', function(e) {
			e.preventDefault();
			if(specs.length > 1) {
				if (confirm('Are you sure want to delete specification.')) {
					var a = parseInt($(this).attr('val'));
					var index = specs.indexOf(a);
					if (index > -1) {
			   			specs.splice(index, 1);
					}
					$(this).parent().parent().get(0).remove();
				}
				
				bindEvent();
			}
			
		});				
	
	}
	
	
	
}

function getSpecString() {
	
	var valStr = '{';
	for(i=0;i<specs.length;i++){
		console.log(specs[i]);

		if($('#spec'+specs[i]).val()!="" || $('textarea[name=value'+specs[i]+']').val()!=""){
			value=$('textarea[name=value'+specs[i]+']').val().replace(/\n/g,'<br />');
			//console.log(value);
			valStr += '"'+$('#spec'+specs[i]).val()+'": "'+value+'",';
		}

		// if($('#spec'+specs[i]).val()!="" || $('input[name=value'+specs[i]+']').val()!=""){
		// 	valStr += '"'+$('#spec'+specs[i]).val()+'": "'+$('input[name=value'+specs[i]+']').val()+'",';
		// }
	}
	if(specs.length!=0){
		valStr = valStr.substring(0, valStr.length - 1);
	}
	valStr += '}';
	console.log(valStr);
	return(valStr);
}

Product.fetchAllSpecification = function() {
	var req = {};
	req.action = 'fetchAllSpecification';
	req.status = '1';

	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		// console.log(res);
		res = $.parseJSON(res);
		if(res.status == 1) {

			Product.fillSpecifications(res.specs);
			//Cats = res.categories;
			//toastr.success(res.message);
		}
		else {
			toastr.error(res.message);
		}
	});		
}

SpecificationsHtml ='';

Product.fillSpecifications= function(specs) {
	html = '<option value="0">Select Specification</option>';
	$.each(specs, function(i, spec) {
		html += '<option value="'+spec.id+'">'+spec.name+'</option>';
	});
	SpecificationsHtml = html;
	$('.Specification').html(html);
}
