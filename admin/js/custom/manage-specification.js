$(function() {

	Specification.addSpecification();
	Specification.fetchAllSpecification();
	
	Specification.updateSpecification();
	//Specification.filter();
	
})
var Specification = {};



Specification.addSpecification = function() {
	$('#add-spec-form').off('submit');
	$('#add-spec-form').on('submit', function(e) {
		e.preventDefault();
		
		if (Specification.validateAddSpecification()) {
			var req = {};
			
			req.action = 'addSpecification';
			$.ajax({
				'type'	:	'post',
				'url'	:	FileEndPoint,
				// 'data'	:	JSON.stringify(req)
				data: new FormData($('#add-spec-form').get(0)),
		    	processData: false,
				contentType: false

			}).done(function(res) {
				console.log(res);
				res = $.parseJSON(res);
				if(res.status == 1) {
					// Specification.fetchAllCategories();
					toastr.success(res.message);
					$('#add-spec-modal').modal('hide');
					setTimeout(function() {document.location = "manage-specification.php";}, 800);
				}
				else {
					toastr.error(res.message);
					
				}
			});		
		};
	});
}

Specification.validateAddSpecification = function() {

	status = $('#add-status').val();
	if(status=='0'){
		toastr.error("Please Select Status.");
		return false;
	}	
	
	return true;
}


Specification.fetchAllSpecification = function() {
	var req = {};
	req.action = 'fetchAllSpecification';

	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		// console.log(res);
		res = $.parseJSON(res);
		if(res.status == 1) {

			Specification.fillSpecification(res.specs);
			//Cats = res.categories;
			//toastr.success(res.message);
		}
		else {
			toastr.error(res.message);
		}
	});		
}


Specification.fillSpecification = function(specs) {
	html = '';
	$('div#error-msg').html("");
	$.each(specs, function(i, spec) {
		status = (spec.status == 1)? 'Active':'Not-Active';
		html += '<tr data-id="'+spec.id+'"><td>'+spec.name+'</td><td>'+status+'</td><td>';
		if(spec.status == '1'){	       
	        	html += ' <a class="btn btn-danger btn-xs Specification-status" title="Deactivate"><i class="fa fa-times"></i></a>';
	        }else{
				html += ' <a class="btn btn-success btn-xs Specification-status" title="Activate"><i class="fa fa-check"></i></a>';
	        
	        }
		//html += '<td><a class="btn btn-danger btn-xs Specification-status" title="Active"><i class="fa fa-check"></i></a>';
       	html += ' <a data-toggle="modal" href="#edit-spec-modal" class="btn btn-primary btn-xs edit-spec" title="View / Edit"><i class="fa fa-pencil"></i></a></td>';
        //html += ' <a class="btn btn-danger btn-xs delete-spec" title="Delete"><i class="fa fa-trash-o"></i></a></td>';
		html += '</tr>';
	});
	$('#specification tbody').html(html);
	Specification.editSpecification();
	Specification.deleteSpecification();
	Specification.changeSpecificationStatus();
	if(specs.length==0){
		$('div#error-msg').html("0 result found.");
	}
}


Specification.editSpecification = function(argument) {
	$('.edit-spec').off('click');
	$('.edit-spec').on('click', function(e) {
		e.preventDefault();
		spec_id = $(this).parents('tr').data('id');
		$('#edit-spec-id').val(spec_id);

		var req = {};
		req.action = 'getSpecificationDetails';
		req.spec_id = spec_id;
		$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
		}).done(function(res) {
			// console.log(res);
			res = $.parseJSON(res);
			if(res.status == 1) {
				$('#edit-name').val(res.spec.name);
				
				$('#edit-status').val(res.spec.status);
				
				$('#edit-spec-modal').modal('show');
			}
			else {
				toastr.error(res.message);
			}
		});	
	});
}




Specification.validateEditSpecification = function() {

	status = $('#edit-status').val();
	if(status=='0'){
		toastr.error("Please Select Status.");
		return false;
	}	
	
	return true;
}

Specification.updateSpecification = function() {
	$('#edit-form-form').off('submit');
	$('#edit-spec-form').on('submit', function(e) {
		e.preventDefault();
		
		if (Specification.validateEditSpecification()) {
			var req = {};
			req.action = 'updateSpecification';
			$.ajax({
				'type'	:	'post',
				'url'	:	FileEndPoint,
				// 'data'	:	JSON.stringify(req)
				data: new FormData($(this).get(0)),
		    	processData: false,
				contentType: false

			}).done(function(res) {
				// console.log(res);
				res = $.parseJSON(res);
				if(res.status == 1) {
					toastr.success(res.message);
					$('#edit-spec-modal').modal('hide');
					Specification.fetchAllSpecification();					
				}
				else {
					toastr.error(res.message);
					
				}
			});		
		};
	});
}


Specification.deleteSpecification = function() {
	$('.delete-spec').off('click');
	$('.delete-spec').on('click', function(e) {
		e.preventDefault();
		spec_id = $(this).parents('tr').data('id');
		$('#edit-spec-id').val(spec_id);
		name = $(this).parents('tr:eq(0)').find('td:eq(0)').text();

		if (confirm('Are you sure want to delete: '+name)) {
			var req = {};
			req.action = 'deleteSpecification';
			req.spec_id = spec_id;
			$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
			}).done(function(res) {
				// console.log(res);
				res = $.parseJSON(res);
				if(res.status == 1) {
					toastr.info(res.message);
					Specification.fetchAllSpecification();
				}
				else {
					toastr.error(res.message);
				}
			});
		}
	});
}

Specification.changeSpecificationStatus = function() {
	$('.Specification-status').off('click');
	$('.Specification-status').on('click', function(e) {
		e.preventDefault();
		spec_id = $(this).parents('tr:eq(0)').data('id');
		var req = {};
		req.action = 'changeSpecificationStatus';
		req.status = 0;
		if ($(this).find('i').hasClass('fa-times')) {
			req.status = 0;
		};
		if ($(this).find('i').hasClass('fa-check')) {
			req.status = 1;
		};
		req.spec_id = spec_id;
		console.log(req);
		$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
		}).done(function(res) {
			// console.log(res);
			res = $.parseJSON(res);
			if(res.status == 1) {
				toastr.info(res.message);
				Specification.fetchAllSpecification();
			}
			else {
				toastr.error(res.message);
			}
		});	
	});
}

/*

Specification.filter = function() {
	$('#filter').off('submit');
	$('#filter').on('submit', function(e) {
		e.preventDefault();				
			
		$.ajax({
			'type'	:	'post',
			'url'	:	FileEndPoint,
			// 'data'	:	JSON.stringify(req)
			data: new FormData($('#filter').get(0)),
	    	processData: false,
			contentType: false

		}).done(function(res) {
			
			res = $.parseJSON(res);
			//console.log(res);
			if(res.status == 1) {
				Category.fillCategories(res.categories);
				toastr.success(res.message);
			}
			else {
				toastr.error(res.message);
			}
		});
		
	});
}	*/