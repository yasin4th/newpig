var order_id;
$(function() {
	fetchOrders();
	filterOrder();
	$(".dpd1").datepicker();
	$(".dpd2").datepicker();
	$(".dpd1").keypress(function(e) {e.preventDefault();});
	$(".dpd2").keypress(function(e) {e.preventDefault();});
	
})

function fetchOrders() {
	var req = {};
	req.action = 'fetchOrders';
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		// console.log(res);
		res = $.parseJSON(res);
		if(res.status == 1) {
			fillOrders(res.orders);
			// toastr.success(res.message);
		}
		else {
			toastr.error(res.message);
		}
	});		
}

function fillOrders(orders) {
	html = '';
	$('div#error-msg').html("");
	$.each(orders, function(i, order) {
			html += '<tr data-id="'+order.id+'">';
			html += '<td>'+order.first_name+' '+order.last_name+'</td>';
			html += '<td>'+order.email+'</td>';
			html += '<td>'+order.phone_number+'</td>';
			html += '<td>'+order.order_time+'</td>';
			//status = (order.status == 1)? 'Open':'In-Progress';
			html += '<td>'+order.statusName+'</td><td>';			
			html += '<a data-toggle="modal" href="#change-status" class="btn btn-success btn-xs" title="Change Status"><i class="fa fa-exchange"></i></a>';
			html += ' <a href="product-order-details.php?order_id='+order.id+'" class="btn btn-primary btn-xs" title="Product Order Details"><i class="fa fa-info-circle"></i></a> </td>';
			html += '</tr>';
	});
	// console.log(html);

	if(orders.length==0){
		$('div#error-msg').html("0 result found.");
	}
	$('#orders-table tbody').html(html);
	bindEvent();
	changeOrderStatus();
}

changeOrderStatus = function() {
	$('#change').off('click');
	$('#change').on('click', function(e) {
		
		e.preventDefault();
		//order_id = $(this).parents('tr:eq(0)').data('id');
		var req = {};
		req.action = 'changeOrderStatus';
		req.status = $('#status').val();
		
		req.order_id = order_id;
		//alert(req);
		//console.log(req);
		$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
		}).done(function(res) {
			// console.log(res);
			res = $.parseJSON(res);
			if(res.status == 1) {
				toastr.info(res.message);
				window.location = "manage-order.php";
			}
			else {
				toastr.error(res.message);
			}
		});	
	});
}

bindEvent =function() {
	$('a[href=#change-status]').off('click');
	$('a[href=#change-status]').on('click', function(e) {
		e.preventDefault();
		order_id = $(this).parents('tr:eq(0)').data('id');
	});
}

filterOrder = function() {

	$('#filter').off('submit');
	$('#filter').on('submit', function(e) {
		e.preventDefault();				
		$.ajax({
			'type'	:	'post',
			'url'	:	FileEndPoint,
			// 'data'	:	JSON.stringify(req)
			data: new FormData($('#filter').get(0)),
	    	processData: false,
			contentType: false

		}).done(function(res) {
			
			res = $.parseJSON(res);
			console.log(res);
			if(res.status == 1) {
				fillOrders(res.orders);
				//toastr.success(res.message);
			}
			else {
				//toastr.error(res.message);
			}
		});		
	});
}	

