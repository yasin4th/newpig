var categorys, category_id;
$(function() {

	CKEDITOR.config.customConfig = 'basic-toolbar-config.js';
	CKEDITOR.disableAutoInline = true;
	CKEDITOR.replace('details-add', { height: 100 });
	CKEDITOR.replace('details-edit', { height: 100 });


	fetchAllCategories();
	//addClass();
	//filterClasses();
	//setEventLoadSubCategories();
	//fetchAllClass();
	//updateCategory();
	setJcrop($('#class-image'), $('#crop-image'), updateCords, 1.5, 1) // add category jcrop
	setJcrop($('#edit-class-image'),  $('#crop-image-edit'), updateCords1, 1.5, 0) // edit category jcrop

})

function fetchAllCategories() {
	var req = {};
	req.action = 'fetchAllCategories';

	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		// console.log(res);
		res = $.parseJSON(res);
		if(res.status == 1) {
			categorys = res.categories;
			fillCategories(res.categories);
			addClass();
			filterClasses();
			setEventLoadSubCategories();
			fetchAllClass();
			updateCategory();
			//alert("hello");
			//toastr.success(res.message);
		}
		else {
			toastr.error(res.message);
		}
	});		
}

function fillCategories(categories) {
	//catagories = categories;
	html = '<option value="0"> Select Category </option>';
	$.each(categories, function(i, category) {
		if (category.parent_id == 0) {
			html += '<option value="'+category.id+'"> '+category.name+' </option>';
		};
	});
	$('.categories').html(html);
	$('select[name=catname]').html(html);
}

function addClass() {
	$('#add-class-form').off('submit');
	$('#add-class-form').on('submit', function(e) {
		e.preventDefault();
		
		CKEDITOR.instances['details-add'].updateElement();

		if (validateAddClass()) {
			var req = {};
			
			req.action = 'addClass';
			$.ajax({
				'type'	:	'post',
				'url'	:	FileEndPoint,
				// 'data'	:	JSON.stringify(req)
				data: new FormData($(this).get(0)),
		    	processData: false,
				contentType: false

			}).done(function(res) {
				//console.log(res);
				res = $.parseJSON(res);
				if(res.status == 1) {
					fetchAllClass();
					toastr.success(res.message);
					//$('#add-class').modal('hide');
					//$(this)[0].reset();
					setTimeout(function() {document.location = "manage-class.php";}, 1000);
				}
				else {
					toastr.error(res.message);
				}
			});		
		};
	});
}

function validateAddClass() {

	cat = $('#categories').val();
	if(cat == '0'){
		toastr.error("Please Select Category");
		return false;
	}
	subcat = $('#sub-categories').val();
	if(subcat == '0'){
		toastr.error("Please Select Sub-Category");
		return false;
	}
	status = $('#class-status').val();
	if(status=='0'){
		toastr.error("Please Select Status");
		return false;
	}
	return true;
}


function setEventLoadSubCategories() {
	$('.categories').off('change');
	$('.categories').on('change', function(e) {
		e.preventDefault();
		var cat = {};
		cat.id = $(this).attr('id');
		cat.sub_cat = $(this).parents('.row:eq(0)').find('select:eq(1)');
		fillSubCategories(cat);		
	});
	$('select[name=catname]').off('change');
	$('select[name=catname]').on('change', function(e) {
		e.preventDefault();
		var cat = {};
		cat.id = $(this).val();		
		fillSubCategories1(cat.id);
	});
}

function fillSubCategories(cat) {
	//console.log(cat);
	html = '<option value="0"> Select Sub-Categories </option>';
	$.each(categorys, function(i, category) {
		if (category.parent_id == $('#'+cat.id).val() && $('#'+cat.id).val()!='0') {
			html += '<option value="'+category.id+'">'+category.name+'</option>';
		};
	});
	cat.sub_cat.html(html);
}
function fillSubCategories1(cat) {
	//console.log(cat);
	html = '<option value="0"> Select Sub-Categories </option>';
	$.each(categorys, function(i, category) {
		if (category.parent_id == cat) {
			html += '<option value="'+category.id+'"> '+category.name+' </option>';
		};
	});
	$('select[name=subcatname').html(html);
}

function fetchAllClass() {
	var req = {};
	req.action = 'fetchAllClass';

	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		// console.log(res);
		res = $.parseJSON(res);
		if(res.status == 1) {
			fillClasses(res.classes);
			// toastr.success(res.message);
		}
		else {
			toastr.error(res.message);
		}
	});		
}

fillClasses = function(classes) {
	html = '';
	$('div#error-msg').html("");
	$.each(classes, function(i, cls) {
		html += '<tr data-id="'+cls.id+'">';
		var cat = "";
		var subcat = "";
		var subid = "";
		$.each(categorys, function(i, category){
			if( category.id==cls.category_id){
				subcat=category.name;
				subid=category.parent_id;
			}
		});
		$.each(categorys, function(is, category){
			if( category.id==subid){
				cat=category.name;				
			}
		});
        html += '<td>'+cls.category+'</td>';
        html += '<td>'+cls.sub_category+'</td>';
        html += '<td>'+cls.name+'</td>';
        html += '<td>'+cls.description+'</td>';
        html += '<td> <img src="../'+cls.image+'" class="img-responsive" alt="'+cls.name+'" style="width: 50px;"> </td>';
        status = (cls.status == 1)? 'Active':'Not-Active';
        html += '<td>'+status+'</td>';
        html += '<td><a class="edit-class btn btn-primary btn-xs" title="View / Edit"><i class="fa fa-pencil"></i></a> <a class="delete-class btn btn-danger btn-xs" title="Delete"><i class="fa fa-trash-o "></i></a></td>';
        html += '</tr>';
	});
	$('#class-table tbody').html(html);
	editClass();
	deleteClass();
	if(classes.length==0){
		$('div#error-msg').html("0 result found.");
	}
}



editClass = function(argument) {
	$('.edit-class').off('click');
	$('.edit-class').on('click', function(e) {
		e.preventDefault();
		class_id = $(this).parents('tr').data('id');
		$('#edit-class-id').val(class_id);

		var req = {};
		req.action = 'getClassDetails';
		req.class_id = class_id;
		$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
		}).done(function(res) {
			// console.log(res);
			res = $.parseJSON(res);
			if(res.status == 1) {
				$.each(categorys, function(i, category) {
					if (category.id == res.class.category_id) {
						category_id = category.parent_id
					};
				});

				$('#edit-class-category').val(category_id).change();
				$('#edit-class-sub-category').val(res.class.category_id);
				$('#edit-class-name').val(res.class.name);
				$('#edit-class-description').val(res.class.description);
				$('#edit-class-status').val(res.class.status);
				$('#edit-class-preview-image').attr('src', '../'+res.class.image);
				$('#meta-title-edit').val(res.class.meta_title);
				$('#meta-keywords-edit').val(res.class.meta_keywords);
				$('#meta-description-edit').val(res.class.meta_description);
				CKEDITOR.instances['details-edit'].setData( res.class.details );
				$('#view-edit-class-modal').modal('show');
			}
			else {
				toastr.error(res.message);
			}
		});	
	});
}


updateCategory = function() {
	$('#edit-category-form').off('submit');
	$('#edit-category-form').on('submit', function(e) {
		e.preventDefault();
		CKEDITOR.instances['details-edit'].updateElement();
		if (updateClassValidation()) {
			var req = {};
			req.action = 'updateClass';
			$.ajax({
				'type'	:	'post',
				'url'	:	FileEndPoint,
				// 'data'	:	JSON.stringify(req)
				data: new FormData($(this).get(0)),
		    	processData: false,
				contentType: false

			}).done(function(res) {
				// console.log(res);
				res = $.parseJSON(res);
				if(res.status == 1) {
					toastr.success(res.message);
					fetchAllClass();
					$('#view-edit-class-modal').modal('hide');
				}
				else {
					toastr.error(res.message);
				}
			});		
		};
	});
}

function updateClassValidation() {

	cat = $('#edit-class-category').val();
	if(cat == '0'){
		toastr.error("Please Select Category");
		return false;
	}
	subcat = $('#edit-class-sub-category').val();
	if(subcat == '0'){
		toastr.error("Please Select Sub-Category");
		return false;
	}
	status = $('#edit-class-status').val();
	if(status=='0'){
		toastr.error("Please Select Status");
		return false;
	}
	return true;
}


deleteClass = function() {
	$('.delete-class').off('click');
	$('.delete-class').on('click', function(e) {
		e.preventDefault();
		class_id = $(this).parents('tr').data('id');
		
		name = $(this).parents('tr:eq(0)').find('td:eq(0)').text();

		if (confirm('Are you sure want to delete: '+name)) {
			var req = {};
			req.action = 'deleteClass';
			req.class_id = class_id;
			$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
			}).done(function(res) {
				// console.log(res);
				res = $.parseJSON(res);
				if(res.status == 1) {
					fetchAllClass();
					toastr.info(res.message);
				}
				else {
					toastr.error(res.message);
				}
			});
		}	
	});
}

filterClasses = function() {

	$('#filter').off('submit');
	$('#filter').on('submit', function(e) {
		e.preventDefault();				
		$.ajax({
			'type'	:	'post',
			'url'	:	FileEndPoint,
			// 'data'	:	JSON.stringify(req)
			data: new FormData($('#filter').get(0)),
	    	processData: false,
			contentType: false

		}).done(function(res) {
			
			res = $.parseJSON(res);
			console.log(res);
			if(res.status == 1) {
				fillClasses(res.classes);
				//toastr.success(res.message);
			}
			else {
				//toastr.error(res.message);
			}
		});		
	});
}	
