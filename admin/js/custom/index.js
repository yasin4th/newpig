$(function() {
	User.login();
	User.forgotPassword();
})

var User = {};

User.login = function() {
	$('#admin-login-form').off('submit');
	$('#admin-login-form').on('submit', function(e) {
		e.preventDefault();

		email = $('#e-mail').val();
		password = $('#password').val();
		$('#show-error').css('color', 'rgba(255, 165, 0, 0.98)').text('Please Wait...').show("slow");

		if (User.validateLogin()) {
			var req = {};
			req.email = email;
			req.password = password;
			req.action = 'adminLogin';
			$.ajax({
				'type'	:	'post',
				'url'	:	EndPoint,
				'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				if(res.status == 1) {
					console.log(res);
					$('#show-error').css('color', 'rgb(91, 199, 80)').text(res.message).show("slow");
					
					setTimeout(function() {document.location = "redirect.php";}, 1000);
				}
				else {
					$('#show-error').css('color', 'rgba(255, 0, 0, 0.87)').text(res.message).show("slow");				}
			});	
	
		};
	});
}

User.validateLogin = function() {

	var	email = $('#e-mail').val();
	var	password = $('#password').val();
	var check = true;
	unsetError($('#e-mail'));
	unsetError($('#password'));

	if (email == '') {
		setError($('#e-mail'), 'Please fill correct email.');
		check = false;
	};
	if (password == '') {
		setError($('#password'), 'Please fill password.');
		check = false;
	};

	return check;
}

User.forgotPassword = function() {
	
	$('#forgot-password-form').off('submit');
	$('#forgot-password-form').on('submit', function(e) {
		e.preventDefault();

		email = $('#fg-email').val();
		unsetError($('#fg-email'));
		
		if (email != '') {
			var req = {};
			req.email = email;
			req.action = 'forgotPassword';
			$.ajax({
				'type'	:	'post',
				'url'	:	EndPoint,
				'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				if(res.status == 1) {
					console.log(res);
					$('#forgot-password-modal').modal("hide");
					$('#show-error').css('color', 'rgb(91, 199, 80)').text(res.message);
					$('#show-error').show("slow");
				}
				else {
					$('#forgot-password-modal').modal("hide");
					$('#show-error').css('color', 'rgba(255, 0, 0, 0.87)').text(res.message);
					$('#show-error').show("slow");
				}
			});
		}
		else {
			setError($('#fg-email'), 'Please fill your E-mail.');
		};
	});
}

