$(function() {
	CKEDITOR.config.customConfig = 'basic-toolbar-config.js';
	CKEDITOR.disableAutoInline = true;
	CKEDITOR.replace('description', { height: 200 });

	fetchAllCategories();
	fetchAllProducts();
	setEventLoadSubCategories();
	setEventLoadProducts();
	addArticleForm();
	
})


addArticleForm = function() {
	$('#add-article-form').off('submit');
	$('#add-article-form').on('submit', function(e) {
		e.preventDefault();
		
		if (validateAddArticle()) {
			var req = {};
			req.action = 'addArticle';
			
			req.name = $('#title').val();
			req.description = CKEDITOR.instances['description'].getData();//$('#description').val();
			req.category_id = $('#categories').val();
			req.sub_category_id = $('#sub-category').val();
			req.product_id = $('#select-products').val();
			req.status = $("#status").val();
			
			$.ajax({
				'type'	:	'post',
				'url'	:	EndPoint,
				'data'	:	JSON.stringify(req)
			}).done(function(res) {
				console.log(res);
				res = $.parseJSON(res);
				if(res.status == 1) {
					toastr.success(res.message);
					setTimeout(function() {document.location = "solution-to-your-problem.php";}, 1000);
				}
				else {
					toastr.error(res.message);
					setTimeout(function() {document.location = "solution-to-your-problem.php";}, 1000);
				}
			});		
		};
	});
}

validateAddArticle = function() {
	
	status = $('#status').val();
	if(status=='-1'){
		toastr.error("Please Select Status");
		return false;
	}
	var sd = CKEDITOR.instances['description'].getData().replace(/<[^>]*>/gi, '').length;	
	if(!sd){
		toastr.error("Please Enter Description.");
		return false;
	}

	return true;
}
