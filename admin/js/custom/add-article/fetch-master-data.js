var Categories, Products;
fetchAllCategories = function() {
	var req = {};
	req.action = 'fetchAllCategories';

	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		// console.log(res);
		res = $.parseJSON(res);
		if(res.status == 1) {
			Categories = res.categories;
			fillCategories(res.categories);
			// toastr.success(res.message);
		}
		else {
			toastr.error(res.message);
		}
	});		
}


fillCategories = function(categories) {
	html = '<option value="0"> Select Category </option>';
	$.each(categories, function(i, category) {
		if (category.parent_id == 0) {
			html += '<option value="'+category.id+'"> '+category.name+' </option>';
		};
	})
	$('.categories').html(html);
}


setEventLoadSubCategories = function() {
	$('#categories').off('change');
	$('#categories').on('change', function(e) {
		e.preventDefault();

		var cat = {};
		cat.id = $(this).attr('id');
		cat.sub_cat = $('#sub-category');
		fillSubCategories(cat);
	});
}

fillSubCategories = function(cat) {

	html = '<option value="0"> Select Sub-Categories </option>';
	$.each(Categories, function(i, category) {
		if (category.parent_id == $('#'+cat.id).val() && $('#'+cat.id).val()!='0') {
			html += '<option value="'+category.id+'">'+category.name+'</option>';
		};
	});
	//console.log(html);
	cat.sub_cat.html(html);
}

fetchAllProducts = function() {
	var req = {};
	req.action = 'fetchAllProducts';

	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		// console.log(res);
		res = $.parseJSON(res);
		if(res.status == 1) {
			Products = res.products;
			console.log(Products);
			//fillAllProducts(res.products);
			// toastr.success(res.message);
		}
		else {
			toastr.error(res.message);
		}
	});		
}

setEventLoadProducts = function() {
	$('#sub-category').off('change');
	$('#sub-category').on('change', function(e) {
		//alert("hi");
		e.preventDefault();
		var cat = {};
		cat.id = $(this).attr('id');
		cat.sub_cat = $('#select-products');
		fillProducts(cat);
	});
}

fillProducts = function(cat) {
	html = '<option value="0"> Select Product </option>';
	$.each(Products, function(i, product) {
		if (product.category_id == $('#'+cat.id).val()  && $('#'+cat.id).val()!='0') {
			html += '<option value="'+product.id+'"> '+product.name+' </option>';
		}
	})
	cat.sub_cat.html(html);
}
