$(function() {
	User.fetchUsers();
	
})

var User = {};

User.fetchUsers = function() {
	var req = {};
	req.action = 'getUsers';

	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		// console.log(res);
		res = $.parseJSON(res);
		if(res.status == 1) {
			users = res.users;
			User.fillUsers(users);
			// toastr.success(res.message);
		}
		else {
			toastr.error(res.message);
		}
	});		
}

User.fillUsers = function(users) {
	html = "";
	var count = 0;
	$.each(users, function(i, user) {
		//if (product.parent_id == 0) {
			html += '<tr data-id="'+user.id+'">';
	        // html += '<td>'+count+'</td>';
	        html += '<td>'+user.first_name+' '+user.last_name+'</td>';
	        html += '<td>'+user.email+'</td>';
	        html += '<td>'+user.phone+'</td>';
	        
	        html += '<td>'+user.desc+'</td>';
	        //html += '<td>'+user.status+'</td>';
	        
	        status = (user.status == "1")? 'Active':'Not Active';
	        html += '<td>'+status+'</td>';
	        html += '<td><a href="edit-user.php?user-id='+user.id+'" class="btn btn-primary btn-xs" title="View / Edit"><i class="fa fa-pencil"></i></a>';
	        if(user.status == '1'){	       
	        	html += ' <a class="btn btn-success btn-xs user-status" title="Deactivate"><i class="fa fa-times"></i></a>';
	        }else{
				html += ' <a class="btn btn-success btn-xs user-status" title="Deactivate"><i class="fa fa-check"></i></a>';
	        
	        }
	        html += ' <a class="btn btn-danger btn-xs delete-user" title="Delete"><i class="fa fa-trash-o "></i></a>';
	        html += '</td>';
	        html += '</tr>';
	        count++;
		
	});

	if(count==0){
		$('div#error-msg').html("0 result found.");
	}
	$('#users-table tbody').html(html);
	User.deleteUser();
	User.changeUserStatus();
}

User.deleteUser = function() {
	$('.delete-user').off('click');
	$('.delete-user').on('click', function(e) {
		e.preventDefault();
		user_id = $(this).parents('tr:eq(0)').data('id');
		name  = $(this).parents('tr:eq(0)').find('td:eq(4)').text();
		if (confirm('Are you sure want to delete: '+name)) {
			var req = {};
			req.action = 'deleteUser';
			req.user_id = user_id;
			$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
			}).done(function(res) {
				// console.log(res);
				res = $.parseJSON(res);
				if(res.status == 1) {
					toastr.info(res.message);
					User.fetchUsers();
				}
				else {
					toastr.error(res.message);
				}
			});	
		};
	});
}


User.changeUserStatus = function() {
	$('.user-status').off('click');
	$('.user-status').on('click', function(e) {
		e.preventDefault();
		user_id = $(this).parents('tr:eq(0)').data('id');
		var req = {};
		req.action = 'changeUserStatus';
		req.status = 0;
		if ($(this).find('i').hasClass('fa-times')) {
			req.status = 0;
		};
		if ($(this).find('i').hasClass('fa-check')) {
			req.status = 1;
		};
		req.user_id = user_id;
		//console.log(req);
		$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
		}).done(function(res) {
			// console.log(res);
			res = $.parseJSON(res);
			if(res.status == 1) {
				toastr.info(res.message);
				User.fetchUsers();
			}
			else {
				toastr.error(res.message);
			}
		});	
	});
}
