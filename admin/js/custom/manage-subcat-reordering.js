var Cats;
$(function() {
	fetchAllCategories();
	fillSubCategories();
	setEvent();
});



fetchAllCategories = function() {
	var req = {};
	req.action = 'fetchAllCategories';

	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		// console.log(res);
		res = $.parseJSON(res);
		if(res.status == 1) {
			Cats = res.categories;
			fillCategories(res.categories);
			$("select").change();	
					
			// toastr.success(res.message);
		}
		else {
			toastr.error(res.message);
		}
	});		
}

fillCategories = function(categories) {
	html = '';
	
	$.each(categories, function(i, category) {
		if (category.parent_id == 0) {
			html += "<option value='"+category.id+"'>"+category.name+"</option>";
			//console.log(category.id);
		};
	});
	$("select").html(html);
	$("select").change();
	/*$("#sortable").sortable({opacity: 0.6});
	$("#sortable").disableSelection();*/	
}
fillSubCategories = function() {
	$('select').off('change');
	$('select').on('change', function(e) {
		var cat = $("select").val();
		html = '';	
		$.each(Cats, function(i, category) {
			if (category.parent_id == cat) {
				html += "<li id='ID_"+category.id+"'><strong>"+category.name+"</strong></li>";
				//console.log(category.id);
			};
		});
		$("#sortable").html(html);
		$("#sortable").sortable({opacity: 0.6});
		$("#sortable").disableSelection();
	});	
}


setEvent = function() {
	$('#save-order').off('click');
	$('#save-order').on('click', function(e) {
		var order = $('#sortable').sortable("serialize");
		var req= {};
		req.action = 'saveSubCategoryOrder';
		req.parent_id = $("select").val();
		req.order = order;
		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			//console.log(res);
			res = $.parseJSON(res);
			if(res.status == 1) {
				fetchAllCategories();			
				toastr.success(res.message);
			}
			else {
				toastr.error(res.message);
			}
		});
	});
}