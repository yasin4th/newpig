$(function() {
	video_id = getUrlParameter('video_id');
	
	setEventLoadSubCategories();
	getVideoDetails();
	updateVideoForm();

});


function getVideoId(url) {
    var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
    var match = url.match(regExp);

    if (match && match[2].length == 11) {
        return match[2];
    } else {
        return 'error';
    }
}


function getVideoDetails() {
	var req = {};
	req.action = 'fetchVideo';
	req.video_id = video_id;
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		// console.log(res);
		res = $.parseJSON(res);
		if(res.status == 1) {
			Categories = res.categories;
			fillCategories(Categories);
			$('#categories').val(res.video.category_id).change();
			
			fillAllProducts(res.products);

			$('#title').val(res.video.name);
			$('#url').val('https://youtube.com/embed/'+res.video.url);
			$('#select-products').val(res.video.product_id);
			$('#sub-category').val(res.video.sub_category_id);
			$('#video-preview').attr('src', 'https://youtube.com/embed/'+res.video.url);
			$('#description').val(res.video.description);
			$('#status').val(res.video.active);		

		}
		else {
			toastr.error(res.message);
		}
	});		
}



updateVideoForm = function() {
	$('#update-video-form').off('submit');
	$('#update-video-form').on('submit', function(e) {
		e.preventDefault();
		
		if (validateEditVideo()) {
			var req = {};
			req.action = 'updateVideo';
			req.video_id = video_id;
			
			
			req.name = $('#title').val();
			req.url = getVideoId($('#url').val());
			req.category_id = $('#categories').val();
			req.sub_category_id = $('#sub-category').val();
			req.product_id = $('#select-products').val();
			req.desc = $('#description').val();
			req.active = $('#status').val();
			
			
			$.ajax({
				'type'	:	'post',
				'url'	:	EndPoint,
				'data'	:	JSON.stringify(req)
			}).done(function(res) {
				console.log(res);
				res = $.parseJSON(res);
				if(res.status == 1) {
					toastr.success(res.message);
					window.location = "manage-videos.php";
				}
				else {
					toastr.error(res.message);
				}
			});		
		};
	});
}

validateEditVideo = function() {
	status = $('#status').val();
	if(status=='-1'){
		toastr.error("Please Select Status");
		return false;
	}
	return true;
}
