var Categories;

fillCategories = function(categories) {
	html = '<option value="0"> Select Category </option>';
	$.each(categories, function(i, category) {
		if (category.parent_id == 0) {
			html += '<option value="'+category.id+'"> '+category.name+' </option>';
		};
	})
	$('.categories').html(html);
}


setEventLoadSubCategories = function() {
	$('.categories').off('change');
	$('.categories').on('change', function(e) {
		e.preventDefault();
		var cat = {};
		cat.id = $(this).attr('id');
		cat.sub_cat = $('#sub-category');
		fillSubCategories(cat);		
	});
}

fillSubCategories = function(cat) {
	html = '<option value="0"> Select Sub-Categories </option>';
	$.each(Categories, function(i, category) {
		if (category.parent_id == $('#'+cat.id).val() && $('#'+cat.id).val()!='0') {
			html += '<option value="'+category.id+'">'+category.name+'</option>';
		};
	});
	cat.sub_cat.html(html);
}

fillAllProducts = function(products) {
	html = '<option value="0"> Select Products </option>';
	$.each(products, function(i, product) {
		html += '<option value="'+product.id+'"> '+product.name+' </option>';
	})
	$('.products').html(html);
}
