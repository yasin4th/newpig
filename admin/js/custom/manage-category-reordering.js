$(function() {
	fetchAllCategories();
	setEvent();
});



fetchAllCategories = function() {
	var req = {};
	req.action = 'fetchAllCategories';

	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		// console.log(res);
		res = $.parseJSON(res);
		if(res.status == 1) {
			fillCategories(res.categories);			
			// toastr.success(res.message);
		}
		else {
			toastr.error(res.message);
		}
	});		
}

fillCategories = function(categories) {
	html = '';
	
	$.each(categories, function(i, category) {
		if (category.parent_id == 0) {
			html += "<li id='ID_"+category.id+"' class='sort'><strong>"+category.name+"</strong></li>";
			console.log(category.id);
		};
	});
	$("#sortable").html(html);
	$("#sortable").sortable({opacity: 0.6});
	$("#sortable").disableSelection();	
}

setEvent = function() {
	$('#save-order').off('click');
	$('#save-order').on('click', function(e) {
		var order = $('#sortable').sortable("serialize");
		var req= {};
		req.action = 'saveCategoryOrder';
		req.order = order;
		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			// console.log(res);
			res = $.parseJSON(res);
			if(res.status == 1) {
				fetchAllCategories();			
				toastr.success(res.message);
			}
			else {
				toastr.error(res.message);
			}
		});
	});
}