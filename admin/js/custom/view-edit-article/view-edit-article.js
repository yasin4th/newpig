var description, editor;
editor = CKEDITOR.replace('description', { height: 200 });
$(function() {
	CKEDITOR.config.customConfig = 'basic-toolbar-config.js';
	CKEDITOR.disableAutoInline = true;

	article_id = getUrlParameter('article_id');
	fetchAllCategories();
	fetchAllProducts();	
	setEventLoadSubCategories();
	setEventLoadProducts();
	
	getArticleDetails();
	updateArticleForm();
	
})


function getArticleDetails() {
	var req = {};
	req.action = 'fetchArticle';
	req.article_id = article_id;
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		// console.log(res);
		res = $.parseJSON(res);
		if(res.status == 1) {
			Categories = res.categories;
			fillCategories(Categories);
			$('#categories').val(res.article.category_id).change();
			
			//fillAllProducts(res.products);

			$('#title').val(res.article.name);
			$('#description').val(res.article.description);
			
			$('#sub-category').val(res.article.sub_category_id).change();
			$('#status').val(res.article.active);
			$('#select-products').val(res.article.product_id);
			$('#description').val(res.article.description);
			//CKEDITOR.instances['description'].setData( res.article.description );
			description = res.article.description;
			editor.on("instanceReady", function(event)
			{
			     CKEDITOR.instances['description'].setData( description );
              	 setTimeout(function() {CKEDITOR.instances['description'].setData( description );},2000);
			     //put your code here
			});
		}
		else {
			toastr.error(res.message);
		}
	});		
}



updateArticleForm = function() {
	$('#update-article-form').off('submit');
	$('#update-article-form').on('submit', function(e) {
		e.preventDefault();
		
		if (validateEditArticle()) {
			var req = {};
			req.action = 'updateArticle';
			req.article_id = article_id;
			
			req.name = $('#title').val();
			req.description = CKEDITOR.instances['description'].getData();
			req.category_id = $('#categories').val();
			req.sub_category_id = $('#sub-category').val();
			req.product_id = $('#select-products').val();
			req.status = $('#status').val();
			
			$.ajax({
				'type'	:	'post',
				'url'	:	EndPoint,
				'data'	:	JSON.stringify(req)
			}).done(function(res) {
				console.log(res);
				res = $.parseJSON(res);
				if(res.status == 1) {
					toastr.success(res.message);
					setTimeout(function() {document.location = "solution-to-your-problem.php";}, 1000);
				}
				else {
					toastr.error(res.message);
					setTimeout(function() {document.location = "solution-to-your-problem.php";}, 1000);
				}
			});		
		};
	});
}

validateEditArticle = function() {

	var name = $('#title').val();
	if(name==''){
		toastr.error("Please Enter Name");
		return false;
	}
	var status = $('#status').val();
	if(status=='-1'){
		toastr.error("Please Select Status");
		return false;
	}
	var sd = CKEDITOR.instances['description'].getData().replace(/<[^>]*>/gi, '').length;	
	if(!sd){
		toastr.error("Please Enter Description.");
		return false;
	}
	return true;
}
