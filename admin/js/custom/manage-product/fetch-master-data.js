var Categories, Cls;
fetchAllCategories = function() {
	var req = {};
	req.action = 'fetchAllCategories';

	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		// console.log(res);
		res = $.parseJSON(res);
		if(res.status == 1) {
			Categories = res.categories;
			fillCategories(res.categories);
			// toastr.success(res.message);
		}
		else {
			toastr.error(res.message);
		}
	});		
}


fillCategories = function(categories) {
	html = '<option value="0"> Select Product Category </option>';
	$.each(categories, function(i, category) {
		if (category.parent_id == 0) {
			html += '<option value="'+category.id+'"> '+category.name+' </option>';
		};
	});
	$('#categories').html(html);
}


setEventLoadSubCategories = function() {
	$('#categories').off('change');
	$('#categories').on('change', function(e) {
		e.preventDefault();
		var cat = {};
		cat.id = $(this).attr('id');
		cat.sub_cat = $('#sub-category');
		fillSubCategories(cat);		
	});
}

fillSubCategories = function(cat) {
	html = '<option value="0"> Select Sub-Categories </option>';
	$.each(Categories, function(i, category) {
		if (category.parent_id == $('#'+cat.id).val() && $('#'+cat.id).val()!='0') {
			html += '<option value="'+category.id+'">'+category.name+'</option>';
		};
	});
	$('#sub-category').html(html);
}

setEventLoadClasses = function() {
	$('#sub-category').off('change');
	$('#sub-category').on('change', function(e) {
		e.preventDefault();
		var cat = {};
		cat.id = $(this).attr('id');
		cat.classes = $('#product-class');
		fetchAllClass(cat);		
	});
}

function fetchAllClass(cat) {
	var req = {};
	req.action = 'fetchAllClass';
	req.category_id = $('#'+cat.id).val();

	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		// console.log(res);
		res = $.parseJSON(res);
		if(res.status == 1) {
			//Cls = cat.classes;
			res.target = cat.classes;
			fillClasses(res);
			// toastr.success(res.message);
		}
		else {
			toastr.error(res.message);
		}
	});		
}

fillClasses = function(data) {
	html = '<option value="0"> Select Class </option>';
	$.each(data.classes, function(i, cls) {
		html += '<option value="'+cls.id+'">'+cls.name+'</option>';
	});
	$(data.target).html(html);
}

fetchAllClasses = function(){
	var req = {};
	req.action = 'fetchAllClass';

	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		// console.log(res);
		res = $.parseJSON(res);
		if(res.status == 1) {
			//console.log(res.classes);
			Cls = res.classes;
			//res.target = cat.classes;
			//fillClasses(res);
			// toastr.success(res.message);
		}
		else {
			//toastr.error(res.message);
		}
	});		
}

var Product = {};

Product.fetchAllProducts = function() {

	var req = {};
	req.action = 'fetchAllProducts';
	req.columns = ['id', 'name', 'part_name', 'active', 'stock', 'image'];
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		// console.log(res);
		res = $.parseJSON(res);
		if(res.status == 1) {
			
			console.log(res);
			Product.fillProducts(res.products);
			// toastr.success(res.message);
		}
		else {
			toastr.error(res.message);
		}
	});
}
Product.fillProducts = function(products) {
	// $('#products-table').dataTable().clear();
	html = "";
	var count = 1;
	$('div#error-msg').html("");
	$.each(products, function(i, product) {
		//if (product.parent_id == 0) {
			html += '<tr data-id="'+product.id+'">';
	        // html += '<td>'+count+'</td>';
	        categoryName = (product.category==null)?"Not Set":product.category;
	        subcategoryName = (product.sub_category==null)?"Not Set":product.sub_category;
	        className = (product.class==null)?"Not Set":product.class;
	        partName = (product.part_name=='')?"Not Set":product.part_name;
	        
	        html += '<td>'+categoryName+'</td>';
	        html += '<td>'+subcategoryName+'</td>';
	        html += '<td>'+className+'</td>';
	        html += '<td>'+product.name+'</td>';
	        html += '<td>'+partName+'</td>';
	        html += '<td> <img src="../'+product.image+'" class="img-responsive" alt="'+product.name+'" style="width: 50px;"> </td>';
	        status = (product.stock == "1")? 'In-Stock':'Out-of-Stock';
	        html += '<td>'+status+'</td>';
	        html += '<td><a href="edit-product.php?edit-product-id='+product.id+'" class="btn btn-primary btn-xs" title="View / Edit"><i class="fa fa-pencil"></i></a>';
	        html += ' <a href="view-product.php?product_id='+product.id+'" class="btn btn-primary btn-xs" title="Product Details"><i class="fa fa-info-circle"></i></a>';
			if(product.active == '1'){
	        	html += ' <a class="btn btn-danger btn-xs product-status" title="Deactivate"><i class="fa fa-times"></i></a>';
	        }else{
				html += ' <a class="btn btn-success btn-xs product-status" title="Activate"><i class="fa fa-check"></i></a>';
	        }
	        html += ' <a class="btn btn-danger btn-xs delete-product" title="Delete"><i class="fa fa-trash-o "></i></a>';
	        html += '</td>';
	        html += '</tr>';
	        count++;
		
	});
	if (typeof oTable != "undefined") {
		oTable.fnDestroy();
	}
	$('#products-table tbody').html(html);

	

	oTable = $('#products-table').dataTable({

        // set the initial value
       	"pageLength": 10,
       	"ordering": true,
        
        //"ordering": false, 

        "language": {
            "lengthMenu": " _MENU_ records"
        },
        "columnDefs": [{ // set default column settings
            'orderable': false,
            'targets': 'no-sort'
        }, {
            "searchable": true,
            "targets": [0]
        }],
        "order": [
            [0, "asc"]
        ] 
    });
		
	// $('#products-table').dataTable();
	//$('#products-table_filter input,#products-table_filter label').remove();
	$('#products-table_filter').remove();
	Product.deleteProduct();
	Product.changeProductStatus();
	if(products.length==0){
		//$('div#error-msg').html("0 result found.");
		html = '<tr class="odd"><td valign="top" colspan="'+$('#products-table thead th').length+'" class="dataTables_empty">No matching records found</td></tr>';
		$('#products-table tbody').html(html);
	}
	
}

Product.deleteProduct = function() {
	$('.delete-product').off('click');
	$('.delete-product').on('click', function(e) {
		e.preventDefault();
		product_id = $(this).parents('tr:eq(0)').data('id');
		name  = $(this).parents('tr:eq(0)').find('td:eq(3)').text();
		if (confirm('Are you sure want to delete: '+name)) {
			var req = {};
			req.action = 'deleteProduct';
			req.product_id = product_id;
			$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
			}).done(function(res) {
				// console.log(res);
				res = $.parseJSON(res);
				if(res.status == 1) {
					toastr.info(res.message);
					Product.fetchAllProducts();
				}
				else {
					toastr.error(res.message);
				}
			});	
		};
	});
}


Product.changeProductStatus = function() {
	$('.product-status').off('click');
	$('.product-status').on('click', function(e) {
		e.preventDefault();
		product_id = $(this).parents('tr:eq(0)').data('id');
		var req = {};
		req.action = 'changeProductStatus';
		req.status = 0;
		if ($(this).find('i').hasClass('fa-times')) {
			req.status = 0;
		};
		if ($(this).find('i').hasClass('fa-check')) {
			req.status = 1;
		};
		req.product_id = product_id;
		console.log(req);
		$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
		}).done(function(res) {
			// console.log(res);
			res = $.parseJSON(res);
			if(res.status == 1) {
				toastr.info(res.message);
				Product.fetchAllProducts();
			}
			else {
				toastr.error(res.message);
			}
		});	
	});
}

filterProducts = function() {
	$('#filter').off('submit');
	$('#filter').on('submit',function(e){
		
		e.preventDefault();				
		$.ajax({
			'type'	:	'post',
			'url'	:	FileEndPoint,
			// 'data'	:	JSON.stringify(req)
			data: new FormData($('#filter').get(0)),
	    	processData: false,
			contentType: false

		}).done(function(res) {
			
			res = $.parseJSON(res);
			console.log(res);
			if(res.status == 1) {
				Product.fillProducts(res.products);
				//fillClasses(res.classes);
				toastr.success(res.message);
			}
			else {
				//toastr.error(res.message);
			}
		});
	});
		
}

getCategoryName = function(id) {
	//alert(id);
	var name = "";

	$.each(Categories, function(i,category){
		if(category.id == id){
			name=category.name;			
		}
	});
	return name;
}

