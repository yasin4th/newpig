var categorys;
$(function function_name (argument) {
	Video.fetchVideos();
	Category.fetchAllCategories();
	Video.filter();

})


var Video = {};
var Category = {};

Video.fetchVideos = function() {
	var req = {};
	req.action = 'fetchVideos';

	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		// console.log(res);
		res = $.parseJSON(res);
		if(res.status == 1) {
			Video.fill(res.video);
			// toastr.success(res.message);
		}
		else {
			toastr.error(res.message);
		}
	});
}

Video.fill = function(videos) {
	html = '';
	$('div#error-msg').html("");
	$.each(videos, function(i, video) {
		html += '<tr data-id="'+video.id+'">';
		// html += '<td>'+(i+1)+'</td>';
		if(video.category==null){
			html += '<td>Not Set</td>';
		}else {html += '<td>'+(video.category)+'</td>';}
		if(video.sub_category==null){
			html += '<td>Not Set</td>';
		}else{html += '<td>'+(video.sub_category)+'</td>';}
		
		html += '<td>'+(video.name)+'</td>';
		html += '<td>'+'<a href="view-edit-video.php?video_id='+video.id+'" class="btn btn-primary btn-xs" title="Edit Video Details"><i class="fa fa-pencil"></i></a> &nbsp;';

		if (video.active == 1) {
			html += '<a class="video-status btn btn-danger btn-xs" title="Deactivate"><i class="fa fa-times"></i> </a>';
		} else{
			html += '<a class="video-status btn btn-success btn-xs" title="Activate"> <i class="fa fa-check"></i></a>';
		};

		html += '&nbsp;<a class="delete-video btn btn-danger btn-xs" title="Delete"><i class="fa fa-trash-o "></i></a> </td>';
		html += '</tr>';
	});
	
	$('#videos-table tbody').html(html);
	Video.deleteVideo();
	Video.changeVideoStatus();
	if(videos.length==0){
		$('div#error-msg').html("0 result found.");
	}
}

Video.filter = function() {

	$('#filter').off('submit');
	$('#filter').on('submit', function(e) {
		e.preventDefault();				
		$.ajax({
			'type'	:	'post',
			'url'	:	FileEndPoint,
			// 'data'	:	JSON.stringify(req)
			data: new FormData($('#filter').get(0)),
	    	processData: false,
			contentType: false

		}).done(function(res) {
			
			res = $.parseJSON(res);
			console.log(res);
			if(res.status == 1) {
				Video.fill(res.video);
				toastr.success(res.message);
			}
			else {
				toastr.error(res.message);
			}
		});		
	});
}



Video.deleteVideo = function() {
	$('.delete-video').off('click');
	$('.delete-video').on('click', function(e) {
		e.preventDefault();
		video_id = $(this).parents('tr:eq(0)').data('id');
		name  = $(this).parents('tr:eq(0)').find('td:eq(2)').text();
		if (confirm('Are you sure want to delete: '+name)) {
			var req = {};
			req.action = 'deleteVideo';
			req.video_id = video_id;
			$.ajax({
				'type'	:	'post',
				'url'	:	EndPoint,
				'data'	:	JSON.stringify(req)
			}).done(function(res) {
				// console.log(res);
				res = $.parseJSON(res);
				if(res.status == 1) {
					toastr.info(res.message);
					Video.fetchVideos();
				}
				else {
					toastr.error(res.message);
				}
			});	
		};
	});
}


Video.changeVideoStatus = function() {
	$('.video-status').off('click');
	$('.video-status').on('click', function(e) {
		e.preventDefault();
		video_id = $(this).parents('tr:eq(0)').data('id');
		var req = {};
		req.action = 'changeVideoStatus';
		req.status = 0;
		if ($(this).find('i').hasClass('fa-times')) {
			req.status = 0;
		};
		if ($(this).find('i').hasClass('fa-check')) {
			req.status = 1;
		};
		req.video_id = video_id;
		$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
		}).done(function(res) {
			// console.log(res);
			res = $.parseJSON(res);
			if(res.status == 1) {
				toastr.info(res.message);
				Video.fetchVideos();
			}
			else {
				toastr.error(res.message);
			}
		});	
	});
}

Category.fetchAllCategories = function() {
	var req = {};
	req.action = 'fetchAllCategories';

	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		// console.log(res);
		res = $.parseJSON(res);
		if(res.status == 1) {
			categorys = res.categories;
			//Category.fillSubCategories(res.categories);
			Category.fillCategories(res.categories);
			setEventLoadSubCategories();
			// toastr.success(res.message);
		}
		else {
			toastr.error(res.message);
		}
	});		
}

Category.fillCategories = function(categories) {
	html = '<option value="0"> Select Category </option>';
	//html = '';
	$.each(categories, function(i, category) {
		if(category.parent_id==0){
		html += '<option value="'+category.id+'"> '+category.name+' </option>';}
	})
	$('#categories').html(html);
}

setEventLoadSubCategories = function() {
	$('#categories').off('change');
	$('#categories').on('change', function(e) {
		e.preventDefault();
		var cat = {};
		cat.id = $(this).attr('id');
		cat.sub_cat = $('#sub-category');
		fillSubCategories(cat);		
	});
}

fillSubCategories = function(cat) {
	html = '<option value="0"> Select Sub-Categories </option>';
	$.each(categorys, function(i, category) {
		if (category.parent_id == $('#'+cat.id).val() && $('#'+cat.id).val()!='0') {
			html += '<option value="'+category.id+'">'+category.name+'</option>';
		};
	});
	$('#sub-category').html(html);
}