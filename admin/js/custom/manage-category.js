$(function() {
	
	CKEDITOR.config.customConfig = 'basic-toolbar-config.js';
	CKEDITOR.disableAutoInline = true;
	CKEDITOR.replace('details-add', { height: 100 });
	CKEDITOR.replace('details-edit', { height: 100 });

	setJcrop($('#category-image'), $('#crop-image'), updateCords, 1.5, 1) // add category jcrop
	setJcrop($('#edit-category-image'),  $('#crop-image-edit'), updateCords1, 1.5, 0) // edit category jcrop
	Category.addCategory();
	Category.fetchAllCategories();
	Category.updateCategory();
	Category.filter();
	
});
var Category = {};
var Cats = '';


Category.addCategory = function() {
	$('#add-category-form').off('submit');
	$('#add-category-form').on('submit', function(e) {
		e.preventDefault();

		CKEDITOR.instances['details-add'].updateElement();		

		if (Category.validateAddCategory()) {
			var req = {};
			
			req.action = 'addCategory';
			$.ajax({
				'type'	:	'post',
				'url'	:	FileEndPoint,
				// 'data'	:	JSON.stringify(req)
				data: new FormData($('#add-category-form').get(0)),
		    	processData: false,
				contentType: false

			}).done(function(res) {
				console.log(res);
				res = $.parseJSON(res);
				if(res.status == 1) {
					//Category.fetchAllCategories();
					toastr.success(res.message);
					$('#add-category-modal').modal('hide');
					setTimeout(function() {document.location = "manage-category.php";}, 1000);
				}
				else {
					toastr.error(res.message);
					
				}
			});		
		};
	});
}

Category.validateAddCategory = function() {

	status = $('#category-status').val();
	if(status=='0'){
		toastr.error("Please Select Status.");
		return false;
	}	
	name = $('#category-name').val();
	if(name==''){
		toastr.error("Please Enter Category Name.");
		return false;
	}
	img = $('#category-image').val();
	if(img==''){
		toastr.error("Please Select Image.");
		return false;
	}

	return true;
}

Category.validateEditCategory = function() {

	status = $('#edit-category-status').val();
	if(status=='0'){
		toastr.error("Please Select Status.");
		return false;
	}	
	name = $('#edit-category-name').val();
	if(name==''){
		toastr.error("Please Enter Category Name.");
		return false;
	}
	// img = $('#category-image').val();
	// if(img==''){
	// 	toastr.error("Please Select Image.");
	// 	return false;
	// }

	return true;
}


Category.fetchAllCategories = function() {
	var req = {};
	req.action = 'fetchAllCategories';

	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		// console.log(res);
		res = $.parseJSON(res);
		if(res.status == 1) {
			Category.fillCategories(res.categories);
			Cats = res.categories;
			// toastr.success(res.message);
		}
		else {
			toastr.error(res.message);
		}
	});		
}

Category.fillCategories = function(categories) {
	html = '';
	$('div#error-msg').html("");
	$.each(categories, function(i, category) {
		if (category.parent_id == 0) {
			html += '<tr data-id="'+category.id+'">';
	        html += '<td>'+category.name+'</td>';
	        html += '<td>'+category.description+'</td>';
	        html += '<td> <img src="../'+category.image+'" class="img-responsive" alt="'+category.name+'" style="width: 50px;"> </td>';
	        status = (category.status == 1)? 'Active':'Not-Active';
	        html += '<td>'+status+'</td>';
	        html += '<td><a class="edit-category btn btn-primary btn-xs" title="View / Edit"><i class="fa fa-pencil"></i></a> <a class="delete-category btn btn-danger btn-xs" title="Delete"><i class="fa fa-trash-o "></i></a></td>';
	        html += '</tr>';
		};
	});
	$('#categories-table tbody').html(html);
	Category.editCategory();
	Category.deleteCategory();
	if(categories.length==0){
		$('div#error-msg').html("0 result found.");
	}
}


Category.editCategory = function(argument) {
	$('.edit-category').off('click');
	$('.edit-category').on('click', function(e) {
		e.preventDefault();
		category_id = $(this).parents('tr').data('id');
		$('#edit-category-id').val(category_id);

		var req = {};
		req.action = 'getCategoryDetails';
		req.category_id = category_id;
		$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
		}).done(function(res) {
			// console.log(res);
			res = $.parseJSON(res);
			if(res.status == 1) {
				$('#edit-category-name').val(res.category.name);
				$('#edit-category-description').val(res.category.description);
				$('#edit-category-status').val(res.category.status);
				$('#edit-category-preview-image').attr('src', '../'+res.category.image);
				$('#meta-title-edit').val(res.category.meta_title);
				$('#meta-keywords-edit').val(res.category.meta_keywords);
				$('#meta-description-edit').val(res.category.meta_description);
				CKEDITOR.instances['details-edit'].setData( res.category.details );
				$('#view-edit-category').modal('show');
			}
			else {
				toastr.error(res.message);
			}
		});	
	});
}


Category.updateCategory = function() {
	$('#edit-category-form').off('submit');
	$('#edit-category-form').on('submit', function(e) {
		e.preventDefault();

		CKEDITOR.instances['details-edit'].updateElement();
		
		if (Category.validateEditCategory()) {
			var req = {};
			req.action = 'updateCategory';
			$.ajax({
				'type'	:	'post',
				'url'	:	FileEndPoint,
				// 'data'	:	JSON.stringify(req)
				data: new FormData($(this).get(0)),
		    	processData: false,
				contentType: false

			}).done(function(res) {
				// console.log(res);
				res = $.parseJSON(res);
				if(res.status == 1) {
					toastr.success(res.message);
					$('#view-edit-category').modal('hide');
					setTimeout(function() {document.location = "manage-category.php";}, 1500);
					//Category.fetchAllCategories();
				}
				else {
					toastr.error(res.message);
					
				}
			});		
		};
	});
}


Category.deleteCategory = function() {
	$('.delete-category').off('click');
	$('.delete-category').on('click', function(e) {
		e.preventDefault();
		category_id = $(this).parents('tr').data('id');
		$('#edit-category-id').val(category_id);
		name = $(this).parents('tr:eq(0)').find('td:eq(0)').text();

		if (confirm('Are you sure want to delete: '+name)) {
			var req = {};
			req.action = 'deleteCategory';
			req.category_id = category_id;
			$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
			}).done(function(res) {
				// console.log(res);
				res = $.parseJSON(res);
				if(res.status == 1) {
					toastr.info(res.message);
					Category.fetchAllCategories();
				}
				else {
					toastr.error(res.message);
				}
			});
		}
	});
}


Category.filter = function() {
	$('#filter').off('submit');
	$('#filter').on('submit', function(e) {
		e.preventDefault();				
			
		$.ajax({
			'type'	:	'post',
			'url'	:	FileEndPoint,
			// 'data'	:	JSON.stringify(req)
			data: new FormData($('#filter').get(0)),
	    	processData: false,
			contentType: false

		}).done(function(res) {
			
			res = $.parseJSON(res);
			//console.log(res);
			if(res.status == 1) {
				Category.fillCategories(res.categories);
				toastr.success(res.message);
			}
			else {
				toastr.error(res.message);
			}
		});
		
	});
}

