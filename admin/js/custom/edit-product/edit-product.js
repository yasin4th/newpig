var product_id, editor, sm_description , de_description;
var product;

CKEDITOR.config.customConfig = 'basic-toolbar-config.js';
CKEDITOR.disableAutoInline = true;
editor1 = CKEDITOR.replace('small-description', { height: 100 });
editor2 = CKEDITOR.replace('detailed-description', { height: 100 });
$(function() {
	
	
	product_id = getUrlParameter('edit-product-id');
	if(product_id == undefined)
		document.location = "manage-product.php";
	$('#edit-product-id').val(product_id);
	fetchAllCategories();
	Product.fetchAllSpecification();
	//setEventLoadSubCategories();
	//setEventLoadClasses();
	
	//getProductDetails();
	Product.update();
	bindEvent();

	// setJcrop($('#product-image'), $('#crop-image'), updateCords, 1.5,0); 
	imagePreview($('#product-image'), $('#product-image-preview'));
});


function getProductDetails() {
	var req = {};
	req.action = 'getProductDetails';
	req.product_id = product_id;
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1) {
			product = res.product;
			//Categories = res.categories.categories;
			//fillCategories(res.categories.categories);
			$('#categories').val(res.product.category_id).change();

			$('#sub-category').val(res.product.sub_category_id).change();
			
			$('#product-name').val(res.product.name);
			$('#product-part-name').val(res.product.part_name);
			$('#product-supplier-name').val(res.product.supplier_part_name);
			$('#quantity').val(res.product.quantity);
			$('#mrp').val(res.product.mrp);
				
			$('#height').val(res.product.height);
			$('#width').val(res.product.width);
			$('#weight').val(res.product.weight);
			$('#product-image-preview').attr('src', '../'+res.product.image);
			
			$('#media-title').val(res.product.media_title);
			$('#media-url').val(res.product.media_url);
			$('#meta-title').val(res.product.meta_title);
			$('#meta-keywords').val(res.product.meta_keywords);
			$('#meta-description').val(res.product.meta_description);
			$('#stock').val(res.product.stock);

			$('#point1').val(res.product.point1);
			$('#point2').val(res.product.point2);
			$('#point3').val(res.product.point3);
			
			// $('#crop-image').attr("src","../"+res.product.image);
			// jQuery(function($) {
	 		//    $('#crop-image').Jcrop();
	 		// });
			//$('#product-image').attr("src","../"+res.product.image);
			//setJcrop($('#product-image'), $('#crop-image'), updateCords, 1.5,0);
			//$('#product-image').change();

			// $('.jcrop-holder img[title="Jcrop Image Preview"]').attr("src","../"+res.product.image);
			// $('.jcrop-holder img[title="Jcrop Image Preview"]').attr("style","display: block; visibility: visible; width: 234px; height: 139px; border: none; margin: 0px; padding: 0px; position: absolute; top: 0px; left: 0px;");
			//$('img[title="Jcrop Image Preview"]').attr("height","100px");
			

			//$('#specs').text(res.product.specs);

			fillSpecs(res.specs);
			//console.log(res.product.specs);
			// $('#small-description').val(res.product.small_description);
			// $('#detailed-description').val(res.product.detailed_description);


			if(res.product.featured=='1'){
				$( "#featured" ).prop( "checked", true );
			}
			
			sm_description = res.product.small_description;
			de_description = res.product.detailed_description;

			// console.log(sm_description);
			// console.log(de_description);
			
			// editor1.on("instanceReady", function(event)
			// {
			//      editor1.setData( sm_description );
			//      //put your code here
			// });

			// editor2.on("instanceReady", function(event)
			// {
			//     editor2.setData( de_description );
			//      //put your code here
			// });
			 editor1.setData( sm_description );
			 editor2.setData( de_description );
			/*CKEDITOR.instances['small-description'].setData( res.product.small_description );
			CKEDITOR.instances['detailed-description'].setData( res.product.detailed_description );*/
			
		}
		else {
			toastr.error(res.message);
		}
	});		
}

var Product = {};

Product.update = function() {
	$('#edit-product-form').off('submit');
	$('#edit-product-form').on('submit', function(e) {
		e.preventDefault();
		CKEDITOR.instances['small-description'].updateElement();
		CKEDITOR.instances['detailed-description'].updateElement();
		
		if (Product.validate()) {
			var req = {};

			var specStr = getSpecString();
			$('textarea[name=specs]').text(specStr);

			req.action = 'updateProduct';
			$.ajax({
				'type'	:	'post',
				'url'	:	FileEndPoint,
				// 'data'	:	JSON.stringify(req)
				data: new FormData($(this).get(0)),
		    	processData: false,
				contentType: false

			}).done(function(res) {
				//console.log(res);
				res = $.parseJSON(res);
				if(res.status == 1) {
					// Product.fetchAllCategories();
					toastr.success(res.message);
					setTimeout(function() {document.location = "manage-product.php";}, 1000);
				}
				else {
					toastr.error(res.message);
					setTimeout(function() {document.location = "manage-product.php";}, 1000);
				}
			});		
		};
	});
}

Product.validate = function() {
	cat = $('#categories').val();
	if(cat=='0'){
		toastr.error("Please Select Category");
		return false;
	}
	subcat = $('#sub-category').val();
	if(subcat=='0'){
		toastr.error("Please Select Sub-Category");
		return false;
	}
	for(i=0;i<specs.length;i++){
		if($('#spec'+specs[i]).val()==0){
			toastr.error("Please select specification.");
			return false;
		}
	}
	for(i=0;i<specs.length;i++){
		count=0;
		for(j=0;j<specs.length;j++){

			a=$('#spec'+specs[i]).val();
			b=$('#spec'+specs[j]).val();
			if(a==b){
				count++;
			}
		}
		if(count>1){
			toastr.error("Duplicate specification.");
			return false;
		}

	}
	
	return true;
}
var specCount = 0;
var specs = [];

bindEvent = function() {
	addSpec();	
	subSpec();
	if(specs.length > 1){
		$('.btn-add-specifications:last').show();
		$('.btn-add-specifications:not(:last)').hide();
		$('.btn-minus-specifications').show();

	}else if(specs.length == 1 ){

		$('.btn-add-specifications').show();
		$('.btn-minus-specifications').hide();
	}
}

fillSpecs = function(specsData) {
	//alert(specs);
	// var json = $.parseJSON(specsData);
	var html = "";
	var keys = $.each(specsData, function(value, spec) {
		  //alert( spec + " "+value);
		//console.log(spec);
		//html += '<tr><td>'+spec+':</td><td>'+value+'</td></tr>';
		
		value = spec.value.replace(/<br\s*[\/]?>/g,'\n');
		// spec.value.replace(/<br\/>/g,'\n');

		html += '<div class="form-group specifications"><div val="spec'+specCount+'" class="row"><div class="col-md-5">';
		html += '<Select id="spec'+specCount+'" class="Specification form-control" >'+SpecificationsHtml+'</Select>';
		html += '</div>';
		html += '<div class="col-md-6">';
		html += '<textarea name="value'+specCount+'" class="form-control" >'+value+'</textarea>';
		//html += '<input type="text" name="value'+specCount+'" class="form-control" value="'+spec.value+'">';
		html += '</div>';
		html += '<div class="col-md-1">';
		html += '<a class="btn-minus-specifications" val="'+specCount+'"><i class="fa fa-trash-o fa-2x"></i></a>';
  		html += '<a class="btn-add-specifications pull-right" val="'+specCount+'"><i class="fa fa-plus-circle fa-2x"></i></a>';
		html += '</div></div></div>';
		// $('Select#spec'+specCount).val(spec.spec_id);
		specs.push(specCount);
		specCount++;
	});
	$('div.specifications').append(html);
	var count = 0;
	$.each(specsData, function(value, spec) {
		$('Select#spec'+count).val(spec.spec_id);
		count++;
	});
	if(count == 0)
	{
		specCount = 0;
		html = '';
		html += '<div class="form-group specifications"><div val="spec'+specCount+'" class="row"><div class="col-md-5">';
		html += '<Select id="spec'+specCount+'" class="Specification form-control" >'+SpecificationsHtml+'</Select>';
		html += '</div>';
		html += '<div class="col-md-6">';
		html += '<textarea name="value'+specCount+'" class="form-control" ></textarea>';
		//html += '<input type="text" name="value'+specCount+'" class="form-control" value="'+spec.value+'">';
		html += '</div>';
		html += '<div class="col-md-1">';
		html += '<a class="btn-minus-specifications" val="'+specCount+'" style="display:none;"><i class="fa fa-trash-o fa-2x"></i></a>';
  		html += '<a class="btn-add-specifications pull-right" val="'+specCount+'"><i class="fa fa-plus-circle fa-2x"></i></a>';
		html += '</div></div></div>';
		$('div.specifications').append(html);
		specs.push(specCount);
	}
	bindEvent();


}


addSpec = function() {
	
	$('.btn-add-specifications').off('click');
	$('.btn-add-specifications').on('click',function(e) {
		e.preventDefault();
		//console.log("hello");
		specCount++;
		html = "";
		html += '<div class="form-group specifications"><div class="row"><div class="col-md-5">';
		html += '<Select type="text" id="spec'+specCount+'" class="Specification form-group form-control">'+SpecificationsHtml+'</select>';		
		html += '</div>';
		html += '<div class="col-md-6">';
		html += '<textarea name="value'+specCount+'" class="form-control" ></textarea>';
		//html += '<input type="text" name="value'+specCount+'" class="form-control" >';
		html += '</div>';
		html += '<div class="col-md-1">';
		html += '<a class="btn-minus-specifications" val="'+specCount+'"><i class="fa fa-trash-o fa-2x"></i></a>';
  		html += '<a class="btn-add-specifications pull-right" val="'+specCount+'"><i class="fa fa-plus-circle fa-2x"></i></a>';
		html += '</div></div></div>';
		$(this).parents('div.specifications-border').append(html);
		
		specs.push(specCount);
		bindEvent();
		
	});


}

subSpec = function() {
	if(specs.length > 1){
		$('.btn-minus-specifications').off('click');
		$('.btn-minus-specifications').on('click', function(e) {
			e.preventDefault();
			if(specs.length > 1) {
				if (confirm('Are you sure want to delete specification.')) {
					var a = parseInt($(this).attr('val'));
					
					var index = specs.indexOf(a);
					if (index > -1) {
			   			specs.splice(index, 1);
					}
					$(this).parent().parent().get(0).remove();
				}
				
				bindEvent();
			}
		});
	}
}

function getSpecString() {
	
	var valStr = '{';
	for(i=0;i<specs.length;i++){
		//console.log(specs[i]);

		if($('input[name=spec'+specs[i]+']').val()!="" || $('textarea[name=value'+specs[i]+']').val()!=""){
			value=$('textarea[name=value'+specs[i]+']').val().replace(/\n/g,'<br />');
			//console.log(value);
			valStr += '"'+$('#spec'+specs[i]).val()+'": "'+value+'",';
		}

		// if($('input[name=spec'+specs[i]+']').val()!="" || $('input[name=value'+specs[i]+']').val()!=""){
		// 	valStr += '"'+$('#spec'+specs[i]).val()+'": "'+$('input[name=value'+specs[i]+']').val()+'",';
		// }
	}
	valStr = valStr.substring(0, valStr.length - 1);
	valStr += '}';
	console.log(valStr);
	return(valStr);
}


Product.fetchAllSpecification = function() {
	var req = {};
	req.action = 'fetchAllSpecification';
	
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		// console.log(res);
		res = $.parseJSON(res);
		if(res.status == 1) {

			Product.fillSpecifications(res.specs);
			//Cats = res.categories;
			//toastr.success(res.message);
		}
		else {
			toastr.error(res.message);
		}
	});		
}

SpecificationsHtml ='';

Product.fillSpecifications= function(specs) {
	html = '<option value="0">Select Specification</option>';
	$.each(specs, function(i, spec) {
		html += '<option value="'+spec.id+'">'+spec.name+'</option>';
	});
	SpecificationsHtml = html;
	$('.Specification').html(html);
}
