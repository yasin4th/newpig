$(function() {
	changePasssword();
})

changePasssword = function() {
	$('#change-password-form').off('submit');
	$('#change-password-form').on('submit', function(e) {
		e.preventDefault();
		console.log('submited')
		if (validateForm()) {
			var req = {};
			req.old_password = $('#old-pass').val();
			req.new_password = $('#new-pass').val();
			req.action = 'changePasssword';
			$.ajax({
				'type'	:	'post',
				'url'	:	EndPoint,
				'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				console.log(res);
				if(res.status == 1){
					$('#message').css('color', 'rgb(91, 199, 80)').text(res.message).show(500);
					resetForm();
				}
				else {
					$('#message').css('color', 'rgba(255, 0, 0, 0.87)').text(res.message).show(1200);
				}

			});
		}
	});
}

validateForm = function() {
	old_password = $('#old-pass').val();
	new_password = $('#new-pass').val();
	confirm_password = $('#confirm-pass').val();
	unsetError($('#old-pass'));
	unsetError($('#new-pass'));
	unsetError($('#confirm-pass'));

	if (old_password == '') {
		setError($('#old-pass'), 'Please fill old password.');
		return false;
	};
	if (new_password == '') {
		setError($('#new-pass'), 'Please fill new password.');
		return false;
	};
	if (confirm_password == '') {
		setError($('#confirm-pass'), 'Please fill confirm password.');
		return false;
	};
	if (new_password != confirm_password) {
		setError($('#confirm-pass'), 'Please fill same passowrd in new password and confirm password.');
		return false;
	};

	return  true;
}

resetForm =function(){
	$('#old-pass').val('');
	$('#new-pass').val('');
	$('#confirm-pass').val('');
}