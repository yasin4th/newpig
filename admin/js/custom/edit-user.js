var user_id;
var User = {};

$(function() {
	user_id = getUrlParameter('user-id')
	$('#edit-user-id').val(user_id);
	
	User.getUserDetails();
	User.update();
	//bindEvent();
});



User.getUserDetails = function() {
	var req = {};
	req.action = 'getUserDetails';
	req.user_id = user_id;
	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		// console.log(res);
		if(res.status == 1) {			
			//console.log(res.user.id);
			$('#first-name').val(res.user.first_name);
			$('#last-name').val(res.user.last_name);
			$('#email').val(res.user.email);
			$('#phone').val(res.user.phone);
			$('#role').val(res.user.role);		
			
		}
		else {
			toastr.error(res.message);
		}
	});		
}

User.update = function() {
	$('#edit-user-form').off('submit');
	$('#edit-user-form').on('submit', function(e) {
		e.preventDefault();
				
		if (User.validate()) {
			var req = {};
			req.user_id = user_id;
			req.fname = $('#first-name').val();
			req.lname = $('#last-name').val();
			req.email = $('#email').val();
			req.phone = $('#phone').val();
			req.role = $('#role').val();

			req.action = 'editUser';
			$.ajax({
				'type'	:	'post',
				'url'	:	EndPoint,
				'data'	:	JSON.stringify(req)
				
			}).done(function(res) {
				//console.log(res);
				res = $.parseJSON(res);
				if(res.status == 1) {
					// User.fetchAllCategories();
					toastr.success(res.message);
				}
				else {
					toastr.error(res.message);
				}
			});		
		};
	});
}

User.validate = function() {

	return true;
}