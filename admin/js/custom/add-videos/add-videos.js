$(function() {
	fetchAllCategories();
	fetchAllProducts();
	setEventLoadSubCategories();
	setEventLoadProducts();
	addVideoForm();
});

function getVideoId(url) {
    var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
    var match = url.match(regExp);

    if (match && match[2].length == 11) {
        return match[2];
    } else {
        return 'error';
    }
}

addVideoForm = function() {
	$('#add-video-form').off('submit');
	$('#add-video-form').on('submit', function(e) {
		e.preventDefault();
		
		if (validateAddVideo()) {
			var req = {};
			req.action = 'addVideo';
			
			req.name = $('#title').val();
			req.url = getVideoId($('#url').val());
			req.category_id = $('#categories').val();
			req.sub_category_id = $('#sub-category').val();
			req.product_id = $('#select-products').val();
			req.desc = $('#video-description').val();
			req.status = $("#status").val();
			
			$.ajax({
				'type'	:	'post',
				'url'	:	EndPoint,
				'data'	:	JSON.stringify(req)
			}).done(function(res) {
				console.log(res);
				res = $.parseJSON(res);
				if(res.status == 1) {
					toastr.success(res.message);
					setTimeout(function() {document.location = "manage-videos.php";}, 1000);
				}
				else {
					toastr.error(res.message);
				}
			});		
		};
	});
}

validateAddVideo = function() {
	
	status = $('#status').val();
	if(status=='-1'){
		toastr.error("Please Select Status");
		return false;
	}	
	return true;
}
