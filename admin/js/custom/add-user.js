$(function() {
	addUser();

})

function addUser() {
	$('#add-user-form').off('submit');
	$('#add-user-form').on('submit', function(e) {
		e.preventDefault();
		
		if (validateUser()) {
			var req = {};
			
			req.fname = $('#first-name').val();
			req.lname = $('#last-name').val();
			req.email = $('#email').val();
			req.password = $('#phone').val();
			req.role = $('#role').val();
			req.status = $('#status').val();
			req.action = 'addUser';		

			$.ajax({
				'type'	:	'post',
				'url'	:	EndPoint,
				'data'	:	JSON.stringify(req)	

			}).done(function(res) {
				console.log(res);
				res = $.parseJSON(res);
				if(res.status == 1) {
					toastr.success(res.message);
					setTimeout(function() {document.location = "manage-role.php";}, 1000);					
				}
				else {
					toastr.error(res.message);
				}
			});		
		};
	});
}

validateUser = function() {

	name = $('#first-name').val();
	if(name==''){
		toastr.error("Please Select Name");
		return false;
	}
	email = $('#email').val();
	if(email==''){
		toastr.error("Please Enter E-mail");
		return false;
	}
	phone = $('#phone').val();
	if(phone==''){
		toastr.error("Please Enter Phone No");
		return false;
	}

	return true;
}