$(function() {

	CKEDITOR.config.customConfig = 'basic-toolbar-config.js';
	CKEDITOR.disableAutoInline = true;
	CKEDITOR.replace('details-add', { height: 100 });
	CKEDITOR.replace('details-edit', { height: 100 });


	Category.fetchAllCategories();

	Category.addCategory();
	Category.updateCategory();
	Category.filter();
	

	setJcrop($('#category-image'), $('#crop-image'), updateCords, 1.5,1) // add category jcrop
	setJcrop($('#edit-category-image'),  $('#crop-image-edit'), updateCords1, 1.5,0) // edit category jcrop

	// imagePreview($('#edit-category-image'), $('#edit-category-preview-image')); // edit category image preview
})
var Category = {};
var Cats;

Category.addCategory = function() {
	$('#add-category-form').off('submit');
	$('#add-category-form').on('submit', function(e) {
		e.preventDefault();

		CKEDITOR.instances['details-add'].updateElement();
		
		if (Category.validateAddCategory()) {
			var req = {};
			
			req.action = 'addCategory';
			$.ajax({
				'type'	:	'post',
				'url'	:	FileEndPoint,
				// 'data'	:	JSON.stringify(req)
				data: new FormData($('#add-category-form').get(0)),
		    	processData: false,
				contentType: false

			}).done(function(res) {
				console.log(res);
				res = $.parseJSON(res);
				if(res.status == 1) {
					//Category.fetchAllCategories();
					toastr.success("Sub-Category added successfully.");
					$('#add-Sub-category').modal('hide');
					setTimeout(function() {document.location = "manage-sub-category.php";}, 800);
				}
				else {
					toastr.error(res.message);
				}
			});		
		}
	});
}

Category.validateAddCategory = function() {

	cat = $('.parent-categories').val();
	if(cat=='0'){
		toastr.error("Please Select Category");
		return false;
	}
	status = $('#category-status').val();
	if(status==0){
		toastr.error("Please Select Status");
		return false;
	}
	return true;
}




Category.editCategory = function(argument) {
	$('.edit-category').off('click');
	$('.edit-category').on('click', function(e) {
		e.preventDefault();

		
		category_id = $(this).parents('tr').data('id');
		$('#edit-category-id').val(category_id);

		var req = {};
		req.action = 'getCategoryDetails';
		req.category_id = category_id;
		$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
		}).done(function(res) {
			// console.log(res);
			res = $.parseJSON(res);
			if(res.status == 1) {
				$('#edit-category-parent-id').val(res.category.parent_id);
				$('#edit-category-name').val(res.category.name);
				$('#edit-category-description').val(res.category.description);
				$('#edit-category-status').val(res.category.status);
				$('#edit-category-preview-image').attr('src', '../'+res.category.image);
				$('#meta-title-edit').val(res.category.meta_title);
				$('#meta-keywords-edit').val(res.category.meta_keywords);
				$('#meta-description-edit').val(res.category.meta_description);
				CKEDITOR.instances['details-edit'].setData( res.category.details );
				$('#view-edit-Sub-category-modal').modal('show');
			}
			else {
				toastr.error(res.message);
			}
		});	
	});
}


Category.updateCategory = function() {
	$('#edit-category-form').off('submit');
	$('#edit-category-form').on('submit', function(e) {
		e.preventDefault();

		CKEDITOR.instances['details-edit'].updateElement();
		
		if (Category.validateEditCategory()) {
			var req = {};
			req.action = 'updateCategory';
			$.ajax({
				'type'	:	'post',
				'url'	:	FileEndPoint,
				// 'data'	:	JSON.stringify(req)
				data: new FormData($(this).get(0)),
		    	processData: false,
				contentType: false

			}).done(function(res) {
				// console.log(res);
				res = $.parseJSON(res);
				if(res.status == 1) {
					toastr.success("Sub-Category Updated.");
					$('#view-edit-Sub-category-modal').modal('hide');
					//Category.fetchAllCategories();
					//setTimeout(function() {document.location = "manage-sub-category.php";}, 800);
				}
				else {
					toastr.error(res.message);
				}
			});		
		};
	});
}

Category.validateEditCategory = function() {

	cat = $('#edit-category-parent-id').val();
	if(cat=='0'){
		toastr.error("Please Select Category");
		return false;
	}
	status = $('#edit-category-status').val();
	if(status==0){
		toastr.error("Please Select Status");
		return false;
	}
	return true;
}


Category.deleteCategory = function(argument) {
	$('.delete-category').off('click');
	$('.delete-category').on('click', function(e) {
		e.preventDefault();
		category_id = $(this).parents('tr').data('id');
		$('#edit-category-id').val(category_id);
		name = $(this).parents('tr:eq(0)').find('td:eq(1)').text();

		if (confirm('Are you sure want to delete: '+name)) {
			var req = {};
			req.action = 'deleteCategory';
			req.category_id = category_id;
			$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
			}).done(function(res) {
				// console.log(res);
				res = $.parseJSON(res);
				if(res.status == 1) {
					toastr.info(res.message);
					Category.fetchAllCategories();	
				}
				else {
					toastr.error(res.message);
				}
			});	
		}
	});
}





Category.fetchAllCategories = function() {
	var req = {};
	req.action = 'fetchAllCategories';

	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		// console.log(res);
		res = $.parseJSON(res);
		if(res.status == 1) {
			Category.fillSubCategories(res.categories);
			Category.fillCategories(res.categories);
			Cats = res.categories;
			// toastr.success(res.message);
		}
		else {
			toastr.error(res.message);
		}
	});		
}


Category.fillSubCategories = function(categories) {
	html = '';
	$('div#error-msg').html("");
	$.each(categories, function(i, category) {
		if (category.parent_id != 0) {
			html += '<tr data-id="'+category.id+'">';
			var name = "";
			$.each(categories, function(i, category1){
				if( category1.id==category.parent_id)
				{
					name=category1.name;
				}
			});
	        html += '<td>'+name+'</td>';
	        html += '<td>'+category.name+'</td>';
	        html += '<td>'+category.description+'</td>';
	        html += '<td> <img src="../'+category.image+'" class="img-responsive" alt="'+category.name+'" style="width: 50px;"> </td>';
	        status = (category.status == 1)? 'Active':'Not-Active';
	        html += '<td>'+status+'</td>';
	        html += '<td><a class="edit-category btn btn-primary btn-xs" title="View / Edit"><i class="fa fa-pencil"></i></a> <a class="delete-category btn btn-danger btn-xs" title="Delete"><i class="fa fa-trash-o "></i></a></td>';
	        html += '</tr>';
		};
	})
	$('#sub-categories-table tbody').html(html);
	Category.editCategory();
	Category.deleteCategory();
	if(categories.length==0){
		$('div#error-msg').html("0 result found.");
	}
}

Category.fillSubCategories1 = function(categories) {
	html = '';
	$('div#error-msg').html("");
	$.each(categories, function(i, category) {
		if (category.parent_id != 0)
		{
			html += '<tr data-id="'+category.id+'">';
	        html += '<td>'+category.cname+'</td>';
	        html += '<td>'+category.name+'</td>';
	        html += '<td>'+category.description+'</td>';
	        html += '<td> <img src="../'+category.image+'" class="img-responsive" alt="'+category.name+'" style="width: 50px;"> </td>';
	        status = (category.status == 1)? 'Active':'Not-Active';
	        html += '<td>'+status+'</td>';
	        html += '<td><a class="edit-category btn btn-primary btn-xs" title="View / Edit"><i class="fa fa-pencil"></i></a> <a class="delete-category btn btn-danger btn-xs" title="Delete"><i class="fa fa-trash-o "></i></a></td>';
	        html += '</tr>';
		};
	})
	$('#sub-categories-table tbody').html(html);
	Category.editCategory();
	Category.deleteCategory();
	if(categories.length==0)
	{
		$('div#error-msg').html("0 result found.");
	}
}

Category.fillCategories = function(categories) {
	html = '<option value="0"> Select Category </option>';	
	$.each(categories, function(i, category) {
		if(category.parent_id==0){
		html += '<option value="'+category.id+'"> '+category.name+' </option>';}
	})
	$('.parent-categories').html(html);
	$('#categories').html(html);
}

Category.filter = function() {

	$('#filter').off('submit');
	$('#filter').on('submit', function(e) {
		e.preventDefault();				
		$.ajax({
			'type'	:	'post',
			'url'	:	FileEndPoint,
			// 'data'	:	JSON.stringify(req)
			data: new FormData($('#filter').get(0)),
	    	processData: false,
			contentType: false

		}).done(function(res) {
			
			res = $.parseJSON(res);
			//console.log(res);
			if(res.status == 1) {
				Category.fillSubCategories1(res.categories);
				toastr.success(res.message);
			}
			else {
				toastr.error(res.message);
			}
		});
		
	});
}

