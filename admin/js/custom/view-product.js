var product_id;

$(function() {
	product_id = getUrlParameter('product_id');
	fetchAllProducts();
	fetchAllCategories();
	setEventLoadSubCategories();
	setEventLoadClasses();
	filterProducts();
	saveRelated();
	setEventCheckAll();	
})


fillProductsInTable = function(products) {
	// $('#products-table').dataTable().clear();
	html = "";
	var count = 1;
	$('div#error-msg').html("");
	$.each(products, function(i, product) {
		if (product.id != product_id) {
			html += '<tr data-id="'+product.id+'">';
	        // html += '<td>'+count+'</td>';
	        categoryName = (product.category==null)?"Not Set":product.category;
	        subcategoryName = (product.sub_category==null)?"Not Set":product.sub_category;
	        className = (product.class==null)?"Not Set":product.class;
	        partName = (product.part_name=='')?"Not Set":product.part_name;
	        html += '<td><input type="checkbox" name ="name'+product.id+'" value="'+product.id+'" class="related"></td>';
	        html += '<td>'+categoryName+'</td>';
	        html += '<td>'+subcategoryName+'</td>';
	        html += '<td>'+className+'</td>';
	        html += '<td>'+product.name+'</td>';
	        html += '<td>'+partName+'</td>';
	        html += '<td> <img src="../'+product.image+'" class="img-responsive" alt="'+product.name+'" style="width: 50px;"> </td>';
	        status = (product.stock == "1")? 'In-Stock':'Out-of-Stock';
	        //html += '<td>'+status+'</td>';
	        /*html += '<td><a href="edit-product.php?edit-product-id='+product.id+'" class="btn btn-primary btn-xs" title="View / Edit"><i class="fa fa-pencil"></i></a>';
	        html += ' <a href="view-product.php?product_id='+product.id+'" class="btn btn-primary btn-xs" title="Product Details"><i class="fa fa-info-circle"></i></a>';
			if(product.active == '1'){
	        	html += ' <a class="btn btn-success btn-xs product-status" title="Deactivate"><i class="fa fa-times"></i></a>';
	        }else{
				html += ' <a class="btn btn-success btn-xs product-status" title="Activate"><i class="fa fa-check"></i></a>';
	        }
	        html += ' <a class="btn btn-danger btn-xs delete-product" title="Delete"><i class="fa fa-trash-o "></i></a>';
	        html += '</td>';*/
	        html += '</tr>';
	        count++;
	    }
		
	});
	/*if (typeof oTable != "undefined") {
		oTable.fnDestroy();
	}*/
	$('#products-table tbody').html(html);

	//oTable = $('#products-table').dataTable();

	// $('#products-table').dataTable();
	//$('#products-table_filter input,#products-table_filter label').remove();
	
	//$('#products-table_filter').remove();
	
	if(products.length==0){
		//$('div#error-msg').html("0 result found.");
		html = '<tr class="odd"><td valign="top" colspan="'+$('#products-table thead th').length+'" class="dataTables_empty">No matching records found</td></tr>';
		$('#products-table tbody').html(html);
	}
	
}



filterProducts = function() {
	$('#filter').off('submit');
	$('#filter').on('submit',function(e){
		
		e.preventDefault();				
		$.ajax({
			'type'	:	'post',
			'url'	:	FileEndPoint,
			// 'data'	:	JSON.stringify(req)
			data: new FormData($('#filter').get(0)),
	    	processData: false,
			contentType: false

		}).done(function(res) {
			
			res = $.parseJSON(res);
			console.log(res);
			if(res.status == 1) {
				fillProductsInTable(res.products);
				//fillClasses(res.classes);
				toastr.success(res.message);
			}
			
		});
	});		
}
fetchAllProducts = function() {
	var req = {};
	req.action = 'getProductDetails';
	req.product_id = product_id;

	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		console.log(res);
		if(res.status == 1) {
			fillProducts(res.product);
			fillSpecs(res.specs);
			fillRelatedProduct(res.related);
			// toastr.success(res.message);
		}
		else {
			toastr.error(res.message);
			//location.replace("manage-product.php");
		}
	});		
}

fillProducts = function(product) {
	$('.product-name').html(product.name);
	

	$('.product-image').attr('src', '../'+product.image);
	$('p.product-part-name').html(product.part_name);
	$('span.mrp').html('A$ '+product.mrp);
	$('span.quantity').html(product.quantity);
	status = (product.stock == "1")? 'In-Stock':'Out-of-Stock';
	$('span.stock').html(status);
	$('span.weight').html(product.weight+' Kg');
	$('#media-title').text(product.media_title);
	$('#media-url').text(product.media_url);
			

	$('#video-preview').attr('src', 'https://youtube.com/embed/'+product.video_url);
	$('.small-description').html(product.small_description);
	$('#description').html('<div class="col-sm-12">'+product.detailed_description+'</div>');

	point_html = '';
	if(product.point1.trim() != '')
	{
		point_html += '<li>'+product.point1+'</li>';
	}
	if(product.point2.trim() != '')
	{
		point_html += '<li>'+product.point2+'</li>';
	}
	if(product.point3.trim() != '')
	{
		point_html += '<li>'+product.point3+'</li>';
	}
	$('.points').html(point_html);
	
	/*var json = $.parseJSON(product.specs);
	var html = "<tbody>";
	var keys = $.map(json, function(value, spec) {
			//alert( spec + " "+value);
		  	html += '<tr><td>'+spec+':</td><td>'+value+'</td></tr>';
	});
	html += '</tbody>';
	$('table#specs').html(html);*/
	if(product.relimg != null){
		html = '<img class="img-responsive" src="'+product.relimg+'" alt="" />';
		$('.related-img').html(html);
	}else{
		$('.related-img').remove();
	}
	if(product.media_url == ''){
		$('.video').remove();
	}
}

fillSpecs = function(specs) {
	//alert("hi");
	var html = "<tbody>";
	$.each(specs, function(i,spec){
		html += '<tr><td>'+spec.name+':</td><td>'+spec.value+'</td></tr>';		
	});
	html += '</tbody>';
	$('table#specs').html(html);
}

fillRelatedProduct = function(related) {
	html = '<table id="" class="table table-bordered table-striped table-condensed">';
	html += '<thead>';
	html += '<tr>';
	//html += '<th><input type="checkbox" id="checkall" disabled="disabled"></th>';
    html += '<th>Product Category</th>';
    html += '<th>Product Sub-Category</th>';
    html += '<th>Product Class</th>';
    html += '<th>Product Name</th>';
    html += '<th>Supplier Part No.</th>';
    html += '<th>Image</th>';                        
    html += '<th>Actions</th>';                        
    html += '</tr>';
    html += '</thead>';
    html += '<tbody>';
	//alert("hi");
	//var count = 1;
	//$('div#error-msg').html("");
	$.each(related, function(i, product) {
		if (product.id != product_id) {
			html += '<tr data-id="'+product.id+'">';
	        
	        categoryName = (product.category==null)?"Not Set":product.category;
	        subcategoryName = (product.sub_category==null)?"Not Set":product.sub_category;
	        className = (product.class==null)?"Not Set":product.class;
	        partName = (product.part_name=='')?"Not Set":product.part_name;
	        //html += '<td><input type="checkbox" checked="checked" name ="name'+product.related_id+'" value="'+product.related_id+'" class="rel"></td>';
	        html += '<td>'+categoryName+'</td>';
	        html += '<td>'+subcategoryName+'</td>';
	        html += '<td>'+className+'</td>';
	        html += '<td>'+product.name+'</td>';
	        html += '<td>'+partName+'</td>';
	        html += '<td><img src="../'+product.image+'" class="img-responsive" alt="'+product.name+'" style="width: 50px;"> </td>';	        
	        //html += '<td><a>Delete</a></td>'
	        html += '<td><a class="btn btn-danger btn-xs delete-rel" data-id="'+product.related_id+'" title="Delete"><i class="fa fa-trash-o "></i></a></td>';
	    }		
	});
	html += '</tbody>';
	html += '</table>';
	/*if (typeof oTable != "undefined") {
		oTable.fnDestroy();
	}*/
	$('#related-product').html(html);
	setEventRel();
	
	/*if(products.length==0){
		//$('div#error-msg').html("0 result found.");
		html = '<tr class="odd"><td valign="top" colspan="'+$('#products-table thead th').length+'" class="dataTables_empty">No matching records found</td></tr>';
		$('#products-table tbody').html(html);
	}*/
}

// populating 
var Categories, Cls;
fetchAllCategories = function() {
	var req = {};
	req.action = 'fetchAllCategories';

	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		// console.log(res);
		res = $.parseJSON(res);
		if(res.status == 1) {
			Categories = res.categories;
			fillCategories(res.categories);
			// toastr.success(res.message);
		}
		else {
			toastr.error(res.message);
		}
	});		
}


fillCategories = function(categories) {
	html = '<option value="0"> Select Category </option>';
	$.each(categories, function(i, category) {
		if (category.parent_id == 0) {
			html += '<option value="'+category.id+'"> '+category.name+' </option>';
		};
	});
	$('#categories').html(html);
}


setEventLoadSubCategories = function() {
	$('#categories').off('change');
	$('#categories').on('change', function(e) {
		e.preventDefault();
		var cat = {};
		cat.id = $(this).attr('id');
		cat.sub_cat = $('#sub-category');
		fillSubCategories(cat);		
	});
}

fillSubCategories = function(cat) {
	html = '<option value="0"> Select Sub Category </option>';
	$.each(Categories, function(i, category) {
		if (category.parent_id == $('#'+cat.id).val() && $('#'+cat.id).val()!='0') {
			html += '<option value="'+category.id+'">'+category.name+'</option>';
		};
	});
	$('#sub-category').html(html);
}

setEventLoadClasses = function() {
	$('#sub-category').off('change');
	$('#sub-category').on('change', function(e) {
		e.preventDefault();
		var cat = {};
		cat.id = $(this).attr('id');
		cat.classes = $('#product-class');
		fetchAllClass(cat);		
	});
}

function fetchAllClass(cat) {
	var req = {};
	req.action = 'fetchAllClass';
	req.category_id = $('#'+cat.id).val();

	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		// console.log(res);
		res = $.parseJSON(res);
		if(res.status == 1) {
			//Cls = cat.classes;
			res.target = cat.classes;
			fillClasses(res);
			// toastr.success(res.message);
		}
		else {
			toastr.error(res.message);
		}
	});		
}

fillClasses = function(data) {
	html = '<option value="0"> Select Class </option>';
	$.each(data.classes, function(i, cls) {
		html += '<option value="'+cls.id+'">'+cls.name+'</option>';
	});
	$(data.target).html(html);
}

fetchAllClasses = function(){
	var req = {};
	req.action = 'fetchAllClass';
	

	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		// console.log(res);
		res = $.parseJSON(res);
		if(res.status == 1) {
			console.log(res.classes);
			Cls = res.classes;
			//res.target = cat.classes;
			//fillClasses(res);
			// toastr.success(res.message);
		}
		else {
			//toastr.error(res.message);
		}
	});		
}

saveRelated = function() {
	$('#save-related').off('click');
	$('#save-related').on('click', function(e) {
		e.preventDefault();
		var rel = [];
		var prod = $('.related:checked');
		$.each(prod, function(i, pro) {
			//console.log($(pro).val());
			rel.push($(pro).val());
		})

		// save related products

		var req = {};
		req.action = 'saveRelatedProduct';
		req.product_id = product_id;
		req.data = rel;
		console.log(req);
		$.ajax({
			'type'	:	'post',
			'url'	:	EndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			console.log(res);
			if(res.status == 1) {
				$('#related-product-modal').modal('hide');
				$("#products-table tbody").html('');
				fetchAllProducts();				
				toastr.success(res.message);
			}
			else {
				toastr.error(res.message);
			}
		});		

	});
}

setEventCheckAll = function() {
	$("#checkall").change(function() {
	    if(this.checked) {
	        $( ".related" ).prop( "checked", true );
	    }else{
	    	$( ".related" ).prop( "checked", false );
	    }
	});
}

setEventRel = function() {

	$('.delete-rel').off('click');
	$('.delete-rel').on('click', function(e) {
		e.preventDefault();
		var req = {};

			req.action = 'unsetRelatedProduct';
			req.product_id = product_id;
			req.related_id = $(this).attr('data-id');
			//console.log(req);
			$.ajax({
				'type'	:	'post',
				'url'	:	EndPoint,
				'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				console.log(res);
				if(res.status == 1) {
					fetchAllProducts();
					
					toastr.success(res.message);
				}
				else {
					toastr.error(res.message);
				}
			});
	});
	/*$(".rel").change(function() {
		
		if(this.checked) {
			var req = {};

			req.action = 'setRelatedProduct';
			req.product_id = product_id;
			req.related_id = $(this).eq(0).val();
			//console.log(req);
			$.ajax({
				'type'	:	'post',
				'url'	:	EndPoint,
				'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				console.log(res);
				if(res.status == 1) {
					// fillProducts(res.product);
					// fillSpecs(res.specs);
					toastr.success(res.message);
				}
				else {
					toastr.error(res.message);
				}
			});		

	        // alert("set"+$(this).eq(0).val());
	    }else{
	    	var req = {};

			req.action = 'unsetRelatedProduct';
			req.product_id = product_id;
			req.related_id = $(this).eq(0).val();
			//console.log(req);
			$.ajax({
				'type'	:	'post',
				'url'	:	EndPoint,
				'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				console.log(res);
				if(res.status == 1) {
					// fillProducts(res.product);
					// fillSpecs(res.specs);
					toastr.success(res.message);
				}
				else {
					toastr.error(res.message);
				}
			});		
	    	// alert("unset"+$(this).eq(0).val());
	    }
	})*/
}