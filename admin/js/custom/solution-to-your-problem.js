var categorys;
$(function function_name (argument) {
	Category.fetchAllCategories();
	Article.fetchArticles();
	
	Article.filter();

})

var Article = {};
var Category = {};

Article.fetchArticles = function() {
	var req = {};
	req.action = 'fetchArticles';

	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		// console.log(res);
		res = $.parseJSON(res);
		if(res.status == 1) {
			Article.fill(res.article);
			// toastr.success(res.message);
		}
		else {
			toastr.error(res.message);
		}
	});		
}

Article.fill = function(articles) {
	html = '';
	$('div#error-msg').html("");
	$.each(articles, function(i, article) {
		html += '<tr data-id="'+article.id+'">';
		// html += '<td>'+(i+1)+'</td>';
		category = article.category;
		sub_category = article.sub_category;
		if(article.category==null){
			category="Not Set";
		}
		if(article.sub_category==null){
			sub_category="Not Set";
		}

		html += '<td>'+category+'</td>';
		html += '<td>'+sub_category+'</td>';
		html += '<td>'+article.name+'</td>';
		html += '<td>'+'<a href="view-edit-article.php?article_id='+article.id+'" class="btn btn-primary btn-xs" title="Edit Article Details"><i class="fa fa-pencil"></i></a> &nbsp;';

		if (article.active == 1) {
			html += '<a class="article-status btn btn-danger btn-xs" title="Deactivate"><i class="fa fa-times"></i> </a>';
		} else{
			html += '<a class="article-status btn btn-success btn-xs" title="Activate"> <i class="fa fa-check"></i></a>';
		};

		html += '&nbsp;<a class="delete-article btn btn-danger btn-xs" title="Delete"><i class="fa fa-trash-o "></i></a> </td>';
		html += '</tr>';
	});
	
	$('#articles-table tbody').html(html);
	Article.deleteArticle();
	Article.changeArticleStatus();
	if(articles.length==0){
		$('div#error-msg').html("0 result found.");
	}
}



Article.deleteArticle = function() {
	$('.delete-article').off('click');
	$('.delete-article').on('click', function(e) {
		e.preventDefault();
		article_id = $(this).parents('tr:eq(0)').data('id');
		name  = $(this).parents('tr:eq(0)').find('td:eq(2)').text();
		if (confirm('Are you sure want to delete: '+name)) {
			var req = {};
			req.action = 'deleteArticle';
			req.id = article_id;
			$.ajax({
				'type'	:	'post',
				'url'	:	EndPoint,
				'data'	:	JSON.stringify(req)
			}).done(function(res) {
				// console.log(res);
				res = $.parseJSON(res);
				if(res.status == 1) {
					toastr.info(res.message);
					Article.fetchArticles();
				}
				else {
					toastr.error(res.message);
				}
			});	
		};
	});
}


Article.changeArticleStatus = function() {
	$('.article-status').off('click');
	$('.article-status').on('click', function(e) {
		e.preventDefault();
		article_id = $(this).parents('tr:eq(0)').data('id');
		var req = {};
		req.action = 'changeArticleStatus';
		req.status = 0;
		if ($(this).find('i').hasClass('fa-times')) {
			req.status = 0;
		};
		if ($(this).find('i').hasClass('fa-check')) {
			req.status = 1;
		};
		req.article_id = article_id;
		$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
		}).done(function(res) {
			// console.log(res);
			res = $.parseJSON(res);
			if(res.status == 1) {
				toastr.info(res.message);
				Article.fetchArticles();
			}
			else {
				toastr.error(res.message);
			}
		});	
	});
}

Category.fetchAllCategories = function() {
	var req = {};
	req.action = 'fetchAllCategories';

	$.ajax({
		'type'	:	'post',
		'url'	:	EndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		// console.log(res);
		res = $.parseJSON(res);
		if(res.status == 1) {
			categorys = res.categories;
			//Category.fillSubCategories(res.categories);
			Category.fillCategories(res.categories);
			setEventLoadSubCategories();
			// toastr.success(res.message);
		}
		else {
			toastr.error(res.message);
		}
	});		
}

Article.filter = function() {

	$('#filter').off('submit');
	$('#filter').on('submit', function(e) {
		e.preventDefault();				
		$.ajax({
			'type'	:	'post',
			'url'	:	FileEndPoint,
			// 'data'	:	JSON.stringify(req)
			data: new FormData($('#filter').get(0)),
	    	processData: false,
			contentType: false

		}).done(function(res) {
			
			res = $.parseJSON(res);
			//console.log(res);
			if(res.status == 1) {
				Article.fill(res.article);
				toastr.success(res.message);
			}
			else {
				toastr.error(res.message);
			}
		});		
	});
}

Category.fillCategories = function(categories) {
	html = '<option value="0"> Select Category </option>';
	//html = '';
	$.each(categories, function(i, category) {
		if(category.parent_id==0){
		html += '<option value="'+category.id+'"> '+category.name+' </option>';}
	})
	$('#categories').html(html);
}



setEventLoadSubCategories = function() {
	$('#categories').off('change');
	$('#categories').on('change', function(e) {
		e.preventDefault();
		var cat = {};
		cat.id = $(this).attr('id');
		cat.sub_cat = $('#sub-category');
		fillSubCategories(cat);		
	});
}

fillSubCategories = function(cat) {
	html = '<option value="0"> Select Sub-Categories </option>';
	$.each(categorys, function(i, category) {
		if (category.parent_id == $('#'+cat.id).val() && $('#'+cat.id).val()!='0') {
			html += '<option value="'+category.id+'">'+category.name+'</option>';
		};
	});
	$('#sub-category').html(html);
}