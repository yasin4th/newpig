var EndPoint = '../vendor/requests.php';
var FileEndPoint = '../vendor/file-requests.php';

$(function() {
	/*
	* function to show ajax loader
	*/
	$(document).ajaxStart(function(){
	    $('#loader').show();
	});

	/*
	* function to hide ajax loader
	*/
	$(document).ajaxComplete(function(){
	    $('#loader').hide();
	});

});


function getUrlParameter(sParam) {
	sParam = sParam.toLowerCase();
	var sPageURL = window.location.search.substring(1).toLowerCase();
	var sURLVariables = sPageURL.split('&');
	for (var i = 0; i < sURLVariables.length; i++) 
	{
		var sParameterName = sURLVariables[i].split('=');
		if (sParameterName[0] == sParam) 
		{
			return sParameterName[1];
		}
	}
}

function setError(where, what) {
	var parent = $(where).parents('div:eq(0)');
	parent.addClass('has-error').append('<span class="help-block">' + what + '</span>');
}

function unsetError(where) {
	var parent = $(where).parents('div:eq(0)');
	parent.removeClass('has-error').find('.help-block').remove();
}


function imagePreview(file_selector, preview_target) {
	$(file_selector).on('change', function(e) {
		e.preventDefault();
		if($(this)[0].files[0].size < 1048576) {
			var file, img;
		    if ((file = this.files[0])) {
		        /*img = new Image();
		        img.onload = function () {
		            if(this.width == 300 && this.height == 200) {
						$('#test-program-form').submit();
		            }
		            else {
						toastr.error('Error: File Resolution Is Not  300 X 200 px.')
		            }
		        };
		        img.src = _URL.createObjectURL(file);*/
		        $(preview_target).attr('src', URL.createObjectURL($(this)[0].files[0]));
		    }
		}
		else {
			toastr.info('File size larger than 1 MB.')
		}
	});
}


function setJcrop(input, output, callback, aspectRatio, jcrop_api) {
	$(input).on('change', function(e) {
		if($(this)[0].files[0].size < 1048576) {
			var file, img;
		    if ((file = this.files[0])) {
		    	if (jcrop_api == 1) {
			        add_jcrop_api.setImage(URL.createObjectURL($(this)[0].files[0]));
			    }
			    else {
			        edit_jcrop_api.setImage(URL.createObjectURL($(this)[0].files[0]));
			    };
			    if (jcrop_api) {
    				$('#jcrop-image-modal').modal('show');
			    } else{
    				$('#jcrop-image-modal-edit').modal('show');
			    };
		   }
		}
		else {
			toastr.error('File size larger than 1 MB.')
		}
	});

	
	$(output).Jcrop({
		onChange: callback,
		onSelect: callback,
		aspectRatio: aspectRatio,
		boxWidth: 500,
		boxHeight: 500,
		// bgOpacity:   .4,
		// minSize: [253, 139],
		// setSelect:   [0, 0, 100, 90],
    },function(){
      // Store the API in the jcrop_api variable
      if (jcrop_api == 1) {
      	add_jcrop_api = this;
      }
      else {
      	edit_jcrop_api = this;
      };

	});


	
}
		        
function updateCords(c) {
	$('#x').val(c.x);
	$('#y').val(c.y);
	$('#w').val(c.w);
	$('#h').val(c.h);
};
		        
function updateCords1(c) {
	$('#x1').val(c.x);
	$('#y1').val(c.y);
	$('#w1').val(c.w);
	$('#h1').val(c.h);
};
