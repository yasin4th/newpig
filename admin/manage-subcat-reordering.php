<?php
  @session_start();

  if(isset($_SESSION['role'])){
    $role = $_SESSION['role'];
  }else {
    header('Location: index.php');
  }
  if($role!='1'){
      header('Location: redirect.php');
  }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <?php include('html/head-tag.php'); ?>
   
  </head>
  <body>

  <section id="container" class="">
      <!--header start-->
      <?php include('html/header.php'); ?>
      <!--header end-->
      <!--sidebar start-->
      <?php include('html/sidemenu.php'); ?>
      <!--sidebar end-->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
              <!-- page start-->
              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                            Manage Sub-Category Reordering
                            
                          </header>
                          <div class="panel-body">
                            <div class="row">
                              <div class="col-sm-6">
                    
                                <label>Select Category : </label>
                                <select name="cat" id="categories" class="form-control">
                                  <option value="">Select Category</option>
                                </select>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-sm-12">
                                <ol id="sortable">
                                  
                                </ol>
                                <button id="save-order" class="btn btn-success pull-right">Save Order</button>
                              </div>
                            </div>
                            <section>
                              
                            </section>
                          </div>
                        </div>
                      </section>
                  </div>
              </div>
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->

     

      <!--footer start-->
      <?php include('html/footer.php'); ?>
      <!--footer end-->
  </section>

    
  <?php include('html/js-files.php'); ?>
  <script type="text/javascript" src="js/jquery-ui.min.js"></script>
  <script type="text/javascript" src="js/custom/manage-subcat-reordering.js"></script>

  </body>
</html>