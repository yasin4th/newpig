<?php
  @session_start();

  if(isset($_SESSION['role'])){
    $role = $_SESSION['role'];
  }else {
    header('Location: index.php');
  }
  if($role!='1'){
      header('Location: redirect.php');
  }
?><!DOCTYPE html>
<html lang="en">
  <head>
    <?php include('html/head-tag.php'); ?>
  </head>

  <body>

  <section id="container" class="">
      <!--header start-->
      <?php include('html/header.php'); ?>
      <!--header end-->
      <!--sidebar start-->
      <?php include('html/sidemenu.php'); ?>
      <!--sidebar end-->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
              <!-- page start-->
              <div class="row">
                <div class="col-lg-12">
                  <section class="panel">
                    <header class="panel-heading">
                      View and Edit Video
                      <a href="manage-videos.php" class="btn btn-success pull-right btn-xs">Back</a>
                    </header>
                    <div class="panel-body">
                      <form id="update-video-form" role="form">
                        <div class="row">
                          <div class="col-sm-3">
                            <div class="form-group">
                              <label>Product Category</label>
                                <select id="categories" class="categories form-control">
                                  <option value="0">Select Product Category</option>
                                </select>
                            </div>
                          </div>
                          <div class="col-sm-3">
                            <div class="form-group">
                              <label>Product Sub-Category</label>
                                <select id="sub-category" class="form-control">
                                  <option>Select Product Category</option>
                                </select>
                            </div>
                          </div>
                          <div class="col-sm-3">
                            <div class="form-group">
                              <label>Product Type</label>
                              <select id="select-products" class="products form-control">
                                <option value="0">Select Product Type</option>
                              </select>
                            </div>
                          </div>
                          <div class="col-sm-3">
                            <div class="form-group">
                              <label>Status</label>
                              <select id="status" class="form-control">
                                <option value="-1"> Select Status </option>
                                <option value="1"> Active </option>
                                <option value="0"> Not Active </option>
                              </select>
                            </div>
                          </div>
                          <div class="col-sm-8">
                            <div class="form-group">
                              <label>Video Title</label>
                              <input id="title" type="text" class="form-control" required="required" placeholder="Video Name">
                            </div>
                          </div>
                          <div class="col-sm-8">
                            <div class="form-group">
                              <label>Video Description</label>
                              <textarea id="description" name="description" class="form-control"></textarea>
                            </div>
                          </div>
                          <div class="col-sm-8">
                            <div class="form-group">
                              <label>Media URL</label>
                              <input id="url" type="text" class="form-control" required="required" placeholder="Media URL">
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-sm-12">
                              <div class="col-sm-12">
                                <div class="video">
                                  <iframe id="video-preview" width="560" height="315" src="" frameborder="0" allowfullscreen></iframe>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-sm-12">
                            <div class="form-group pull-right">
                              <button type="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i> Save</button>
                              <a class="btn btn-default">Cancel</a>
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                  </section>
                </div>
              </div>
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->
      <!--footer start-->
      <?php include('html/footer.php'); ?>
      <!--footer end-->
  </section>

    
  <?php include('html/js-files.php'); ?>

  <script type="text/javascript" src="js/custom/view-edit-video/view-edit-video.js"></script>

  <script type="text/javascript" src="js/custom/view-edit-video/fetch-master-data.js"></script>


  </body>
</html>