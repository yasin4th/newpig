<?php
  @session_start();

  if(isset($_SESSION['role'])){
    $role = $_SESSION['role'];
  }else {
    header('Location: index.php');
  }
  if($role=='3'){
      header('Location: redirect.php');
  }
?><!DOCTYPE html>
<html lang="en">
  <head>
    <?php include('html/head-tag.php'); ?>
  </head>

  <body>

  <section id="container" class="">
      <!--header start-->
      <?php include('html/header.php'); ?>
      <!--header end-->
      <!--sidebar start-->
      <?php include('html/sidemenu.php'); ?>
      <!--sidebar end-->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
              <!-- page start-->
              <div class="row">
                <div class="col-lg-12">
                  <form id="add-product-form" role="form">
                    <section class="panel">
                      <header class="panel-heading">
                        Add Product
                        <a href="manage-product.php" class="btn btn-success pull-right btn-xs">Back</a>
                      </header>
                      <div class="panel-body">
                          <div class="row">
                            <div class="col-sm-3">
                              <div class="form-group">
                                <label>Category</label>
                                <select id="categories" name="categories" class="categories form-control">
                                  <option value="0"> Select Category</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-sm-3">
                              <div class="form-group">
                                <label>Sub-Category</label>
                                <select name="sub-category" id="sub-category" class="form-control">
                                  <option value="0"> Sub-Category</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-sm-3">
                              <div class="form-group">
                                <label>Class</label>
                                <select name="product-class" id="product-class" class="form-control">
                                  <option value="0"> Select Class</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-sm-3">
                              <div class="form-group">
                                <label>Stock</label>
                                <select id="stock" name="stock" class="form-control">
                                  <option value="1"> In-stock</option>
                                  <option value="0"> Out of Stock</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-sm-3">
                              <div class="form-group">
                                <label> Product Name</label>
                                <input id="product-name" name="product-name" type="text" class="form-control" placeholder="Product Name">
                              </div>
                            </div>
                            <div class="col-sm-3">
                              <div class="form-group">
                                <label> Supplier Part Number </label>
                                <input id="product-part-name" name="product-part-name" type="text" class="form-control" placeholder="Ex. MAT333">
                              </div>
                            </div>
                            <div class="col-sm-3">
                              <div class="form-group">
                                <label> Matthews Part Number </label>
                                <input id="product-supplier-name" name="product-supplier-name" type="text" class="form-control" placeholder="Ex. SUP-MAT333">
                              </div>
                            </div>
                           
                            <div class="col-sm-3">
                              <div class="form-group">
                                <label> Quantity (Packets) </label>
                                <input id="quantity" name="quantity" type="text" class="form-control" placeholder="Product Count">
                              </div>
                            </div>
                            <div class="col-sm-3">
                              <div class="form-group">
                                <label> MRP A<i class="fa fa-usd"></i> </label>
                                <input id="mrp" name="mrp" type="text" class="form-control" placeholder="Product Price">
                              </div>
                            </div>
                            <div class="col-sm-3">
                              <div class="form-group">
                                <label>Height</label>
                                <input name="height" type="text" class="form-control" placeholder="Product Height">
                              </div>
                            </div>
                            <div class="col-sm-3">
                              <div class="form-group">
                                <label>Width</label>
                                <input name="width" type="text" class="form-control" placeholder="Product Width">
                              </div>
                            </div>
                            <div class="col-sm-3">
                              <div class="form-group">
                                <label>Weight</label>
                                <input name="weight" type="text" class="form-control" placeholder="Product Weight">
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-sm-12">
                                <div class="col-sm-6">
                                  <div class="form-group">
                                    <label>Small Description</label>
                                    <textarea type="text" id="small-description" name="small-description" required="required" class="form-control" rows="4" placeholder="Small Description"></textarea>
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                  <div class="form-group">
                                    <label>Detailed Description</label>
                                    <textarea type="text" id="detailed-description" name="detailed-description" class="form-control" rows="4" placeholder="Detailed Description"  contenteditable="true" style="min-height: 100px; height: auto;"></textarea>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label>Product Image <small>( size: 268x249 px )</small></label>
                                <div class="row">
                                  <div class="col-md-3">
                                    <div class="add-product-image">
                                      <a data-toggle="modal" href="#jcrop-image-modal">Preview Image</a>
                                      <!-- <img id="product-image-preview" src="../images/product/hero-absorbents.png" class="img-responsive" alt=""> -->
                                      <input type="hidden" id="x" name="x" value="0">
                                      <input type="hidden" id="y" name="y" value="0">
                                      <input type="hidden" id="w" name="w" value="0">
                                      <input type="hidden" id="h" name="h" value="0">
                                    </div>
                                  </div>
                                  <div class="col-md-9">
                                    <input name="product-image" id="product-image" type="file" class="btn form-control">
                                    <input name="action" id="action" type="hidden" value="addProduct">
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label>Related Image <small>( size: 268x249 px )</small></label>
                                <div class="row">
                                  <div class="col-md-12">
                                    <input name="related-image[]" id="related-image" type="file" multiple="multiple" class="btn form-control">
                                  </div>
                                </div>
                                <div class="row mrg-top10">
                                  <div class="col-sm-12">
                                    <div class="add-related-img">
                                      <!-- <div class="related-img">
                                        <img src="../images/product/hero-absorbents.png" class="img-responsive" alt="">
                                      </div> -->
                                      
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-sm-12">
                              <h4>Specifications</h4>
                              <div class="col-md-12 specifications-border">
                                <div class="form-group specifications">
                                  <div class="row">
                                    <div class="col-md-5">
                                      <Select class='Specification form-group form-control' id="spec0" >

                                      </Select>
                                    </div>
                                    <div class="col-md-6">
                                      <textarea name="value0" class="form-control"></textarea>
                                      <!-- <input type="text" name="value0" class="form-control"> -->
                                    </div>
                                    <div class="col-md-1">
                                      <a class="btn-minus-specifications" val='0' style="display:none;"><i class="fa fa-trash-o fa-2x"></i></a>
                                      <a class="btn-add-specifications pull-right"><i class="fa fa-plus-circle fa-2x"></i></a>
                                    </div>
                                  </div>                                  

                                </div>
                                                                 
                              </div>                              
                            </div>
                          </div>
                          <div class="row">
                            <!-- <div class="col-sm-12">
                              <div class="vedio">
                                <iframe width="560" height="315" src="https://www.youtube.com/embed/GlFlSlAa-j4" frameborder="0" allowfullscreen></iframe>
                              </div>
                            </div> -->
                            <div class="col-sm-12">
                              <h4>Media</h4>
                            </div>
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label>Media Title</label>
                                <input id="media-title" name="media-title" type="text" class="form-control" placeholder="Media title">
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label>Media URL</label>
                                <input id="media-url" name="media-url" type="text" class="form-control" placeholder="Media URL">
                              </div>
                            </div>
                          </div>



                          <div class="row">
                            <div class="col-sm-12">
                              <h4>Meta Data</h4>
                              <div class="col-sm-6">
                                <div class="form-group">
                                  <label>Meta Title</label>
                                  <input id="meta-title" name="meta-title" type="text" class="form-control" placeholder="Meta Title">
                                </div>
                              </div>
                              <div class="col-sm-6">
                                <div class="form-group">
                                  <label>Meta Keywords</label>
                                  <input id="meta-keywords" name="meta-keywords" type="text" class="form-control" placeholder="Meta Keywords">
                                </div>
                              </div>
                              <div class="col-sm-12">
                                <div class="form-group">
                                  <label>Meta Description</label>
                                  <textarea id="meta-description" name="meta-description"  class="form-control" rows="5" cols="150" placeholder="Meta Description"></textarea>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-sm-12">
                            <h4>Featured Points</h4>
                              <div class="col-sm-4">
                                <div class="form-group">
                                    <input id="point1" name="point1" type="text" class="form-control" placeholder="Point1">
                                </div>
                              </div>
                              <div class="col-sm-4">
                                <div class="form-group">  
                                    <input id="point2" name="point2" type="text" class="form-control" placeholder="Point2">
                                </div>
                              </div>
                              <div class="col-sm-4">
                                <div class="form-group">
                                    <input id="point3" name="point3" type="text" class="form-control" placeholder="Point3">
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-sm-3">
                            <div class="form-group">
                              <input type="checkbox" id="featured" name="featured" value="1">Featured                              
                            </div>
                          </div>

                          <textarea name="specs" style="display:none;"></textarea>
                          </div>
                          <div class="row">
                            <div class="col-sm-12">
                              <div class="form-group pull-right">
                                <button type="submit" class="btn btn-success"><i class="fa fa-plus"></i>Add</button>
                                <a class="btn btn-default">Cancel</a>
                              </div>
                            </div>
                          </div>
                      </div>
                    </section>
                  </form>
                </div>
              </div>

              <!-- <div class="row mrg-top30">
                <div class="col-sm-12">
                  <section class="panel">
                    <header class="panel-heading">
                      Add Related Product
                    </header>
                    <div class="panel-body">
                      <div class="related-items-content">
                        <div class="row">
                          <div class="col-sm-5">
                            <div class="form-group">
                              <label>Related Product Title</label>
                              <input type="text" class="form-control" placeholder="Media title">
                            </div>
                          </div>
                          <div class="col-sm-5">
                            <div class="form-group">
                              <label>Upload Product Images</label>
                              <input name="image" id="image" type="file" class="btn form-control">
                              <input name="action" id="action" type="hidden" value="addProduct">
                            </div>
                          </div>

                          <div class="col-sm-2">
                            <div class="form-group mrg-top24 pull-right">
                              <a class="btn btn-success">Save</a>
                              <a class="btn btn-default">Cancel</a>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6 related-product">
                            <button type="button" class="btn btn-round btn-white btn-xs mrg-top10 pull-right"><i class="fa fa-times"></i></button>
                            <a href="#"><img class="img-responsive" src="../images/product/absorbent/pig-loose-absorbents.jpg" alt="Absorbents" title="" />
                            <p class="text-center">PIG Loose Absorbents</p></a>
                          </div>
                          <div class="col-md-6 related-product">
                            <button type="button" class="btn btn-round btn-white btn-xs mrg-top10 pull-right"><i class="fa fa-times"></i></button>
                            <a href="#"><img class="img-responsive" src="../images/product/absorbent/pig-loose-absorbents.jpg" alt="Absorbents" title="" />
                            <p class="text-center">PIG Loose Absorbents</p></a>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6 related-product">
                            <button type="button" class="btn btn-round btn-white btn-xs mrg-top10 pull-right"><i class="fa fa-times"></i></button>
                            <a href="#"><img class="img-responsive" src="../images/product/absorbent/pig-loose-absorbents.jpg" alt="Absorbents" title="" />
                            <p class="text-center">PIG Loose Absorbents</p></a>
                          </div>
                          <div class="col-md-6 related-product">
                            <button type="button" class="btn btn-round btn-white btn-xs mrg-top10 pull-right"><i class="fa fa-times"></i></button>
                            <a href="#"><img class="img-responsive" src="../images/product/absorbent/pig-loose-absorbents.jpg" alt="Absorbents" title="" />
                            <p class="text-center">PIG Loose Absorbents</p></a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </section>
                </div>
              </div> -->
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->
            <!-- Start jcrop image Modal -->
      <div class="modal fade" id="jcrop-image-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md">
          <div class="modal-content">
            
            <div class="modal-body">
              <div style="height:400px">
                <img title="Jcrop Image Preview" src="" alt="Croping Image" id="crop-image" class="img-responsive" >
                <!-- <p class="text-center">Actual Image</p> -->
              </div>
              <!-- <div style="float: right;">
                <div id="preview-pane">
                    <div class="preview-container"> -->
                        <!-- <img src="images/na4.jpg" class="jc-image jcrop-preview" alt="Preview" id="image-preview" /> -->
                       <!-- <img title="Jcrop Image Preview" class="jc-image jcrop-preview" src="" alt="Croping Image Preview" id="crop-preview" >
                    </div>
                    <p class="text-center">Final Image - Resolution (500x500 pixels)</p>
                </div>
              </div> -->
              <div class="row">
                <div class="col-md-12">
                  <hr>
                  <a data-dismiss="modal" class="btn btn-default pull-right">Set</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--End jcrop image modal -->

      <!--footer start-->
      <?php include('html/footer.php'); ?>
      <!--footer end-->
  </section>

    
  <?php include('html/js-files.php'); ?>
  <script type="text/javascript" src="js/ckeditor/ckeditor.js" ></script>
  <script type="text/javascript" src="js/custom/add-product/add-product.js" ></script>
  <script type="text/javascript" src="js/custom/add-product/fetch-master-data.js" ></script>


  </body>
</html>