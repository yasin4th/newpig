<!DOCTYPE html>
<html lang="en">
<head>
	<title>	Why New Pig? - Best quality spill absorbents & containment solutions </title>

	<meta name="description" content="iQSafety - New Pig is home to PIG® Mat, the world's best-selling absorbent mat. If it leaks, drips, splatters or spills, New Pig make hardworking products to absorb, contain and clean it up." />
    <?php include('html/head-tag.php'); ?>
</head>
<!--/head-->

<body>
	<!--header-->
	<?php include('html/header.php'); ?>
	<!--/header-->
	
	<section class="mrg-top30">
		<div class="container">
			<div class="row"> 
				<div class="col-sm-3">
					<?php include('html/about-side-bar.php'); ?>
				</div>

	    		<div class="col-sm-9 padding-right mrg-bot30">
    				<h2 class="main-title"><span class="white-bg">Why New Pig?</span></h2>
	    			<div class="row">
	    				<div class="col-md-12">
		    				<div class="about">
				    			<p>iQSafety is committed to bringing the very best to Australia which is why we partner with companies like New Pig USA.</p>
				    			<p>If it leaks, drips, splatters or spills, New Pig USA make hardworking products to absorb, contain and clean it up.
				    			 Their original PIG® Sock launched the industrial absorbent industry, and 27 years later they're still inventing new
				    			  solutions to help you keep your workplace clean, safe and productive.</p>
				    			<p>New Pig is home to PIG® Mat, the world's best-selling absorbent mat. They invented the PIG® SPILLBLOCKER® Dike,
				    			 the PIG® DRAINBLOCKER® Drain Cover, the PIG® Burpless® Drum Funnel and the PIG® Latching Drum Lid, just to name a few.</p>
				    			  <p>PIG® Products set the standard for quality, durability and features you just won't find anywhere else.</p>
				    			  <p>We are proud to bring into Australia such high quality innovative products.</p>
		    			    </div>
	    			    </div>
    			    </div>
	    		</div>   			
	    	</div> 
		</div>
	</section>
	
	<!--Footer-->
	<?php include('html/footer.php'); ?>
	<!--/Footer-->

	<!--/js-files-->
	<?php include('html/js-files.php'); ?>
	<script type="text/javascript"> 
	   	$(function () { 
	      	$('.about-side-bar h4 a').removeClass('active'); 
	      	$('.about-side-bar h4 a#why-new-pig').addClass('active'); 
	   	}); 
	</script>
	<!--/js-files-->

</body>
</html>