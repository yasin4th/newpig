<!DOCTYPE html>
<html lang="en">
<head>
	<title>My Cart - My Spill Clean-up & Safety Products</title>
    <meta name="description" content="My Cart - My Spill Clean-up & Safety Products" />
    <?php include('html/head-tag.php'); ?>
</head><!--/head-->

<body>
	<!--header-->
	<?php include('html/header.php'); ?>
	<!--/header-->
	
	<section class="mrg-top30" id="cart_items">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="features_items"><!--features_items-->
						<h2 class="title text-center"><span class="white-bg">Quote Request</span></h2>
						<div class="table-responsive cart_info">
							<table id="cart-products-table" class="table table-condensed">
								<thead>
									<tr class="cart_menu">
										<td class="image">Item</td>
										<td class="description"></td>
										<td class="quantity">Quantity</td>
										<td></td>
									</tr>
								</thead>
								<tbody></tbody>
								<tfoot>
									<tr>
										<td colspan="6" class="text-center" style="border-top: 1px solid #F7F7F0;">Make any changes above? &nbsp;&nbsp;&nbsp;<input id="update-cart-products" name="update" class="btn btn-primary mrg-top0" type="button" value="Update"></td>
									</tr>
								</tfoot>
							</table>
						</div>
					</div><!--features_items-->
				</div>
				<div class="col-sm-12 mrg-bot50">
					<div class="pull-right">
						<a href="products" name="update" class="btn btn-primary mrg-top0">Add Products</a>
						<a href="checkout" name="update" class="btn btn-primary mrg-top0 btn-checkout">Request Quote</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<!--Footer-->
	<?php include('html/footer.php'); ?>
	<!--/Footer-->

	<!--/js-files-->
	<?php include('html/js-files.php'); ?>
	<!--/js-files-->
	<script type="text/javascript" src="js/custom/cart.js" ></script>

</body>
</html>