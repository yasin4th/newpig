<!DOCTYPE html>
<html lang="en">
<head>
	<title>	FAQ's - All about Skill Kits, Absorbents & Containment Solutions </title>
	<meta name="description" content="Frequently Asked Question's - Know all about Skill Kits, Spill Absorbents & Containment Solutions." />

    <?php include('html/head-tag.php'); ?>
</head>

<!--/head-->

<body>
	<!--header-->
	<?php include('html/header.php'); ?>
	<!--/header-->
	
	<section class="mrg-top30">
		<div class="container">
			<div class="row"> 
				<div class="col-sm-3">
					<?php include('html/about-side-bar.php'); ?>
				</div>

	    		<div class="col-sm-9 padding-right mrg-bot30">
    				<h2 class="main-title"><span class="white-bg">FAQ's</span></h2>
	    			<div class="row">
	    				<div class="col-md-12">
		    				<div class="about">
		    					<h4>Q. What is a spill kit?</h4>
				    			<p>A. Spill kits clean up hazardous and/or toxic spills that may happen in the workplace. These highly effective kits prevent
				    			 injury or damage to workers (such as slipping over) and the environment (if the spill gets into storm water). </p>
				    			 <h4>Q. So who needs a spill kit?</h4>
				    			<p>A. Every business that deals with any sort of chemicals, fuels, oils or other substances that can possibly cause injury or
				    			 damage to people or the environment should have a spill kit as part of their emergency response plan. So if your business
				    			  stores these substances, uses them or transports them, you need a spill kit! Some examples are: factories, workshops,
				    			   food & beverage processors, farms, shopping centres, schools, mechanic shops, transport companies, mining operations,
				    			    petrol stations, metal plants, industrial manufacturers, construction companies, power stations and so on. </p>
			    			    <h4>Q. Great, so how do I choose what type & size spill kit I need?</h4>
				    			<p>A. First, go with type: the type of spill kit you need depends on your potential spill risks. Having the wrong type
				    			 of kit can actually make things worse. There are three types of kits: </p>
				    			 <ul>
				    			 	<li>Chemical Neutralizing Spill Kits, also called “Haz Mat Kits” (for hazardous materials): these absorb acids,
				    			 	 bases and when you don't know what the chemical is</li>
				    			 	<li>Oil Spill Kit:, these absorb oil-based liquids (but not water)</li>
				    			 	<li>Universal Spill Kit: these absorb water, solvents coolants and oils</li>
				    			 </ul>	
				    			  <p>The easiest way to choose what size kit is think about the volume of your worst-case scenario spill. What’s the
				    			   largest amount of liquid that could spill from a single container or holding tank?</p>
			    			    <ul>
			    			    	<li>Chemical Neutralizing Spill (“Haz Mat”) Kits: can absorb from 3 litres (this size kit comes in a hi-vis bag)
			    			    	 right up to 214 litres (comes in a 240-litre wheelie bin)</li>
			    			    	<li>Oil Spill Kit: can absorb from 1 litre (also comes in a hi-vis bag) up to 198 litres (kit comes in a 240-litre wheelie bin)</li>
			    			    	<li>Universal Spill Kit: there are 2 sizes, able to absorb up to 101 litres and up to 213 litres</li>
			    			    </ul>
				    			  <p>But it won’t always be practical to absorb the entire spill. You can use spill kits in conjunction with non-absorbent dykes and
				    			   drain covers (e.g.: SPILLBLOCKER® Dike and DRAINBLOCKER® Drain Covers) to help channel or contain liquids so they can be recovered
				    			    with vacuums or pumps. In some cases, once most of the liquid has been removed, you can then use absorbents on the rest.</p>
				    			    <p>And, if you’re unsure, call us on 1800 HOT HOG (1800 468 464) and we’ll guide you through what’s best for your business. </p>
				    			<h4>Q. Why should I use expensive absorbents when I can just as well use clay?</h4>
				    			<p>A. Well you can use clay, but there are time, money and disposal reasons why other absorbents are better… </p>
				    			<ul>
				    				<li>Clay is actually not an effective absorbent: it only picks up once its weight by becoming coated with liquid — which eventually
				    				 seeps through to the floor rather than staying contained. </li>
				    				<li>Because clay has very limited absorption capacity, you use more material to absorb the spill and spend more labour doing it.
				    				 Costly on two fronts. </li>
				    				<li>Gritty clay particles can mark your floors, or worse, damage sensitive machine parts (no one wants downtime from clay pellets
				    				 damaging equipment internally). Plus, clay sticks to shoes, so it can get tracked throughout your office and factory, making it
				    				  dirty and unsafe, if the spill area isn’t properly blocked off. </li>
				    				<li>You can’t dispose of clay in landfill, nor can you incinerate it. </li>
				    				<li>Clay generates silica dust, which fills the air and can cause chronic lung damage. (Whether it’s short term productivity loss
				    				 with workers feeling ill or lingering health issues, you don't want them.) </li>
				    				<li>Clay is heavy, and gets even heavier with liquid spills. Cleaning it up is just plain hard work: sweeping and shovelling it is
				    				 messy, time-consuming and back breaking. Cleaning up leaks and spills should be easy, but using clay actually makes it harder and takes longer.  </li>
				    			</ul>
				    			<p>In contrast, New Pig absorbents pick up many times their weight, are all fully contained, do not generate toxic dust, aren’t backbreaking to use, and are simple to dispose of! </p>
				    			<h4>Q. Do I need any spill prevention products on site?</h4>
				    			<p>A. Absolutely! Liquid spills can be highly detrimental to the environment. If a spill happens, legally you must contain it and prevent it spreading into stormwater. </p>
				    			<p>And not only should you have the right spill kit on hand, but you’ll need the appropriate personal protection equipment (usually known as PPE) for handling a spill too. </p>
				    			<h4>Q. Why is New Pig product expensive?</h4>
				    			<p>A. New Pig products have been designed from nearly 30 years of providing solutions to ensure and maintain a good, clean and safe workplace at a cost-effective price with limited
				    			 downtime. We’re not the cheapest on the market, but we certainly think we’re the best value! </p>
				    			 <h4>Q. What is your shipping policy?</h4>
				    			 <p>A. Provided you order before 1pm Australian Eastern Standard Time (AEST), you’ll have your order shipped the same day. </p>
				    			 <h4>Q. Can I buy direct from the USA?</h4>
				    			 <p>A. No. iQSafety is the authorised distributor in Australia. </p>
				    			 <h4>Q. What if I want to buy a New Pig product that is not listed on the Australian website?</h4>
				    			 <p>A. iQSafety stocks most of the popular items in our Melbourne warehouse. If it is not in stock, it will be on its way. However, if it is a product we don't stock, we will be happy to ship it in for you.  </p>
				    			 <h4>Q. Can I make a purchase online?</h4>
				    			 <p>Not just yet — but we’re working on that!</p>
				    			 <h4>Q. Who needs a storage cabinet?</h4>
				    			 <p>A. Businesses that deal with flammable, corrosive or other potentially toxic chemicals need, by law, to store them safely. </p>
				    			 <h4>Q. Why should I buy Justrite cabinets?</h4>
				    			 <p>Justrite cabinets are tough and hardy — just what you want when storing potentially hazardous and toxic chemicals! They meet the
				    			  Australian standards and requirements and the chemical-resistant steel minimises any corrosion or humidity effects, while the double-wall
				    			   construction in the flammable cabinets offers insulating air space for fire resistance. </p>
		    			    </div>
	    			    </div>
    			    </div>
	    		</div>   			
	    	</div> 
		</div>
	</section>
	
	<!--Footer-->
	<?php include('html/footer.php'); ?>
	<!--/Footer-->

	<!--/js-files-->
	<?php include('html/js-files.php'); ?>
	<script type="text/javascript"> 
	   	$(function () { 
	      	$('.about-side-bar h4 a').removeClass('active'); 
	      	$('.about-side-bar h4 a#faqs').addClass('active'); 
	   	}); 
	</script>
	<!--/js-files-->

</body>
</html>