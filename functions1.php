<?php
    function getData($params) 
    {
        $request = new stdClass();
        
        foreach ($params as $key => $value) {
            $request->$key = $value;      
        }
        
        require_once('vendor/requests1.php');            
        
        $res = Api::{$params['action']}($request);
                
        $json = json_encode($res);
        $array = json_decode($json,true);
        // pp($array);
        if($array['status'] == 1)
        {
            return $array;
        }
        else{
            return $params;
        }
    }
    
    function dd($var){
        echo '<pre>';
        print_r($var);
        echo '</pre>';
        die();
    }

    function pp($var){
        echo '<pre>';
        print_r($var);
        echo '</pre>';
    }

    function createNavigation($cats = [])
    {
        $str = "";
        if(count($cats))
        {
            foreach ($cats as $cat)
            {
                if($cat['parent_id']==0 && $cat['status']==1)
                {
                    $str .= '<li>';
                    $str .= '<a href="products/'.$cat['alias'].'">'.$cat['name'].'</a>';
                    $str .= '<ul class="sub-menu-cat">';

                    foreach ($cats as $subcat)
                    {
                        if ($subcat['parent_id'] == $cat['id'] && $subcat['status'] == 1) {

                            $catalias = preg_replace('/_[a-z]\d+/', '', $subcat['calias']);
                            
                            $str .= '<li class="dropdown">';
                            $str .= '<a href="'.$catalias.'/'.$subcat['alias'].'">'.$subcat['name'].'</a>';
                            $str .= '</li>';
                        }
                    }

                    $str .= '</ul>';
                    $str .= '</li>';
                }
            }
            
        }
        $str .= '<li><a href="best_sellers">Best Sellers</a></li>';
        return $str;
    }

    function createSidebar($cats = [])
    {
        $str = "";
        if(count($cats))
        {
            $count = 1;
            foreach ($cats as $cat)
            {
                if($cat['parent_id']==0 && $cat['status']==1)
                {
                    $str .= '<div class="panel panel-default"><div class="panel-heading"><h4 class="panel-title">';
                    $str .= '<a data-toggle="collapse" data-parent="#accordian" href="#count'.$count.'">';
                    $str .= '<span class="badge pull-right"><i class="fa fa-plus"></i></span>';
                    $str .= $cat['name'];
                    $str .= '</a>';
                    $str .= '</h4></div>';

                    //$str .= '<a href="sub-categories.php?category_id='+category.id+'">'+category.name+'</a>';
                    //$str .= '<ul class="sub-menu-cat">'
                    $str .= '<div id="count'.$count.'" class="panel-collapse collapse"><div class="panel-body"><ul>';

                    foreach ($cats as $subcat)
                    {
                        if ($subcat['parent_id'] == $cat['id'] && $subcat['status'] == 1) {

                            $catalias = preg_replace('/_[a-z]\d+/', '', $subcat['calias']);
                            
                            $str .= '<li><a href="'.$catalias.'/'.$subcat['alias'].'"><i class="fa fa-angle-right"></i> '.$subcat['name'].' </a></li>';
                            $str .= '</li>';
                            
                        }
                    }
                    $str .= '</ul></div></div></div>';                
                }

                $count++;
            }
            
        }
        $str .= '<div class="panel panel-default"><div class="panel-heading">';
        $str .= '<h4 class="panel-title"><a href="best_sellers">Best Sellers</a></h4>';
        $str .= '</div></div>';

        return $str;
    }
    
?>