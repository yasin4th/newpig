<!DOCTYPE html>
<html lang="en">
<head>
    <?php include('html/head-tag.php'); ?>
</head>
<!--/head-->

<body>
	<!--header-->
	<?php include('html/header.php'); ?>
	<!--/header-->
	
	<section class="mrg-top30">
		<div class="container">
			<div class="row">  	
	    		<div class="col-md-9">
    				<h2 class="title"><span class="white-bg">Search Result</span></h2>
                    <?php
                        $q = $_GET['q'];
                        $prod = getData(array('action' => 'fetchSearchProducts', 'search' => $q));
                        // dd($products);
                    ?>
    				<h4 class="search-reasult-counts">
                        <?php 
                            echo '<span class="counts-number">' . count($prod['products']) . '</span> ';
                            echo (count($prod['products']) == 1) ? 'result' : 'results';
                            echo ' for <span class="search-title">"'.$q.'"</span>';
                        ?>

                    <!--<span class="counts-number">50</span> results for <span class="search-title">spill kit.</span>--></h4>
	    			<div class="search-result">
                    <?php
                        $str = '';
                        foreach ($prod['products'] as $product) {

                            $subcat = strstr($product['subcatalias'], '_', true);

    	    				$str .= '<div class="row search-item">';
                            $str .= '<div class="col-md-3"><a href="' . $subcat . '/' . $product['alias'] . '" class="text-center">';
                            $str .= '<img src="'.$product['image'].'" class="img-responsive" alt="'.$product['name'].'" title="'.$product['name'].'" />';
                            $str .= '</a></div><div class="col-md-9">';
                            $str .= '<h4>' .$product['name']. '</h4>';
                            $str .= '<p>'.$product['part_name'].'</p>';
                            $str .= '<p>'.substr($product['detailed_description'], 0, 300).'</p>';
                            $str .= '<a href="' . $subcat . '/'.$product['alias'].'" class="pull-right">View Details</a>';
                            $str .= '</div></div>';
                        }
                        echo $str;
                    ?>
	    			</div>
					<!--<ul class="pagination">
						<li class="disabled"><a href="#" aria-label="Previous">&laquo;</a></li>
						<li class="active"><a href="#">1</a></li>
						<li><a href="#">2</a></li>
						<li><a href="#">3</a></li>
						<li><a href="#">4</a></li>
						<li><a href="#" aria-label="Next">&raquo;</a></li>
					</ul>-->
	    		</div>
	    		<div class="col-md-3">
					<!--start find solution to your problem-->
					<div class="solution-problem">
						<h2>Find solution to your problem!</h2>
						<div class="problem">
                            <?php
                                $str = '';
                                $articles = getData(array('action' => 'fetchArticles', 'active' => '1'));
                                for ($i=0; $i < count($articles['article']) && $i < 8; $i++) { 
                                    $str .= "<p><i class='fa fa-certificate'></i><a href='blog/{$articles['article'][$i]['alias']}'> {$articles['article'][$i]['name']} </a></p>";
                                }
                                if(count($articles['article'])) {
                                    $str .= '<p><a href="blog" class="pull-right">View all solution</a></p>';
                                }
                                echo $str;
                            ?>
							<!-- <p><i class="fa fa-certificate"></i><a href="#"> Need to be prepared for a spill?</a></p>
							<p><i class="fa fa-certificate"></i><a href="#"> Need to choose an absorbent?</a></p>
							<p><i class="fa fa-certificate"></i><a href="#"> 3 Questions to Ask When Selecting a Spill Kit</a></p>
							<p><a href="#" class="pull-right">View all solution</a></p> -->
						</div>
					</div>
					<!--end find solution to your problem-->
					<!--start same day shipping-->
					<div class="same-day-shipping">
						<h2>Same Day <span>Shipping!</span></h2>
						<img src="images/home/same-day-shipping.png" class="img-responsive" alt="" />
						<p>Most in-stock orders ship the same day if ordered before 1pm EST</p>
					</div>
					<!--end same day shipping-->
    			</div>    			
	    	</div> 
		</div>
	</section>
	
	<!--Footer-->
	<?php include('html/footer.php'); ?>
	<!--/Footer-->

	<!--/js-files-->
	<?php include('html/js-files.php'); ?>
	<!-- <script src="js/custom/search.js"></script> -->
	<!--/js-files-->

</body>
</html>