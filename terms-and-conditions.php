<!DOCTYPE html>
<html lang="en">
<head>
	<title>	Terms & Conditions - iQSafety - Spill Clean-up Products & Safety Solution </title>
	<meta name="description" content="Terms & Conditions - iQSafety - Spill Clean-up Products & Safety Solution." />

    <?php include('html/head-tag.php'); ?>
</head>
<!--/head-->

<body>
	<!--header-->
	<?php include('html/header.php'); ?>
	<!--/header-->
	
	<section class="mrg-top30">
		<div class="container">
			<div class="row"> 
				<div class="col-sm-3">
					<?php include('html/side-bar.php'); ?>
				</div>

	    		<div class="col-sm-9 padding-right mrg-bot30">
	    			<h2>Terms & Conditions</h2>
	    			<div class="row">
	    				<div class="col-md-12">
		    				<div class="about">
		    					<h4>About these Website Terms of Use</h4>
				    			<p>Your use of the matthews.com.au website ("our website") owned by Matthews Australasia Pty Ltd (ACN 079 545 145) 
				    			("we", "our" or "us") is subject to these Terms of Use, our Privacy Policy and any other terms, conditions or disclaimers 
				    			displayed on our website (collectively "Website Terms").  By using our website you will be deemed to accept the Website 
				    			Terms and agree to be bound by them.</p>
				    			<p>We may change the Website Terms from time to time by publishing an updated version on our website. By continuing to 
				    			use our website you will be deemed to accept the updated Website Terms and agree to be bound by them.</p>
				    			<h4>No unlawful, infringing or offensive activity</h4>
				    			<p>You must not post or transmit to or via our website any information or material or otherwise use our website for any 
				    			activity which breaches any laws or regulations, infringes a third party’s rights or privacy or is contrary to any 
				    			relevant standards or codes, including generally accepted community standards.</p>
				    			<h4>No interference</h4>
				    			<p>You must not attempt to change, add to, remove, deface, hack or otherwise interfere with our website or any 
				    			material or content displayed on our website.</p>
				    			<h4>We may suspend or terminate your access</h4>
				    			<p>We may suspend or terminate your access to all or any part of our website at any time, if you breach these 
				    			Website Terms in our reasonable opinion.</p>
				    			<h4>No warranties or representations</h4>
				    			<p>To the maximum extent permitted by law, we do not represent or warrant that the content on our website is accurate, 
				    			reliable, suitable, or complete.  In particular, although we use reasonable care and skill in providing our website, 
				    			we cannot promise that our website will be continuously available or virus or fault free.</p>
				    			<h4>Liability to you</h4>
				    			<p>Except as set out under this section, we may be liable to you for breach of contract or negligence under the principles 
				    			applied by the courts. We are not liable for any loss or damage to the extent that it is caused by you. To the maximum 
				    			extent permitted by law, we exclude any liability to you that may otherwise arise as a result from your use of our 
				    			website in connection with any business purpose.</p>
				    			<p>If we are not entitled by law to exclude liability arising from breach of a statutory duty or other legislation, 
				    			then to the extent we are permitted to do so we limit that liability to resupply of the relevant services, 
				    			information and associated services, as the case may be.</p>
				    			<h4>Your liability to us</h4>
				    			<p>You are liable to us for breach of the Website Terms or negligence under the principles applied by the courts.  
				    			You are not liable to us for any loss to the extent that it is caused by us.</p>
				    			<h4>Our trade marks</h4>
				    			<p>Our websites include registered trade marks and trade marks which may be the subject of pending applications 
				    			or which are otherwise protected by law; including but not limited to the words MATTHEWS, MATTHEWS INTELLIGENT 
				    			IDENTIFICATION and iQSAFETY.  You may not use these trade marks, or the names of any of our related companies 
				    			without our prior written consent.</p>
				    			<h4>Our content</h4>
				    			<p>All copyright and other intellectual property rights subsisting in our website and the material on our website 
				    			(including, without limitation, the software, design, text and graphics comprised in our website and the selection 
				    			and layout of our website) are owned or licensed by us and protected by the laws of Australia and other countries.</p>
				    			<p>You may view our website and its contents using your web browser or, where expressly invited to do so, share 
				    			certain content on social media. You must not otherwise reproduce, transmit (including broadcast), communicate, 
				    			adapt, distribute, sell, modify or publish or otherwise use any of the material on our website, including audio 
				    			and video excerpts, except as permitted by statute or with our prior written consent.</p>
				    			<p>We do not guarantee that access to our website will be uninterrupted or that our website is free from viruses 
				    			or anything else which may damage any computer which accesses our website or any data on such a computer.</p>
				    			<h4>Links to third party sites</h4>
				    			<p>Our website may contain links to third party websites. The links are provided solely for your convenience 
				    			and do not indicate, expressly or impliedly, any endorsement by us of the sites or the information, products 
				    			or services provided at or via those sites. You access those sites and use the information, products and services 
				    			made available at those sites solely at your own risk.</p>
				    			<h4>Linking to our website</h4>
				    			<p>You may create a link to our website. However, you must not:</p>
				    			<ul>
				    			  	<li>display any page of our website in any distorted or altered form;</li>
				    			  	<li>create a link to our websites on any site unless that site conforms to accepted standards of public 
				    			  	decency and good taste, does not expose us to any risk of liability under any criminal or civil law 
				    			  	and does not disparage us or our goods or services; or</li>
				    			  	<li>create any link or use any link in any way to represent or imply falsely, deceptively or confusingly 
				    			  	that we sponsor, endorse or are affiliated with or related to any third party (including you) or any 
				    			  	product or you or a third party are providing, or are the source of, any goods or services provided by us.</li>
				    			</ul>
				    			<p>We reserve the right to withdraw linking permission by giving notice to you or updating these Website Terms.  
				    			You agree to indemnify us against all actions, claims, costs, demands, damages or liability arising in any manner 
				    			from any link that you create.</p>
				    			<h4>Governing law</h4>
				    			<p>These Website Terms are governed by and construed in accordance with the laws of the State of Victoria, Australia. 
				    			In the event that a dispute arises from these Website Terms, you agree to submit to the non-exclusive jurisdiction 
				    			of the courts of Victoria, Australia.</p>
		    			    </div>
	    			    </div>
    			    </div>
	    		</div>   			
	    	</div> 
		</div>
	</section>
	
	<!--Footer-->
	<?php include('html/footer.php'); ?>
	<!--/Footer-->

	<!--/js-files-->
	<?php include('html/js-files.php'); ?>
	<!--/js-files-->

</body>
</html>