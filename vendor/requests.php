<?php

chdir(dirname(__DIR__));

//loading zend framework
include __DIR__.'/Zend/Loader/AutoloaderFactory.php';
Zend\Loader\AutoloaderFactory::factory(array(
		'Zend\Loader\StandardAutoloader' => array(
				'autoregister_zf' => true,
				'db' => 'Zend\Db\Sql'
		)
));

// Get the JSON request and convert it from string to php array
$request = json_decode(file_get_contents("php://input"));

//Kill the request if action is not specified
if(!isset($request->action)) {
	$res = new stdClass();
	$res->status = 0;
	$res->message = "No action specified";
	echo json_encode($res);
	die();
}

include 'custom/Api.php';
$res = new stdClass();

switch($request->action) {
	case "adminLogin":
		$res = Api::adminLogin($request);
		if ($res->status == 1) {
			@session_start();
			$_SESSION['user_id'] = $res->user['id'];
			$_SESSION['role'] = $res->user['role'];
			$_SESSION['user'] = $res->user['email'];
		}
		break;

	case 'forgotPassword':
		$res = Api::forgotPassword($request);
		break;


	case 'changePasssword':
		@session_start();
		if (isset($_SESSION['user_id'])) {
			$request->user_id = $_SESSION['user_id'];
			$res = Api::changePasssword($request);
		} else {
			die(print_r($_SESSION));
			$res->status = 0;
			$res->message = "Session expired please login again.";			
		}
		
		break;
	
	case 'fetchAllCategories':
		$res = Api::fetchAllCategories($request);
		break;

	case 'fetchAllSubCategories':
		$res = Api::fetchAllCategories($request);
		break;

	case 'deleteCategory':
		$res = Api::deleteCategory($request);
		break;

	case 'saveCategoryOrder':
		$res = Api::saveCategoryOrder($request);
		break;

	case 'saveSubCategoryOrder':
		$res = Api::saveCategoryOrder($request);
		break;

	case 'fetchAllSpecification':
		$res = Api::fetchAllSpecification($request);
		break;

	case 'changeSpecificationStatus':
		$res = Api::changeSpecificationStatus($request);
		break;

	case 'getCategoryDetails':
		$res = Api::getCategoryDetails($request);
		break;

	case 'getSpecificationDetails':
		$res = Api::getSpecificationDetails($request);
		break;

	case 'fetchSpecificationsByProductId':
		$res = Api::fetchSpecificationsByProductId($request);
		break;

	case 'deleteSpecification':
		$res = Api::deleteSpecification($request);
		break;

	case 'fetchAllClass':
		$res = Api::fetchAllClass($request);
		break;

	case 'deleteClass':
		$res = Api::deleteClass($request);
		break;

	case 'getClassDetails':
		$res = Api::getClassDetails($request);
		break;



	case 'fetchAllProducts':
		$res = Api::fetchAllProducts($request);
		break;

	case 'fetchSearchProducts':
		$res = Api::fetchSearchProducts($request);
		break;

	case 'deleteProduct':
		$res = Api::deleteProduct($request);
		break;

	case 'getProductDetails':
		$res = Api::getProductDetails($request);
		break;
	

	case 'changeProductStatus':
		$res = Api::changeProductStatus($request);
		break;

	case 'filterProducts':
		$res = Api::filterProducts($request);
		break;

	case 'saveRelatedProduct':
		$res = Api::saveRelatedProduct($request);
		break;

	case 'setRelatedProduct':
		$res = Api::setRelatedProduct($request);
		break;

	case 'unsetRelatedProduct':
		$res = Api::unsetRelatedProduct($request);
		break;

	case 'addArticle':
		$res = Api::addArticle($request);
		break;

	case 'fetchArticle':
		$res = Api::fetchArticle($request);
		break;

	case 'fetchArticles':		
		$res = Api::fetchArticles($request);			
		break;

		
	case 'updateArticle':
		$res = Api::updateArticle($request);
		break;

		
	case 'changeArticleStatus':
		$res = Api::changeArticleStatus($request);
		break;

	case 'deleteArticle':
		$res = Api::deleteArticle($request);
		break;

	case 'addVideo':
		$res = Api::addVideo($request);
		break;

	case 'fetchVideo':
		$res = Api::fetchVideo($request);
		break;

	case 'fetchVideos':
		$res = Api::fetchVideos($request);
		break;

	case 'updateVideo':
		$res = Api::updateVideo($request);
		break;

	case 'changeVideoStatus':
		$res = Api::changeVideoStatus($request);
		break;

	case 'changeOrderStatus':
		$res = Api::changeOrderStatus($request);
		break;

	case 'deleteVideo':
		$res = Api::deleteVideo($request);
		break;

	case 'addProductTocart':
		@session_start();
		if (!(isset($_SESSION['products']))) {
    		$_SESSION['products'] = array();
    	}
		$request->cart = $_SESSION['products'];
		$res = Api::addProductTocart($request);
		if ($res->status == 1) {
			$_SESSION['products'] = $res->cart;
		}
		break;

	case 'showProductsOfCart':
		@session_start();
		
		if (!(isset($_SESSION['products']))) {
    		$_SESSION['products'] = array();
    	}
		$request->cart = $_SESSION['products'];
    	if (count($request->cart) > 0) {
			$res = Api::showProductsOfCart($request);
    	}
    	else {
    		$res->status = 0;
    		$res->message = "Your cart is empty.";
    	}
	break;

	case 'deleteProductFromCart':
		@session_start();
		
		if (!(isset($_SESSION['products']))) {
    		$_SESSION['products'] = array();
    	}
		$request->cart = $_SESSION['products'];
		$res = Api::deleteProductFromCart($request);
		if ($res->status  == 1) {
			$_SESSION['products'] = $res->cart;
		}
		break;

	case 'updateCartProducts':
		@session_start();
		
		if (!(isset($_SESSION['products']))) {
    		$_SESSION['products'] = array();
    	}
		$_SESSION['products'] = $request->cart;
		$res->status = 1;
		$res->message = "Cart Updated";
		break;

	case 'saveCheckoutDetail':
		@session_start();
		
		if (!(isset($_SESSION['products']))) {
    		$_SESSION['products'] = array();
    	}
		$request->cart = $_SESSION['products'];
		$res = Api::saveCheckoutDetail($request);
		@session_start();
		@session_destroy();
		break;

	case 'sendContactDetails':
		$res = Api::sendContactDetails($request);
		break;

	case 'fetchOrders':
		$res = Api::fetchOrders($request);
		break;

	case 'addUser':
		$res = Api::addUser($request);
		break;

	case 'editUser':
		$res = Api::editUser($request);
		break;

	case 'getUsers':
		$res = Api::getUsers($request);
		break;

	case 'getUserDetails':
		$res = Api::getUserDetails($request);
		break;

	case 'deleteUser':
		$res = Api::deleteUser($request);
		break;

	case 'changeUserStatus':
		$res = Api::changeUserStatus($request);
		break;

	case 'getProductsByCustomerId':
		$res = Api::getProductsByCustomerId($request);
		break;

	case 'getProductsByOrderId':
		$res = Api::getProductsByOrderId($request);
		break;


	default:
		$res->status = 0;
		$res->message = "Invalid Action";
		$res->action = $request->action;
	break;
}

//echo out the string converted php array so JSON can be parsed using JS
echo json_encode($res);