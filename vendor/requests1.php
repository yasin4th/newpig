<?php

// for Product-details.php

chdir(dirname(__DIR__));

//loading zend framework
include __DIR__.'/Zend/Loader/AutoloaderFactory.php';
Zend\Loader\AutoloaderFactory::factory(array(
		'Zend\Loader\StandardAutoloader' => array(
				'autoregister_zf' => true,
				'db' => 'Zend\Db\Sql'
		)
));

// Get the JSON request and convert it from string to php array
//$request = json_decode(file_get_contents("php://input"));

//Kill the request if action is not specified
if(!isset($request->action)) {
	$res = new stdClass();
	$res->status = 0;
	$res->message = "No action specified";
	//echo json_encode($res);
	die();
}
if ( !class_exists('Api') ) {
    include 'custom/Api.php';    
}

$res = new stdClass();

switch($request->action) {
	

	case 'getProductDetails':
		$res = Api::getProductDetails($request);
		break;

	case 'getCategoryDetails':
		$res = Api::getCategoryDetails($request);
		break;

	case 'getRelatedImages':
		$res = Api::getProductDetails($request);
		break;

	case 'xmlCategory':
		$res = Api::xmlCategory($request);
		break;

	default:
		$res->status = 0;
		$res->message = "Invalid Action";
		$res->action = $request->action;
	break;
}
	
//echo out the string converted php array so JSON can be parsed using JS
//echo json_encode($res);