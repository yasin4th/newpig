<?php 
	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Sql;
	use Zend\Db\ResultSet\HydratingResultSet;
	use Zend\Db\Sql\Predicate\PredicateInterface;
	use Zend\Db\Sql\Expression;

	require_once('Connection.php');
	require_once('Api.php');
	/**
	* 
	*/
	class Specification extends Connection
	{
		
		function addSpecification($request) {
			$result = new stdClass();
			if($this->checkDuplicate($request->name)){

				try {
					$adapter = $this->adapter;
					$sql = new Sql($adapter);

					// INSERT INTO `category`(`name`, `description`, `status`, `image`) VALUES ('cat 1', 'decs place here', 1, 'images/category/default.png')
					$data = array(
						'name'	=>	$request->name,						
						'status'	=>	$request->status
					);				

					$specification = new TableGateway('specification', $adapter, null, new HydratingResultSet());
					$specification->insert($data);			


					$result->status = 1;
					$result->message = "Specification successfully added.";

					
					return $result;
				} catch(Exception $e) {
					$result->status = 0;
					$result->message = $e->getMessage();
					return $result;
				}
			}else{
				$result->status = 0;
				$result->message = "Already exist.";
				return $result;
			}
		}

		public function fetchAllSpecification($request) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();
				$select->from('specification');
				$where = array('deleted' => 0);

				if(isset($request->status)){
					$where['status'] = $request->status;
				}
				
				

				$select->where($where);
				// $select->columns(array('id', 'name', 'description', 'status', 'image', 'parent_id', 'alias'));
				$select->order('name');
				$selectString = $sql->getSqlStringForSqlObject($select);
				//die($selectString);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();
				
				$result->specs = $rows;
				$result->status = 1;
				$result->message = "Specifications fetched.";

				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		public function getSpecificationDetails($request) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();

				$select->from('specification');

				
				if (isset($request->spec_id)) {
					$select->where(array(
						'id'	=>	$request->spec_id,
						//'deleted' => 0
					));
				}
				
				
				$selectString = $sql->getSqlStringForSqlObject($select);
				
				//die($selectString);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$row = $run->toArray();
				
				$result->spec = $row[0];
				$result->status = 1;
				$result->message = "Specification fetched.";

				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		function updateSpecification($request) {
			$result = new stdClass();
			
			if($this->checkDuplicate($request->name,$request->spec_id)){

				try {
					$adapter = $this->adapter;
					$sql = new Sql($adapter);

					$update = $sql->update();
	                $update->table('specification'); 
	                $data = array(
	                    'name'	=>	$request->name,						
						'status'	=>	$request->status,						
					);
					
	                $update->set($data);
	                $update->where(array(
	                    'id' => $request->spec_id
	                ));
	                $statement = $sql->prepareStatementForSqlObject($update);
	                $statement->execute();

					$result->status = 1;
					$result->message = "Specification successfully updated.";

					// die(print_r($request));
					
					return $result;
				} catch(Exception $e) {
					$result->status = 0;
					$result->message = $e->getMessage();
					return $result;
				}
			}else{
				$result->status = 0;
				$result->message = "Already exist.";
				return $result;
			}
			
		}


		function deleteSpecification($request) {
			$result = new stdClass();
			
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$update = $sql->update();
                $update->table('specification');
                $update->set(array(
                    'deleted'	=>	1
				));
                $update->where(array(
                    'id' => $request->spec_id
                ));
                $statement = $sql->prepareStatementForSqlObject($update);
                $statement->execute();

				$result->status = 1;
				$result->message = "Specification deleted.";

				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		
		function checkDuplicate($name,$id='-1') {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();
				
				$select->from('specification');
						
				$where = new $select->where();
				$where->equalTo('name', $name);		
				$where->notEqualTo('id', $id);		
								
				
				
				$select->where($where);
				
				//$select->columns(array('id', 'name', 'description', 'status', 'image', 'parent_id' ));

				$selectString = $sql->getSqlStringForSqlObject($select);
				//die($selectString);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();
				$c = 0;
				$c = count($rows);

				if($c==0){
					return true;					
				}else{
					return false;
				}

				
			} catch(Exception $e) {
				
				return false;
			}
		} 

		
		function changeSpecificationStatus($request) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$update = $sql->update();
                $update->table('specification');
                $update->set(array(
                    'status'	=>	$request->status
				));
                $update->where(array(
                    'id' => $request->spec_id
                ));
				
                $selectString = $sql->getSqlStringForSqlObject($update);
				//die($selectString);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
                
				$result->status = 1;
				$result->message = "Specification Updated.";

				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		public function fetchSpecificationsByProductId($request) {
			$result = new stdClass();
			//die(print_r($request));
			try {
				$adapter = $this->adapter;

				$sql = new Sql($adapter);

				$select = $sql->select();
				$select->from(array('p_spec' => 'product_specification'), array('value'))
						->join(array('spec' => 'specification'),
                			'p_spec.spec_id = spec.id',
                    			array('name' => 'name'), 'left');
						
				
				$where = array('p_spec.product_id' => $request->product_id);
				
				
				$select->where($where);
				$selectString = $sql->getSqlStringForSqlObject($select);
				//die($selectString);
				
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();
				
				$result = $rows;
				
				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
			
		}
	}   





	