<?php 
	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Sql;
	use Zend\Db\ResultSet\HydratingResultSet;
	use Zend\Db\Sql\Predicate\PredicateInterface;
	use Zend\Db\Sql\Expression;

	require_once('Connection.php');

	/**
	* 
	*/
	class Classification extends Connection
	{
		
		function addClass($request) {
			$result = new stdClass();

			if($this->checkDuplicate($request->name,$request->sub_category)){

			
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				// INSERT INTO `class`(`name`, `decscription`, `category_id`, `status`, `image`) VALUES
				$data = array(
					'name'	=>	$request->name,
					'description'	=>	$request->description,
					'category_id'	=>	$request->sub_category,
					'status'	=>	$request->status,
					'meta_title' => $request->meta_title,
					'meta_keywords' => $request->meta_keywords,
					'meta_description' => $request->meta_description,
					'details'	=>	$request->details
				);
				// die(print_r($data));
				$class = new TableGateway('class', $adapter, null, new HydratingResultSet());
				$class->insert($data);

				$id = $class->getLastInsertValue();

				$adapter1 = $this->adapter;
				$sql1 = new Sql($adapter1);
				$update = $sql1->update();
                $update->table('class');
                $data1 = array(
                    'alias' => Api::formDashedString($request->name).'_n'.$id
				);
                $update->set($data1);
                $update->where(array(
                    'id' => $id
                ));
                //die(print_r($request));
                $statement1 = $sql1->prepareStatementForSqlObject($update);
                $statement1->execute();


				$result->status = 1;
				$result->message = "Class successfully added.";


				if (isset($request->image)) {
					// die(print_r($request));
					$ext = pathinfo($request->image['name'], PATHINFO_EXTENSION);

					$dir = '../images/class/';
					$file_name = $request->name.'_'.$id.'.'.$ext;


					//if ($condition) {
					if (move_uploaded_file($request->image['tmp_name'], $dir.$file_name)) {
						$img_url = 'images/class/'.$file_name;
						$update = $sql->update();
	                    $update->table('class');
	                    $update->set(array(
	                    	'image'	=>	$img_url, 
	                    ));
	                    $update->where(array(
	                        'id' => $id
	                    ));
	                    $statement = $sql->prepareStatementForSqlObject($update);
	                    $statement->execute();
					}
					else {
						$result->status = 0;
						$result->message = "Class added but error in uploading image.";
					}



					if($ext=='jpeg' || $ext=='jpg'){

					$img = @imagecreatefromjpeg($request->image['tmp_name']);
					$croped_image = @imagecrop($img, $request->cords);
					$condition = @imagejpeg($croped_image, $dir.$file_name,90 );

					}
					if($ext=='png'){

					$img = @imagecreatefrompng($request->image['tmp_name']);
					$croped_image = @imagecrop($img, $request->cords);
					$condition = @imagepng($croped_image, $dir.$file_name );

					}					

					if ($condition) {
					//if (move_uploaded_file($request->image['tmp_name'], $dir.$file_name)) {
						$img_url = 'images/class/'.$file_name;
						$update = $sql->update();
	                    $update->table('class');
	                    $update->set(array(
	                    	'image'	=>	$img_url, 
	                    ));
	                    $update->where(array(
	                        'id' => $id
	                    ));
	                    $statement = $sql->prepareStatementForSqlObject($update);
	                    $statement->execute();
					}
					
					// if ($ext in array('jpg', 'jpeg', 'png', 'gif')) {
					// }
				}
				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}else{
			$result->status = 0;
			$result->message = "Class Already exist. Please choose a different Name.";
			return $result;
		}
		}

		public function fetchAllCLass($request) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();
				$select->from(array('cls' => 'class'))
				->join(array('sc'=>'category'),'sc.id = cls.category_id', array('sub_category' => 'name', 'subcatalias' => 'alias'),'left')
				->join(array('c'=>'category'),'c.id = sc.parent_id', array('category' => 'name'),'left');
				$where = array('cls.deleted' => 0);
				
				if (isset($request->category_id)) {
					$where['cls.category_id'] = $request->category_id;
				}
				if (isset($request->alias)) {
				 	$id=Api::decodeId($request->alias);
				 	$where['cls.category_id'] = $id;
				}

				$select->where($where);
				$select->order('name');
				$select->columns(array('id', 'name', 'description', 'category_id', 'status', 'image', 'alias', 'meta_title', 'meta_keywords', 'meta_description'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();
				
				$result->classes = $rows;
				$result->status = 1;
				$result->message = "Class fetched.";

				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		public function getClassDetails($request) {
			$result = new stdClass();
			
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);


				$select = $sql->select();

				$select->from(array('cls' => 'class'))
						->join(array('sc'=>'category'),'sc.id = cls.category_id', array('scname' => 'name','salias' => 'alias'),'left')
						->join(array('c'=>'category'),'sc.parent_id = c.id', array('cname' => 'name','calias' => 'alias'),'left');

				if(isset($request->class_id)){
					$select->where(array(
						'cls.id'	=>	$request->class_id,
						'cls.deleted' => 0
					));
				}else if(isset($request->alias)){
					$select->where(array(
						'cls.id'	=>	Api::decodeId($request->alias),
						'cls.deleted' => 0
					));
				}else{
					$select->where(array(
						'cls.deleted' => 0
					));
				}				
				$select->columns(array('id', 'name', 'description', 'category_id', 'status', 'image', 'alias', 'meta_title', 'meta_keywords', 'meta_description', 'details'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				//die($selectString);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$row = $run->toArray();
				
				$result->class = $row[0];
				$result->status = 1;
				$result->message = "Class fetched.";

				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		function updateClass($request) {
			$result = new stdClass();

			if($this->checkDuplicate($request->name,$request->sub_category,$request->id)){

			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$update = $sql->update();
                $update->table('class');
                $data = array(
                    'name'	=>	$request->name,
					'description'	=>	$request->description,
					'category_id'	=>	$request->sub_category,
					'status'	=>	$request->status,
					'alias' => Api::formDashedString($request->name).'_n'.$request->id,
					'meta_title' => $request->meta_title,
					'meta_keywords' => $request->meta_keywords,
					'meta_description' => $request->meta_description,
					'details'	=>	$request->details
				);

                $update->set($data);
                $update->where(array(
                    'id' => $request->id
                ));
                $statement = $sql->prepareStatementForSqlObject($update);
                $statement->execute();

				$result->status = 1;
				$result->message = "Class successfully updated.";

				// die(print_r($request));
				if (isset($request->image)) {
					// die(print_r($request));
					$ext = pathinfo($request->image['name'], PATHINFO_EXTENSION);

					$dir = '../images/class/';
					$file_name = $request->name.'_'.$request->id.'.'.$ext;

					if($ext=='jpeg' || $ext=='jpg'){

					$img = imagecreatefromjpeg($request->image['tmp_name']);
					$croped_image = imagecrop($img, $request->cords);
					$condition = imagejpeg($croped_image, $dir.$file_name,90 );

					}
					if($ext=='png'){

					$img = imagecreatefrompng($request->image['tmp_name']);
					$croped_image = imagecrop($img, $request->cords);
					$condition = imagepng($croped_image, $dir.$file_name );

					}
					

					if ($condition) {
					// if (move_uploaded_file($request->image['tmp_name'], $dir.$file_name)) {
						$img_url = '/images/class/'.$file_name;
						$update = $sql->update();
	                    $update->table('class');
	                    $update->set(array(
	                        'image'	=>	$img_url, 
	                    ));
	                    $update->where(array(
	                        'id' =>	 $request->id
	                    ));
	                    $statement = $sql->prepareStatementForSqlObject($update);
	                    $statement->execute();
					}
					else {
						$result->status = 0;
						$result->message = "Class updated but error in uploading image.";
					}
					// if ($ext in array('jpg', 'jpeg', 'png', 'gif')) {
					// }
				}
				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}}
			else{
				$result->status = 0;
				$result->message = "Class Already exist. Please choose a different Name.";
				return $result;
			}
		}


		function deleteClass($request) {
			$result = new stdClass();

			if($this->alreadyExist($request->class_id)){
				try {
					$adapter = $this->adapter;
					$sql = new Sql($adapter);

					$update = $sql->update();
	                $update->table('class');
	                $update->set(array(
	                    'deleted'	=>	1
					));
	                $update->where(array(
	                    'id' => $request->class_id
	                ));
	                $statement = $sql->prepareStatementForSqlObject($update);
	                $statement->execute();

					$result->status = 1;
					$result->message = "Class deleted.";

					return $result;
				} catch(Exception $e) {
					$result->status = 0;
					$result->message = $e->getMessage();
					return $result;
				}
			}else{
				$result->status = 0;
				$result->message = "Unable to delete as it has associated products.";
				return $result;
			}
		}


		function filterClass($request) {
			//die(print_r($request));
			$result = new stdClass();
			
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();
				// SELECT products.name, products.part_name, products.supplier_part_name, products.small_description, products.image,  order_details.id AS order_id, order_details.product_id, order_details.quantity FROM order_details LEFT JOIN products ON products.id = order_details.product_id WHERE 
				//$select->from('class');
				$select->from(array('cls' => 'class'))
				->join(array('sc'=>'category'),'sc.id = cls.category_id', array('sub_category' => 'name'),'left')
				->join(array('c'=>'category'),'c.id = sc.parent_id', array('category' => 'name'),'left');
				$where = array('cls.deleted' => 0);

				if (isset($request->classname)) {
					$select->where('cls.name LIKE "%'.$request->classname.'%"');
					
				}
				if (isset($request->status)) {
					$where['cls.status'] = $request->status;
					
				}
				if (isset($request->subcat)) {
					$where['cls.category_id'] = $request->subcat;					
				}
				if (isset($request->cat)) {
					$where['c.id'] = $request->cat;
					//$select->where('category_id IN (SELECT id FROM category WHERE parent_id = '.$request->cat.')');
				}
				
				//$where['deleted'] = '0';
				$select->where($where);
				$select->order('id DESC');
				$select->columns(array('id', 'name', 'description', 'status', 'image', 'category_id', 'alias', 'meta_title', 'meta_keywords', 'meta_description'));

				$selectString = $sql->getSqlStringForSqlObject($select);
				//die($selectString);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();
				
				
				$result->classes = $rows;
				
				$result->status = 1;
				$result->message = "Filtered";

				return $result;	
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		function checkDuplicate($name, $subcat = '0', $id = '0') {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();
				
				$select->from('class');
						
				$where = new $select->where();
				$where->equalTo('deleted', 0);
				$where->equalTo('category_id', $subcat);
				$where->notEqualTo('id', $id);

				//$where = array('parent_id' => $parent);
				
								
				if (isset($name)) {
					$where->equalTo('name', $name);

				}
				
				$select->where($where);
				
				//$select->columns(array('id', 'name', 'description', 'status', 'image', 'parent_id' ));

				$selectString = $sql->getSqlStringForSqlObject($select);
				//die($selectString);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();
				$c = 0;
				$c = count($rows);

				if($c==0){
					return true;					
				}else{
					return false;
				}

				
			} catch(Exception $e) {
				
				return false;
			}
		} 
		function alreadyExist($cls) {
			//var_dump($cat);
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();
				
				$select->from('products');
						
				$where = new $select->where();
				$where->equalTo('class_id', $cls);	
				$select->where($where);
				

				$selectString = $sql->getSqlStringForSqlObject($select);
				
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();
				$c = 0;
				$c = count($rows);

				if($c==0){
					// deleting sub category
					return true;					
							
				}else{
					//deleting category and subcategory exist
					return false;
				}



				
			} catch(Exception $e) {
				
				return false;
			}
		}
	}