<?php 
	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Sql;
	use Zend\Db\ResultSet\HydratingResultSet;
	use Zend\Db\Sql\Predicate\PredicateInterface;
	use Zend\Db\Sql\Expression;

	require_once('Connection.php');
	require_once('Utility.php');
	/**
	* 
	*/
	class CustomerCart extends Connection
	{
		
		public function showProductsOfCart($request) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$ids = array();
				foreach ($request->cart as $c => $product) {
					$ids[] = $product->product_id;
				}


				$select = $sql->select();
				$select->from('products');
				
				$where = array('deleted' => 0);
				$where['id'] = $ids;
				$select->where($where);
				$select->columns(array('id', 'name', 'part_name', 'image'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				// die($selectString);
				// $selectString = 'SELECT products.id, products.name, products.image, products.stock, products.active, class.name AS class_name, cat.name AS sub_category, (SELECT category.name FROM category WHERE category.id = cat.parent_id) AS category FROM products LEFT JOIN category AS cat ON products.category_id = cat.id LEFT JOIN class ON products.class_id = class.id WHERE products.deleted = 0 order by id desc';
				
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();
				
				$productsWithQuantity = array();
				foreach ($rows as $row) {
					foreach ($request->cart as $product) {
						if($product->product_id == $row['id'])
						{
							$row['quantity'] = $product->quantity;
							$productsWithQuantity[] = $row;
						}
					}
				}

                $result->products = $productsWithQuantity;
				$result->status = 1;
				$result->message = "Products fetched.";

				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}


		public function addProductTocart($request) {
			$result = new stdClass();
			  try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$ids = array();
                foreach ($request->cart as $c => $product) {
					$ids[] = $product->product_id;
				}
				
				if (in_array($request->product_id, $ids)) {
					$result->cart = $request->cart;
					$result->status = 0;
					$result->message = "Product already added in your cart";
				}else {
					$data = new stdClass();
			        $data->product_id = $request->product_id;
			    	$data->quantity = $request->quantity ;
					$request->cart[] = $data;
					
					$result->cart = $request->cart;
					$result->status = 1;
					$result->message = "Product added into your quote";
				}
				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

			public function deleteProductFromCart($request) {
				$result = new stdClass();
				  try {
					$adapter = $this->adapter;
					$sql = new Sql($adapter);

	                $products = array();
	                foreach ($request->cart as $x => $product ) {
	            	    if ($product->product_id == $request->id) {
	            	    	unset($request->cart[$x]);
	            	    }
	                }
	                
	                $result->cart = $request->cart;
					$result->status = 1;
					$result->message = "Product removed.";

					return $result;
				} catch(Exception $e) {
					$result->status = 0;
					$result->message = $e->getMessage();
					return $result;
				}

			}

			public function saveCheckoutDetail($request) {
				//die(print_r($request));
				$result = new stdClass();
				try {
					$adapter = $this->adapter;
					$sql = new Sql($adapter);
                    $state="";

                    switch ($request->state_id) {
                      case "917190000":
                          $state = "ACT";
                          break;
                      case "917190001":
                          $state = "NSW";
                          break;
                      case "917190002":
                          $state = "NT";
                          break;
                      case "917190003":
                          $state = "QLD";
                          break;
                      case "917190004":
                          $state = "SA";
                          break;
                      case "917190005":
                          $state = "TAS";
                          break;
                      case "917190006":
                          $state = "VIC";
                          break;
                      case "917190007":
                          $state = "WA";
                          break;
                      case "917190008":
                          $state = "Other";
                          break;
                  }                         
                  
					$data = array (
						'first_name'=>	$request->first_name,
						'last_name'	=>	$request->last_name,
						'email'	    =>	$request->email,
						'phone_number'	=>	$request->phone_number,
						'company_name'	=>	$request->company_name,
						'country'	    =>	$request->country,
						'address'	    =>	$request->address,
						'city_locality'	=>	$request->city_id,
						'state_region'	=>	$state,
						'postal_zip_code'=>	$request->postal_zip_code,
						'comments'=>	$request->comments,
						'order_time'	=>	time()
					);
                  	//$request->state_id,
					// die(print_r($data));
					$customer_detail = new TableGateway('customer_details', $adapter, null, new HydratingResultSet());
					$customer_detail->insert($data);
					$id = $customer_detail->getLastInsertValue();
					
					$statement = 'INSERT INTO `order_details`(`order_id`, `product_id`, `quantity`) VALUES ';
					foreach ($request->cart as $x => $product ) {
	            	    $statement .= '('.$id.','.$product->product_id.','.$product->quantity.'),';
	                }
	                $statement = rtrim($statement, ',');
	                $run = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);


					$result1 = new stdClass();
						
					$adapter = $this->adapter;
					$sql1 = new Sql($adapter);

					$select1 = $sql1->select();
					// SELECT products.name, products.part_name, products.supplier_part_name, products.small_description, products.image,  order_details.id AS order_id, order_details.product_id, order_details.quantity FROM order_details LEFT JOIN products ON products.id = order_details.product_id WHERE 
					$select1->from(array('order_details' => 'order_details'), array())
							->join(array('products' => 'products'),
	                			'products.id = order_details.product_id',
	                    			array('name', 'part_name', 'supplier_part_name', 'small_description', 'image','stock','mrp'), 'left');
					
					$where1 = array('products.deleted' => 0);
					
					$where1['order_details.order_id'] = $id;
					
					$select1->where($where1);
					$select1->columns(array('order_id' => 'id', 'product_id', 'quantity'));

					$selectString = $sql1->getSqlStringForSqlObject($select1);
					$run1 = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$rows1 = $run1->toArray();				
					
					$result1->products = $rows1;
					$a = "";
					$count=1;
                    $products="";
					
					foreach ($result1->products as $key => $value) {
						$a .= '<tr><td><p>'.$count.'</p></td><td><p>'.$value['part_name'].'</p></td><td><p>PIG<sup>&reg;</sup>&nbsp;'.$value['name'].'</p></td><td><p>'.$value['quantity'].'</p></td></tr>';
						//$products .= $value['quantity'].' x '.$value['part_name'].' '.$value['name'].'\n';
						$count++;
					};

					$country = ($request->country != 'Other')?$request->country:'';

	                $admin_mail = '<table border="0" cellpadding="0" cellspacing="1" style="line-height:normal; width:791px">';
	                //$admin_mail .= '<tbody><tr><td style="width:390px"><p><strong>Billed To:</strong><br />'.$request->first_name.' '.$request->last_name.'<br /><br />Company: '.$request->company_name.'<br />Email: '.$request->email.'<br />Address: '.$request->address.',<br />City: '.$request->city_id.'<br />State: '.$request->state_id.'<br />Country: '.$country.'<br />Postal Zip Code: '.$request->postal_zip_code.'<br /><br /><strong>Product Details:</strong></p></td></tr><tr><td>';
					$admin_mail .= '<tbody><tr><td style="width:390px"><p><strong>Billed To:</strong><br />'.$request->first_name.' '.$request->last_name.'<br /><br />Company: '.$request->company_name.'<br />Email: '.$request->email.'<br />Address: '.$request->address.',<br />City: '.$request->city_id.'<br />State: '.$state.'<br />Country: '.$country.'<br />Postal Zip Code: '.$request->postal_zip_code.'<br /><br /><strong>Product Details:</strong></p></td></tr><tr><td>';
                    $admin_mail .= '<table cellpadding="0" cellspacing="0" style="width:785px"><tbody><tr><td><p><strong>S.No.</strong></p></td><td><p><strong>SKU</strong></p></td><td><p><strong>Product Name</strong></p></td><td><p><strong>Unit</strong></p></td></tr>    '.$a.'    </tbody></table>';
	                $admin_mail .= '<p> '.$request->comments.'</p><p><strong>Please retain for your records.</strong></p>';


	                $customer_mail = '<table border="0" cellpadding="0" cellspacing="1" style="line-height:normal; width:791px">';
	                $customer_mail .= '<tbody><tr><td style="width:390px"><p>Hello '.$request->first_name.' '.$request->last_name.',</p><p>Thanks for submitting your quote request. Our customer service team will get in touch shortly.</p><p>Your order details.</p></td></tr><tr><td>';
	                $customer_mail .= '<table border="0" cellpadding="0" cellspacing="0" style="width:785px"><tbody><tr><td><p><strong>S.No.</strong></p></td><td><p><strong>SKU</strong></p></td><td><p><strong>Product Name</strong></p></td><td><p><strong>Unit</strong></p></td></tr>    '.$a.'    </tbody></table>';
	                $customer_mail .= '<p> '.$request->comments.'</p><p><strong>Please retain for your records.</strong></p>';

	                // Send emails 
	                $util = new Utility();
	                $util->sendEmail($request->email,'Your Quote Request Received.',$customer_mail);
	                $util->sendEmail('Customer.service@matthews.com.au', "New Quote Request Received", $admin_mail);
                    $util->sendEmail('bwnichol@matthews.com.au', "New Quote Request Received", $admin_mail);
                    $util->sendEmail('jipsa@iqsafetyproducts.com.au', "New Quote Request Received", $admin_mail);
                    
                    $result->id = $id;
                    $result->status = 1;
				    $result->message = "Customer details successfully saved.";
					return $result;
				}
				catch(Exception $e) {
					$result->status = 0;
					$result->message = "ERROR: Some thing Went Wrong.";
					$result->error = $e->getMessage();
				}
			}


			public function fetchOrders($request) {
				$result = new stdClass();
				try {
					$adapter = $this->adapter;
					$sql = new Sql($adapter);

					$select = $sql->select();
					//$select->from('customer_details');
					
					$select->from(array('cd' => 'customer_details'), array())
						->join(array('os' => 'order_status'),
                			'cd.status = os.id',
                    			array('statusName' => 'description'), 'left');
				
					$where = array('cd.deleted' => 0);
					
					$select->where($where);


					$select->columns(array('id', 'first_name', 'last_name', 'email', 'phone_number', 'company_name', 'country', 'address', 'city_locality', 'state_region', 'postal_zip_code', 'status', 'order_time' => new Expression('FROM_UNIXTIME(order_time,"%d-%m-%y %h:%i %p")')));
					$select->order('id desc');
					//$select->where(array('deleted' => 0));
					$selectString = $sql->getSqlStringForSqlObject($select);
					$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$rows = $run->toArray();
					
					$result->orders = $rows;
					$result->status = 1;
					$result->message = "Customers fetched.";

					return $result;
				} catch(Exception $e) {
					$result->status = 0;
					$result->message = $e->getMessage();
					return $result;
				}
			}

			public function filterOrder($request) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				
				$select->from(array('cd' => 'customer_details'), array())
						->join(array('os' => 'order_status'),
                			'cd.status = os.id',
                    			array('statusName' => 'description'), 'left');
				// SELECT products.name, products.part_name, products.supplier_part_name, products.small_description, products.image,  order_details.id AS order_id, order_details.product_id, order_details.quantity FROM order_details LEFT JOIN products ON products.id = order_details.product_id WHERE 
				//$select->from('products');
				
				$where = array('cd.deleted' => 0);

				if (isset($request->name)) {
					$select->where('(cd.first_name LIKE "%'.$request->name.'%" OR cd.last_name LIKE "%'.$request->name.'%")');
					
				}
				if (isset($request->status)) {
					$where['cd.status'] = $request->status;					
				}
				
				//die(var_dump($from));
				if (isset($request->to) && isset($request->from)) {
					$from = strtotime($request->from);
					$to = strtotime($request->to);
					$select->where('(cd.order_time BETWEEN "'.$from.'" AND "'.$to.'")');			
				}
				if (isset($request->from)) {
					$from = strtotime($request->from);
					
					$select->where('cd.order_time > "'.$from.'"');			
				}

				$select->where($where);
				$select->order('id DESC');
				//$select->columns(array('id', 'name', 'image' ));

				$select->columns(array('id', 'first_name', 'last_name', 'email', 'phone_number', 'company_name', 'country', 'address', 'city_locality', 'state_region', 'postal_zip_code', 'status', 'order_time' => new Expression('FROM_UNIXTIME(order_time,"%d-%m-%y %h:%i %p")')));
				
						
				$selectString = $sql->getSqlStringForSqlObject($select);
				//die($selectString);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();
				$result->orders = $rows;

				//die(print_r($result));
				
				$result->status = 1;
				$result->message = "Filtered";

				return $result;	
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		public function sendContactDetails($request) {
				$result = new stdClass();
				try {				
					

	                $customer_mail = $request->name.' '.$request->email.' '.$request->mobile_number.' '.$request->message;

	                
	                $util = new Utility();
	                $util->sendEmail('Customer.service@matthews.com.au','Contact Us.','Subject : '.$customer_mail);
	                $util->sendEmail('bwnichol@matthews.com.au','Contact Us.','Subject : '.$customer_mail);
                    $util->sendEmail('jipsa@iqsafetyproducts.com.au','Contact Us.','Subject : '.$customer_mail);
                   
                    $result->status = 1;
				    $result->message = "Contact form submitted. We will revert back.";
					return $result;
				}
				catch(Exception $e) {
					$result->status = 0;
					$result->message = "ERROR: Some thing Went Wrong.";
					$result->error = $e->getMessage();
				}
			}
	}