<?php 
	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Sql;
	use Zend\Db\ResultSet\HydratingResultSet;
	use Zend\Db\Sql\Predicate\PredicateInterface;
	use Zend\Db\Sql\Expression;

	require_once 'Connection.php';

	/**
	* 
	*/
	class User extends Connection
	{
		
		public function register($res) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('users');
				$select->where(array(
					'email'	=>	htmlentities($data->email),
					'deleted'	=> 0
				));
				$select->columns(array('email'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$res = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$rows = $res->toArray();
				if(count($rows) == 0) {
					$insert = new TableGateway('users', $adapter, null, new HydratingResultSet());
					$hashed = md5($data->password);
					$insert->insert(array(
						'first_name'	=>	htmlentities($data->firstname),
						'last_name'	=>	htmlentities($data->lastname),
						'email'	=>	htmlentities($data->email),
						'password'	=>	$hashed,
						'created_on'	=>	time(),
						'role'	=>	1
					));

					$id = $insert->getLastInsertValue();
					
					if(count($id)) {
						$select = $sql->select();
						$select->from('users');
						$select->where(array(
									'id'	=>	$id
						));
						$select->columns(array('id','email','role'));
						$selectString = $sql->getSqlStringForSqlObject($select);
						$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$row = $temp->toArray();

						$result->user = $row[0];
						$result->status = 1;
					}
				}
				else {
					$result->status = 0;
					$result->message = "E-mail already registered.";
				}
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		public function addUser($data) {
			$result = new stdClass();
			if($this->checkDuplicate($data->email)){
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('users');
				$select->where(array(
					'email'	=>	htmlentities($data->email),
					'deleted'	=> 0
				));
				$select->columns(array('email'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				//die($selectString);
				$res = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$rows = $res->toArray();
				if(count($rows) == 0) {
					$insert = new TableGateway('users', $adapter, null, new HydratingResultSet());
					$hashed = md5($data->password);
					$insert->insert(array(
						'first_name'	=>	htmlentities($data->fname),
						'last_name'	=>	htmlentities($data->lname),
						'email'	=>	htmlentities($data->email),
						'password'	=>	$hashed,
						'phone'	=>	$data->password,
						'created_on'	=>	time(),
						'role'	=>	$data->role
					));

					$id = $insert->getLastInsertValue();
					
					if(count($id)) {
						$select = $sql->select();
						$select->from('users');
						$select->where(array(
									'id'	=>	$id
						));
						$select->columns(array('id','email','role'));
						$selectString = $sql->getSqlStringForSqlObject($select);
						$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$row = $temp->toArray();

						$result->user = $row[0];
					}
					$result->status = 1;
					$result->message = "User Added Successfully";
				}
				else {
					$result->status = 0;
					$result->message = "E-mail already registered.";
				}
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
			}else{
				$result->status = 0;
				$result->message = "Already exist.";
				return $result;
			}
		}


		public function adminLogin($req) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$hashed_pass = md5($req->password);
				$admin_roles = array(1,2,3);

				$select = $sql->select();
				$select->from('users');
				
				$where = new $select->where();
				$where->equalTo('email', htmlentities($req->email));
				$where->equalTo('password', $hashed_pass);
				$where->equalTo('status', 1);
				$where->equalTo('deleted', 0);
				$where->in('role',$admin_roles);
				$select->where($where);
				$select->columns(array('id','email','role','first_name'));
				$statement = $sql->getSqlStringForSqlObject($select);
				//die($statement);
				$temp = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$rows = $temp->toArray();
				
				if(count($rows) == 1) {
					$update = $sql->update();
					$update->table('users');
					$update->set(array(
						'last_login'	=>	time()
					));
					$update->where(array(
						'id'	=>	$rows[0]['id']
					));
					$statement = $sql->getSqlStringForSqlObject($update);
					$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					
					$result->status = 1;
					$result->user = $rows[0];
					session_start();
					$_SESSION['user_id']=$rows[0]['id'];
					$_SESSION['role']=$rows[0]['role'];
					$_SESSION['name']=$rows[0]['first_name'];
					

					//print_r($_SESSION);	
					$result->message = "Successfully Logged In.";
					return $result;
				}
				

				@session_start();
				@session_destroy();
				$result->status = 0;
				$result->message = 'Invalid Credentials...';
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}



		public function forgotPassword($request)
		{

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				// if ($request->captcha_code == $request->org_captcha_code ) {
					$select = $sql->select();
					$select->from('users');
					$select->where(array(
						'email'	=>	$request->email,
						'deleted' => 0
					));
					$selectString = $sql->getSqlStringForSqlObject($select);
					$rows = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$rows = $rows->toArray();					

					if (count($rows) == 1) {
						$user = $rows[0];

						$code = md5(time().uniqid()); 
					
						$update = $sql->update();
						$update->table('users');
						$update->set(array(
							'reset_code' => $code
						));
						$update->where(array(
							'id' => '6',
						));
						$statement = $sql->getSqlStringForSqlObject($update);
						$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
						
						

						$url = 'http://www.evnxt.com/newpig/dev/admin/reset-password.php?token='.$code;
					    $mailTo = "info@newpig.com";
					    $mailTo = "utkarsh27a@gmail.com";
	                    $mailFrom = "noreply@evnxt.com"; 
	                    $mailSubject="Reset Password";
	                    $mailHeader = "From: ".$mailFrom."\r\n"; 
	                    $mailHeader .= "Content-type: text/html; charset=iso-8859-1\r\n"; 
	                    $mailBody='<p><span style="background-color:rgb(255, 255, 255); color:rgb(34, 34, 34)">Hello Admin </span>
	                                  <br></p>
	                              <p><span style="background-color:rgb(255, 255, 255); color:rgb(34, 34, 34)">Please follow this link to reset your password: '.$url.'</span>
	                                  <br>
	                              </p>';
	                    $sender = mail($mailTo, $mailSubject, $mailBody, $mailHeader);
	                    if ($sender) {
							$result->status = 1;
							$result->message = "We send a password reset link to your e-mail.";
	                    } else {
							$result->status = 0;
							$result->message = "Error in sending mail, please try after some time.";
	                    }
	                
					} else {
						$result->status = 0;
						$result->message = "Your e-mail for forgot password is incorrect.";
					}
					
					
				// }
				// else{
				// 	$result->status = 0;
				// 	$result->message = "You entered incorrect captcha-code , please try again.";
				// }
				return $result;
			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}

		}


		public function resetPassword($request)
		{

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();
				$select->from('users');
				$select->where(array(
					'reset_code'	=>	$request->code
				));
				// $select->columns(array('id','username'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$rows = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$rows = $rows->toArray();
				$temp = $rows[0];

				if (count($rows) == 1) {
					
					$update = $sql->update();
					$update->table('users');
					$update->set(array(
						'password' => md5($request->password),
						// 'reset_code' => 'xyz12555'
					));
					$update->where(array(
						'id' => $temp['id'],
					));
					$statement = $sql->getSqlStringForSqlObject($update);
					// die($statement);
					$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					
					$result->status = 1;
					$result->message = "Password Successfully Changed.";
				}
				else{
					$result->status = 0;
					$result->message = "Your code is incorrect.";
				}
				
				return $result;
			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}
		}


		public function changePasssword($request) {

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$old_password = md5($request->old_password);
				$new_password = md5($request->new_password);

				$select = $sql->select();
				$select->from('users');
				$select->where(array(
					'id'	=>	$request->user_id
				));
				// $select->columns(array('id','username'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$rows = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$rows = $rows->toArray();
				$user = $rows[0];

				if ($user['password'] === $old_password) {
					
					$update = $sql->update();
					$update->table('users');
					$update->set(array(
						'password' => $new_password,
						// 'reset_code' => 'xyz12555'
					));
					$update->where(array(
						'id' => $user['id'],
					));
					$statement = $sql->getSqlStringForSqlObject($update);
					// die($statement);
					$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					
					$result->status = 1;
					$result->message = "Password Successfully Changed.";
				}
				else{
					$result->status = 0;
					$result->message = "Your current password is incorrect.";
				}
				
				return $result;
			}catch(Exception $e ){
				$result->status = 0;
				$result->message = "ERROR: Some thing Went Wrong.";
				$result->error = $e->getMessage();
				return $result;
			}

		}

		public function getUsers($request) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;

				$sql = new Sql($adapter);

				$select = $sql->select();
				$select->from(array('users' => 'users'), array())
						->join(array('roles' => 'user_roles'),
                			'users.role = roles.id',
                    			array('role' => 'id','desc' => 'description'), 'left');
				
				$where = array('users.deleted' => 0,'users.role != 1');
				if (isset($request->user_id)) {
					$where['users.id'] = $request->user_id;
				}

				if (isset($request->role)) {
					$where['users.role'] = $request->role;
				}
				$select->where($where);
				$select->order('id DESC');
				//$select->columns(array('id', 'category_id', 'class_id', 'name', 'part_name', 'supplier_part_name', 'quantity', 'mrp', 'small_description', 'detailed_description', 'image', 'media_title', 'media_url', 'meta_title', 'meta_keywords', 'meta_description', 'sold_as', 'weight', 'stock', 'active'));
				// $select->columns(array('id', 'name', 'image', 'stock', 'active'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				//die($selectString);
				// $selectString = 'SELECT products.id, products.name, products.image, products.stock, products.active, class.name AS class_name, cat.name AS sub_category, (SELECT category.name FROM category WHERE category.id = cat.parent_id) AS category FROM products LEFT JOIN category AS cat ON products.category_id = cat.id LEFT JOIN class ON products.class_id = class.id WHERE products.deleted = 0 order by id desc';
				
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();
				
				$result->users = $rows;
				$result->status = 1;
				$result->message = "Users fetched.";

				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		public function getUserDetails($request) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;

				$sql = new Sql($adapter);

				$select = $sql->select();
				$select->from(array('users' => 'users'), array())
						->join(array('roles' => 'user_roles'),
                			'users.role = roles.id',
                    			array('role' => 'id','desc' => 'description'), 'left');
				
				$where = array('users.deleted' => 0);
				if (isset($request->user_id)) {
					$where['users.id'] = $request->user_id;
				}
				if (isset($request->role)) {
					$where['users.role'] = $request->role;
				}
				$select->where($where);
				$select->order('id DESC');
				//$select->columns(array('id', 'category_id', 'class_id', 'name', 'part_name', 'supplier_part_name', 'quantity', 'mrp', 'small_description', 'detailed_description', 'image', 'media_title', 'media_url', 'meta_title', 'meta_keywords', 'meta_description', 'sold_as', 'weight', 'stock', 'active'));
				// $select->columns(array('id', 'name', 'image', 'stock', 'active'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				//die($selectString);
				// $selectString = 'SELECT products.id, products.name, products.image, products.stock, products.active, class.name AS class_name, cat.name AS sub_category, (SELECT category.name FROM category WHERE category.id = cat.parent_id) AS category FROM products LEFT JOIN category AS cat ON products.category_id = cat.id LEFT JOIN class ON products.class_id = class.id WHERE products.deleted = 0 order by id desc';
				
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();
				
				$result->user = $rows[0];

				$result->status = 1;
				$result->message = "Users fetched.";

				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}


		function deleteUser($request) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$update = $sql->update();
                $update->table('users');
                $update->set(array(
                    'deleted'	=>	1
				));
                $update->where(array(
                    'id' => $request->user_id
                ));
                $statement = $sql->prepareStatementForSqlObject($update);
                $statement->execute();

				$result->status = 1;
				$result->message = "User deleted.";

				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		function editUser($request) {
			$result = new stdClass();

			
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$update = $sql->update();
                $update->table('users');
                $update->set(array(
                    'first_name'	=>	htmlentities($request->fname),
                    'last_name'	=>	htmlentities($request->lname),
                    'email'	=>	htmlentities($request->email),
                    'phone'	=>	$request->phone,
                    'role'	=>	$request->role
				));
                $update->where(array(
                    'id' => $request->user_id
                ));
                $statement = $sql->prepareStatementForSqlObject($update);
                $statement->execute();

				$result->status = 1;
				$result->message = "User edited.";

				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}


		function changeUserStatus($request) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$update = $sql->update();
                $update->table('users');
                $update->set(array(
                    'status'	=>	$request->status
				));
                $update->where(array(
                    'id' => $request->user_id
                ));
                $statement = $sql->prepareStatementForSqlObject($update);
                $statement->execute();

				$result->status = 1;
				$result->message = "User Updated.";

				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}	
		
		function checkDuplicate($name) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();
				
				$select->from('users');
						
				$where = new $select->where();
				$where->equalTo('deleted', 0);
				// $where->notEqualTo('id', $category);
				// $where->equalTo('parent_id', $parent);

				//$where = array('parent_id' => $parent);
				
				$where->equalTo('email', $name);
				
				$select->where($where);
				
				//$select->columns(array('id', 'name', 'description', 'status', 'image', 'parent_id' ));

				$selectString = $sql->getSqlStringForSqlObject($select);
				//die($selectString);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();
				$c = 0;
				$c = count($rows);

				if($c==0){
					return true;					
				}else{
					return false;
				}

				
			} catch(Exception $e) {
				
				return false;
			}
		} 

	}
?>