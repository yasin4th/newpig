<?php
	class Api {
	
/******************************************* Start User Functions ******************************************************************/
		public static function adminLogin($request) {
			require_once('User.php');
			$user = new User();
			$result = $user->adminLogin($request);
			return $result;
		}

		public static function forgotPassword($request) {
			require_once('User.php');
			$user = new User();
			$result = $user->forgotPassword($request);
			return $result;
		}

		public static function changePasssword($request) {
			require_once('User.php');
			$user = new User();
			$result = $user->changePasssword($request);
			return $result;
		}

		public static function addUser($request) {
			require_once('User.php');
			$user = new User();
			$result = $user->addUser($request);
			return $result;
		}

		public static function editUser($request) {
			require_once('User.php');
			$user = new User();
			$result = $user->editUser($request);
			return $result;
		}

		public static function getUsers($request) {
			require_once('User.php');
			$user = new User();
			$result = $user->getUsers($request);
			return $result;
		}

		public static function getUserDetails($request) {
			require_once('User.php');
			$user = new User();
			$result = $user->getUserDetails($request);
			return $result;
		}

		public static function deleteUser($request) {
			require_once('User.php');
			$user = new User();
			$result = $user->deleteUser($request);
			return $result;
		}

		public static function changeUserStatus($request) {
			require_once('User.php');
			$user = new User();
			$result = $user->changeUserStatus($request);
			return $result;
		}
	

/******************************************* End User Functions ******************************************************************/

/******************************************* Start Category and Sub-Category Functions ******************************************************************/


		public static function addCategory($request) {
			require_once('Category.php');
			$cat = new Category();
			$result = $cat->addCategory($request);
			return $result;
		}


		public static function fetchAllCategories($request) {
			require_once('Category.php');
			$cat = new Category();
			$result = $cat->fetchAllCategories($request);
			return $result;
		}

		public static function fetchAllSubCategories($request) {
			require_once('Category.php');
			$cat = new Category();
			$result = $cat->fetchAllSubCategories($request);
			return $result;
		}


		public static function updateCategory($request) {
			require_once('Category.php');
			$cat = new Category();
			$result = $cat->updateCategory($request);
			return $result;
		}


		public static function deleteCategory($request) {
			require_once('Category.php');
			$cat = new Category();
			$result = $cat->deleteCategory($request);
			return $result;
		}

		public static function saveCategoryOrder($request) {
			require_once('Category.php');
			$cat = new Category();
			$result = $cat->saveCategoryOrder($request);
			return $result;
		}

		public static function saveSubCategoryOrder($request) {
			require_once('Category.php');
			$cat = new Category();
			$result = $cat->saveCategoryOrder($request);
			return $result;
		}


		public static function getCategoryDetails($request) {
			require_once('Category.php');
			$cat = new Category();
			$result = $cat->getCategoryDetails($request);
			return $result;
		}


/******************************************* End Category and Sub-Category Functions ******************************************************************/


/******************************************* Start Product Class Functions ******************************************************************/



		public static function addClass($request) {
			require_once('Classification.php');
			$cat = new Classification();
			$result = $cat->addClass($request);
			return $result;
		}


		public static function fetchAllCLass($request) {
			require_once('Classification.php');
			$cat = new Classification();
			$result = $cat->fetchAllCLass($request);
			return $result;
		}


		public static function updateClass($request) {
			require_once('Classification.php');
			$cat = new Classification();
			$result = $cat->updateClass($request);
			return $result;
		}


		public static function deleteClass($request) {
			require_once('Classification.php');
			$cat = new Classification();
			$result = $cat->deleteClass($request);
			return $result;
		}


		public static function getClassDetails($request) {
			require_once('Classification.php');
			$cat = new Classification();
			$result = $cat->getClassDetails($request);
			return $result;
		}




/******************************************* End Product Class Functions ******************************************************************/

/******************************************* Start Product Functions ******************************************************************/



		public static function addProduct($request) {
			require_once('Product.php');
			$cat = new Product();
			$result = $cat->addProduct($request);
			return $result;
		}


		public static function fetchAllProducts($request) {
			require_once('Product.php');
			$cat = new Product();
			$result = $cat->fetchAllProducts($request);
			//die(print_r($result));
			return $result;
		}

		public static function fetchSearchProducts($request) {
			require_once('Product.php');
			$cat = new Product();
			$result = $cat->fetchSearchProducts($request);
			return $result;
		}

		public static function getProductDetails($request) {
			require_once('Product.php');
			$cat = new Product();
			$result = $cat->getProductDetails($request);
			return $result;
		}



		

		public static function updateProduct($request) {
			require_once('Product.php');
			$cat = new Product();
			$result = $cat->updateProduct($request);
			return $result;
		}


		public static function deleteProduct($request) {
			require_once('Product.php');
			$cat = new Product();
			$result = $cat->deleteProduct($request);
			return $result;
		}
		
		public static function changeProductStatus($request) {
			require_once('Product.php');
			$cat = new Product();
			$result = $cat->changeProductStatus($request);
			return $result;
		}

		public static function changeSpecificationStatus($request) {
			require_once('Specification.php');
			$spec = new Specification();
			$result = $spec->changeSpecificationStatus($request);
			return $result;
		}

		public static function filterProducts($request) {
			require_once('Product.php');
			$cat = new Product();
			$result = $cat->filterProducts($request);
			return $result;
		}

		public static function saveRelatedProduct($request) {
			require_once('Product.php');
			$cat = new Product();
			$result = $cat->saveRelatedProduct($request);
			return $result;
		}

		public static function setRelatedProduct($request) {
			require_once('Product.php');
			$cat = new Product();
			$result = $cat->setRelatedProduct($request);
			return $result;
		}

		public static function unsetRelatedProduct($request) {
			require_once('Product.php');
			$cat = new Product();
			$result = $cat->unsetRelatedProduct($request);
			return $result;
		}




/******************************************* End Product Functions ******************************************************************/

/******************************************* Start Artiles Functions ******************************************************************/



		public static function addArticle($request) {
			require_once('Article.php');
			$article = new Article();
			$result = $article->addArticle($request);
			return $result;
		}


		public static function fetchArticles($request) {
			require_once('Article.php');
			$article = new Article();
			$result = $article->fetchArticles($request);
			
			return $result;
		}
		
		public static function fetchArticle($request) {
			require_once('Article.php');
			$article = new Article();
			$result = $article->fetchArticle($request);
			return $result;
		}

		public static function updateArticle($request) {
			require_once('Article.php');
			$article = new Article();
			$result = $article->updateArticle($request);
			return $result;
		}


		public static function changeArticleStatus($request) {
			require_once('Article.php');
			$article = new Article();
			$result = $article->changeArticleStatus($request);
			return $result;
		}
		
		public static function deleteArticle($request) {
			require_once('Article.php');
			$article = new Article();
			$result = $article->deleteArticle($request);
			return $result;
		}

		public static function filterArticle($request) {
			require_once('Article.php');
			$article = new Article();
			$result = $article->filterArticle($request);
			return $result;
		}


/******************************************* End Artiles Functions ******************************************************************/


/******************************************* Start Videos Functions ******************************************************************/



		public static function addVideo($request) {
			require_once('Video.php');
			$video = new Video();
			$result = $video->addVideo($request);
			return $result;
		}


		public static function fetchVideos($request) {
			require_once('Video.php');
			$video = new Video();
			$result = $video->fetchVideos($request);
			return $result;
		}
		
		public static function fetchVideo($request) {
			require_once('Video.php');
			$video = new Video();
			$result = $video->fetchVideo($request);
			return $result;
		}

		public static function updateVideo($request) {
			require_once('Video.php');
			$video = new Video();
			$result = $video->updateVideo($request);
			return $result;
		}


		public static function changeVideoStatus($request) {
			require_once('Video.php');
			$video = new Video();
			$result = $video->changeVideoStatus($request);
			return $result;
		}

		public static function changeOrderStatus($request) {
			require_once('Product.php');
			$product = new Product();
			$result = $product->changeOrderStatus($request);
			return $result;
		}
		
		public static function deleteVideo($request) {
			require_once('Video.php');
			$video = new Video();
			$result = $video->deleteVideo($request);
			return $result;
		}

		public static function filterVideos($request) {
			require_once('Video.php');
			$video = new Video();
			$result = $video->filterVideos($request);
			return $result;
		}


/******************************************* End Videos Functions ******************************************************************/

/******************************************* Start Cart Functions ******************************************************************/
		
		public static function showProductsOfCart($request) {
			require_once('CustomerCart.php');			
			$cart = new CustomerCart();
			$result = $cart->showProductsOfCart($request);
			return $result;
		}
		
		public static function addProductTocart($request) {
			require_once('CustomerCart.php');			
			$cart = new CustomerCart();
			$result = $cart->addProductTocart($request);
			return $result;
		}
		
		
		public static function deleteProductFromCart($request) {
			require_once('CustomerCart.php');			
			$cart = new CustomerCart();
			$result = $cart->deleteProductFromCart($request);
			return $result;
		}
		
		public static function saveCheckoutDetail($request) {
			require_once('CustomerCart.php');			
			$cart = new CustomerCart();
			$result = $cart->saveCheckoutDetail($request);
			return $result;
		}

		public static function sendContactDetails($request) {
			require_once('CustomerCart.php');			
			$cart = new CustomerCart();
			$result = $cart->sendContactDetails($request);
			return $result;
		}
		
		public static function fetchOrders($request) {
			require_once('CustomerCart.php');			
			$cart = new CustomerCart();
			$result = $cart->fetchOrders($request);
			return $result;
		}

		public static function getProductsByCustomerId($request) {
			require_once('Product.php');			
			$Product = new Product();
			$result = $Product->getProductsByCustomerId($request);
			return $result;
		}
		
		public static function getProductsByOrderId($request) {
			require_once('Product.php');			
			$Product = new Product();
			$result = $Product->getProductsByOrderId($request);
			return $result;
		}
/******************************************* End Cart Functions ******************************************************************/

		
		public static function filterCategory($request) {
			require_once('Category.php');			
			$Product = new Category();
			$result = $Product->filterCategory($request);
			return $result;
		}

		public static function filterSubCategory($request) {
			require_once('Category.php');			
			$Product = new Category();
			$result = $Product->filterSubCategory($request);
			return $result;
		}

		public static function filterClass($request) {
			require_once('Classification.php');			
			$Product = new Classification();
			$result = $Product->filterClass($request);
			return $result;
		}

		public static function filterOrder($request) {
			require_once('CustomerCart.php');
			$cat = new CustomerCart();
			$result = $cat->filterOrder($request);
			return $result;
		}



		public static function addSpecification($request) {
			require_once('Specification.php');
			$cat = new Specification();
			$result = $cat->addSpecification($request);
			return $result;
		}


		public static function fetchAllSpecification($request) {
			require_once('Specification.php');
			$cat = new Specification();
			$result = $cat->fetchAllSpecification($request);
			return $result;
		}


		public static function updateSpecification($request) {
			require_once('Specification.php');
			$cat = new Specification();
			$result = $cat->updateSpecification($request);
			return $result;
		}


		public static function deleteSpecification($request) {
			require_once('Specification.php');
			$cat = new Specification();
			$result = $cat->deleteSpecification($request);
			return $result;
		}


		public static function getSpecificationDetails($request) {
			require_once('Specification.php');
			$cat = new Specification();
			$result = $cat->getSpecificationDetails($request);
			return $result;
		}

		public static function fetchSpecificationsByProductId($request) {
			require_once('Specification.php');
			$cat = new Specification();
			$result = $cat->fetchSpecificationsByProductId($request);
			return $result;
		}


		public static function formDashedString($q)
	    {
			$q = strtolower($q);
			$q = preg_replace('/[^A-Za-z0-9\-]/', ' ', $q);
			$q = preg_replace('!\s+!', ' ', $q);
			$q = trim($q);
			$q = str_replace(' ', '-', $q);
			$q = preg_replace('/-+/', '-', $q);
			return $q;
		}

		public static function encodeId($userId) {
	        $str = $userId . '||' . time();
	        return base64_encode($str);
    	}
    
	    public static function decodeId($raw) {
	        if($raw == '0' || $raw === 0)
	            return $raw;
	        $temp = explode('||', base64_decode($raw));
	        if(count($temp) > 1)
	            $userId = $temp[0];
	        else
	            $userId = '0';
	        if($userId == '')
	            $userId = '0';
	        return $userId;	        
	    }
 
/******************************************* End Filter Functions ******************************************************************/
		
		public static function xmlCategory($request) {
			require_once('Category.php');
			$cat = new Category();
			$result = $cat->xmlCategory($request);
			return $result;
		}



	}
?>