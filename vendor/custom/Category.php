<?php 
	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Sql;
	use Zend\Db\ResultSet\HydratingResultSet;
	use Zend\Db\Sql\Predicate\PredicateInterface;
	use Zend\Db\Sql\Expression;

	require_once('Connection.php');
	require_once('Api.php');
	/**
	* 
	*/


	
	class Category extends Connection
	{
		
		function addCategory($request) {
			//die(print_r($request));
			$result = new stdClass();
			if($this->checkDuplicate($request->name,$request->parent_id)){

				try {
					$adapter = $this->adapter;
					$sql = new Sql($adapter);

					// INSERT INTO `category`(`name`, `description`, `status`, `image`) VALUES ('cat 1', 'decs place here', 1, 'images/category/default.png')
					$data = array(
						'name'	=>	$request->name,
						'description'	=>	$request->description,
						'status'	=>	$request->status,
						'meta_title' => $request->meta_title,
						'meta_keywords' => $request->meta_keywords,
						'meta_description' => $request->meta_description,
						'details'	=>	$request->details
					);

					if (isset($request->parent_id)) {
						$data['parent_id'] = $request->parent_id;
					}

					$category = new TableGateway('category', $adapter, null, new HydratingResultSet());
					$category->insert($data);

					$id = $category->getLastInsertValue();

					$adapter1 = $this->adapter;
					$sql1 = new Sql($adapter1);
					$update = $sql1->update();
	                $update->table('category');
	                if (isset($request->parent_id) && $request->parent_id != '0') {						
		                $data1 = array('alias' => Api::formDashedString($request->name).'_s'.$id);
					}else{
						$data1 = array('alias' => Api::formDashedString($request->name).'_c'.$id);
					}
	                
	                $update->set($data1);
	                $update->where(array(
	                    'id' => $id
	                ));
	                //die(print_r($request));
	                $statement1 = $sql1->prepareStatementForSqlObject($update);
	                $statement1->execute();


					$result->status = 1;
					$result->message = "Category successfully added.";
					//die(print_r($request));
					if (isset($request->image)) {
						// die(print_r($request));
						$ext = pathinfo($request->image['name'], PATHINFO_EXTENSION);
						//die(print_r($ext.' *' ));
						$dir = '../images/categories/';
						$file_name = $request->name.'_'.$id.'.'.$ext;

						//$jpeg_quality = 90;
						
						
						//if ($condition) {
						if (move_uploaded_file($request->image['tmp_name'], $dir.$file_name)) {
							$img_url = 'images/categories/'.$file_name;
							$update = $sql->update();
		                    $update->table('category');
		                    $update->set(array(
		                        'image'	=>	$img_url, 
		                    ));
		                    $update->where(array(
		                        'id' => $id
		                    ));
							$selectString = $sql->getSqlStringForSqlObject($update);
							// die($selectString);
		                    $run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
		                 }
						else {
							$result->status = 0;
							$result->message = "Category added but error in uploading image.";
						}

						
						if($ext=='jpeg' || $ext=='jpg'){
					

						$img = @imagecreatefromjpeg($request->image['tmp_name']);

						$croped_image = @imagecrop($img, $request->cords);
						$condition = @imagejpeg($croped_image, $dir.$file_name,90 );

						}
						if($ext=='png'){

						$img = @imagecreatefrompng($request->image['tmp_name']);
						$croped_image = @imagecrop($img, $request->cords);
						$condition = @imagepng($croped_image, $dir.$file_name );

						}
						if ($condition) {
						//if (move_uploaded_file($request->image['tmp_name'], $dir.$file_name)) {
							$img_url = 'images/categories/'.$file_name;
							$update = $sql->update();
		                    $update->table('category');
		                    $update->set(array(
		                        'image'	=>	$img_url, 
		                    ));
		                    $update->where(array(
		                        'id' => $id
		                    ));
							$selectString = $sql->getSqlStringForSqlObject($update);
							// die($selectString);
		                    $run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
		                }						

						// if ($ext in array('jpg', 'jpeg', 'png', 'gif')) {
						// }
					}
					return $result;
				} catch(Exception $e) {
					$result->status = 0;
					$result->message = $e->getMessage();
					return $result;
				}
			}else{
				$result->status = 0;
				$result->message = "Already exist.";
				return $result;
			}
		}

		public function fetchAllCategories($request) {
			
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();

				$select->from(array('sc' => 'category'))
						->join(array('c'=>'category'),'sc.parent_id = c.id', array('cname' => 'name','calias' => 'alias'),'left');

						
				$where = array('sc.deleted' => 0);
				
				if (isset($request->parent_id)) {
					$where['sc.parent_id'] = $request->parent_id;
				}
				if (isset($request->alias)) {
				 	$id=Api::decodeId($request->alias);
				 	$where['sc.parent_id'] = $id;
				}
				

				$select->where($where);
				$select->columns(array('id', 'name', 'description', 'status', 'image', 'parent_id', 'alias', 'display_order', 'meta_title', 'meta_keywords', 'meta_description'));
				$select->order('c.display_order');
				$select->order('sc.display_order');

				$selectString = $sql->getSqlStringForSqlObject($select);
				//die($selectString);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();
				
				$result->categories = $rows;
				$result->status = 1;
				$result->message = "Categories fetched.";

				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		public function getCategoryDetails($request) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();

				$select->from(array('sc' => 'category'))
						->join(array('c'=>'category'),'sc.parent_id = c.id', array('cname' => 'name','calias' => 'alias'),'left');

				
				if (isset($request->category_id)) {
					$select->where(array(
						'sc.id'	=>	$request->category_id,
						'sc.deleted' => 0
					));
				}
				if (isset($request->alias)) {
				 	$id = Api::decodeId($request->alias);
				 	$select->where(array(
						'sc.id'	=>	$id,
						'sc.deleted' => 0
					));										
				}
				$select->columns(array('id', 'name', 'description', 'status', 'image', 'parent_id', 'alias' , 'meta_title', 'meta_keywords', 'meta_description', 'details'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				
				//die($selectString);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$row = $run->toArray();
				
				$result->category = $row[0];
				$result->status = 1;
				$result->message = "Category fetched.";

				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		function updateCategory($request) {
			$result = new stdClass();
			if(isset($request->parent_id)){
				$validate = $this->checkDuplicate($request->name,$request->parent_id,$request->category_id);			
			}else{
				$validate = $this->checkDuplicate($request->name,'0',$request->category_id);				
			}
			if($validate){

				try {
					$adapter = $this->adapter;
					$sql = new Sql($adapter);

					$update = $sql->update();
	                $update->table('category');
	                $data = array(
	                    'name'	=>	$request->name,
						'description'	=>	$request->description,
						'status'	=>	$request->status,
						'meta_title' => $request->meta_title,
						'meta_keywords' => $request->meta_keywords,
						'meta_description' => $request->meta_description,
						'details'	=>	$request->details						
					);
					if (isset($request->parent_id)) {
						$data['parent_id'] = $request->parent_id;
						$data['alias'] = Api::formDashedString($request->name).'_s'.$request->category_id;
					}else{
						$data['alias'] = Api::formDashedString($request->name).'_c'.$request->category_id;
					}
	                $update->set($data);
	                $update->where(array(
	                    'id' => $request->category_id
	                ));
	                $statement = $sql->prepareStatementForSqlObject($update);
	                $statement->execute();

					$result->status = 1;
					$result->message = "Category successfully updated.";

					// die(print_r($request));
					if (isset($request->image)) {
						// die(print_r($request));
						$ext = pathinfo($request->image['name'], PATHINFO_EXTENSION);

						$dir = '../images/categories/';
						$file_name = $request->name.'_'.$request->category_id.'.'.$ext;

						$condition = false;
						if($ext=='jpeg' || $ext=='jpg'){

						$img = @imagecreatefromjpeg($request->image['tmp_name']);
						$croped_image = @imagecrop($img, $request->cords);
						$condition = @imagejpeg($croped_image, $dir.$file_name,90 );

						}
						if($ext=='png'){

						$img = imagecreatefrompng($request->image['tmp_name']);
						$croped_image = imagecrop($img, $request->cords);
						$condition = imagepng($croped_image, $dir.$file_name );

						}											
						
						//if ($condition) {
						if (move_uploaded_file($request->image['tmp_name'], $dir.$file_name)) {
							$img_url = 'images/categories/'.$file_name;
							$update = $sql->update();
		                    $update->table('category');
		                    $update->set(array(
		                        'image'	=>	$img_url, 
		                    ));
		                    $update->where(array(
		                        'id' =>	 $request->category_id
		                    ));
		                    $statement = $sql->prepareStatementForSqlObject($update);
		                    $statement->execute();
						}
						else {
							$result->status = 0;
							$result->message = "Category updated but error in uploading image.";
						}

						if ($condition) {
						//if (move_uploaded_file($request->image['tmp_name'], $dir.$file_name)) {
							$img_url = 'images/categories/'.$file_name;
							$update = $sql->update();
		                    $update->table('category');
		                    $update->set(array(
		                        'image'	=>	$img_url, 
		                    ));
		                    $update->where(array(
		                        'id' =>	 $request->category_id
		                    ));
		                    $statement = $sql->prepareStatementForSqlObject($update);
		                    $statement->execute();
						}
						
						// if ($ext in array('jpg', 'jpeg', 'png', 'gif')) {
						// }
					}
					return $result;
				} catch(Exception $e) {
					$result->status = 0;
					$result->message = $e->getMessage();
					return $result;
				}
			}else{
				$result->status = 0;
				$result->message = "Already exist.";
				return $result;
			}
			
		}


		function deleteCategory($request) {
			$result = new stdClass();
			if($this->alreadyExist($request->category_id)){


				try {
					$adapter = $this->adapter;
					$sql = new Sql($adapter);

					$update = $sql->update();
	                $update->table('category');
	                $update->set(array(
	                    'deleted'	=>	1
					));
	                $update->where(array(
	                    'id' => $request->category_id
	                ));
	                $statement = $sql->prepareStatementForSqlObject($update);
	                $statement->execute();

					$result->status = 1;
					$result->message = "Category deleted.";

					return $result;
				} catch(Exception $e) {
					$result->status = 0;
					$result->message = $e->getMessage();
					return $result;
				}
			}else{
				$result->status = 0;
				$result->message = "Unable to delete as it has associated subcategory/products.";
				return $result;
			}
		}


		function saveCategoryOrder($request) {
			$order = $request->order;
			$order = explode('&ID[]=', $order);
			$order[0] = substr($order[0], 5);
			
			$result = new stdClass();
			

				try {
					$adapter = $this->adapter;
					$sql = new Sql($adapter);
					foreach ($order as $key => $value) {
						# code...
					
						$update = $sql->update();
		                $update->table('category');
		                $update->set(array(
		                    'display_order'	=>	$key+1
						));
		                $update->where(array(
		                    'id' => $value
		                ));
		                $statement = $sql->prepareStatementForSqlObject($update);
		                $statement->execute();
	            	}
					$result->status = 1;
					$result->message = "Category Order Saved.";

					return $result;
				} catch(Exception $e) {
					$result->status = 0;
					$result->message = $e->getMessage();
					return $result;
				}
			
		}

		function saveSubCategoryOrder($request) {
			$order = $request->order;
			$order = explode('&ID[]=', $order);
			$order[0] = substr($order[0], 5);
			
			$result = new stdClass();
			

				try {
					$adapter = $this->adapter;
					$sql = new Sql($adapter);
					foreach ($order as $key => $value) {
						# code...
					
						$update = $sql->update();
		                $update->table('category');
		                $update->set(array(
		                    'display_order'	=>	$key+1
						));
		                $update->where(array(
		                    'id' => $value,
		                    'parent_id' => $request->parent_id
		                ));
		                $statement = $sql->prepareStatementForSqlObject($update);
		                $statement->execute();
	            	}
					$result->status = 1;
					$result->message = "Category Order Saved.";

					return $result;
				} catch(Exception $e) {
					$result->status = 0;
					$result->message = $e->getMessage();
					return $result;
				}
			
		}

		function filterCategory($request) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();
				// SELECT products.name, products.part_name, products.supplier_part_name, products.small_description, products.image,  order_details.id AS order_id, order_details.product_id, order_details.quantity FROM order_details LEFT JOIN products ON products.id = order_details.product_id WHERE 
				$select->from('category');
				
				$where = array('category.deleted' => 0);

				if (isset($request->catname)) {
					$select->where('name LIKE "%'.$request->catname.'%"');
					
				}
				if (isset($request->status)) {
					$where['status'] = $request->status;					
				}
				$where['parent_id'] = '0';
				$where['deleted'] = '0';
				$select->where($where);
				$select->columns(array('id', 'name', 'description', 'status', 'image', 'parent_id' ));

				$selectString = $sql->getSqlStringForSqlObject($select);
				//die($selectString);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();
				
				
				$result->categories = $rows;
				
				$result->status = 1;
				$result->message = "Category selected.";

				return $result;	
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		function filterSubCategory($request) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();
				
				$select->from(array('sc' => 'category'))
						->join(array('c'=>'category'),'sc.parent_id = c.id', array('cname' => 'name'),'left');
				
				if (isset($request->subcatname)) {
					$select->where('sc.name LIKE "%'.$request->subcatname.'%"');
				}
				if (isset($request->status)) {
					$where['sc.status'] = $request->status;					
				}
				if (isset($request->cat)) {
					$where['sc.parent_id'] = $request->cat;		
				}
				
				$where['sc.deleted'] = '0';
				$select->where($where);
				$select->order('id DESC');
				$select->columns(array('id', 'name', 'description', 'status', 'image', 'parent_id' ));

				$selectString = $sql->getSqlStringForSqlObject($select);
				
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();
				
				$result->categories = $rows;
				
				$result->status = 1;
				$result->message = "Filtered";

				return $result;	
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		function checkDuplicate($name, $parent = '0', $category = '-1') {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();
				
				$select->from('category');
						
				$where = new $select->where();
				$where->equalTo('deleted', 0);
				$where->notEqualTo('id', $category);
				$where->equalTo('parent_id', $parent);

				//$where = array('parent_id' => $parent);
				
					$where->equalTo('name', $name);
								
				if (isset($name)) {

				}
				
				$select->where($where);
				
				//$select->columns(array('id', 'name', 'description', 'status', 'image', 'parent_id' ));

				$selectString = $sql->getSqlStringForSqlObject($select);
				//die($selectString);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();
				$c = 0;
				$c = count($rows);

				if($c==0){
					return true;					
				}else{
					return false;
				}

				
			} catch(Exception $e) {
				
				return false;
			}
		} 

		function alreadyExist($cat) {
			//var_dump($cat);
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();
				
				$select->from('category');
						
				$where = new $select->where();
				$where->equalTo('parent_id', $cat);	
				$select->where($where);
				

				$selectString = $sql->getSqlStringForSqlObject($select);
				//die($selectString);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();
				$c = 0;
				$c = count($rows);

				if($c==0){
					// deleting sub category
					$adapter1 = $this->adapter;
					$sql1 = new Sql($adapter1);

					$select = $sql1->select();
					
					$select->from('products');
							
					$where1 = new $select->where();
					$where1->equalTo('category_id', $cat);	
					$select->where($where1);
					

					$selectString = $sql1->getSqlStringForSqlObject($select);
					//die($selectString);
					$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$rows1 = $run->toArray();
					$c = 0;
					$c = count($rows1);
					if($c==0){
						return true;
					}else{
						return false;
					}
							
				}else{
					//deleting category and subcategory exist
					return false;
				}



				
			} catch(Exception $e) {
				
				return false;
			}
		}		
	}