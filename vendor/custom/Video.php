<?php 
	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Sql;
	use Zend\Db\ResultSet\HydratingResultSet;
	use Zend\Db\Sql\Predicate\PredicateInterface;
	use Zend\Db\Sql\Expression;

	require_once('Connection.php');
	/**
	* 
	*/
	class Video extends Connection
	{		
		function addVideo($request) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				// INSERT INTO `videos`('name', 'url', 'category_id', 'sub_category_id', 'product_id') VALUES
				$data = array(
					'name'	=>	$request->name,
					'url'	=>	$request->url,
					'description'	=>	$request->desc,
					'category_id'	=>	$request->category_id,
					'sub_category_id'	=>	$request->sub_category_id,
					'product_id'	=>	$request->product_id,
					'active'	=>	$request->status
				);
				// die(print_r($data));
				$video = new TableGateway('videos', $adapter, null, new HydratingResultSet());
				$video->insert($data);

				$id = $video->getLastInsertValue();
				$result->status = 1;
				$result->message = "Video successfully added.";
				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		public function fetchVideos($request) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();
				$select->from(array('v' => 'videos'))
				->join(array('sc'=>'category'),'sc.id = v.sub_category_id', array('sub_category' => 'name'),'left')
				->join(array('c'=>'category'),'c.id = v.category_id', array('category' => 'name'),'left');


				$where = array('v.deleted' => 0);
				
				if (isset($request->category_id)) {
					$where['v.category_id'] = $request->category_id;
				}
				if (isset($request->active)) {
					$where['v.active'] = $request->active;
				}

				if (isset($request->video_id)) {
					$where['v.id'] = $request->video_id;

					require_once('Category.php');
					$category = new Category();
					$result->categories = $category->fetchAllCategories($request);
					require_once('Product.php');
					$product = new Product();
					$result->product = $product->fetchAllProducts($request);

				}

				$select->where($where);
				$select->order('id desc');
				$select->columns(array('id', 'name', 'url', 'category_id', 'sub_category_id', 'product_id', 'active', 'description'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				//$selectString = 'SELECT videos.id, videos.name, videos.url, c.name AS category, videos.category_id, s_c.name AS sub_category, videos.sub_category_id, videos.product_id, videos.active FROM videos LEFT JOIN category AS c ON videos.category_id = c.id  LEFT JOIN category AS s_c ON videos.sub_category_id = s_c.id WHERE videos.deleted = 0';
				//die($selectString);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();
				
				$result->video = $rows;
				$result->status = 1;
				$result->message = "Videos fetched.";

				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		public function fetchVideo($request) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();
				$select->from('videos');
				$where = array('deleted' => 0);
				
				if (isset($request->category_id)) {
					$where['category_id'] = $request->category_id;
				}
				if (isset($request->active)) {
					$where['active'] = $request->active;
				}

				if (isset($request->video_id)) {
					$where['id'] = $request->video_id;

					require_once('Category.php');
					$category = new Category();
					$result->categories = $category->fetchAllCategories($request)->categories;
					require_once('Product.php');
					$product = new Product();
					$result->products = $product->fetchAllProducts($request)->products;

				}

				$select->where($where);
				$select->order('id desc');
				//$select->columns(array('id', 'name', 'url', 'category_id', 'sub_category_id', 'product_id', 'active'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();
				
				$result->video = $rows[0];
				$result->status = 1;
				$result->message = "Videos fetched.";

				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		function updateVideo($request) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$update = $sql->update();
                $update->table('videos');
                $data = array(
                    'name'	=>	$request->name,
					'url'	=>	$request->url,
					'category_id'	=>	$request->category_id,
					'sub_category_id'	=>	$request->sub_category_id,
					'product_id'	=>	$request->product_id,
					'description' =>	$request->desc,
					'active'	=>	$request->active
				);

                $update->set($data);
                $update->where(array(
                    'id' => $request->video_id
                ));
                $statement = $sql->prepareStatementForSqlObject($update);
                $statement->execute();

				$result->status = 1;
				$result->message = "Video successfully updated.";

				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}


		function changeVideoStatus($request) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$update = $sql->update();
                $update->table('videos');
                $update->set(array(
                    'active'	=>	$request->status
				));
                $update->where(array(
                    'id' => $request->video_id
                ));
                $statement = $sql->prepareStatementForSqlObject($update);
                $statement->execute();

				$result->status = 1;
				$result->message = "Video Updated.";

				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		function deleteVideo($request) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$update = $sql->update();
                $update->table('videos');
                $update->set(array(
                    'deleted'	=>	1
				));
                $update->where(array(
                    'id' => $request->video_id
                ));
                $statement = $sql->prepareStatementForSqlObject($update);
                $statement->execute();

				$result->status = 1;
				$result->message = "Video deleted.";

				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		function filterVideos($request) {
			//die(print_r($request));
			$result = new stdClass();
			
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();
				// SELECT products.name, products.part_name, products.supplier_part_name, products.small_description, products.image,  order_details.id AS order_id, order_details.product_id, order_details.quantity FROM order_details LEFT JOIN products ON products.id = order_details.product_id WHERE 
				//$select->from('class');
				$select->from(array('v' => 'videos'))
				->join(array('sc'=>'category'),'sc.id = v.sub_category_id', array('sub_category' => 'name'),'left')
				->join(array('c'=>'category'),'c.id = v.category_id', array('category' => 'name'),'left');

				$where = array('v.deleted' => 0);

				if (isset($request->name)) {
					$select->where('v.name LIKE "%'.$request->name.'%"');
					
				}
				if (isset($request->status)) {
					$where['v.active'] = $request->status;
					
				}
				if (isset($request->subcat)) {
					$where['v.sub_category_id'] = $request->subcat;					
				}
				if (isset($request->cat)) {
					$where['v.category_id'] = $request->cat;
					//$select->where('category_id IN (SELECT id FROM category WHERE parent_id = '.$request->cat.')');
				}
				
				//$where['deleted'] = '0';
				$select->where($where);
				$select->order('id DESC');
				$select->columns(array('id', 'name', 'description', 'active', 'category_id', 'sub_category_id', 'product_id' ));

				$selectString = $sql->getSqlStringForSqlObject($select);
				//die($selectString);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();
				
				
				$result->video = $rows;
				
				$result->status = 1;
				$result->message = "Filtered";

				return $result;	
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

	}