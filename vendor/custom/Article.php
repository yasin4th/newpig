<?php 
	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Sql;
	use Zend\Db\ResultSet\HydratingResultSet;
	use Zend\Db\Sql\Predicate\PredicateInterface;
	use Zend\Db\Sql\Expression;

	require_once('Connection.php');
	require_once('Api.php');
	/**
	* 
	*/
	class Article extends Connection
	{
		
		function addArticle($request) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				// INSERT INTO `articles`('name', 'description', 'category_id', 'sub_category_id', 'product_id') VALUES
				$data = array(
					'name'	=>	$request->name,
					'description'	=>	$request->description,
					'category_id'	=>	$request->category_id,
					'sub_category_id'	=>	$request->sub_category_id,
					'product_id'	=>	$request->product_id,
					'active'	=>	$request->status
					
				);
				
				$article = new TableGateway('articles', $adapter, null, new HydratingResultSet());
				$article->insert($data);

				$id = $article->getLastInsertValue();


				$adapter1 = $this->adapter;
				$sql1 = new Sql($adapter1);
				$update = $sql1->update();
                $update->table('articles');    
                $data1 = array(
                    'alias' => Api::formDashedString($request->name).'_b'.$id
				);
                $update->set($data1);
                $update->where(array(
                    'id' => $id
                ));
                //die(print_r($request));
                $statement1 = $sql1->prepareStatementForSqlObject($update);
                $statement1->execute();

                

				$result->status = 1;
				$result->message = "Article successfully added.";
				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		public function fetchArticles($request) {
			// $request->article_id = 11;
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();
				// SELECT products.name, products.part_name, products.supplier_part_name, products.small_description, products.image,  order_details.id AS order_id, order_details.product_id, order_details.quantity FROM order_details LEFT JOIN products ON products.id = order_details.product_id WHERE 
				//$select->from('class');
				$select->from(array('a' => 'articles'))
				->join(array('sc'=>'category'),'sc.id = a.sub_category_id', array('sub_category' => 'name'),'left')
				->join(array('c'=>'category'),'c.id = a.category_id', array('category' => 'name'),'left');
				

				$where = array('a.deleted' => 0);

				if (isset($request->active)) {
					$where['a.active'] = $request->active;
				}
				if (isset($request->category_id)) {
					$where['a.category_id'] = $request->category_id;
				}

				if (isset($request->article_id)) {
					$where['a.id'] = $request->article_id;

					require_once('Category.php');
					$category = new Category();
					$result->categories = $category->fetchAllCategories($request);
					require_once('Product.php');
					$product = new Product();
					$result->product = $product->fetchAllProducts($request);

				}

				$select->where($where);
				$select->order('id desc');
				$select->columns(array('id', 'name', 'description', 'category_id', 'sub_category_id', 'product_id', 'alias', 'active'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				// die($selectString);
				//$selectString = 'SELECT articles.id, articles.name, articles.description, c.name AS category, articles.category_id, s_c.name AS sub_category, articles.sub_category_id, articles.product_id, articles.active FROM articles LEFT JOIN category AS c ON articles.category_id = c.id  LEFT JOIN category AS s_c ON articles.sub_category_id = s_c.id WHERE articles.deleted = 0';
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();
                // print_r($rows);
				$result->article = $rows;
				$result->status = 1;
				$result->message = "Articles fetched.";	

				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		public function fetchArticle($request) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();
				$select->from('articles');
				$where = array('deleted' => 0);
				
				if (isset($request->category_id)) {
					$where['category_id'] = $request->category_id;
				}

				if (isset($request->article_id)) {
					$where['id'] = $request->article_id;

					require_once('Category.php');
					$category = new Category();
					$result->categories = $category->fetchAllCategories($request)->categories;
					require_once('Product.php');
					$product = new Product();
                  	$request->columns = ['id', 'name'];
					$result->products = $product->fetchAllProducts($request)->products;

				}

				$select->where($where);
				$select->order('id desc');
				$select->columns(array('id', 'name', 'description', 'category_id', 'sub_category_id', 'product_id', 'alias', 'active'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();
				
				$result->article = $rows[0];
				$result->status = 1;
				$result->message = "Articles fetched.";

				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		function updateArticle($request) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$update = $sql->update();
                $update->table('articles');
                $data = array(
                    'name'	=>	$request->name,
					'description'	=>	$request->description,
					'category_id'	=>	$request->category_id,
					'sub_category_id'	=>	$request->sub_category_id,
					'product_id'	=>	$request->product_id,
					'active'	=>	$request->status,
					'alias' => Api::formDashedString($request->name).'_b'.$request->article_id

				);

                $update->set($data);
                $update->where(array(
                    'id' => $request->article_id
                ));
                $statement = $sql->prepareStatementForSqlObject($update);
                $statement->execute();

				$result->status = 1;
				$result->message = "Article successfully updated.";

				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}


		function changeArticleStatus($request) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$update = $sql->update();
                $update->table('articles');
                $update->set(array(
                    'active'	=>	$request->status
				));
                $update->where(array(
                    'id' => $request->article_id
                ));
                $statement = $sql->prepareStatementForSqlObject($update);
                $statement->execute();

				$result->status = 1;
				$result->message = "Article Updated.";

				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		function deleteArticle($request) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$update = $sql->update();
                $update->table('articles');
                $update->set(array(
                    'deleted'	=>	1
				));
                $update->where(array(
                    'id' => $request->id
                ));
                $statement = $sql->prepareStatementForSqlObject($update);
                $statement->execute();

				$result->status = 1;
				$result->message = "Article deleted.";
				
				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		function filterArticle($request) {
			//die(print_r($request));
			$result = new stdClass();
			
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();
				// SELECT products.name, products.part_name, products.supplier_part_name, products.small_description, products.image,  order_details.id AS order_id, order_details.product_id, order_details.quantity FROM order_details LEFT JOIN products ON products.id = order_details.product_id WHERE 
				//$select->from('class');
				$select->from(array('a' => 'articles'))
				->join(array('sc'=>'category'),'sc.id = a.sub_category_id', array('sub_category' => 'name'),'left')
				->join(array('c'=>'category'),'c.id = a.category_id', array('category' => 'name'),'left')
				->join(array('p'=>'products'),'p.id = a.product_id', array('pname' => 'name'),'left');

				$where = array('a.deleted' => 0);

				if (isset($request->name)) {
					$select->where('a.name LIKE "%'.$request->name.'%"');
					
				}
				if (isset($request->status)) {
					$where['a.active'] = $request->status;
					
				}
				if (isset($request->subcat)) {
					$where['a.sub_category_id'] = $request->subcat;					
				}
				if (isset($request->cat)) {
					$where['a.category_id'] = $request->cat;
					//$select->where('category_id IN (SELECT id FROM category WHERE parent_id = '.$request->cat.')');
				}
				
				//$where['deleted'] = '0';
				$select->where($where);
				$select->order('id DESC');
				$select->columns(array('id', 'name', 'description', 'active', 'category_id', 'sub_category_id', 'product_id' ));

				$selectString = $sql->getSqlStringForSqlObject($select);
				//die($selectString);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();
				
				
				$result->article = $rows;
				
				$result->status = 1;
				$result->message = "Filtered";

				return $result;	
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

	}