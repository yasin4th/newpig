<?php 
	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Sql;
	use Zend\Db\ResultSet\HydratingResultSet;
	use Zend\Db\Sql\Predicate\PredicateInterface;
	use Zend\Db\Sql\Expression;

	require_once('Connection.php');
	require_once('Api.php');

	/**
	* 
	*/
	class Product extends Connection
	{
		
		function addProduct($request) {
			$result = new stdClass();
			//die(print_r($request));
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);				
				$data = array(
					'category_id' => $request->category_id,
					'class_id' => $request->class_id,
					'name' => $request->name,
					'part_name' => $request->part_name,
					'supplier_part_name' => $request->supplier_part_name,
					'quantity' => $request->quantity,
					'mrp' => $request->mrp,
					'small_description' => $request->small_description,
					'detailed_description' => $request->detailed_description,
					'media_title' => $request->media_title,
					'media_url' => $request->media_url,
					'meta_title' => $request->meta_title,
					'meta_keywords' => $request->meta_keywords,
					'meta_description' => $request->meta_description,

					'point1' => $request->point1,
					'point2' => $request->point2,
					'point3' => $request->point3,

					'specs' => $request->specs,
					'height' => $request->height,
					'width' => $request->width,
					'weight' => $request->weight,
					'stock' => $request->stock,
					'featured' => $request->featured
				);

				$product = new TableGateway('products', $adapter, null, new HydratingResultSet());
				$product->insert($data);

				$id = $product->getLastInsertValue();
				//die($id);

				
				$sql = new Sql($adapter);
				$update = $sql->update();
                $update->table('products');
                $data = array(
                    'alias' => Api::formDashedString($request->name).'_p'.$id
				);
                $update->set($data);
                $update->where(array(
                    'id' => $id
                ));
                
                $statement = $sql->prepareStatementForSqlObject($update);
                $statement->execute();
               
               	$specs = json_decode($request->specs, true);
               	//print_r($specs);
               	foreach ($specs as $key => $value) {
               		# code...
               		
               		$data = array(
						'product_id' => $id,
						'spec_id' => $key,
						'value' => $value,
					);
					$spec = new TableGateway('product_specification', $adapter, null, new HydratingResultSet());
					$spec->insert($data);
               	}                

				$result->status = 1;
				$result->message = "Product successfully added.";


				if (isset($request->image)) {
					// die(print_r($request));
					$ext = pathinfo($request->image['name'], PATHINFO_EXTENSION);

					$dir = '../images/product/';
					$file_name = $request->name.'_'.$id.'.'.$ext;



					//if ($condition) {
					if (move_uploaded_file($request->image['tmp_name'], $dir.$file_name)) {
						$img_url = 'images/product/'.$file_name;
						$update = $sql->update();
	                    $update->table('products');
	                    $update->set(array(
	                        'image'	=>	$img_url,
	                    ));
	                    $update->where(array(
	                        'id' => $id
	                    ));
	                    $statement = $sql->prepareStatementForSqlObject($update);
	                    $statement->execute();
					}
					else {
						$result->status = 0;
						$result->message = "Product added but error in uploading main image.";
					}

					if($ext=='jpeg' || $ext=='jpg'){

					$img = @imagecreatefromjpeg($request->image['tmp_name']);
					$croped_image = @imagecrop($img, $request->cords);
					$condition = @imagejpeg($croped_image, $dir.$file_name,90 );

					}
					if($ext=='png'){

					$img = @imagecreatefrompng($request->image['tmp_name']);
					$croped_image = @imagecrop($img, $request->cords);
					$condition = @imagepng($croped_image, $dir.$file_name );

					}

					if ($condition) {
					// if (move_uploaded_file($request->image['tmp_name'], $dir.$file_name)) {
						$img_url = 'images/product/'.$file_name;
						$update = $sql->update();
	                    $update->table('products');
	                    $update->set(array(
	                        'image'	=>	$img_url, 
	                    ));
	                    $update->where(array(
	                        'id' => $id
	                    ));
	                    $statement = $sql->prepareStatementForSqlObject($update);
	                    $statement->execute();
					}
					
					// if ($ext in array('jpg', 'jpeg', 'png', 'gif')) {
					// }
				}

				if (isset($request->related_image)) {
					foreach ($request->related_image['tmp_name'] as $n => $tmp_name) {
						$ext = pathinfo($request->related_image['name'][$n], PATHINFO_EXTENSION);

						$dir = '../images/product/related-images/';
						$file_name = $request->name.'_'.$id.'_'.$n.'.'.$ext;
						if (move_uploaded_file($tmp_name, $dir.$file_name)) {
							$img_url = 'images/product/related-images/'.$file_name;
							
							// INSERT INTO `related_images`(`product_id`, `url`) VALUES 
							$product = new TableGateway('related_images', $adapter, null, new HydratingResultSet());
							$product->insert(array(
								'product_id' => $id,
								'url' =>	$img_url
							));

		                    $statement = $sql->prepareStatementForSqlObject($update);
		                    $statement->execute();
						}
						
						
					}
				}

				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
				
		}

		public function fetchAllProducts($request) {
			
			$result = new stdClass();

			try {
				$adapter = $this->adapter;

				$sql = new Sql($adapter);

				$select = $sql->select();
				$select->from(array('products' => 'products'), array())
						->join(array('c' => 'category'),
                			'c.id = products.category_id',
                    			array('sub_category' => 'name','subcatalias'=>'alias','category_id'=>'id'), 'left')
						->join(array('s_c' => 'category'),
                			's_c.id = c.parent_id',
                    			array('category' => 'name'), 'left')
						->join(array('class' =>'class'),
                			'class.id = products.class_id',
                    			array('class' => 'name','calias' => 'alias'), 'left');
						/*->join(array('rel' =>'related_images'),
                			'rel.product_id = products.id',
                    			array('relimg' => 'url'), 'left');*/
				
				$where = array('products.deleted' => 0,'c.deleted' => 0);
				if (isset($request->category_id)) {
					$where['products.category_id'] = $request->category_id;
				}
				if (isset($request->class_id)) {
					$where['products.class_id'] = $request->class_id;
				}
				if (isset($request->featured)) {
					$where['products.featured'] = $request->featured;
				}
				if (isset($request->calias)) {
				 	$id=Api::decodeId($request->calias);
				 	$where['products.class_id'] = $id;				 
				}
				if (isset($request->active)) {
					$where['products.active'] = $request->active;
				}
				if (isset($request->alias)) {
				 	$id=Api::decodeId($request->alias);
				 	$where['products.category_id'] = $id;				 
				}
				$select->where($where);
				$select->order('name');
                if (isset($request->columns)) {
                    $select->columns($request->columns);
                }
                else {
					$select->columns(array('id', 'class_id', 'name', 'part_name', 'supplier_part_name', 'quantity', 'mrp', 'image', 'media_title', 'media_url', 'meta_title', 'meta_keywords', 'meta_description', 'small_description', 'detailed_description', 'weight', 'stock', 'active', 'specs','featured', 'alias', 'point1', 'point2', 'point3'));
                }//$select->columns(array('id', 'name', 'image', 'stock', 'active'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				// $selectString = 'SELECT products.id, products.name, products.image, products.stock, products.active, class.name AS class_name, cat.name AS sub_category, (SELECT category.name FROM category WHERE category.id = cat.parent_id) AS category FROM products LEFT JOIN category AS cat ON products.category_id = cat.id LEFT JOIN class ON products.class_id = class.id WHERE products.deleted = 0 order by id desc';
				
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();
				

				$count = 0;
				foreach ($rows as $key) {
					# code...
					$sql = new Sql($adapter);

					$select = $sql->select();
					$select->from('related_images');
					$where = array('product_id' => $key['id']);
					$select->where($where);
					$selectString = $sql->getSqlStringForSqlObject($select);
					//die($selectString);
					// $selectString = 'SELECT products.id, products.name, products.image, products.stock, products.active, class.name AS class_name, cat.name AS sub_category, (SELECT category.name FROM category WHERE category.id = cat.parent_id) AS category FROM products LEFT JOIN category AS cat ON products.category_id = cat.id LEFT JOIN class ON products.class_id = class.id WHERE products.deleted = 0 order by id desc';
					
					$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$res = $run->toArray();
					if(count($res)){

						$rows[$count]['relimage'] = $res[0]['url'];
					}else{
						$rows[$count]['relimage'] = '';
					}
					$count++;
				}								
				
				$result->products = $rows;			
				
				$result->status = 1;
				$result->message = "Products fetched.";
				
				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
			
		}

		public function fetchRelatedProducts($request) {
			$result = new stdClass();

			try {
				$adapter = $this->adapter;

				$sql = new Sql($adapter);

				$select = $sql->select();

				$select->from(array('rp'=>'related_products'),array())
						->join(array('p'=>'products'),'rp.related_id = p.id',array('name','part_name','image','alias','point1','point2','point3'),'left')
						->join(array('c' => 'category'),
                			'c.id = p.category_id',
                    			array('sub_category' => 'name','sub_category_alias' => 'alias'), 'left')
						->join(array('s_c' => 'category'),
                			's_c.id = c.parent_id',
                    			array('category' => 'name'), 'left')
						->join(array('class' =>'class'),
                			'class.id = p.class_id',
                    			array('class' => 'name'), 'left');
				
						/*->join(array('rel' =>'related_images'),
                			'rel.product_id = products.id',
                    			array('relimg' => 'url'), 'left');*/
				
				$where = array('p.deleted' => 0,'c.deleted' => 0);
								
				if (isset($request->product_id)) {
				 	$where['rp.product_id'] = $request->product_id;
				}
				$select->where($where);
				//$select->order('id desc');
				//$select->columns(array('id', 'category_id', 'class_id', 'name', 'part_name', 'supplier_part_name', 'quantity', 'mrp', 'small_description', 'detailed_description', 'image', 'media_title', 'media_url', 'meta_title', 'meta_keywords', 'meta_description', 'sold_as', 'weight', 'stock', 'active'));
				// $select->columns(array('id', 'name', 'image', 'stock', 'active'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				//die($selectString);
				// $selectString = 'SELECT products.id, products.name, products.image, products.stock, products.active, class.name AS class_name, cat.name AS sub_category, (SELECT category.name FROM category WHERE category.id = cat.parent_id) AS category FROM products LEFT JOIN category AS cat ON products.category_id = cat.id LEFT JOIN class ON products.class_id = class.id WHERE products.deleted = 0 order by id desc';
				
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();
				
				//$result->relatedproducts = $rows;
				

				return $rows;
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
			
		}

		public function fetchSearchProducts($request) {
			//$search = explode('+', $request->search);
			$search = str_replace('%20', ' ', $request->search);
			$result = new stdClass();
			try {
				$adapter = $this->adapter;

				$sql = new Sql($adapter);

				$select = $sql->select();
				
				$select->from(array('products' => 'products'), array())
						->join(array('c' => 'category'),
                			'c.id = products.category_id',
                    			array('sub_category' => 'name','subcatalias'=>'alias'), 'left');
				
				$select->where('(products.name LIKE "%'.$search.'%" OR products.part_name LIKE "%'.$search.'%" OR products.supplier_part_name LIKE "%'.$search.'%") AND products.active = "1"');
				//$select->where('MATCH(products.name) AGAINST (\''.$search.'\')');
				
				$selectString = $sql->getSqlStringForSqlObject($select);
				//die($selectString);
				// $selectString = 'SELECT products.id, products.name, products.image, products.stock, products.active, class.name AS class_name, cat.name AS sub_category, (SELECT category.name FROM category WHERE category.id = cat.parent_id) AS category FROM products LEFT JOIN category AS cat ON products.category_id = cat.id LEFT JOIN class ON products.class_id = class.id WHERE products.deleted = 0 order by id desc';
				
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();
				
				$result->products = $rows;
				$result->status = 1;
				$result->message = "Products fetched.";
				
				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		public function getProductDetails($request) {

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				
				$select = $sql->select();
				$select->from(array('products' => 'products'), array())
						->join(array('c' => 'category'),
                			'c.id = products.category_id',
                    			array('sub_category' => 'name','sub_category_id'=>'id','subcatalias'=>'alias'), 'left')
						->join(array('s_c' => 'category'),
                			's_c.id = c.parent_id',
                    			array('category' => 'name','category_id'=>'id','catalias'=>'alias'), 'left')
						->join(array('class' =>'class'),
                			'class.id = products.class_id',
                    			array('class' => 'name','class_alias' => 'alias'), 'left')
						->join(array('videos' =>'videos'),
                			'videos.product_id = products.id',
                    			array('video_url' => 'url'), 'left');
						/*->join(array('rel' =>'related_images'),
                			'rel.product_id = products.id',
                    			array('relimg' => 'url'), 'left');*/
				
				$where = array('products.deleted' => 0);
				if (isset($request->category_id)) {
					$where['products.category_id'] = $request->category_id;
				}
				if (isset($request->class_id)) {
					$where['products.class_id'] = $request->class_id;
				}
				if (isset($request->product_id)) {
					$where['products.id'] = $request->product_id;


					require_once('Specification.php');
					$specification = new Specification();
					$result->specs = $specification->fetchSpecificationsByProductId($request);
					$result->related = $this->fetchRelatedProducts($request);
					$result->relimg = $this->getRelatedImages($request);
				}
				if (isset($request->alias)) {
					$where['products.id'] = Api::decodeId($request->alias);
					$request->product_id = Api::decodeId($request->alias);
					require_once('Specification.php');
					$specification = new Specification();
					$result->specs = $specification->fetchSpecificationsByProductId($request);
					$result->relimg = $this->getRelatedImages($request);
				}
				
				$select->where($where);
				$select->columns(array('id', 'class_id', 'name', 'part_name', 'supplier_part_name', 'quantity', 'mrp', 'small_description', 'detailed_description', 'image', 'media_title', 'media_url', 'meta_title', 'meta_keywords', 'meta_description', 'point1', 'point2', 'point3', 'weight', 'stock', 'active', 'specs','featured','alias'));
				
				$selectString = $sql->getSqlStringForSqlObject($select);
				//die($selectString);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$row = $run->toArray();
				if(count($row)){
					$result->product = $row[0];
				}else{
					$result->status = 0;
					$result->message = "No Product found.";
					return $result;
				}
				// $request->category_id = $row[0]['category_id'];
				// require_once('Category.php');
				// $category = new Category();
				// $result->categories = $category->fetchAllCategories($request);
				// $parent_category = $category->getCategoryDetails($request);
				// $result->parent_category = $parent_category->category['parent_id'];
				// require_once('Classification.php');
				// $class = new Classification();
				// $result->class = $class->fetchAllClass($request);

				$result->status = 1;
				$result->message = "Product data fetched.";

				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		function updateProduct($request) {
			$result = new stdClass();
			
			// /print_r($request);
			// echo '<pre>';
			// print_r($request);
			// echo '</pre>';
			// die();
			

			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$update = $sql->update();
                $update->table('products');                
                $data = array(
                    'category_id' => $request->category_id,
					'class_id' => $request->class_id,
					'name' => $request->name,
					'part_name' => $request->part_name,
					'supplier_part_name' => $request->supplier_part_name,
					'quantity' => $request->quantity,
					'mrp' => $request->mrp,
					'small_description' => $request->small_description,
					'detailed_description' => $request->detailed_description,
					'media_title' => $request->media_title,
					'media_url' => $request->media_url,
					'meta_title' => $request->meta_title,
					'meta_keywords' => $request->meta_keywords,
					'meta_description' => $request->meta_description,

					'point1' => $request->point1,
					'point2' => $request->point2,
					'point3' => $request->point3,

					'specs' => $request->specs,
					'height' => $request->height,
					'width' => $request->width,
					'weight' => $request->weight,
					'stock' => $request->stock,
					'featured' => $request->featured,
					'alias' => Api::formDashedString($request->name).'_p'.$request->id
				);
                $update->set($data);
                $update->where(array(
                    'id' => $request->id
                ));
                
                $statement = $sql->prepareStatementForSqlObject($update);
                $statement->execute();

                $sql = new Sql($adapter);

                $where = array(
						'product_id' => $request->id						
				);
				$spec = new TableGateway('product_specification', $adapter, null, new HydratingResultSet());
				$spec->delete($where);

                
                $specs = json_decode($request->specs, true);
               	//print_r($specs);
               	foreach ($specs as $key => $value) 
               	{             		
               		
               		$data = array(
						'product_id' => $request->id,
						'spec_id' => $key,
						'value' => $value,
					);
					$spec = new TableGateway('product_specification', $adapter, null, new HydratingResultSet());
					$spec->insert($data);
               	}

				$result->status = 1;
				$result->message = "Product details successfully updated.";

				// die(print_r($request));
				if (isset($request->image)) {
					// die(print_r($request));
					$ext = pathinfo($request->image['name'], PATHINFO_EXTENSION);

					$dir = '../images/product/';
					$file_name = $request->name.'_'.$request->id.'.'.$ext;

					//if ($condition) {
					if (move_uploaded_file($request->image['tmp_name'], $dir.$file_name)) {
						$img_url = 'images/product/'.$file_name;
						$update = $sql->update();
	                    $update->table('products');
	                    $update->set(array(
	                        'image'	=>	$img_url, 
	                    ));
	                    $update->where(array(
	                        'id' => $request->id
	                    ));
	                    $statement = $sql->prepareStatementForSqlObject($update);
	                    $statement->execute();
					}
					else {
						$result->status = 0;
						$result->message = "Product added but error in uploading main image.";
					}


					/*if($ext=='jpeg' || $ext=='jpg'){

					$img = @imagecreatefromjpeg($request->image['tmp_name']);
					$croped_image = @imagecrop($img, $request->cords);
					$condition = @imagejpeg($croped_image, $dir.$file_name,90 );

					}
					if($ext=='png'){

					$img = @imagecreatefrompng($request->image['tmp_name']);
					$croped_image = @imagecrop($img, $request->cords);
					$condition = @imagepng($croped_image, $dir.$file_name );

					}

					if ($condition) {
					// if (move_uploaded_file($request->image['tmp_name'], $dir.$file_name)) {
						$img_url = 'images/product/'.$file_name;
						$update = $sql->update();
	                    $update->table('products');
	                    $update->set(array(
	                        'image'	=>	$img_url, 
	                    ));
	                    $update->where(array(
	                        'id' => $request->id
	                    ));
	                    $statement = $sql->prepareStatementForSqlObject($update);
	                    $statement->execute();
					}*/
					
					// if ($ext in array('jpg', 'jpeg', 'png', 'gif')) {
					// }
				}

				if (isset($request->related_image)) {
					foreach ($request->related_image['tmp_name'] as $n => $tmp_name) {
						$ext = pathinfo($request->related_image['name'][$n], PATHINFO_EXTENSION);

						$dir = '../images/product/related-images/';
						$file_name = $request->name.'_'.$request->id.'_'.$n.'.'.$ext;
						if (move_uploaded_file($tmp_name, $dir.$file_name)) {
							$img_url = 'images/product/related-images/'.$file_name;
							
							// INSERT INTO `related_images`(`product_id`, `url`) VALUES 
							$product = new TableGateway('related_images', $adapter, null, new HydratingResultSet());
							$product->insert(array(
								'product_id' => $request->id,
								'url' =>	$img_url
							));

		                    $statement = $sql->prepareStatementForSqlObject($update);
		                    $statement->execute();
						}
											
					}
				}
				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
			
		}


		function deleteProduct($request) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$update = $sql->update();
                $update->table('products');
                $update->set(array(
                    'deleted'	=>	1
				));
                $update->where(array(
                    'id' => $request->product_id
                ));
                $statement = $sql->prepareStatementForSqlObject($update);
                $statement->execute();

				$result->status = 1;
				$result->message = "Product deleted.";

				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}


		function changeProductStatus($request) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$update = $sql->update();
                $update->table('products');
                $update->set(array(
                    'active'	=>	$request->status
				));
                $update->where(array(
                    'id' => $request->product_id
                ));
                $statement = $sql->prepareStatementForSqlObject($update);
                $statement->execute();

				$result->status = 1;
				$result->message = "Product Updated.";

				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}


		function changeOrderStatus($request) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$update = $sql->update();
                $update->table('customer_details');
                $update->set(array(
                    'status'	=>	$request->status
				));
                $update->where(array(
                    'id' => $request->order_id
                ));
                $statement = $sql->prepareStatementForSqlObject($update);
                //die($statement);
                $statement->execute();

				$result->status = 1;
				$result->message = "Order Updated.";

				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		

		public function getProductsByCustomerId($request) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();
				// SELECT products.name, products.part_name, products.supplier_part_name, products.small_description, products.image,  order_details.id AS order_id, order_details.product_id, order_details.quantity FROM order_details LEFT JOIN products ON products.id = order_details.product_id WHERE 
				$select->from(array('order_details' => 'order_details'), array())
						->join(array('products' => 'products'),
                			'products.id = order_details.product_id',
                    			array('name', 'part_name', 'supplier_part_name', 'small_description', 'image','stock','mrp'), 'left');
				
				$where = array('products.deleted' => 0);
				if (isset($request->customer_id)) {
					$where['order_details.customer_id'] = $request->customer_id;
				}
				$select->where($where);
				$select->columns(array('order_id' => 'id', 'product_id', 'quantity'));

				$selectString = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();
				
				
				$result->products = $rows;
				
				$result->status = 1;
				$result->message = "Product Updated.";

				return $result;	
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		public function getProductsByOrderId($request) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();
				// SELECT products.name, products.part_name, products.supplier_part_name, products.small_description, products.image,  order_details.id AS order_id, order_details.product_id, order_details.quantity FROM order_details LEFT JOIN products ON products.id = order_details.product_id WHERE 
				$select->from(array('order_details' => 'order_details'), array())
						->join(array('products' => 'products'),
                			'products.id = order_details.product_id',
                    			array('name', 'part_name', 'supplier_part_name', 'small_description', 'image','stock','mrp'), 'left');
				
				$where = array('products.deleted' => 0);
				if (isset($request->order_id)) {
					$where['order_details.order_id'] = $request->order_id;
				}
				$select->where($where);
				$select->columns(array('order_id' => 'id', 'product_id', 'quantity'));

				$selectString = $sql->getSqlStringForSqlObject($select);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();				
				
				$result->products = $rows;

				$sql = new Sql($adapter);

				$select = $sql->select();
							
				$select->from('customer_details');
				if (isset($request->order_id)) {
					$where1['id'] = $request->order_id;
				}
				
				$select->where($where1);
				$selectString = $sql->getSqlStringForSqlObject($select);
				

				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();
				
				$result->customer = $rows[0];
				
				$result->status = 1;
				$result->message = "Products Fetched.";

				return $result;	
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		public function filterProducts($request) {
			
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();
				// SELECT products.name, products.part_name, products.supplier_part_name, products.small_description, products.image,  order_details.id AS order_id, order_details.product_id, order_details.quantity FROM order_details LEFT JOIN products ON products.id = order_details.product_id WHERE 
				//$select->from('products');
				$select->from(array('p' => 'products'))
						->join(array('sc'=>'category'),'sc.id = p.category_id', array('sub_category' => 'name'),'left')
						->join(array('c'=>'category'),'sc.parent_id = c.id', array('category' => 'name'),'left')
						->join(array('cls'=>'class'),'p.class_id = cls.id', array('class' => 'name'),'left');
				$where = array('p.deleted' => 0);

				if (isset($request->name)) {
					$select->where('(p.name LIKE "%'.$request->name.'%" OR p.part_name LIKE "%'.$request->name.'%" OR p.supplier_part_name LIKE "%'.$request->name.'%")');
					
				}
				if (isset($request->class)) {
					$where['p.class_id'] = $request->class;					
				}
				if (isset($request->subcat)) {
					$where['p.category_id'] = $request->subcat;					
				}
				if (isset($request->cat)) {
					$where['sc.parent_id'] = $request->cat;
					//$select->where('category_id IN (SELECT id FROM category WHERE parent_id = '.$request->cat.')');					
				}
				if (isset($request->featured)) {
					$where['p.featured'] = $request->featured;
					//$select->where('category_id IN (SELECT id FROM category WHERE parent_id = '.$request->cat.')');					
				}
				
				//$where['deleted'] = '0';
				$select->where($where);
				$select->order('id DESC');
				//$select->columns(array('id', 'name', 'image' ));

				$selectString = $sql->getSqlStringForSqlObject($select);
				//die($selectString);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();
				$result->products = $rows;

				//die(print_r($result));
				
				$result->status = 1;
				$result->message = "Filtered";

				return $result;	
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}


		function saveRelatedProduct($request) {
			$result = new stdClass();						

			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
               	
               	foreach ($request->data as $key => $value) {
               		# code...
               		if($this->checkDuplicateRelatedProduct($request->product_id, $value)){
	               		$data = array(
							'product_id' => $request->product_id,						
							'related_id' => $value
						);
						$related = new TableGateway('related_products', $adapter, null, new HydratingResultSet());
						$related->insert($data);
					}
               	}

				$result->status = 1;
				$result->message = "Related Product successfully added.";
				
				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
			
		}

		function setRelatedProduct($request) {
			$result = new stdClass();						

			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
               	
               
           		if($this->checkDuplicateRelatedProduct($request->product_id, $request->related_id)){
               		$data = array(
						'product_id' => $request->product_id,						
						'related_id' => $request->related_id
					);
					$related = new TableGateway('related_products', $adapter, null, new HydratingResultSet());
					$related->insert($data);
				}
               

				$result->status = 1;
				$result->message = "Related Product Added.";
				
				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
			
		}

		function unsetRelatedProduct($request) {
			$result = new stdClass();						

			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

                $where = array(
						'product_id' => $request->product_id,
						'related_id' => $request->related_id						
				);
				$spec = new TableGateway('related_products', $adapter, null, new HydratingResultSet());
				$spec->delete($where);

				$result->status = 1;
				$result->message = "Related Product Removed.";
				
				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
			
		}



		function checkDuplicate($name, $subcat = '0', $id = '-1') {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();
				
				$select->from('products');
						
				$where = new $select->where();
				$where->equalTo('deleted', 0);
				$where->equalTo('category_id', $subcat);
				$where->notEqualTo('id', $id);

				//$where = array('parent_id' => $parent);
				
								
				if (isset($name)) {
					$where->equalTo('name', $name);

				}
				
				$select->where($where);
				
				//$select->columns(array('id', 'name', 'description', 'status', 'image', 'parent_id' ));

				$selectString = $sql->getSqlStringForSqlObject($select);
				//die($selectString);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();
				$c = 0;
				$c = count($rows);

				if($c==0){
					return true;					
				}else{
					return false;
				}

				
			} catch(Exception $e) {
				
				return false;
			}
		}


		function checkDuplicateRelatedProduct($product_id, $related_id) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = $sql->select();
				
				$select->from('related_products');
						
				$where = new $select->where();
				$where->equalTo('product_id', $product_id);
				$where->equalTo('related_id', $related_id);
				
				$select->where($where);				
				
				$selectString = $sql->getSqlStringForSqlObject($select);
				//die($selectString);
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();
				$c = 0;
				$c = count($rows);

				if($c==0){
					return true;
				}else{
					return false;
				}				
			} catch(Exception $e) {				
				return false;
			}

		}

		public function getRelatedImages($request) {
			$result = new stdClass();
			
			try {
				$adapter = $this->adapter;

				$sql = new Sql($adapter);

				$select = $sql->select();
				$select->from('related_images');
				
				$where = array('product_id' => $request->product_id);
				
				$select->where($where);
				$selectString = $sql->getSqlStringForSqlObject($select);
				//die($selectString);
				
				$run = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$rows = $run->toArray();
				
				$result = $rows;
				
				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
			
		}
	}
?>