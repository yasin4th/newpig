<?php

//loading zend framework
include __DIR__.'/Zend/Loader/AutoloaderFactory.php';
Zend\Loader\AutoloaderFactory::factory(array(
		'Zend\Loader\StandardAutoloader' => array(
				'autoregister_zf' => true,
				'db' => 'Zend\Db\Sql'
		)
));

$result = new stdClass();
if(isset($_POST['action'])) {
	include 'custom/Api.php';
	$data = new stdClass();
	$action = $_POST['action'];

	switch ($action) {

		case 'addCategory':
			
			@session_start();
			if(isset($_SESSION['user_id'])) {
				$data->userId = $_SESSION['user_id'];

				if (isset($_POST['parent-id'])) {
					$data->parent_id = $_POST['parent-id'];
				}

				$data->name = $_POST['category-name'];
				$data->description = $_POST['category-description'];
				$data->status = $_POST['category-status'];
				if ($_FILES['category-image']['name']) {
					$data->image = $_FILES['category-image'];
					$data->cords = array('x' =>$_POST['x'] , 'y' => $_POST['y'], 'width' => $_POST['w'], 'height'=> $_POST['h']);
				}
				$data->meta_title = $_POST['meta-title-add'];
				$data->meta_keywords = $_POST['meta-keywords-add'];
				$data->meta_description = $_POST['meta-description-add'];
				$data->details = $_POST['details-add'];

				//die(print_r($data));

				$result = Api::addCategory($data);
			}
			else {
				$result->status = 2;
				$result->message = "Session Expired. Please Login Again.";
			}
		break;		

		case 'updateCategory':
			
			@session_start();
			if(isset($_SESSION['user_id'])) {
				$data->userId = $_SESSION['user_id'];
				if ($_POST['edit-category-id'] != 0) {
					$data->category_id = $_POST['edit-category-id'];

					if (isset($_POST['edit-category-parent-id'])) {
						$data->parent_id = $_POST['edit-category-parent-id'];
					}
					
					$data->name = $_POST['edit-category-name'];
					$data->description = $_POST['edit-category-description'];
					$data->status = $_POST['edit-category-status'];
					if ($_FILES['edit-category-image']['name']) {
						$data->image = $_FILES['edit-category-image'];
						$data->cords = array('x' =>$_POST['x1'] , 'y' => $_POST['y1'], 'width' => $_POST['w1'], 'height'=> $_POST['h1']);
					}
					$data->meta_title = $_POST['meta-title-edit'];
					$data->meta_keywords = $_POST['meta-keywords-edit'];
					$data->meta_description = $_POST['meta-description-edit'];
					$data->details = $_POST['details-edit'];

					// die(print_r($data));
					$result = Api::updateCategory($data);
				} else {
					$result->status = 0;
					$result->message = "Error: Please refresh page and try again.";
				}
			}
			else {
				$result->status = 2;
				$result->message = "Session Expired. Please Login Again.";
			}
		break;

		case 'addClass':
			
			@session_start();
			if(isset($_SESSION['user_id'])) {
				$data->userId = $_SESSION['user_id'];

				if (isset($_POST['parent-id'])) {
					$data->parent_id = $_POST['parent-id'];
				}

				$data->category = $_POST['categories'];
				$data->sub_category = $_POST['sub-categories'];
				$data->name = $_POST['class-name'];
				$data->description = $_POST['class-description'];
				$data->status = $_POST['class-status'];
				if ($_FILES['class-image']['name']) {
					$data->image = $_FILES['class-image'];
					$data->cords = array('x' =>$_POST['x'] , 'y' => $_POST['y'], 'width' => $_POST['w'], 'height'=> $_POST['h']);
				}
				$data->meta_title = $_POST['meta-title-add'];
				$data->meta_keywords = $_POST['meta-keywords-add'];
				$data->meta_description = $_POST['meta-description-add'];
				$data->details = $_POST['details-add'];
				
				// die(print_r($_POST));
				$result = Api::addClass($data);
			}
			else {
				$result->status = 2;
				$result->message = "Session Expired. Please Login Again.";
			}
		break;

		case 'updateClass':
			
			@session_start();
			if(isset($_SESSION['user_id'])) {
				$data->userId = $_SESSION['user_id'];
				if ($_POST['edit-class-id'] != 0) {

					$data->category = $_POST['edit-class-category'];
					$data->sub_category = $_POST['edit-class-sub-category'];
					$data->id = $_POST['edit-class-id'];
					$data->name = $_POST['edit-class-name'];
					$data->description = $_POST['edit-class-description'];
					$data->status = $_POST['edit-class-status'];
					if ($_FILES['edit-class-image']['name']) {
						$data->image = $_FILES['edit-class-image'];
						$data->cords = array('x' =>$_POST['x1'] , 'y' => $_POST['y1'], 'width' => $_POST['w1'], 'height'=> $_POST['h1']);
					}
					$data->meta_title = $_POST['meta-title-edit'];
					$data->meta_keywords = $_POST['meta-keywords-edit'];
					$data->meta_description = $_POST['meta-description-edit'];
					$data->details = $_POST['details-edit'];
					// die(print_r($data));
					$result = Api::updateClass($data);
				} else {
					$result->status = 0;
					$result->message = "Error: Please refresh page and try again.";
				}
			}
			else {
				$result->status = 2;
				$result->message = "Session Expired. Please Login Again.";
			}
		break;
		

		case 'addProduct':
			
			@session_start();
			if(isset($_SESSION['user_id'])) {
				$data->userId = $_SESSION['user_id'];
				
				$data->category_id = $_POST['sub-category'];
				$data->class_id = $_POST['product-class'];
				$data->name = $_POST['product-name'];
				$data->part_name = $_POST['product-part-name'];
				$data->supplier_part_name = $_POST['product-supplier-name'];
				$data->quantity = $_POST['quantity'];
				$data->mrp = $_POST['mrp'];
				$data->small_description = $_POST['small-description'];
				$data->detailed_description = $_POST['detailed-description'];
				$data->height = $_POST['height'];
				$data->weight = $_POST['weight'];
				$data->width = $_POST['width'];
				$data->specs = $_POST['specs'];
				
				$data->media_title = $_POST['media-title'];
				$data->media_url = $_POST['media-url'];
				$data->meta_title = $_POST['meta-title'];
				$data->meta_keywords = $_POST['meta-keywords'];
				$data->meta_description = $_POST['meta-description'];
				$data->stock = $_POST['stock'];
				//$data->spec0 = $_POST['spec0'];
				if (isset($_POST['featured'])) {
					$data->featured = $_POST['featured'];					
				}else{
					$data->featured = '0';
				}
				

				if ($_FILES['product-image']['name']) {
					$data->image = $_FILES['product-image'];
					$data->cords = array('x' =>$_POST['x'] , 'y' => $_POST['y'], 'width' => $_POST['w'], 'height'=> $_POST['h']);
				}
				if ($_FILES['related-image']['name']) {
					$data->related_image = $_FILES['related-image'];
				}

				$data->point1 = $_POST['point1'];
				$data->point2 = $_POST['point2'];
				$data->point3 = $_POST['point3'];

				//die(print_r($data->related_image));
				$result = Api::addProduct($data);
			}
			else {
				$result->status = 2;
				$result->message = "Session Expired. Please Login Again.";
			}
		break;


		case 'updateProduct':
			
			@session_start();
			if(isset($_SESSION['user_id'])) {
				$data->userId = $_SESSION['user_id'];
				if ($_POST['edit-product-id'] != 0) {
				
					$data->id = $_POST['edit-product-id'];
					$data->category_id = $_POST['sub-category'];
					$data->class_id = $_POST['product-class'];
					$data->name = $_POST['product-name'];
					$data->part_name = $_POST['product-part-name'];
					$data->supplier_part_name = $_POST['product-supplier-name'];
					$data->quantity = $_POST['quantity'];
					$data->mrp = $_POST['mrp'];
					$data->small_description = $_POST['small-description'];
					$data->detailed_description = $_POST['detailed-description'];
					$data->specs = $_POST['specs'];
					
					$data->height = $_POST['height'];
					$data->weight = $_POST['weight'];
					$data->width = $_POST['width'];
					
					$data->media_title = $_POST['media-title'];
					$data->media_url = $_POST['media-url'];
					$data->meta_title = $_POST['meta-title'];
					$data->meta_keywords = $_POST['meta-keywords'];
					$data->meta_description = $_POST['meta-description'];
					$data->stock = $_POST['stock'];
					
					if (isset($_POST['featured'])) {
						$data->featured = $_POST['featured'];					
					}else{
						$data->featured = '0';
					}
					// die(print_r($data));

					if ($_FILES['product-image']['name']) {
						$data->image = $_FILES['product-image'];
						//$data->cords = array('x' =>$_POST['x'] , 'y' => $_POST['y'], 'width' => $_POST['w'], 'height'=> $_POST['h']);
					}
					if ($_FILES['related-image']['name']) {
						$data->related_image = $_FILES['related-image'];
					}

					$data->point1 = $_POST['point1'];
					$data->point2 = $_POST['point2'];
					$data->point3 = $_POST['point3'];

					// die(print_r($data->related_image));
					$result = Api::updateProduct($data);
				}
			}
			else {
				$result->status = 2;
				$result->message = "Session Expired. Please Login Again.";
			}
		break;

		case 'filterCategory':
			
			@session_start();
			if(isset($_SESSION['user_id'])) {
				//$data->userId = $_SESSION['user_id'];
				if (isset($_POST['catname']) && isset($_POST['status'])) {
				
					//$data->catname = $_POST['catname'];
					//$data->status = $_POST['status'];
				
					if(!empty($_POST['catname'])){
						$data->catname=$_POST['catname'];	
					}
					if($_POST['status'] != '0'){
						$data->status=$_POST['status'];
					}

					//die(print_r($data));
					
					$result = Api::filterCategory($data);
				}
			}
			else {
				$result->status = 2;
				$result->message = "Session Expired. Please Login Again.";
			}
		break;


		case 'filterSubCategory':
			
			@session_start();
			if(isset($_SESSION['user_id'])) {
				//$data->userId = $_SESSION['user_id'];
				if (isset($_POST['catname']) && isset($_POST['status']) && isset($_POST['subcatname'])) {
				
					//$data->catname = $_POST['catname'];
					//$data->status = $_POST['status'];
				
					if( $_POST['catname']!= '0'){
						$data->cat=$_POST['catname'];	
					}
					if( !empty($_POST['subcatname'])){
						$data->subcatname=$_POST['subcatname'];	
					}
					if( $_POST['status'] != '0'){
						$data->status=$_POST['status'];
					}

					//die(print_r($data));
					
					$result = Api::filterSubCategory($data);
				}
			}
			else {
				$result->status = 2;
				$result->message = "Session Expired. Please Login Again.";
			}
		break;

		case 'filterClass':
			
			@session_start();
			if(isset($_SESSION['user_id'])) {
				//$data->userId = $_SESSION['user_id'];
				if (isset($_POST['catname']) && isset($_POST['status']) && isset($_POST['subcatname']) && isset($_POST['classname'])) {
				
					//$data->catname = $_POST['catname'];
					//$data->status = $_POST['status'];
				
					if( $_POST['catname'] != '0'){
						$data->cat=$_POST['catname'];	
					}
					if( $_POST['subcatname'] != '0'){
						$data->subcat=$_POST['subcatname'];	
					}
					if( $_POST['status'] != '0'){
						$data->status=$_POST['status'];
					}
					if(!empty($_POST['classname'])){
						$data->classname=$_POST['classname'];
					}

					//die(print_r($data));
					
					$result = Api::filterClass($data);
				}
			}
			else {
				$result->status = 2;
				$result->message = "Session Expired. Please Login Again.";
			}
		break;

		case 'filterProduct':
			
			@session_start();
			if(isset($_SESSION['user_id'])) {
				//$data->userId = $_SESSION['user_id'];
				if (isset($_POST['cat']) && isset($_POST['subcat']) && isset($_POST['name']) && isset($_POST['class'])) {
				
					//$data->catname = $_POST['catname'];
					//$data->status = $_POST['status'];
				
					if( $_POST['cat']!= '0'){
						$data->cat = $_POST['cat'];	
					}
					if( $_POST['subcat']!= '0'){
						$data->subcat = $_POST['subcat'];	
					}
					if( $_POST['class']!= '0'){
						$data->class = $_POST['class'];	
					}
					if( !empty($_POST['name'])){
						$data->name=$_POST['name'];	
					}
					if( !empty($_POST['featured'])){
						$data->featured=$_POST['featured'];	
					}
					

					//die(print_r($data));
					
					$result = Api::filterProducts($data);
				}
			}
			else {
				$result->status = 2;
				$result->message = "Session Expired. Please Login Again.";
			}
		break;

		case 'filterArticle':
			
			@session_start();
			if(isset($_SESSION['user_id'])) {
				//$data->userId = $_SESSION['user_id'];
				if (isset($_POST['cat']) && isset($_POST['status']) && isset($_POST['subcat']) && isset($_POST['name'])) {
				
					//$data->catname = $_POST['catname'];
					//$data->status = $_POST['status'];
				
					if( $_POST['cat'] != '0'){
						$data->cat=$_POST['cat'];	
					}
					if( $_POST['subcat'] != '0'){
						$data->subcat=$_POST['subcat'];	
					}
					if( $_POST['status'] != '-1'){
						$data->status=$_POST['status'];
					}
					if(!empty($_POST['name'])){
						$data->name=$_POST['name'];
					}

					//die(print_r($data));
					
					$result = Api::filterArticle($data);
				}
			}
			else {
				$result->status = 2;
				$result->message = "Session Expired. Please Login Again.";
			}
		break;

		case 'filterVideos':
			
			@session_start();
			if(isset($_SESSION['user_id'])) {
				//$data->userId = $_SESSION['user_id'];
				if (isset($_POST['cat']) && isset($_POST['status']) && isset($_POST['subcat']) && isset($_POST['name'])) {
				
					//$data->catname = $_POST['catname'];
					//$data->status = $_POST['status'];
				
					if( $_POST['cat'] != '0'){
						$data->cat=$_POST['cat'];	
					}
					if( $_POST['subcat'] != '0'){
						$data->subcat=$_POST['subcat'];	
					}
					if( $_POST['status'] != '-1'){
						$data->status=$_POST['status'];
					}
					if(!empty($_POST['name'])){
						$data->name=$_POST['name'];
					}

					//die(print_r($data));
					
					$result = Api::filterVideos($data);
				}
			}
			else {
				$result->status = 2;
				$result->message = "Session Expired. Please Login Again.";
			}
		break;

		case 'filterOrder':
			
			@session_start();
			if(isset($_SESSION['user_id'])) {
				//$data->userId = $_SESSION['user_id'];
				
				if (isset($_POST['name']) && isset($_POST['status'] )) {
				
					//$data->catname = $_POST['catname'];
					//$data->status = $_POST['status'];

					if(!empty($_POST['name'])){
						$data->name=$_POST['name'];
					}
					if(!empty($_POST['from'])){
						$data->from=$_POST['from'];
					}
					if(!empty($_POST['to'])){
						$data->to=$_POST['to'];
					}
					if($_POST['status'] != '0'){
						$data->status=$_POST['status'];
					}

					//die(print_r($data));
					
					$result = Api::filterOrder($data);
				}
			}
			else {
				$result->status = 2;
				$result->message = "Session Expired. Please Login Again.";
			}
		break;

		case 'addSpecification':
			
			@session_start();
			if(isset($_SESSION['user_id'])) {
				$data->userId = $_SESSION['user_id'];

				$data->name = $_POST['add-name'];
				
				$data->status = $_POST['add-status'];
				
				

				//die(print_r($_POST));
				$result = Api::addSpecification($data);
			}
			else {
				$result->status = 2;
				$result->message = "Session Expired. Please Login Again.";
			}
		break;		

		case 'updateSpecification':
			
			@session_start();
			if(isset($_SESSION['user_id'])) {
				$data->userId = $_SESSION['user_id'];
				if ($_POST['edit-spec-id'] != '') {
					
					$data->name = $_POST['edit-name'];
				
					$data->status = $_POST['edit-status'];	

					$data->spec_id = $_POST['edit-spec-id'];
					$result = Api::updateSpecification($data);
				} else {
					$result->status = 0;
					$result->message = "Error: Please refresh page and try again.";
				}
			}
			else {
				$result->status = 2;
				$result->message = "Session Expired. Please Login Again.";
			}
		break;

		default:
			$result->status = 0;
			$result->message = 'Invalid Input set. Please try again.';
		break;
	}
}
else
{
	$result->status = 0;
	$result->message = 'Invalid Input set. Please try again.';
}

echo json_encode($result);
