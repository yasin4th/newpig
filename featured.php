<!DOCTYPE html>
<html lang="en">
<head>
	<title>iQSafety - New Pig Best Sellers - Absorbents Mats & Pillows</title>
	<meta name="description" content="iQSafety - New Pig Best Sellers products - Pig Absorbents Mats & Pillows.">
	<meta name="keywords" content="Best Sellers" >

    <?php include('html/head-tag.php'); ?>
</head><!--/head-->

<body>
	<!--header-->
	<?php include('html/header.php'); ?>
	<!--/header-->
	
	<section class="mrg-top30">
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<?php include('html/side-bar.php'); ?>
				</div>
				
				<div id="all-categories" class="col-sm-9 padding-right">
					<div class="features_items"><!--features_items-->
						<h2 class="title"><span class="white-bg"> Best Sellers </span></h2>
						<span id="countProducts"></span>
						<div id="products">

						</div>
						<!-- <div class="col-sm-4">
							<div class="product-image-wrapper">
								<div class="single-products">
									<div class="productinfo text-center">
										<img src="images/product/absorbent/pig-universal-absorbent-mats.jpg" alt="Absorbents" title="" />
										<p>PIG Universal Absorbent Mats</p>
										<a href="products-details.php" class="btn btn-primary add-to-cart"><i class="fa fa-eye"></i>View </a>
									</div>
								</div>
							</div>
						</div> -->
					</div><!--features_items-->
				</div>
			</div>
		</div>
	</section>
	
	<!--Footer-->
	<?php include('html/footer.php'); ?>
	<!--/Footer-->

	<!--/js-files-->
	<?php include('html/js-files.php'); ?>
	<script type="text/javascript" src="js/custom/featured.js"></script>
	<!--/js-files-->

</body>
</html>