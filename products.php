<?php
	$meta_title='';
	$meta_keywords='';
	$meta_description='';

	if(isset($_GET['id'])){
		if(!empty($_GET['id'])){
			$request = new stdClass();
			$request->action = 'getClassDetails';
			
			$id=$_GET['id'];	
			$request->class_id = $_GET['id'];
				
			require('vendor/requests1.php');

			$res = Api::getClassDetails($request);
			//print_r($res);
			//$array = json_decode($res->product[0], true );
			$json = json_encode($res);
			$array = json_decode($json, true);

			if(count($array['class']) == 0){
				header('Location: ../404.php');
			}
			$meta_title=$array['class']['meta_title'];
			$meta_keywords=$array['class']['meta_keywords'];
			$meta_description=$array['class']['meta_description'];
			$details=$array['class']['details'];

            $name = $array['class']['name'];
            $catalias = preg_replace('/_[a-z\d]+/', '', $array['class']['calias']);
            $subcat = preg_replace('/_[a-z\d]+/', '', $array['class']['salias']);
            $breadcrumb = "<a href='products/{$array['class']['calias']}'> {$array['class']['cname']} </a> > <a href='{$catalias}/{$array['class']['salias']}'>{$array['class']['scname']}</a> > <a href='{$subcat}/{$array['class']['alias']}' > {$array['class']['name']} </a>";    
		}else{
			header('Location: ../404.php');
		}

	}else{
		header('Location: ../404.php');
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="description" content="<?=$meta_description; ?>">
	<meta name="keywords" content="<?=$meta_keywords; ?>">
	<title><?=$meta_title;?></title>
    <?php include('html/head-tag.php'); ?>
</head><!--/head-->

<body>
	<!--header-->
	<?php include('html/header.php'); ?>
	<!--/header-->
	
	<section class="mrg-top30">
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<?php include('html/side-bar.php'); ?>
				</div>
				
				<div class="col-sm-9 padding-right">
					<h1 id="title" class="title"><span class="white-bg"><?=$name;?></span></h1>
					<p id="breadcrumb"><?=$breadcrumb;?></p>
					<div class="features_items"><!--features_items-->
                        <?php
                            $pro = getData1(array('action' => 'fetchAllProducts', 'active' => '1', 'class_id' => $id));
						
                            //if(count($pro['products'])){
                        ?>
						<h5 class="title"><!-- <span class="white-bg">Products</span> --></h5>
						<span id="countProducts"><?=count($pro->products);?> items found</span>
						<!--<p class="text-justify">Welcome to our products page. We have a wide range of spill prevention and spill management products
						 as well as save storage solutions.<br>Scroll below to explore our range of innovative, high quality 
						 products that are preferred by many customers ranging from large manufacturers to small businesses 
						 including car repair workshops and Government organisations. <br>If you have a question about what 
						 would work for you, please contact us or call us and we can work with you to find the best solution 
						 for your application.</p>-->
						<div id="classwise-products">

                            <?php
                                $str = '<div class="row">';

                                foreach ($pro->products as $product) {
                                    $str .= '<div class="col-sm-4"> <div class="product-image-wrapper"> <div class="single-products"> <div class="productinfo text-center">';
                                    
                                    $subcat = preg_replace('/_[a-z\d]+/', '', $product['calias']);
                                    $str .= '<a href="'.$subcat.'/'.$product['alias'].'">';
                                    $str .= '<div class="img-block">';
                                    if($product['relimage'] != ''){
                                        $str .= "<img src='{$product['image']}' alt='{$product['name']}' title='{$product['name']}' onmouseover=\"this.src='{$product['relimage']}\'; onmouseout=\"this.src='{$product['image']}';\" />";
                                    }else{
                                        $str .= '<img src="'.$product['image'].'" alt="'.$product['name'].'" title="'.$product['name'].'" />';
                                    }
                                    $str .= '</div>';

                                    if(strlen($product['name']) > 50)
                                    {
                                        $n = substr($product['name'], 0, 50);
                                        $str .= "<p>{$n} ... ({$product['part_name']})</p>";
                                    }
                                    else
                                    {   
                                        $str .= "<p>{$product['name']} ({$product['part_name']})</p>";                                            
                                    }           
                                            
                                    $str .= '</a>';

                                    $str .= '<div class="points-div">';
                                    $str .= "<ul class='points'>";
                                    if(trim($product['point1']) != '')
                                    {
                                        $str .= "<li>".trim($product['point1'])."</li>";
                                    }
                                    if(trim($product['point2']) != '')
                                    {
                                        $str .= "<li>".trim($product['point2'])."</li>";
                                    }
                                    if(trim($product['point3']) != '')
                                    {
                                        $str .= "<li>".trim($product['point3'])."</li>";
                                    }

                                    $str .= "</ul>";
                                    $str .= "<a class='btn btn-primary btn-block cart' data-id='{$product['id']}'><i class='fa fa-shopping-cart'></i> Add to Quote</a>";
                                    $str .= '</div>';

                                    $str .= '</div></div></div></div>';
                                }
                                $str .= "</div>";
                                echo $str;
                            ?>
							<!--
							<div class="col-sm-4">
								<div class="product-image-wrapper">
									<div class="single-products">
										<div class="productinfo text-center">
											<img src="images/product/absorbent/pig-universal-absorbent-mats.jpg" alt="Absorbents" title="" />
											<p>PIG Universal Absorbent Mats</p>
											<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-eye"></i>View </a>
										</div>
									</div>
								</div>
							</div> 
							-->
						</div>

						<div class="row">
							<div class="col-sm-12 col-md-12">				
								<?php if(trim($details) != '') { ?>
								<div class="details">
									<?php echo $details;?>
								</div>
								<?php } ?>
							</div>
						</div>

					</div><!--features_items-->
				</div>
			</div>
    		     
		</div>
	</section>

	<!--Footer-->
	<?php include('html/footer.php'); ?>
	<!--/Footer-->

	<!--/js-files-->
	<?php include('html/js-files.php'); ?>
	<script type="text/javascript" src="js/custom/products.js"></script>
	<!--/js-files-->
	<script type="text/javascript">
		var class_id;
		var class_id = '<?=$id;?>';
	</script>

</body>
</html>