<?php
	$meta_title='';
	$meta_keywords='';
	$meta_description='';
	if(isset($_GET['id'])){
		if(!empty($_GET['id'])){
			$request = new stdClass();
			$request->action = 'getProductDetails';
			// echo '<pre>';
			//  print_r($_SERVER);
			// echo '</pre>';		
				
			$request->product_id = $_GET['id'];
				
			require('vendor/requests1.php');

			

			$res = Api::getProductDetails($request);
			
			//$array = json_decode($res->product[0], true );
			//$json = json_encode($res);
			//$array = json_decode($json,true);
			
			// if($array['status'] != 1){
			// 	header('Location: ../404.php');
			// }
			
			if($res->status == 0)
			{
				header('Location: ../404');
			}
			$result = $res->product;
			
			
			$specs = $res->specs;
			$relimg = $res->relimg;
			$related = $res->related;
			$meta_title=$result['meta_title'];
			$meta_keywords=$result['meta_keywords'];
			$meta_description=$result['meta_description'];
			
			$point1 = trim($result['point1']);
			$point2 = trim($result['point2']);
			$point3 = trim($result['point3']);

			//echo strstr($result['subcatalias'], '_', true).'/'.$result['alias'];
			
			// echo '<pre>';
			// print_r($related);
			// echo '</pre>';
			//die();
		}
		else
		{
			header('Location: ../404');
		}

	}else{
		header('Location: ../404');
	}
?>	
<!DOCTYPE html>

<html lang="en">
<head>
	<title><?=strip_tags($result['name']); ?></title>
	<meta name="description" content="<?=$meta_description; ?>">
	<meta name="keywords" content="<?=$meta_keywords; ?>">
    <?php include('html/head-tag.php'); ?>
    <link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto main stylesheet" charset="utf-8" />
	
</head>
<!--/head-->

<body>
	<!--header-->
	<?php include('html/header.php'); ?>
	<!--/header-->
	
	<section class="mrg-top30">
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<?php include('html/side-bar.php'); ?>
				</div>
				
				<div class="col-sm-9 padding-right">
					<div class="product-details"><!--product-details-->	

						<p id="breadcrumb">
						<?php if($result['category']!=''){?>
						<a href="products/<?=$result['catalias'];?>"><?=$result['category'];?></a> > 
						<?php } ?>
						<a href="<?=strstr($result['catalias'], '_', true);?>/<?=$result['subcatalias'];?>"><?=$result['sub_category'];?></a> 
						<?php
							if($result['class_id']!=0){?>
						> <a href="<?=strstr($result['subcatalias'], '_', true);?>/<?=$result['class_alias'];?>"><?=$result['class'];?></a>
						<?php } ?>

						 </p>
						<div class="col-sm-5">
							<a rel="prettyPhoto[gallery1]" href="<?=$result['image'];?>">
							<div class="view-product">
								<img style="height:260px;width:260px;" class="product-name" src="<?=$result['image']; ?>" alt="" title="<?=$result['image']; ?>" />
								<!--<h3>ZOOM</h3>-->
							</div>
							</a>
				    	<?php if(count($relimg)) { ?>
							<div id="similar-product" class="carousel slide" data-ride="carousel">
								
								<!-- Wrapper for slides -->
								
							    <div class="carousel-inner">

							    		<?php foreach($relimg as $key=>$img) {
							    			if($key == 0){ echo '<div class="item active">'; }else
							    			if($key%4 == 0){ echo '</div><div class="item">';}
							    		?>
							    		
							    			<a rel="prettyPhoto[gallery1]" href="<?=$img['url'];?>">
							    				<div class="related-img">
							    					<img class="img-responsive" src="<?=$img['url'];?>" alt="" />
							    				</div>
							    			</a>									  
							    		
										<?php }echo '</div>'; ?>

								</div>

								
								<a class="left recommended-item-control" href="#similar-product" data-slide="prev">
									<i class="fa fa-angle-left"></i>
								</a>
								<a class="right recommended-item-control" href="#similar-product" data-slide="next">
									<i class="fa fa-angle-right"></i>
								</a>						
								
							</div>
						<?php } ?>

						</div>
						<div class="col-sm-7">
							<div class="product-information product-details"><!--/product-information-->
								<h2 class="product-name"><?=$result['name']; ?></h2>
								<?php if($result['part_name']!=""){?><p class="product-part-name"><?=$result['part_name']; ?></p><?php } ?>
								<?php if($result['small_description']!=""){?><p class="small-description"><?=$result['small_description']; ?></p><?php } ?>
								<div class="points-div">
									<ul class="points">
									<?php
										if($point1 != ''){echo "<li>$point1</li>";}
										if($point2 != ''){echo "<li>$point2</li>";}
										if($point3 != ''){echo "<li>$point3</li>";}
									?>
									</ul>
								</div>
								
								<span class="quantity-div">
																	
									<label>Quantity:</label>
									<input id="product-quantity" type="number" value="1" min="1" />
									<a id="add-to-cart" class="btn btn-primary cart">
										<i class="fa fa-shopping-cart"></i>
										Add to Quote
									</a>
								</span>
							</div><!--/product-information-->
						</div>
					</div><!--/product-details-->
					
					<div class="category-tab shop-details-tab"><!--category-tab-->
						<div class="col-sm-12">
							<ul class="nav nav-tabs">
								<li class="active"><a href="#description" data-toggle="tab">Description</a></li>
								<?php if($specs!=""){?><li><a href="#specifications" data-toggle="tab">Specifications</a></li><?php } ?>
								<?php if($result['media_url']!=""){?>

								<li><a href="#media" data-toggle="tab">Media</a></li>
								<?php } ?>
								<!-- <li class="active"><a href="#reviews" data-toggle="tab">Reviews (5)</a></li> -->
							</ul>
						</div>
						<div class="tab-content">
							<div class="tab-pane fade active in" id="description" >
								<div class="col-sm-12 padd-LR25">
									<?=$result['detailed_description']; ?>
								</div>
							</div>
							
							<div class="tab-pane fade" id="specifications" >
								<div class="col-sm-12 padd-LR25">
									<table class="table table-bordered" id="specs">
										<tbody>
											<?php
												
												foreach($specs as $spec)
												{													
													echo '<tr><td>'.$spec['name'].':</td><td>'.$spec['value'].'</td></tr>';
												}
											?>
											
										</tbody>
									</table>
								</div>
							</div>
							
							<div class="tab-pane fade" id="media" >
								<div class="col-sm-12 padd-LR25">
									<p><?=$result['media_title']; ?></p>
									<div class="vedio">
										<iframe id="video-preview" width="560" height="315" src="https://youtube.com/embed/<?=$result['media_url']; ?>" frameborder="0" allowfullscreen></iframe>
									</div>
								</div>
							</div>						
							
							
						</div>
					</div><!--/category-tab-->
					<?php
					if(count($related)){ ?>
					<div class="related-items">
						<div class="related-items-heading">
							<h4>You may also like...</h4>
						</div>

						<div class="related-items-content">

							<div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
								<div class="carousel-inner featured-product">
									<!-- <div class="item active left"> -->
										

<?php 	
	$count = 1;
	$first = true;

	foreach($related as $product){		
		if($count == 1 && $first){
	
		echo '<div class="item active">';		
			
		}else if($count == 1 && !$first){ 
		//if more x*3 + 1		
		echo '</div><div class="item">';
		
		} 
		$first = false;		
		$subcatalias=$product['sub_category_alias'];
		$subcatalias = strstr($subcatalias, '_', true);
		
		
		//1 item
		echo '<div class="col-sm-4">';
		echo '<div class="product-image-wrapper">';
		echo " <a href='$subcatalias/" . $product['alias'] . "'>";
		echo '<div class="single-products"><div class="productinfo text-center">';
		echo "<img src='" . $product['image'] . "' alt='" . $product['name'] . "'><p>" . $product['name'] . " (" . $product['part_name'] . ")</p>";
		echo "</div></div></a>";
		echo "<div class='points-div'><ul class='points'>";
		if(trim($product['point1']))
		{
			echo "<li>".$product['point1']."</li>";
		}
		if(trim($product['point2']))
		{
			echo "<li>".$product['point2']."</li>";
		}
		if(trim($product['point3']))
		{
			echo "<li>".$product['point3']."</li>";
		}
		echo "</ul>";
		echo "<a class='btn btn-primary btn-block cart cart-sub' data-id='".$product['id']."'><i class='fa fa-shopping-cart'></i> Add to Quote</a>";
		echo "</div>";
		echo "</div></div>";
			
		
		if($count == 3){
			$count = 0;			
		}
		$count++;
		
	}

?>
</div>





			</div>
			<a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
				<i class="fa fa-angle-left"></i>
			</a>
			<a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
				<i class="fa fa-angle-right"></i>
			</a>
		</div>
	</div>
</div>

<?php } ?>



					
				</div>
			</div>
		</div>
	</section>
	
	<!--Footer-->
	<?php include('html/footer.php'); ?>
	<!--/Footer-->

	<!--/js-files-->
	<?php include('html/js-files.php'); ?>
	<!--<script type="text/javascript" src="js/custom/product-details.js"></script>-->
	<!--/js-files-->

	<script src="js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
	
	<script type="text/javascript">
    	$(function() {
	 	
			eventAddToCart();
			$("a[rel='prettyPhoto[gallery1]']").prettyPhoto({
				social_tools:false,
            	deeplinking:false,   
			});
			
		});
		
	    function eventAddToCart() {
			$("#add-to-cart").off('click');
			$("#add-to-cart").on('click',function(e) {
				e.preventDefault();
				if ($("#product-quantity").val() != 0) {
					var req = {}
					req.product_id = '<?=$result['id']; ?>';
					req.quantity = $("#product-quantity").val();
			        req.action = 'addProductTocart';

					$.ajax({
						'type'	:	'post',
						'url'	:	EndPoint,
						'data'	:	JSON.stringify(req)
					}).done(function(res){
						res = $.parseJSON(res);
						if(res.status == 1) {
							toastr.success(res.message);
							setTimeout(function() {window.location="cart"}, 800);
						}
						else {
							toastr.error(res.message);
						}
					});
				}else {
					toastr.info("Product quantity can't be zero.");
				};
			});

			$(".cart-sub").off('click');
			$(".cart-sub").on('click',function(e) {
				e.preventDefault();
				
				var req = {}
				req.product_id = $(this).attr('data-id');
				req.quantity = 1;
		        req.action = 'addProductTocart';

				$.ajax({
					'type'	:	'post',
					'url'	:	EndPoint,
					'data'	:	JSON.stringify(req)
				}).done(function(res){
					res = $.parseJSON(res);
					if(res.status == 1) {
						toastr.success(res.message);
						setTimeout(function() {window.location="cart"}, 800);
					}
					else {
						toastr.error(res.message);
					}
				});				
			});
		}
    </script>

</body>
</html>