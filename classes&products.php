<?php
	$meta_title='';
	$meta_keywords='';
	$meta_description='';

	if(isset($_GET['id'])){
		if(!empty($_GET['id'])){
			$request = new stdClass();
			$request->action = 'getCategoryDetails';
					
			$id = $_GET['id'];
			$request->category_id = $_GET['id'];
				
			require('vendor/requests1.php');

			$res = Api::getCategoryDetails($request);
			//print_r($res);
			//$array = json_decode($res->product[0], true );
			$json = json_encode($res);
			$array = json_decode($json,true);

			// if($array['status'] != 1){
			// 	header('Location: ../404.php');
			// }
            $details          = $array['category']['details'];
            $meta_title       = $array['category']['meta_title'];
            $meta_keywords    = $array['category']['meta_keywords'];
            $meta_description = $array['category']['meta_description'];
            $name = $array['category']['name'];
            $catalias = preg_replace('/_[a-z\d]+/', '', $array['category']['calias']);
            $breadcrumb = "<a href='products/{$array['category']['calias']}'>{$array['category']['cname']}</a> > <a href='{$catalias}/{$array['category']['alias']}'>{$array['category']['name']}</a>";          

		}else{
			header('Location: ../404.php');
		}

	}else{
		header('Location: ../404.php');
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="description" content="<?=$meta_description; ?>">
	<meta name="keywords" content="<?=$meta_keywords; ?>">
	<title><?=$meta_title;?></title>
    <?php include('html/head-tag.php'); ?>
</head><!--/head-->

<body>
	<!--header-->
	<?php include('html/header.php'); ?>
	<!--/header-->
	
	<section class="mrg-top30">
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<?php include('html/side-bar.php'); ?>
				</div>
				
				<div class="col-sm-9 padding-right">
					<div class="features_items"><!--features_items-->						
                        <?php
                            $cls = getData(array('action' => 'fetchAllClass', 'category_id' => $id));
                        ?>				
                        <h1 class="title" id="title"><span class="white-bg"><?=$name;?></span></h1>
                        <p id="breadcrumb"><?=$breadcrumb;?></p>
                        <div id="uniCount"></div>
                        <div id="divClass">
                            <h5 class="title"><!-- <span class="white-bg"> Product Categories </span> --></h5>

                            <span id="countClasses"></span>
                            
                            <div id="classes">
                            <?php
                                if(count($cls['classes']) > 0) {
                                    $str = '<div class="row">';

                                    foreach ($cls['classes'] as $c) {
                                        $subcat = preg_replace('/_[a-z]+\d+/', '', $c['subcatalias']);
                                        $str .= '<div class="col-sm-4"> <div class="product-image-wrapper"> <div class="single-products"> <div class="productinfo text-center">';
                                        $str .= "<a href=\"{$subcat}/{$c['alias']}\">";
                                        $str .= "<div class='img-block'><img src='{$c['image']}' alt='{$c['name']}' title='{$c['name']}' /></div>"; 

                                        if(strlen($c['name']) > 50){
                                            $n = substr($c['name'], 0, 50);
                                            $str .= "<p> {$n}...</p>";
                                        }else{  
                                            $str .= "<p> {$c['name']}</p>";
                                        }
                                        $str .= '</a></div> </div> </div></div>';
                                    }
                                    
                                    $str .= "</div>";
                                    echo $str;
                                }
                            ?>
							</div>
						</div>
                        <?php
                        $pro = getData(array('action' => 'fetchAllProducts', 'category_id' => $id,
								'active' => '1',
								'class_id' => '0',
								'columns' => array('id','alias','name','image','part_name','supplier_part_name','quantity','mrp','point1','point2','point3')));
                        //$pro = getData(array('action' => 'fetchAllProducts', 'category_id' => $id, 'active' => '1', 'class_id' => '0'));
                        if(count($pro['products'])){
                        ?>
                        <div id="divProducts">
							<h5 class="title"><span class="white-bg"> Products </span></h5>
							<span id="countProducts"><?=count($pro['products']);?> items found</span>
							<div id="products">
                                <?php
                                    $str = '<div class="row">';

                                    foreach ($pro['products'] as $product) {
                                        $str .= '<div class="col-sm-4"> <div class="product-image-wrapper"> <div class="single-products"> <div class="productinfo text-center">';
                                        
                                        $subcat = preg_replace('/_[a-z\d]+/', '', $product['subcatalias']);
                                        $str .= '<a href="'.$subcat.'/'.$product['alias'].'">';
                                        $str .= '<div class="img-block">';
                                        if($product['relimage'] != ''){
                                            $str .= "<img src='{$product['image']}' alt='{$product['name']}' title='{$product['name']}' onmouseover=\"this.src='{$product['relimage']}\'; onmouseout=\"this.src='{$product['image']}';\" />";
                                        }else{
                                            $str .= '<img src="'.$product['image'].'" alt="'.$product['name'].'" title="'.$product['name'].'" />';
                                        }
                                        $str .= '</div>';

                                        if(strlen($product['name']) > 50)
                                        {
                                            $n = substr($product['name'], 0, 50);
                                            $str .= "<p>{$n} ... ({$product['part_name']})</p>";
                                        }
                                        else
                                        {   
                                            $str .= "<p>{$product['name']} ({$product['part_name']})</p>";                                            
                                        }           
                                                
                                        $str .= '</a>';

                                        $str .= '<div class="points-div">';
                                        $str .= "<ul class='points'>";
                                        if(trim($product['point1']) != '')
                                        {
                                            $str .= "<li>".trim($product['point1'])."</li>";
                                        }
                                        if(trim($product['point2']) != '')
                                        {
                                            $str .= "<li>".trim($product['point2'])."</li>";
                                        }
                                        if(trim($product['point3']) != '')
                                        {
                                            $str .= "<li>".trim($product['point3'])."</li>";
                                        }

                                        $str .= "</ul>";
                                        $str .= "<a class='btn btn-primary btn-block cart' data-id='{$product['id']}'><i class='fa fa-shopping-cart'></i> Add to Quote</a>";
                                        $str .= '</div>';

                                        $str .= '</div></div></div></div>';
                                    }
                                    $str .= "</div>";
                                    echo $str;
                                ?>
							</div>
						</div>
                        <?php } ?>

						<div class="row">
							<div class="col-sm-12 col-md-12">
								<?php if(trim($details) != '') { ?>
								<div class="details">
									<?php echo $details;?>
								</div>
								<?php } ?>			
							</div>
						</div>						
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<!--Footer-->
	<?php include('html/footer.php'); ?>
	<!--/Footer-->

	<!--/js-files-->
	<?php include('html/js-files.php'); ?>
	<script type="text/javascript" src="js/custom/classes&products.js"></script>
	<!--/js-files-->
	<script type="text/javascript">
		var subcat;
		var subcat = '<?=$id;?>';
	</script>

</body>
</html>