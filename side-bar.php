<div class="left-sidebar">
	<h2>Product Categories</h2>
	<div class="panel-group category-products" id="accordian" side="cat"><!--category-productsr-->
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title"><a href="products">All Product Categories</a></h4>
            </div>
        </div>
        <?=createSidebar($cats['categories']);?>
		<!--
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title"><a href="shop1.php">Specials</a></h4>
			</div>
		</div>
		
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title"><a href="shop1.php">Best Sellers</a></h4>
			</div>
		</div>-->
	</div><!--/category-productsr-->
</div>